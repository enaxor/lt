package app

import (
	"encoding/json"
	// "errors"
	"html/template"
	"lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/darkoatanasovski/htmltags"
	"github.com/microcosm-cc/bluemonday"
	"github.com/revel/revel"
	"github.com/revel/revel/logger"
	"github.com/russross/blackfriday"
	//logger "gopkg.in/natefinch/lumberjack.v2"
)

var (
	// AppVersion revel app version (ldflags)
	AppVersion string

	// BuildTime revel app build-time (ldflags)
	BuildTime string
)

// type Myloghandler struct {
// 	Log (*logger.Record) error
// }

// func (*Myloghandler) Log(*Record) error {
// 	return errors.New("Sample Error")
// }

func init() {
	// Filters is the default set of global filters.
	revel.Filters = []revel.Filter{
		revel.PanicFilter,             // Recover from panics and display an error page instead.
		revel.RouterFilter,            // Use the routing table to select the right Action
		revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
		revel.ParamsFilter,            // Parse parameters into Controller.Params.
		revel.SessionFilter,           // Restore and write the session cookie.
		revel.FlashFilter,             // Restore and write the flash cookie.
		revel.ValidationFilter,        // Restore kept validation errors and save new ones from cookie.
		revel.I18nFilter,              // Resolve the requested language
		HeaderFilter,                  // Add some security based headers
		revel.InterceptorFilter,       // Run interceptors around the action.
		revel.CompressFilter,          // Compress the result.
		revel.ActionInvoker,           // Invoke the action.

	}

	// revel.LogFunctionMap["stdoutjson"] = func(c *logger.CompositeMultiHandler, options *logger.LogOptions) {
	// 		// Set the json formatter to os.Stdout, replace any existing handlers for the level specified
	// 		c.SetJson(os.Stdout, options)
	// 	}

	// basic rotation logs
	logger.LogFunctionMap["rotationpretty"] = func(c *logger.CompositeMultiHandler, options *logger.LogOptions) {
		// Set the json formatter to os.Stdout, replace any existing handlers for the level specified
		// c.SetJson(os.Stdout, options)
		// var l Myloghandler
		// l.Level = LvlDebug
		// c.SetHandler(l, false, LvlDebug)

		options.SetExtendedOptions("maxAgeDays", 7, "maxSizeMB", 1024*10, "maxBackups", 53)
		c.SetJsonFile("/home/shgz/devel/go/src/lt/log/log_longterm.json", options)
	}

	// stdout to debug
	logger.LogFunctionMap["stdoutdebug"] = func(c *logger.CompositeMultiHandler, options *logger.LogOptions) {
		c.SetJson(os.Stdout, options)
	}

	// json file to delete after inserting in db (grafana)
	logger.LogFunctionMap["postgresqljson"] = func(c *logger.CompositeMultiHandler, options *logger.LogOptions) {
		//c.SetJsonFile("/home/shgz/devel/go/src/lt/log/log_grafana_TMP.json", options)

		// options.SetExtendedOptions("colorize", true)
		// c.SetHandler(os.Stdout, options)

		c.SetJsonFile("/home/shgz/devel/go/src/lt/log/log_longterm.json", options)
	}

	// Register startup functions with OnAppStart
	// revel.DevMode and revel.RunMode only work inside of OnAppStart. See Example Startup Script
	// ( order dependent )
	// revel.OnAppStart(ExampleStartupScript)
	revel.OnAppStart(db.InitDB)
	// revel.OnAppStart(FillCache)

	//db.Caching = revel.Config.BoolDefault("cache.enable", true)
	setCachingOnOffStartupScript()

	// rand init
	rand.Seed(time.Now().UTC().UnixNano())

	revel.TemplateFuncs["testtrue"] = func() bool {
		return true
	}
	//interface{}
	revel.TemplateFuncs["pageContain"] = func(currentPage, pageArray string) bool {
		for _, a := range strings.Split(pageArray, ",") {
			if a == currentPage {
				return true
			}
		}
		return false
	}

	revel.TemplateFuncs["currentyear"] = func() int {
		t := time.Now()
		return t.Year()
	}

	revel.TemplateFuncs["checkLangageID"] = func(s []dbmodels.Langage, e int64) bool {
		for _, v := range s {
			if v.ID == e {
				return true
			}
		}
		return false
	}
	revel.TemplateFuncs["checkExpID"] = func(s []dbmodels.JobExperience, e int64) bool {
		for _, v := range s {
			if v.ID == e {
				return true
			}
		}
		return false
	}
	revel.TemplateFuncs["checkContratID"] = func(s []dbmodels.JobContrat, e int64) bool {
		for _, v := range s {
			if v.ID == e {
				return true
			}
		}
		return false
	}
	revel.TemplateFuncs["checkTimeID"] = func(s []dbmodels.JobWorkTime, e int64) bool {
		for _, v := range s {
			if v.ID == e {
				return true
			}
		}
		return false
	}
	revel.TemplateFuncs["checkAppID"] = func(s []dbmodels.SocialApp, e int64) bool {
		for _, v := range s {
			if v.ID == e {
				return true
			}
		}
		return false
	}

	// HasFieldInt check if array in param s contain the value e
	revel.TemplateFuncs["HasFieldInt"] = func(s []int64, e int64) bool {
		for _, a := range s {
			if a == e {
				return true
			}
		}
		return false
	}
	// // date
	// job.SinceStr = toReadeableTime(job.Since, "depuis ")
	// job.UntilStr = toReadeableTime(job.Until, "dans ")
	revel.TemplateFuncs["dateformat"] = func(t time.Time) string {
		// return fmt.Sprintf("le %d / %d / %d",
		// 	t.Day(), t.Month(), t.Year())
		// timeago.French.PastPrefix = "depuis "
		// return timeago.French.Format(t)
		return utils.HumanizeTime(t)
	}

	revel.TemplateFuncs["dateformatalt"] = func(t time.Time) string {
		return utils.HumanizeTimeAlt(t)
	}

	// Minus substract 2 items
	revel.TemplateFuncs["minus"] = func(a int, b int) int {
		return a - b
	}

	// Plus add 2 items
	revel.TemplateFuncs["plus"] = func(a int, b int) int {
		return a + b
	}

	// jsonify
	revel.TemplateFuncs["tagToJson"] = func(input []dbmodels.Tag) string {
		json, err := json.Marshal(input)
		if err != nil {
			return ""
		}
		ret := string(json)
		if ret == "null" {
			return ""
		}
		return ret
	}

	// todo still used ? delete ?
	revel.TemplateFuncs["markDowner"] = func(input string) template.HTML {
		unsafe := blackfriday.MarkdownBasic([]byte(input))
		html := bluemonday.UGCPolicy().SanitizeBytes(unsafe)
		return template.HTML(html)
	}

	revel.TemplateFuncs["htmlToText"] = func(input string) string {
		allowableTags := []string{}
		removeInlineAttributes := true
		stripped, _ := htmltags.Strip(input, allowableTags, removeInlineAttributes)
		return stripped.ToString()
	}

	// Plus add 2 items
	revel.TemplateFuncs["textPreview"] = func(rawtext string, maxTextLen int) string {
		rawtextLen := len(rawtext)
		if rawtextLen > maxTextLen {
			rawtextLen = maxTextLen
		} else if maxTextLen >= rawtextLen {
			return rawtext
		}
		runes := []rune(rawtext)
		strcut := string(runes[0:rawtextLen])
		var retsb strings.Builder
		retsb.WriteString(strcut)
		retsb.WriteString("…")
		return retsb.String()
	}

}

// HeaderFilter adds common security headers
// There is a full implementation of a CSRF filter in
// https://github.com/revel/modules/tree/master/csrf
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {
	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")
	c.Response.Out.Header().Add("Referrer-Policy", "strict-origin-when-cross-origin")

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

func setCachingOnOffStartupScript() {
	// revel.DevMod and revel.RunMode work here
	// Use this script to check for dev mode and set dev/prod startup scripts here!
	if revel.DevMode == true {
		// Dev mode
		db.Caching = false
	} else {
		db.Caching = true
	}
}

//func ExampleStartupScript() {
//	// revel.DevMod and revel.RunMode work here
//	// Use this script to check for dev mode and set dev/prod startup scripts here!
//	if revel.DevMode == true {
//		// Dev mode
//	}
//}
