package utils

import (
	"bytes"
	"image"
	"image/jpeg"
	"image/png"

	"github.com/nfnt/resize"
	"github.com/pkg/errors"
)

// NormalizeImagePng resize to a png image in byte array ready to write to disk
// width: size of the picture
func NormalizeImagePng(data []byte, width uint) ([]byte, error) {
	// manipulation buffer
	buf := new(bytes.Buffer)
	//buf.Write(data)
	// get image format
	_, format, err := image.DecodeConfig(bytes.NewReader(data))
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode image")
	}
	// we can also use: contentType := http.DetectContentType(data); contentType {
	var img image.Image
	if format == "jpeg" {
		// decode jpeg
		img, err = jpeg.Decode(bytes.NewReader(data))
		if err != nil {
			return nil, errors.Wrap(err, "unable to decode jpeg")
		}
	} else if format == "png" {
		// decode png
		img, err = png.Decode(bytes.NewReader(data))
		if err != nil {
			return nil, errors.Wrap(err, "unable to decode png")
		}
	} else {
		return nil, errors.New(format + " is not a supported format")
	}
	// resize
	newImage := resize.Resize(width, 0, img, resize.Lanczos3)

	// convert to png
	if err := png.Encode(buf, newImage); err != nil {
		return nil, errors.Wrap(err, "unable to encode to png")
	}

	// or convert to jpeg
	// var jpgopt jpeg.Options
	// jpgopt.Quality = 100
	// if err := jpeg.Encode(buf, newImage, &jpgopt); err != nil {
	// 	return nil, errors.Wrap(err, "unable to encode to jpeg")
	// }

	return buf.Bytes(), nil
}
