package utils

func iToiSlice(i int64) []int64 {
	var ret []int64
	ret = append(ret, i)
	return ret
}

func removeInterface(s []interface{}, i int) []interface{} {
	s[len(s)-1], s[i] = s[i], s[len(s)-1]
	return s[:len(s)-1]
}

func remove(slice []int, s int) []int {
	return append(slice[:s], slice[s+1:]...)
}
