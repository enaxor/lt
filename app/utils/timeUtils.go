package utils

import (
	"time"

	"github.com/xeonx/timeago"
)

func TimeTrack(start time.Time) time.Duration {
	return time.Since(start)
}

// CreateExpireDateFromNow return a formatted expiration date
// db to_timestamp('2019-06-16 15:21:14', 'YYYY-MM-DD hh24:mi:ss')
func CreateExpireDateFromNow(nbDaysFuther int) string {
	baseDate := time.Now()
	inHour := 24 * nbDaysFuther
	modifiedDate := baseDate.Add(time.Duration(inHour) * time.Hour)
	return modifiedDate.String()
}

// // ToReadeableTime time to 2 day ago
// func ToReadeableTime(t time.Time) string {
// 	timeago.French.PastPrefix = "depuis "
// 	return timeago.French.Format(t)
// }

//Predefined french configuration
var French = timeago.Config{
	PastPrefix:   "il y a ",
	PastSuffix:   "",
	FuturePrefix: "dans ",
	FutureSuffix: "",

	Periods: []timeago.FormatPeriod{
		timeago.FormatPeriod{time.Second, "environ une seconde", "moins d'une minute"},
		timeago.FormatPeriod{time.Minute, "environ une minute", "%d minutes"},
		timeago.FormatPeriod{time.Hour, "environ une heure", "%d heures"},
		timeago.FormatPeriod{timeago.Day, "un jour", "%d jours"},
		timeago.FormatPeriod{timeago.Month, "un mois", "%d mois"},
		timeago.FormatPeriod{timeago.Year, "un an", "%d ans"},
	},

	Zero: "environ une seconde",

	Max:           73 * time.Hour,
	DefaultLayout: "02/01/2006",
}

//Predefined french configuration
var FrenchAlt = timeago.Config{
	PastPrefix:   "depuis ",
	PastSuffix:   "",
	FuturePrefix: "dans ",
	FutureSuffix: "",

	Periods: []timeago.FormatPeriod{
		timeago.FormatPeriod{time.Second, "environ une seconde", "moins d'une minute"},
		timeago.FormatPeriod{time.Minute, "environ une minute", "%d minutes"},
		timeago.FormatPeriod{time.Hour, "environ une heure", "%d heures"},
		timeago.FormatPeriod{timeago.Day, "un jour", "%d jours"},
		timeago.FormatPeriod{timeago.Month, "un mois", "%d mois"},
		timeago.FormatPeriod{timeago.Year, "un an", "%d ans"},
	},

	Zero: "environ une seconde",

	Max:           73 * time.Hour,
	DefaultLayout: "02/01/2006",
}

func HumanizeTime(t time.Time) string {
	return timeago.NoMax(French).Format(t)
}

func HumanizeTimeAlt(t time.Time) string {
	return timeago.NoMax(FrenchAlt).Format(t)
}
