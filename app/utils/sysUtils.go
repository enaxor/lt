package utils

import (
	"net/http"
	"os"
	"strconv"
)

/**
*** File upload
**/

// WriteFileContent create a file from buffer
func WriteFileContent(buf []byte, path string) {

	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	n, err := f.Write(buf)
	if err != nil {
		panic(err)
	}
	ManageLog(Info, Utils, "WriteFileContent", "Write", nil, "path", path, "bytes wrote", strconv.Itoa(n))
}

func DeleteFile(path string) {
	// delete file
	_ = os.Remove(path)
	ManageLog(Info, Utils, "DeleteFile", "os.Remove", nil, "path", path)
}

// GetFileContentType return a string with the format of the file
func GetFileContentType(out *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

// GetFileContentTypeBuffer return a string with the format of the file
func GetFileContentTypeBuffer(fbuffer []byte) (string, error) {

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(fbuffer)

	return contentType, nil
}

// MimeTypeIsDocument return true if mime type is a document
// @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
func MimeTypeIsDocument(mimeType string) bool {
	s := []string{"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/vnd.oasis.opendocument.text", "application/pdf"}
	return Contains(s, mimeType)
}
