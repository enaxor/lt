package utils

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/revel/revel"
)

// InterfaceToString convert an interface to a string
func InterfaceToString(data interface{}) (res string) {
	switch v := data.(type) {
	case float64:
		res = strconv.FormatFloat(data.(float64), 'f', 6, 64)
	case float32:
		res = strconv.FormatFloat(float64(data.(float32)), 'f', 6, 32)
	case int:
		res = strconv.FormatInt(int64(data.(int)), 10)
	case int64:
		res = strconv.FormatInt(data.(int64), 10)
	case uint:
		res = strconv.FormatUint(uint64(data.(uint)), 10)
	case uint64:
		res = strconv.FormatUint(data.(uint64), 10)
	case uint32:
		res = strconv.FormatUint(uint64(data.(uint32)), 10)
	case json.Number:
		res = data.(json.Number).String()
	case string:
		res = data.(string)
	case []byte:
		res = string(v)
	default:
		res = ""
	}
	return
}

// ArrayToString transform an array of string in debug string
func ArrayToString(stringArray []string) string {
	return strings.TrimSpace(strings.Join(stringArray, " "))
}

// Join concat variadic input strings
func Join(strs ...string) string {
	var sb strings.Builder
	for _, str := range strs {
		sb.WriteString(str)
	}
	return sb.String()
}

// IntToString convert an int array to a string delimited by a comma
func IntToString(a []int) string {
	b := ""
	for _, v := range a {
		if len(b) > 0 {
			b += ","
		}
		b += strconv.Itoa(v)
	}

	return b
}

// StringToIntOrZero simple conversion with error handling
func StringToIntOrZero(a string) int {
	if len(a) == 0 {
		return 0
	}
	ret, err := strconv.Atoi(a)
	if err != nil {
		return 0
	}
	return ret
}

// Int64ArrayToString convert an int array to a string delimited by a comma
func Int64ArrayToString(a []int64, delimiter string) string {
	b := ""
	for _, v := range a {
		if len(b) > 0 {
			b += delimiter
		}
		b += strconv.FormatInt(v, 10)
	}

	return b
}

// PreviewText return the start of a string
func PreviewText(a string, lenPreview int) string {
	lena := len(a)
	if lena < lenPreview {
		return a
	}
	return a[:lenPreview] + "…"
}

// StringArrayFirstInt return first item of a int64 array
func StringArrayFirstInt(a []string) int64 {
	if len(a) > 0 {
		return ConvertStringToInt64(a[0])
	}
	return 0
}

// ConvertStringToInt64 so what it says
func ConvertStringToInt64(a string) int64 {
	i, err := strconv.Atoi(a)
	if err != nil {
		return 0
	}
	i64 := int64(i)
	return i64
}

// ParseQueryIntegerArray parse encoded query with param like '1 1'
func ParseQueryIntegerArray(a, sep string) []int64 {
	if len(a) != 0 {
		// split array
		aArr := strings.Split(a, sep)
		if len(aArr) != 0 {
			// convert array
			return ParseStringArrayToInt64Array(aArr)
		}
	}
	return []int64{}
}

// ParseStringArrayToInt64Array return a Int64 Array
func ParseStringArrayToInt64Array(input []string) []int64 {
	var ret []int64
	for _, num := range input {
		i, err := strconv.Atoi(num)
		if err != nil {
			continue
		}
		ret = append(ret, int64(i))
	}
	return ret
}

// GetSimpleSinceHumanReadableDate DATE FORMAT TOOL
func GetSimpleSinceHumanReadableDate(input time.Time) string {
	var ret string
	var since = time.Since(input)
	ret = fmt.Sprint("Since ", since.Hours(), " h")
	return ret
}

// ToString anything to string for debug and log purpose
func ToString(object interface{}) string {
	if object == nil {
		return ""
	}
	ret, err := json.Marshal(object)
	if err != nil {
		revel.AppLog.Error("Unable to marshal object", "method", ToString, "action", "Marshal", "err", err, "object", object)
		return ""
	}
	return string(ret)
}

// Contains return true if item is in slice
func Contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}

// IsValidUUID return true if uuid is valid
func IsValidUUID(u string) bool {
	if len(u) == 0 {
		return false
	}
	_, err := uuid.Parse(u)
	return err == nil
}
