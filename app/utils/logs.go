package utils

import (
	"encoding/json"

	"github.com/lib/pq"
	"github.com/revel/revel"
)

const MetricsMonitoring = true

const Debug = 1
const Info = 2
const Warn = 3
const Error = 4
const Crit = 5
const Fatal = 6
const Panic = 7

var logLevelMap = map[int]string{
	Debug: "DEBUG",
	Info:  "INFO",
	Warn:  "WARN",
	Error: "ERROR",
	Crit:  "CRIT",
	Fatal: "FATAL",
	Panic: "PANIC",
}

const Database = 1
const Controller = 2
const Ajobs = 3
const Utils = 3

var controllerLevelMap = map[int]string{
	Database:   "Database",
	Controller: "Controller",
	Utils:      "Utils",
}

// remove debug log in prod ?
const removeDEBUGinPROD = false

// ManageLog
// Params level, pkg int, method, action string, err error, param ...string
// - level: Debug,Info,Warn,etc
// - pkg: Database, Controller
// - method: function calling
// - action: postion in function
// - err; thrwed, can be null
// - params: list of strings, usefull to debug
func ManageLog(level, pkg int, method, action string, err error, param ...string) {
	// remove debug log in prod
	if removeDEBUGinPROD && !revel.DevMode && level == Debug {
		return
	}

	if errpq, ok := err.(*pq.Error); ok {
		// handle PQ errors
		switch level {
		case 1: // debug
			revel.AppLog.Debug("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		case 2: // info
			revel.AppLog.Info("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		case 3: // warn
			revel.AppLog.Warn("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		case 4: // error
			revel.AppLog.Error("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		case 5: // crit
			revel.AppLog.Crit("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		case 6: // fatal
			revel.AppLog.Fatal("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		case 7: // panic
			revel.AppLog.Panic("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"pqerror", marshalObject(errpq))
		}
	} else if err == nil {
		// handle _no_ errors
		switch level {
		case 1: // debug
			revel.AppLog.Debug("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param)
		case 2: // info
			revel.AppLog.Info("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param)
		case 3: // warn
			revel.AppLog.Warn("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", "nil")
		case 4: // error
			revel.AppLog.Error("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", "nil")
		case 5: // crit
			revel.AppLog.Crit("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", "nil")
		case 6: // fatal
			revel.AppLog.Fatal("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", "nil")
		case 7: // panic
			revel.AppLog.Panic("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", "nil")
		}
	} else {
		// handle others errors
		switch level {
		case 1: // debug
			revel.AppLog.Debug("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		case 2: // info
			revel.AppLog.Info("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		case 3: // warn
			revel.AppLog.Warn("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		case 4: // error
			revel.AppLog.Error("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		case 5: // crit
			revel.AppLog.Crit("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		case 6: // fatal
			revel.AppLog.Fatal("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		case 7: // panic
			revel.AppLog.Panic("ManageLog", "level", logLevelMap[level],
				"package", controllerLevelMap[pkg], "method", method, "action", action,
				"param", param,
				"error", err.Error())
		}
	}
}

func marshalObject(object interface{}) string {
	jobject, err := json.Marshal(object)
	if err != nil {
		return "nil"
	}
	return string(jobject)
}
