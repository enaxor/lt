package db

import (
	"encoding/json"
	"lt/app/utils"
	"strconv"
	"time"
)

// UpdateExpiredJobs this func should be run in a job at least once per day
func UpdateExpiredJobs() {
	tx, _ := db.Begin() // BEGIN
	// prepare
	stmt, err := db.Prepare(updateExpiredJobStatusSQL)
	if err != nil {
		manageErrorLog(err, "prepare", "UpdateExpiredJob_0")
		return
	}
	// query
	res, err := tx.Stmt(stmt).Exec()
	if err != nil {
		manageErrorLog(err, "query", "UpdateExpiredJob_1")
		return
	}
	nbRowArchived, err := res.RowsAffected()
	if err != nil {
		manageErrorLog(err, "scan", "UpdateExpiredJob_2")
		tx.Rollback() // abord
		return
	}
	if err := tx.Commit(); err != nil {
		manageErrorLog(err, "commit", "UpdateExpiredJob_3")
		tx.Rollback() // abord
		return
	}
	manageInfoLog("UpdateExpiredJobs", "commit", "nb archived jobs", strconv.FormatInt(nbRowArchived, 10))
}

var ltLog struct {
	T              time.Time
	Msg            string
	Module         string
	Lvl            string
	Caller         string
	Section        string
	Controller     string
	Method         string
	ActionPath     string
	Name           string
	Error          string
	Elapsed        int64
	ControllerType string
}

// cat logrevel.json | grep -o '[[:alnum:]]*\":' | sed 's/\"://g' | sort | uniq -c | sort
//  59 controllerType
// 143 error
// 152 name
// 236 actionPath
// 236 method
// 240 controller
// 785 section
// 858 caller
// 858 lvl
// 858 module
// 858 msg
// 858 t

// Debug int // 1
// Info  int // 2
// Warn  int // 3
// Error int // 4
// Crit  int // 5

// InsertLog this func should be run in a job at least once per day
func InsertLog(logs []string) (int64, bool) {
	obj := ltLog
	var nbRowArchived int64
	tx, _ := db.Begin()
	stmt, err := db.Prepare(insertLogSQL)
	if err != nil {
		manageErrorLog(err, "prepare", "InsertLog_0")
		return 0, false
	}
	for _, log := range logs {
		err := json.Unmarshal([]byte(log), &obj)
		if err != nil {
			manageErrorLog(err, "unmarshal", "InsertLog_1", "log", log)
			tx.Rollback() // abord
			return 0, false
		}
		res, err := stmt.Exec(obj.T, obj.Msg, obj.Module, obj.Lvl, obj.Caller, obj.Section, obj.Controller, obj.Method, obj.ActionPath, obj.Name, obj.Error, obj.ControllerType, obj.Elapsed, log)
		if err != nil {
			manageErrorLog(err, "query", "InsertLog_2", "logs count", utils.ToString(len(log)))
			tx.Rollback() // abord
			return 0, false
		}
		nbRow, err := res.RowsAffected()
		if err != nil {
			manageErrorLog(err, "scan", "InsertLog_3", "logs count", utils.ToString(len(log)))
			tx.Rollback() // abord
			return 0, false
		}
		nbRowArchived += nbRow
	}
	return nbRowArchived, true
}

// builk sql req IS A BIG SECURTITY ISSUE !
// obj := ltLog
// var nbRowArchived int64
// var sb strings.Builder
// sb.WriteString("INSERT INTO private.logs ")
// sb.WriteString(" (date, msg, module, lvl, caller, section, controller, method, action_path, name, error, controller_type, elapsed, json) VALUES ")
// for i, log := range logs {
// 	err := json.Unmarshal([]byte(log), &obj)
// 	if err != nil {
// 		manageErrorLog(err, "unmarshal", "InsertLog_1", "log", log)
// 		return false
// 	}
// 	sb.WriteString("('")
// 	sb.Write(pq.FormatTimestamp(obj.T))
// 	sb.WriteString("',")
// 	sb.WriteString("'" + strings.Replace(obj.Msg, "'", "''", -1) + "',")
// 	sb.WriteString("'" + obj.Module + "',")
// 	sb.WriteString("'" + obj.Lvl + "',")
// 	sb.WriteString("'" + obj.Caller + "',")
// 	sb.WriteString("'" + obj.Section + "',")
// 	sb.WriteString("'" + obj.Controller + "',")
// 	sb.WriteString("'" + obj.Method + "',")
// 	sb.WriteString("'" + obj.ActionPath + "',")
// 	sb.WriteString("'" + obj.Name + "',")
// 	sb.WriteString("'" + obj.Error + "',")
// 	sb.WriteString("'" + obj.ControllerType + "',")
// 	sb.WriteString("'" + strconv.FormatInt(obj.Elapsed, 10) + "',")
// 	// add more here
// 	sb.WriteString("'" + strings.Replace(log, "'", "''", -1) + "')")
// 	if i+1 < len(logs) {
// 		sb.WriteString(",")
// 	}
// }
// sql := sb.String()
// // BEGIN
// tx, _ := db.Begin()
// // prepare
// stmt, err := db.Prepare(sql)
// if err != nil {
// 	manageErrorLog(err, "prepare", "InsertLog_2")
// 	if pqerr, ok := err.(*pq.Error); ok {
// 		manageErrorLog(err, "prepare", "InsertLog_3", "pqerr", utils.ToString(pqerr), "at", sql[:166+300]) // replace 166 by pqerr.postion
// 	}
//  tx.Rollback() // abord
// 	return false
// }
// // query
// result, err := stmt.Exec()
// if err != nil {
// 	manageErrorLog(err, "query", "InsertLog_4")
//  tx.Rollback() // abord
// 	return false
// }
// nbRowsAffected, err := result.RowsAffected()
// if err != nil {
// 	manageErrorLog(err, "result", "InsertLog_5")
// }
// if err := tx.Commit(); err != nil {
// 	manageErrorLog(err, "commit", "InsertLog_5", "logs count", utils.ToString(nbRowArchived))
// 	tx.Rollback() // abord
// 	return false
// }
// // ok
// revel.AppLog.Info("InsertLog_6 just ran", "affected rows", nbRowsAffected)
// return true
