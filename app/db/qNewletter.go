package db

import (
	"database/sql"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strconv"

	guuid "github.com/google/uuid"
	"github.com/lib/pq"
)

/**
 ** Every one can subscribe to a newletter with any parameters
 ** An user who subscribe get an automatically subscribed newletter based on his latest upddated profile each week
 ** It implies:
 ** multiples newsletter for 1 mail are possible
 ** custom newsletter is always weekly
 **/

//  func IsCustomNewsletterActive() {
// 	auery :=
//  }

// CreateNewletterSub create new subscribtion
// return UUID of activation code (mail is to send)
func CreateNewletterSub(email string, params string, periodicity int) (*guuid.UUID, int) {
	// create activation code for user subscrition validation
	activationCode := guuid.New()
	// prepare
	stmt, err := db.Prepare(createNewletterSub)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	result, err := stmt.Exec(email, params, periodicity, activationCode)
	if err != nil {
		if pqerr, ok := err.(*pq.Error); ok {
			errorCode := string(pqerr.Code)
			nonFatalErrorCodeList := []string{"23505"} // error code for 'already exist'
			if contains(errorCode, nonFatalErrorCodeList) {
				return nil, ItemAlreadyExist
			}
		}
		manageErrorLog(err, "query", "CreateNewletterSub_0", "email", email, "params", params, "periodicity", strconv.Itoa(periodicity))
		return nil, UncoverableDatabaseError
	}
	if nbAffectedRow, err := result.RowsAffected(); nbAffectedRow != 1 {
		manageErrorLog(err, "scan", "CreateNewletterSub_1", "email", email, "params", params, "periodicity", strconv.Itoa(periodicity))
	}
	manageInfoLog("CreateNewletterSub", "1", "email", email, "params", params, "periodicity", strconv.Itoa(periodicity))
	// ok
	return &activationCode, CompletedWithNoError
}

// DeleteNewletterSub delete a sub
func DeleteNewletterSub(activationCode guuid.UUID) int {
	// prepare
	stmt, err := db.Prepare(deleteNewletterSub)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(activationCode)
	if err != nil {
		manageErrorLog(err, "query", "DeleteNewletterSub", "activationCode", utils.ToString(activationCode))
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// GetSubActivationCode DO NOT USE - unit test purpose only
func GetSubActivationCode(email string) (*guuid.UUID, int) {
	stmt, err := db.Prepare(getActivationCode)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row := stmt.QueryRow(email)
	ret, err := scanUUIDRow(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetSubActivationCode_0", "email", email) // todo delete debug utest logs
		return nil, NoItemFound
	}
	return &ret, CompletedWithNoError
}

// SelectSubscriber get a list of periodic sub
// 1 == daily / 2 == weekly / 3 == monthly
func SelectSubscriber(periodicity int) []dbmodels.NewsletterSubscription {
	ret := []dbmodels.NewsletterSubscription{}
	// prepare
	stmt, err := db.Prepare(getSubcriptionsPeriodicitySQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row, err := stmt.Query(periodicity)
	if err != nil {
		manageErrorLog(err, "query", "selectSubscriber_0", "periodicity", strconv.Itoa(periodicity))
		return ret
	}
	defer row.Close()
	ret, err = scanSubcriptionRows(row)
	if err != nil {
		manageErrorLog(err, "scan", "selectSubscriber_1", "periodicity", strconv.Itoa(periodicity))
	}
	return ret
}

// CreateMailsToQueue add mails to sent box
func CreateMailsToQueue(mails []dbmodels.MailQueueItem) []int {
	ret := []int{}
	// prepare
	stmt, err := db.Prepare(createNewsletterQueueItemSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run queries
	for _, mail := range mails {
		id, err := stmt.Query(mail.Email, mail.Title, mail.ContentHTML, mail.ContentText)
		if err != nil {
			manageErrorLog(err, "query", "CreateMailsToQueue_0", "mail", utils.ToString(mail))
			continue
		}
		idint, err := scanRowsForInteger(id)
		if err != nil {
			manageErrorLog(err, "scan", "CreateMailsToQueue_1", "mail", utils.ToString(mail))
			continue
		}
		ret = append(ret, idint)
		manageInfoLog("insert new mail in queue", "CreateMailsToQueue_3", "result.LastInsertId", strconv.Itoa(idint))
	}
	return ret
}

// CreateMailToQueue add mails to queue list
func CreateMailToQueue(mail dbmodels.MailQueueItem) (int64, int) {
	// prepare
	stmt, err := db.Prepare(createNewsletterQueueItemSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run queries
	row := stmt.QueryRow(mail.Email, mail.Title, mail.ContentHTML, mail.ContentText)
	//scan
	ret, err := scanRowForInteger64(row)
	if err != nil {
		manageErrorLog(err, "scan", "CreateMailToQueue_1", "mail", utils.ToString(mail))
		return 0, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// CreateMailToQueue add mails to queue list
// FLEM FUNC to del
// func CreateMailToQueueREB(mail dbmodels.MailQueueItem) []int {
// 	var mails []dbmodels.MailQueueItem
// 	mails = append(mails, mail)
// 	return CreateMailsToQueue(mails)
// }

// GetNewletterQueue select all mails to send
func GetNewletterQueue() []dbmodels.MailQueueItem {
	ret := []dbmodels.MailQueueItem{}
	// prepare
	stmt, err := db.Prepare(getNewletterQueueSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row, err := stmt.Query()
	if err != nil {
		manageErrorLog(err, "query", "GetNewletterQueue_0")
		return ret
	}
	defer row.Close()
	ret, err = scanMailQueueRows(row)
	if err != nil {
		manageErrorLog(err, "query", "GetNewletterQueue_1")
	}
	return ret
}

// GetMailInQueue select a specific mail to send
func GetMailInQueue(mailID int64) *dbmodels.MailQueueItem {
	var ret dbmodels.MailQueueItem
	// prepare
	stmt, err := db.Prepare(getMailInQueueSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row := stmt.QueryRow(mailID)
	if row == nil {
		manageErrorLog(err, "query", "GetMailInQueue_0", "mailID", utils.ToString(mailID))
		return nil
	}
	ret, err = scanMailQueueRow(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetMailInQueue_1", "mailID", utils.ToString(mailID))
		return nil
	}
	return &ret
}

// ActivateNewsletter set the activation boolean to true to start mailing
func ActivateNewsletter(activationCode guuid.UUID) int {
	// prepare
	stmt, err := db.Prepare(comfirmItemSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	result, err := stmt.Exec(activationCode)
	if err != nil {
		manageErrorLog(err, "query", "ActivateNewsletter_0", "activationCode", activationCode.String())
		return UncoverableDatabaseError
	}
	if nb, err := result.RowsAffected(); nb != 1 {
		manageErrorLog(err, "user", "ActivateNewsletter_1", "activationCode", activationCode.String())
		return UncoverableDatabaseError
	}
	// ok
	return 0
}

// SetNewletterItemSent set 'sent' to true
func SetNewletterItemSent(inputID int64) error {
	// prepare
	stmt, err := db.Prepare(sentQueueItemSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run queries
	_, err = stmt.Exec(inputID)
	if err != nil {
		manageErrorLog(err, "query", "SetNewletterItemSent", "inputID", utils.ToString(inputID))
	}
	return err
}

// SCANS

func scanSubcriptionRows(rows *sql.Rows) ([]dbmodels.NewsletterSubscription, error) {
	subs := []dbmodels.NewsletterSubscription{}
	var err error
	var sub dbmodels.NewsletterSubscription
	for rows.Next() {
		err := rows.Scan(&sub.ID, &sub.Email, &sub.Regularity)
		if err != nil {
			return subs, err
		}
		subs = append(subs, sub)
	}
	return subs, err
}

func scanMailQueueRows(rows *sql.Rows) ([]dbmodels.MailQueueItem, error) {
	ret := []dbmodels.MailQueueItem{}
	var err error
	var q dbmodels.MailQueueItem
	for rows.Next() {
		err := rows.Scan(&q.ID, &q.Email, &q.Title, &q.ContentHTML, &q.ContentText, &q.CreatedAt, &q.Sent)
		if err != nil {
			return ret, err
		}
		ret = append(ret, q)
	}
	return ret, err
}

func scanMailQueueRow(row *sql.Row) (dbmodels.MailQueueItem, error) {
	var ret dbmodels.MailQueueItem
	err := row.Scan(&ret.ID, &ret.Email, &ret.Title, &ret.ContentHTML, &ret.ContentText, &ret.CreatedAt, &ret.Sent)
	return ret, err
}

// func scanSuggestedJobMailingRows(rows *sql.Rows) ([]dbmodels.SuggestedJobMailing, error) {
// 	subs := []dbmodels.SuggestedJobMailing{}
// 	var err error
// 	var sub dbmodels.SuggestedJobMailing
// 	for rows.Next() {
// 		err := rows.Scan(&sub.UserID, &sub.Email, &sub.Prenom)
// 		if err != nil {
// 			return subs, err
// 		}
// 		subs = append(subs, sub)
// 	}
// 	return subs, err
// }
