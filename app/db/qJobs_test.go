package db

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/lib/pq"
)

// func TestGetJobs(t *testing.T) {
// 	type args struct {
// 		request jobListRequest
// 	}
// 	tests := []struct {
// 		name string
// 		req  args
// 		want string
// 	}{
// 		// TODO: Add test cases
// 		{name: "nofilterpage1",
// 			req: args{request: jobListRequest{indexStart: 0, indexEnd: 10,
// 				filters: []requestFilter{requestFilter{
// 					filterType: keywordF,
// 					fString:    []string{"Penn", "Teller"},
// 				},
// 				},
// 			}},
// 			want: ""},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := GetJobs(tt.req.request); got != tt.want {
// 				t.Errorf("GetJobs() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func printPqErrord(err *pq.Error) string {
	if err == nil {
		return "printPqError: nil"
	}
	res2B, _ := json.Marshal(err)
	//	fmt.Println(string(res2B))

	return fmt.Sprint("printPqError: " + string(res2B))

	// + err.Severity + fmt.Sprint(err.Code) + err.Message + err.Detail + err.Hint + err.Position +
	// err.InternalQuery + err.Where + err.Schema + err.Table + err.Column + err.DataTypeName + err.Constraint + err.File +
	// err.Line + err.Routine)
}

func TestErrorPrint(t *testing.T) {
	// var err error
	// err = errors.New("math: square root of negative number")

	// var err interface{}

	// meh := err.(pq.Error)
	// meh.Severity = "ERROR"

	//	err.Severity = "ERROR"
	// t.Errorf(printPqErrord(&err))

	// if _, ok := err.(error); ok {
	// 	t.Errorf("STD ERR")
	// }

	// var err error
	// err = errors.New("math: square root of negative number")
	// err = nil
	// if _, ok := err.(*pq.Error); ok {
	// 	t.Errorf("PQ ERR")
	// }

	var err error
	if v := false; err == nil && v {
		t.Errorf(">> 0")
	} else {
		t.Errorf(">> 1")
	}
}
