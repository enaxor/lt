package db

import (
	"database/sql"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strings"

	guuid "github.com/google/uuid"
	"github.com/lib/pq"
)

/********** TAGS */

// CreateTagIfNotExist add another tags, check if label does not exist yet
// You need to pass tx and commit or rollbak outside this func
func CreateTagIfNotExist(label string, external bool) (int64, int) {
	// normalise label
	label = strings.ToLower(label)
	// tag can exist already, check it first
	// prepare
	stmte, err := db.Prepare(checkTagLabelExist)
	if err != nil {
		panic(err)
	}
	defer stmte.Close()
	// query
	row := stmte.QueryRow(label)
	// scan
	var id int64
	id = 0
	err = row.Scan(&id)
	if err == nil {
		// label exist, stop
		return id, CompletedWithNoError
	} else if err != sql.ErrNoRows {
		manageErrorLog(err, "scan", "CreateTag_0", "label", label, "external", utils.ToString(external))
		return 0, UncoverableDatabaseError
	}
	// label does not exist, we create it
	// prepare
	stmtc, err := db.Prepare(addTagsSQL)
	if err != nil {
		panic(err)
	}
	defer stmtc.Close()
	// query
	var rows *sql.Rows
	rows, err = stmtc.Query(label, external)
	if err != nil {
		if pqerr, ok := err.(*pq.Error); ok {
			errorCode := string(pqerr.Code)
			nonFatalErrorCodeList := []string{"23505"} // error code for 'already exist'
			if contains(errorCode, nonFatalErrorCodeList) {
				// query tag again
				row := stmte.QueryRow(label)
				err = row.Scan(&id)
				if err != nil {
					manageErrorLog(err, "query", "CreateTag_1", "label", label, "external", utils.ToString(external))
					return 0, UncoverableDatabaseError
				}
				return id, CompletedWithNoError
			}
		}
		manageErrorLog(err, "query", "CreateTag_2", "label", label, "external", utils.ToString(external))
		return 0, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	id, err = scanRowsForInteger64(rows)
	if err != nil {
		manageErrorLog(err, "scan", "CreateTag_3", "label", label, "external", utils.ToString(external))
		return 0, UncoverableDatabaseError
	}
	return id, CompletedWithNoError
}

// insertJobTag create tag if not exit and link them to job
func insertJobTag(jobUUID guuid.UUID, tags []dbmodels.Tag) int {
	var errint int
	// prepare
	stmt, err := db.Prepare(insertNewJobTagSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// add each tag link
	for _, v := range tags {
		if v.ID == 0 {
			v.ID, errint = CreateTagIfNotExist(v.Label, true)
			if errint != 0 { // error when adding new user tag
				manageErrorLog(nil, "query", "addTagsToJob_0", "jobUUID", jobUUID.String(), "tags", utils.ToString(tags))
				continue // skip this tag
			}
		}
		// add
		_, err = stmt.Exec(jobUUID, v.ID)
		if err != nil {
			// cast err in pqerror
			if pqerr, ok := err.(*pq.Error); ok {
				// i assert err.Code is always a int
				errorCode := string(pqerr.Code)
				// these errors result of a client input error
				nonFatalErrorCodeList := []string{"23505"} // ItemAlreadyExist
				// no fatal error to handle app side
				if contains(errorCode, nonFatalErrorCodeList) {
					return CompletedWithNoError
				}
			}
			manageErrorLog(err, "query", "addTagsToJob_1", "jobUUID", jobUUID.String(), "tags", utils.ToString(tags))
			return UncoverableDatabaseError
		}
		// // link tags to job
		// if err := addJobTagLink(tx, jobUUID, v.ID); err != CompletedWithNoError {
		// 	tx.Rollback()
		// 	return UncoverableDatabaseError
		// }
	}
	// ok
	return CompletedWithNoError
}

func addAggregateJobTagLink(aggJobID guuid.UUID, tagID int64) int {
	stmt, err := db.Prepare(insertNewAggJobTagSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	_, err = stmt.Exec(aggJobID, tagID)
	if err != nil {
		// cast err in pqerror
		if pqerr, ok := err.(*pq.Error); ok {
			// i assert err.Code is always a int
			errorCode := string(pqerr.Code)
			// these errors result of a client input error
			nonFatalErrorCodeList := []string{"23505"}
			// no fatal error to handle app side
			if contains(errorCode, nonFatalErrorCodeList) {
				return CompletedWithNoError
			}
		}
		manageErrorLog(err, "query", "addAggregateJobTagLink", "aggJobID", utils.ToString(aggJobID), "tagID", utils.ToString(tagID))
		return UncoverableDatabaseError
	}
	// ok
	return CompletedWithNoError
}

// GetTags retrieve multiple tags
func GetTags(inputIDs []int64) ([]dbmodels.Tag, int) {
	var ret []dbmodels.Tag
	// prepare
	stmt, err := db.Prepare(getTagsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(pq.Array(inputIDs))
	if err != nil {
		manageErrorLog(err, "query", "GetTags_0", "inputIDs", int64ArrayToString(inputIDs))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanTags(rows)
	if err != nil {
		manageErrorLog(err, "scan", "GetTags_1", "inputIDs", int64ArrayToString(inputIDs))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

func GetMostUsedTags(limit int) ([]dbmodels.Tag, int) {
	var ret []dbmodels.Tag
	// prepare
	stmt, err := db.Prepare(getMostUsedTagsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(limit)
	if err != nil {
		manageErrorLog(err, "query", "GetMostUsedTags_0", "limit", utils.ToString(limit))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanTags(rows)
	if err != nil {
		manageErrorLog(err, "scan", "GetMostUsedTags_1", "limit", utils.ToString(limit))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetTagsFromKeyword get a list of industry from a keyword
func GetTagsFromKeyword(keyword string, approx bool, maxResult int) ([]dbmodels.Tag, int) {
	var ret []dbmodels.Tag

	if approx {
		keyword = keyword + "%"
	}

	stmt, err := db.Prepare(getTagFromKeywordSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rowID, err := stmt.Query(keyword, maxResult)
	if err != nil {
		manageErrorLog(err, "query", "GetTagsFromKeyword_0", "input keyword", keyword)
		return nil, UncoverableDatabaseError
	}
	ret, err = scanUserTagsRows(rowID)
	if err != nil {
		manageErrorLog(err, "scan", "GetTagsFromKeyword_1", "input keyword", keyword)
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

func getCompanyTags(userID guuid.UUID) ([]dbmodels.Tag, int) {
	stmt, err := db.Prepare(getCompanyTagsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	rows, err := stmt.Query(userID)
	if err != nil {
		manageErrorLog(err, "query", "getCompanyTags_0", "input userID", userID.String())
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	tags, err := scanUserTagsRows(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return tags, NoItemFound
		}
		manageErrorLog(err, "scan", "getCompanyTags_1", "input userID", userID.String())
		return tags, UncoverableDatabaseError
	}
	return tags, CompletedWithNoError
}

/***** SCANS */

func scanTags(rows *sql.Rows) ([]dbmodels.Tag, error) {
	var err error
	var ret []dbmodels.Tag
	for rows.Next() {
		var tmp dbmodels.Tag
		err := rows.Scan(&tmp.ID, &tmp.Label, &tmp.Count)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, err
}

// existing tag only
// func updateJobTagLinks(tx *sql.Tx, jobID guuid.UUID, tags []dbmodels.Tag) int {
// 	// DELETE first
// 	stmt, err := db.Prepare(deleteJobTagLinkSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	_, err = tx.Stmt(stmt).Exec(jobID)
// 	if err != nil {
// 		manageErrorLog(err, "query", "updateJobTagLinks_0", "input jobID", jobID.String(), "input tags", utils.ToString(tags))
// 		return UncoverableDatabaseError
// 	}
// 	// CREATE
// 	for _, v := range tags {
// 				// add unexisting tags
// 		// if v.ID == 0 {
// 		// 	v.ID, _ = CreateTagIfNotExist(tx, v.Label, true)
// 		// 	if v.ID == 0 { // error when adding new user tag
// 		// 		continue // try next tag
// 		// 	}
// 		// }
// 		// add tags link
// 		if err := addJobTagLink(tx, jobID, v.ID); err != CompletedWithNoError {
// 			// error
// 			return err
// 		}
// 	}
// 	return CompletedWithNoError
// }

// func oldscanTagsRows(rows *sql.Rows) (map[int64]string, error) {
// 	var err error
// 	ret := make(map[int64]string)
// 	var id int64
// 	var label string
// 	defer rows.Close()

// 	for rows.Next() {
// 		err := rows.Scan(&id, &label)
// 		if err != nil {
// 			return nil, err
// 		}
// 		ret[id] = label
// 	}
// 	return ret, err
// }

// func getCachedTagDB() map[int64]string {
// 	ret := make(map[int64]string)

// 	if err := cache.Get("DBtagDatas", &ret); err != nil || !Caching {
// 		// prepare
// 		stmt, err := db.Prepare(getAllTagsSQL)
// 		if err != nil {
// 			panic(err)
// 		}
// 		// execute
// 		row, err := stmt.Query()
// 		if err != nil {
// 			panic(err)
// 		}
// 		// scan
// 		ret, err := oldscanTagsRows(row)
// 		if err != nil {
// 			revel.AppLog.Error("Database Scan Error", "method", "getCachedTagDB",
// 				"error", err.Error())
// 			return ret
// 		}
// 		// ok, fill cache
// 		go cache.Set("DBtagDatas", ret, 130*time.Minute)
// 		return ret
// 	}
// 	// return an empty map
// 	return ret
// }
