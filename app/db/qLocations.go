package db

import (
	"database/sql"
	"errors"
	"lt/app/db/dbmodels"
	"strconv"
	"strings"
)

const departmentStartID = 0
const aggroStartID = 105
const townStartID = 158
const townEndID = 36989

func isDepartmentID(id int64) bool {
	return id < aggroStartID
}
func isAggloID(id int64) bool {
	return id < townStartID
}
func isTownID(id int64) bool {
	return id >= townStartID && id <= townEndID
}

/********** Location */

// GetLocation get a location than can be a town, dept or agglo
func GetLocation(id int64) (dbmodels.Location, int) {
	var ret dbmodels.Location
	if id == 0 {
		return ret, NoItemFound
	}
	// get right sql
	sqlquery := ""
	if isDepartmentID(id) { // department
		sqlquery = getLocationDeptSQL
	} else if isAggloID(id) { // agglo
		sqlquery = getLocationAggloPreviewSQL
	} else if isTownID(id) { // town
		sqlquery = getLocationTownSQL
	} else {
		manageErrorLog(errors.New("not a valid input ID"), "prepare", "GetLocation_0", "id", strconv.FormatInt(id, 10))
		return ret, NoItemFound
	}
	// prepare
	stmt, err := db.Prepare(sqlquery)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(id)
	// scan
	ret, err = scanLocation(row)
	if err != nil {
		manageErrorLog(err, "query", "GetLocation_1", "id", strconv.FormatInt(id, 10))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetLocationCodePostal search a location dept or town (no agglo atm)
func GetLocationCodePostal(inputCodePostal string) (dbmodels.Location, int) {
	var ret dbmodels.Location
	sqlquery := ""
	if len(inputCodePostal) == 2 {
		// department
		sqlquery = getLocationCodePostalDeptSQL
	} else {
		// town, prob
		sqlquery = getLocationCodePostalTownSQL
	}
	// prepare
	stmt, err := db.Prepare(sqlquery)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputCodePostal)
	// scan
	ret, err = scanLocation(row)
	if err != nil {
		if err == sql.ErrNoRows {
			manageErrorLog(err, "query", "GetLocationCodePostal_0", "inputCodePostal", inputCodePostal)
			return ret, NoItemFound
		}
		manageErrorLog(err, "query", "GetLocationCodePostal_1", "inputCodePostal", inputCodePostal)
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// SearchLocations show suggestion from input string (can be a town, a postal code or a dept label)
func SearchLocations(keyword string, maxresult int) ([]dbmodels.Location, int) {
	var ret []dbmodels.Location
	// prepare keyword
	keyword = strings.ReplaceAll(keyword, "é", "e")
	keyword = strings.ReplaceAll(keyword, "è", "e")
	keyword = strings.ReplaceAll(keyword, "ô", "o")
	keyword = strings.ReplaceAll(keyword, "-", " ")
	keyword = strings.ReplaceAll(keyword, "'", " ")
	keyword = strings.ToUpper(keyword)
	keyword = keyword + "%"
	keywordPermissive := "%" + keyword
	// prepare
	stmt, err := db.Prepare(getLocationSearchSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(keyword, keywordPermissive, maxresult)
	if err != nil {
		manageErrorLog(err, "query", "SearchLocations_0", "keyword", keyword, "maxresult", strconv.Itoa(maxresult))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// read
	ret, err = scanLocations(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "SearchLocations_1", "keyword", keyword, "maxresult", strconv.Itoa(maxresult))
		return nil, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetLocationNearMe show town suggestion from input lat/lon
func GetLocationNearMe(lat, lon string) ([]dbmodels.Location, int) {
	var ret []dbmodels.Location
	// prepare
	stmt, err := db.Prepare(getNearMeLocationsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(lat, lon, 10)
	if err != nil {
		manageErrorLog(err, "query", "GetLocationNearMe_0", "input lat", lat, "input lon", lon)
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// read
	ret, err = scanLocations(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetLocationNearMe_1", "input lat", lat, "input lon", lon)
		return nil, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

func getUserLocation(locationID int64, radius int64) dbmodels.UserLocation {
	var ret dbmodels.UserLocation
	var err int
	ret.MainLocation.RadiusID = radius
	ret.MainLocation, err = GetLocation(locationID)
	if err != CompletedWithNoError {
		// can not proccess
		return ret
	}
	if isDepartmentID(locationID) {
		ret.SearchLocationAll = getTownInDepartment(locationID)
	} else if isAggloID(locationID) {
		ret.SearchLocationAll = getTownInAgglo(locationID)
	} else if isTownID(locationID) {
		ret.SearchLocationAll = getAroundLocation(locationID, radius)
	}
	return ret
}

func getAroundLocation(townID, radius int64) []int64 {
	var ret []int64
	// preapre
	stmt, err := db.Prepare(getTownsInRadiusSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run query
	rows, err := stmt.Query(townID, radius, 20) // limit
	if err != nil {
		manageErrorLog(err, "query", "getAroundLocation_0", "townID", strconv.FormatInt(townID, 10), "radius", strconv.FormatInt(radius, 10))
		return ret
	}
	defer rows.Close()
	// scan
	ret, err = scanRowsForInteger64Array(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret
		}
		manageErrorLog(err, "scan", "getAroundLocation_1", "townID", strconv.FormatInt(townID, 10), "radius", strconv.FormatInt(radius, 10))
		return ret
	}
	return ret
}

func getTownInDepartment(departementID int64) []int64 {
	var ret []int64
	postal := strconv.FormatInt(departementID, 10) + "%"
	// preapre
	stmt, err := db.Prepare(getTownsInDepartmentSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run query
	rows, err := stmt.Query(postal)
	if err != nil {
		manageErrorLog(err, "query", "getTownInDepartment_0", "departementID", strconv.FormatInt(departementID, 10))
		return ret
	}
	defer rows.Close()
	// scan
	ret, err = scanRowsForInteger64Array(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret
		}
		manageErrorLog(err, "scan", "getTownInDepartment_1", "departementID", strconv.FormatInt(departementID, 10))
		return ret
	}
	return ret
}

func getTownInAgglo(aggloID int64) []int64 {
	var ret []int64
	// preapre
	stmt, err := db.Prepare(getTownsInAggloSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run query
	rows, err := stmt.Query(aggloID)
	if err != nil {
		manageErrorLog(err, "query", "getTownInAgglo_0", "aggloID", strconv.FormatInt(aggloID, 10))
		return ret
	}
	defer rows.Close()
	// scan
	ret, err = scanRowsForInteger64Array(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret
		}
		manageErrorLog(err, "scan", "getTownInAgglo_1", "aggloID", strconv.FormatInt(aggloID, 10))
		return ret
	}
	return ret
}

/**
 ** LOCATIONS SCANS
 **/

func scanLocation(row *sql.Row) (dbmodels.Location, error) {
	var ret dbmodels.Location
	err := row.Scan(&ret.ID, &ret.PostalCode, &ret.Label, &ret.LocationLat, &ret.LocationLon, &ret.IsDepartement)
	return ret, err
}

func scanLocations(rows *sql.Rows) ([]dbmodels.Location, error) {
	var ret []dbmodels.Location
	defer rows.Close()
	for rows.Next() {
		var tmp dbmodels.Location
		err := rows.Scan(&tmp.ID, &tmp.PostalCode, &tmp.Label, &tmp.LocationLat, &tmp.LocationLon, &tmp.IsDepartement)
		if err != nil {
			return nil, err
		}
		ret = append(ret, tmp)
	}
	return ret, nil
}

/**
 ** END LOCATIONS SCANS
 **/

// GetLocations get multiples locations
// func GetLocations(inputIDs []int64) (*dbmodels.UserLocation, int) {
// 	var ret []dbmodels.Location
// 	// prepare
// 	stmt, err := db.Prepare(getLocationTownSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	// query
// 	rows, err := stmt.Query(pq.Array(inputIDs))
// 	if err != nil {
// 		manageErrorLog(err, "query", "GetLocations_0", "inputIDs", int64ArrayToString(inputIDs))
// 		return nil, UncoverableDatabaseError
// 	}
// 	defer rows.Close()
// 	// scan
// 	ret, err = scanLocations(rows)
// 	if err != nil {
// 		manageErrorLog(err, "scan", "GetLocations_1", "inputIDs", int64ArrayToString(inputIDs))
// 		if err == sql.ErrNoRows {
// 			return ret, NoItemFound
// 		}
// 		return ret, UncoverableDatabaseError
// 	}
// 	// ok
// 	return ret, CompletedWithNoError
// }
// GetLocationFromKeyword get a list of location from a keyword
// func GetLocationFromKeyword(keyword string, maxResult int) ([]dbmodels.Location, int) {
// 	var ret []dbmodels.Location
// 	keyword = keyword + "%"
// 	// preapre
// 	stmt, err := db.Prepare(getLocationSearchSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	// run query
// 	rows, err := stmt.Query(keyword, maxResult)
// 	if err != nil {
// 		manageErrorLog(err, "query", "GetLocationFromKeyword_0", "keyword", keyword)
// 		return nil, UncoverableDatabaseError
// 	}
// 	defer rows.Close()
// 	// scan
// 	ret, err = scanLocations(rows)
// 	if err != nil {
// 		if err == sql.ErrNoRows {
// 			return ret, NoItemFound
// 		}
// 		manageErrorLog(err, "scan", "GetLocationFromKeyword_1", "keyword", keyword)
// 		return nil, UncoverableDatabaseError
// 	}
// 	// ok
// 	return ret, CompletedWithNoError
// }

// GetLocationTown get 1 location from a town id
// func GetLocationTown(id int64) (dbmodels.Location, int) {
// 	var ret dbmodels.Location
// 	// prepare
// 	stmt, err := db.Prepare(getLocationTownSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	// query
// 	row := stmt.QueryRow(id)
// 	// scan
// 	ret, err = scanLocation(row)
// 	if err != nil {
// 		manageErrorLog(err, "query", "GetLocationTown", "id", strconv.FormatInt(id))
// 		if err == sql.ErrNoRows {
// 			return ret, NoItemFound
// 		}
// 		return ret, UncoverableDatabaseError
// 	}
// 	// ok
// 	return ret, CompletedWithNoError
// }

// GetLocationDept get 1 location from a dept id
// func GetLocationDept(id int64) (dbmodels.Location, int) {
// 	var ret dbmodels.Location
// 	// prepare
// 	stmt, err := db.Prepare(getLocationDeptSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	// query
// 	row := stmt.QueryRow(id)
// 	// scan
// 	ret, err = scanLocation(row)
// 	if err != nil {
// 		manageErrorLog(err, "query", "GetLocationDept", "id", strconv.FormatInt(id))
// 		if err == sql.ErrNoRows {
// 			return ret, NoItemFound
// 		}
// 		return ret, UncoverableDatabaseError
// 	}
// 	// ok
// 	return ret, CompletedWithNoError
// }
