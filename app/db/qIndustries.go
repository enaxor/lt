package db

import (
	"database/sql"
	"lt/app/db/dbmodels"
	"strconv"
)

/********** INDUSTRIES */

// GetIndustryFromKeyword get a list of industry from a keyword
func GetIndustryFromKeyword(keyword string, maxResult int) ([]dbmodels.Industry, int) {
	var ret []dbmodels.Industry
	// right matching
	keyword = keyword + "%"
	// retrieve industry
	stmt, err := db.Prepare(getIndustryFromKeywordSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rowID, err := stmt.Query(keyword, maxResult)
	if err != nil {
		manageErrorLog(err, "query", "GetIndustryFromKeyword_0", "input keyword", keyword)
		return ret, UncoverableDatabaseError
	}
	// scqn
	ret, err = scanCompanyIndustryRow(rowID)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetIndustryFromKeyword_1", "input keyword", keyword)
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetAllIndustry retrieve Industry to select
func GetAllIndustry() []dbmodels.Industry {
	var ret []dbmodels.Industry
	// prepare
	stmt, err := db.Prepare(getAllIndustrySQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rowID, err := stmt.Query()
	if err != nil {
		manageErrorLog(err, "query", "GetAllIndustry_0")
		return ret
	}
	// scqn
	ret, err = scanCompanyIndustryRow(rowID)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret
		}
		manageErrorLog(err, "scan", "GetAllIndustry_1")
		return ret
	}
	return ret
}

// GetIndustryDetail detail of a company industry
func GetIndustryDetail(inputID string) (dbmodels.Industry, int) {
	var ret dbmodels.Industry
	stmt, err := db.Prepare(getIndustrySQL)
	if err != nil {
		panic(err)
	}
	row := stmt.QueryRow(inputID)
	id, label, err := scanIDLabel(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetIndustryDetail_0", "input id", strconv.FormatInt(id, 10))
		return ret, UncoverableDatabaseError
	}
	ret.ID = id
	ret.Label = label
	return ret, CompletedWithNoError
}
