package db

import (
	"database/sql"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strconv"

	"github.com/lib/pq"
)

// GetLangages retrieve multiple langage
func GetLangages(inputID []int64) ([]dbmodels.Langage, int) {
	var ret []dbmodels.Langage
	// prepare
	stmt, err := db.Prepare(getLangagesSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(pq.Array(inputID))
	if err != nil {
		manageErrorLog(err, "query", "GetLangages_0", "inputID", utils.ToString(inputID)) // anciennement int64ArrayToString
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanLangages(rows)
	if err != nil {
		manageErrorLog(err, "scan", "GetLangages_1", "inputID", utils.ToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetRegional get a regional info about a job
func GetRegional(inputID int64) (dbmodels.Regional, int) {
	var ret dbmodels.Regional
	// prepare
	stmt, err := db.Prepare(getRegionalSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputID)
	if err != nil {
		manageErrorLog(err, "query", "GetRegional_0", "inputID", utils.ToString(inputID))
		return ret, UncoverableDatabaseError
	}
	// scan
	ret, err = scanRegional(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetRegional_1", "inputID", utils.ToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetContrat retrieve 1 contrat
func GetContrat(inputID int64) (dbmodels.JobContrat, int) {
	var ret dbmodels.JobContrat
	// prepare
	stmt, err := db.Prepare(getContratSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputID)
	if err != nil {
		manageErrorLog(err, "query", "GetContrat_0", "inputID", utils.ToString(inputID))
		return ret, UncoverableDatabaseError
	}
	// scan
	ret, err = scanContrat(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetContrat_1", "inputID", utils.ToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetContrats retrieve multiple contrats
func GetContrats(inputID []int64) ([]dbmodels.JobContrat, int) {
	var ret []dbmodels.JobContrat
	// prepare
	stmt, err := db.Prepare(getContratsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(pq.Array(inputID))
	if err != nil {
		manageErrorLog(err, "query", "GetContrats_0", "inputID", int64ArrayToString(inputID))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanContrats(rows)
	if err != nil {
		manageErrorLog(err, "scan", "GetContrats_1", "inputID", int64ArrayToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetJobExperience retrieve multiple experience
func getJobExperience(inputID int64) (dbmodels.JobExperience, int) {
	var ret dbmodels.JobExperience
	// prepare
	stmt, err := db.Prepare(getExperienceSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputID)
	if err != nil {
		manageErrorLog(err, "query", "GetExperience_0", "inputID", strconv.FormatInt(inputID, 10))
		return ret, UncoverableDatabaseError
	}
	// scan
	ret, err = scanJobExperience(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetExperience_1", "inputID", strconv.FormatInt(inputID, 10))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetJobExperiences retrieve multiple experience
func GetJobExperiences(inputID []int64) ([]dbmodels.JobExperience, int) {
	var ret []dbmodels.JobExperience
	// prepare
	stmt, err := db.Prepare(getExperiencesSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(pq.Array(inputID))
	if err != nil {
		manageErrorLog(err, "query", "GetExperiences_0", "inputID", int64ArrayToString(inputID))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanJobExperiences(rows)
	if err != nil {
		manageErrorLog(err, "scan", "GetExperiences_1", "inputID", int64ArrayToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetJobTime retrieve 1 job time
func GetJobTime(inputID int64) (dbmodels.JobWorkTime, int) {
	var ret dbmodels.JobWorkTime
	// prepare
	stmt, err := db.Prepare(getJobTimeSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputID)
	if err != nil {
		manageErrorLog(err, "query", "GetJobTime_0", "inputID", strconv.FormatInt(inputID, 10))
		return ret, UncoverableDatabaseError
	}
	// scan
	ret, err = scanJobTime(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetJobTime_1", "inputID", strconv.FormatInt(inputID, 10))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetJobTimes retrieve multiple job time
func GetJobTimes(inputID []int64) ([]dbmodels.JobWorkTime, int) {
	var ret []dbmodels.JobWorkTime
	// prepare
	stmt, err := db.Prepare(getJobTimesSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(pq.Array(inputID))
	if err != nil {
		manageErrorLog(err, "query", "GetJobTimes_0", "inputID", int64ArrayToString(inputID))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanJobTimes(rows)
	if err != nil {
		manageErrorLog(err, "scan", "GetJobTimes_1", "inputID", int64ArrayToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetApplyStatus retrieve an apply status
func GetApplyStatus(inputID int64) (dbmodels.ApplyStatus, int) {
	var ret dbmodels.ApplyStatus
	// prepare
	stmt, err := db.Prepare(getApplyStatusSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputID)
	if err != nil {
		manageErrorLog(err, "query", "GetApplyStatus_0", "inputID", strconv.FormatInt(inputID, 10))
		return ret, UncoverableDatabaseError
	}
	// scan
	id, label, err := scanIDLabel(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetApplyStatus_1", "inputID", strconv.FormatInt(inputID, 10))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	ret.ID = id
	ret.Label = label
	// ok
	return ret, CompletedWithNoError
}

/**** SCANS */

func scanJobTime(row *sql.Row) (dbmodels.JobWorkTime, error) {
	var ret dbmodels.JobWorkTime
	err := row.Scan(&ret.ID, &ret.Label)
	return ret, err
}

func scanJobTimes(rows *sql.Rows) ([]dbmodels.JobWorkTime, error) {
	var ret []dbmodels.JobWorkTime
	for rows.Next() {
		var tmp dbmodels.JobWorkTime
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, nil
}

func scanJobExperience(row *sql.Row) (dbmodels.JobExperience, error) {
	var ret dbmodels.JobExperience
	err := row.Scan(&ret.ID, &ret.Label)
	return ret, err
}

func scanJobExperiences(rows *sql.Rows) ([]dbmodels.JobExperience, error) {
	var ret []dbmodels.JobExperience
	for rows.Next() {
		var tmp dbmodels.JobExperience
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, nil
}

func scanContrats(rows *sql.Rows) ([]dbmodels.JobContrat, error) {
	var ret []dbmodels.JobContrat
	defer rows.Close()
	for rows.Next() {
		var tmp dbmodels.JobContrat
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, nil
}

func scanContrat(row *sql.Row) (dbmodels.JobContrat, error) {
	var ret dbmodels.JobContrat
	err := row.Scan(&ret.ID, &ret.Label)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func scanRegional(row *sql.Row) (dbmodels.Regional, error) {
	var ret dbmodels.Regional
	err := row.Scan(&ret.ID, &ret.Label)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func scanLangages(rows *sql.Rows) ([]dbmodels.Langage, error) {
	var ret []dbmodels.Langage
	defer rows.Close()
	for rows.Next() {
		var tmp dbmodels.Langage
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, nil
}
