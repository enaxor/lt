package db

/******************************************************************************
** GENERICS
**/

// socialapp
const getSocialAppSQL = "SELECT id, label from social_app where id = $1"
const getSocialAppsSQL = "SELECT id, label from social_app where id = any ( $1 )"

// industry
const getIndustryFromKeywordSQL = "SELECT id, label from public.industry where label ILIKE $1 LIMIT $2"
const getIndustrySQL = "SELECT id, label from public.industry where id = $1"
const getAllIndustrySQL = " SELECT industry.id, industry.label from industry " +
	" LEFT OUTER JOIN company on company.industry = industry.id where industry.id != 0 " + // sort result by current usage
	" group by industry.id order by count(company.industry) desc LIMIT 100" // 100 is enough

// tags
const addTagsSQL = "INSERT INTO public.tag (label,external) VALUES ($1,$2) RETURNING id"

// "SELECT id, label from tag where label ILIKE $1 LIMIT $2"
const getTagFromKeywordSQL = " select tag_id as id, label from ( ( select job_tag.tag_id, tag.label, count(*) as c from job_tag " +
	" INNER JOIN job on job.id = job_tag.job_id " +
	" INNER JOIN tag on job_tag.tag_id = tag.id " +
	" where job.status = 2  " +
	" GROUP BY job_tag.tag_id, tag.label " +
	" ) UNION ( " +
	" select aggregate_job_tag.tag_id, tag.label, count(*) as c from aggregate_job_tag " +
	" INNER JOIN aggregate_job on aggregate_job.id = aggregate_job_tag.aggregate_job_id " +
	" INNER JOIN tag on aggregate_job_tag.tag_id = tag.id " +
	" GROUP BY aggregate_job_tag.tag_id, tag.label " +
	" ) ) c where label ILIKE  $1 " +
	" ORDER BY c DESC LIMIT $2"
const getMostUsedTagsSQL = " select tag_id, label, c from ( ( select job_tag.tag_id, tag.label, count(*) as c from job_tag " +
	" INNER JOIN job on job.id = job_tag.job_id INNER JOIN tag on job_tag.tag_id = tag.id " +
	" GROUP BY job_tag.tag_id, tag.label ) " +
	" UNION " +
	" ( select aggregate_job_tag.tag_id, tag.label, count(*) as c from aggregate_job_tag " +
	" INNER JOIN aggregate_job on aggregate_job.id = aggregate_job_tag.aggregate_job_id INNER JOIN tag on aggregate_job_tag.tag_id = tag.id " +
	" GROUP BY aggregate_job_tag.tag_id, tag.label ) " +
	" ORDER BY label LIMIT $1 ) a "
const getTagsSQL = "SELECT id,label from tag where id = any ( $1 )"
const checkTagLabelExist = " SELECT id from tag WHERE LOWER(replace(label, ' ', '')) = LOWER(replace($1, ' ', '')) LIMIT 1 "

// langage
const getLangagesSQL = "SELECT id , label from langage where id = any ( $1 )"

// experience
const getExperiencesSQL = "SELECT id, label from job_experience where id = any ( $1 )"
const getExperienceSQL = "SELECT id, label from job_experience where id = $1 LIMIT 1"

// job time
const getJobTimesSQL = "SELECT id, label from job_time where id = any ( $1 )"
const getJobTimeSQL = "SELECT id, label from job_time where id = $1 LIMIT 1"

// contrat
const getContratsSQL = "SELECT id, label from job_contrat where id = any ( $1 )"
const getContratSQL = "SELECT id, label from job_contrat where id = $1 LIMIT 1"

// regional
const getRegionalSQL = "SELECT id, label from job_regional where id = $1 LIMIT 1"

// apply status todo delete
const getApplyStatusSQL = "SELECT id, label from job_application_status where id = $1"

/******************************************************************************
** LINKS
**/

// company_social_app
const insertCompanyAppsProfileSQL = "INSERT INTO public.company_social_app (user_id, social_app_id) VALUES ($1, $2)"
const deleteCompanyAppsProfileSQL = "DELETE FROM public.company_social_app where user_id = $1"

// company_industry
const insertCompanyIndustryProfileSQL = "INSERT INTO public.company_industry (user_id, industry_id) VALUES ($1, $2)"

// company tag
const getCompanyTagsSQL = "SELECT DISTINCT tag.id, tag.label from job_tag " +
	" inner join tag on tag.id = job_tag.tag_id inner join job on job.id = job_tag.job_id inner join company jbc on jbc.user_id = job.company_id where jbc.user_id = $1 order by label"

// job tag
const insertNewJobTagSQL = "INSERT INTO job_tag (job_id,tag_id) VALUES ($1, $2)"
const deleteJobTagLinkSQL = "DELETE FROM job_tag WHERE job_id = $1"

// aggregate job tag
const insertNewAggJobTagSQL = "INSERT INTO aggregate_job_tag (aggregate_job_id,tag_id) VALUES ($1, $2)"

/******************************************************************************
** WEBUSER
**/

const insertNewUserSQL = "INSERT INTO public.webuser (type,email,password) VALUES ($1,$2,crypt($3, gen_salt('bf', 8))) RETURNING id"
const peekWebUserInfoSQL = "SELECT id FROM public.webuser WHERE email = $1 and password = crypt($2, password)"
const deleteUserSQL = "DELETE FROM public.webuser where id = $1"

const getWebUserSQL = "SELECT " +
	" id, email, avatar, email_validated, disabled_account, subscription_date, last_connection_date from public.webuser " +
	" WHERE id = $1 and disabled_account = false LIMIT 1"
const getWebUserEmailSQL = "SELECT " +
	" id, email, avatar, email_validated, disabled_account, subscription_date, last_connection_date from public.webuser " +
	" WHERE email = $1 and disabled_account = false LIMIT 1"
const updateWebUserSQL = "UPDATE public.webuser " +
	" SET email = $2, avatar = $3, email_validated = $4, disabled_account = $5, last_connection_date = $6  " +
	" where id = $1"
const updateWebUserAvatarSQL = "UPDATE public.webuser SET avatar = $1 where id = $2"
const updateWebUserEmailSQL = "UPDATE public.webuser SET email = $1 where id = $2"
const updateWebUserPasswordSQL = "UPDATE public.webuser SET password = (crypt($1, gen_salt('bf', 8))) where id = $2"
const createPasswordResetRequestSQL = "UPDATE public.webuser SET password_request_id = $2, password_request_id_date = CURRENT_TIMESTAMP where email = $1"
const getUserIDPasswordResetRequestSQL = "SELECT id from public.webuser where password_request_id = $1"
const getUserIDExpiredPasswordResetRequestSQL = "SELECT id from public.webuser where password_request_id_date > (NOW() + '1 day'::interval);"
const deletePasswordResetRequestSQL = "UPDATE public.webuser SET password_request_id = null, password_request_id_date = null where id = $1"

/******************************************************************************
** LOCATIONS
**/

// const getLocationFromKeywordSQL = "SELECT id, code_postal, label, lat, lon, 0 as isdept from location_town where " +
// 	" ( label ILIKE $1 OR code_postal ILIKE $1) LIMIT $2"
const getLocationTownSQL = "SELECT id,code_postal,pretty_label,lat,lon,0 as isdept FROM location_town where id = $1 LIMIT 1"
const getLocationDeptSQL = "SELECT id,code_postal,pretty_label,0,0,1 as isdept FROM location_department where id = $1 LIMIT 1"
const getLocationCodePostalTownSQL = "SELECT id,code_postal,pretty_label,lat,lon,0 as isdept FROM location_town where code_postal = $1 LIMIT 1"
const getLocationCodePostalDeptSQL = "SELECT id,code_postal,pretty_label,0,0,1 as isdept FROM location_department where code_postal = $1 LIMIT 1"
const getLocationAggloPreviewSQL = "SELECT id, codes_postaux[1], pretty_label, 0, 0, 0 as isdept FROM location_agglomeration where id = $1 LIMIT 1"

const getLocationSearchSQL = " SELECT id,code_postal,pretty_label AS label,0,0,1 as isdept from location_department where code_postal ILIKE substring($1, 1, 2) OR label ILIKE $1 " +
	" union " +
	" select id,code_postal,pretty_label,lat,lon,0 as isdept from location_town where code_postal ILIKE $1 OR label ILIKE $1 " +
	" union " +
	" select id,'',pretty_label,0,0,0 as isdept from location_agglomeration where pretty_label ILIKE $2 " +
	" order by id asc,code_postal,label limit $3 "
const getNearMeLocationsSQL = " SELECT id,code_postal,label,lat,lon,0 as isdept FROM location_town " +
	" WHERE acos(sin(radians( $1 )) " + // latitude type 48.3891
	" * sin(radians(lat)) + cos(radians( $1 )) " + // latitude type 48.3891
	" * cos(radians(lat)) * cos(radians(lon) - (radians( $2 )))) " + // longitude type -4.4852
	" * 6371 <= 10 " + // earth radius / rayon en km
	" ORDER BY code_postal LIMIT $3 " // limit
const getTownsInDepartmentSQL = "select id from location_town where code_postal LIKE $1 ORDER BY code_postal"
const getTownsInAggloSQL = "select unnest(codes_postaux) from location_agglomeration where id = $1"
const getTownsInRadiusSQL = " SELECT ls.id FROM location_town ls, " +
	" (select mlc.lat, mlc.lon from location_town mlc where id = $1) lc " + // town center id
	" WHERE acos(sin(radians( lc.lat )) " +
	" * sin(radians(ls.lat)) + cos(radians( lc.lat )) " +
	" * cos(radians(ls.lat)) * cos(radians(ls.lon) - (radians( lc.lon )))) " +
	" * 6371 <= $2 " + // radius
	" ORDER BY code_postal LIMIT $3 "

/******************************************************************************
** COMPANY
**/

const insertNewCompany = "insert into company (user_id) VALUES ($1)"

// public ID management
const getUserIDSQL = "select user_id from company where lower(public_id) = lower($1) limit 1"
const isCompanyPublicIDAlreadyExistSQL = "select 1 from company where user_id != $1 and public_id = $2 LIMIT 1"
const updateCompanyPublicIDSQL = "UPDATE public.company SET public_id = $1 where user_id = $2"

const getCompanyMinimalSQL = "select id, user_id, public_id, company_name from company where user_id = $1"
const getCompanyMinimalPublicIDSQL = "select id, user_id, public_id, company_name from company where public_id = $1"

const getCompaniesPreviewSQL = " SELECT user_id, public_id, company_name, location, webuser.subscription_date, webuser.avatar, " +
	" (SELECT count(*) from job WHERE status = 2 AND company_id = currentCompany.user_id ) AS openJobNumber, " +
	" (select COALESCE(array_agg(tags.tag_id), ARRAY[]::integer[]) from (select job_tag.tag_id, count(*) as c from job_tag INNER JOIN job on job.id = job_tag.job_id where job.company_id = currentCompany.user_id and job.status > 1 group by job_tag.tag_id order by c desc limit $2 ) tags) " +
	" FROM company currentCompany " +
	" INNER JOIN public.webuser ON webuser.id = currentCompany.user_id " +
	" WHERE public_id IS NOT NULL " +
	" ORDER BY openJobNumber DESC LIMIT $1 "

const getCompanyPreviewSQL = " SELECT user_id, public_id, company_name, " +
	" location,(SELECT count(*) from job WHERE status = 2 AND company_id = company.user_id ) AS openJobNumber , " +
	" wuser.avatar " +
	" FROM company INNER JOIN public.webuser wuser ON wuser.id = company.user_id " +
	" WHERE user_id = $1 "
const getCompanySQL = " SELECT company.user_id, company.public_id, webuser.avatar, " +
	" company.industry, industry.label, " +
	" (select COALESCE(ARRAY_AGG(social_app.id), ARRAY[]::INTEGER[]) from public.social_app  " +
	" INNER JOIN public.company_social_app ON social_app.id = company_social_app.social_app_id) AS social_apps, " +
	" company.company_size, company_size.label, " +
	" company.location, " +
	" company_name,founded,description,phone,website, " +
	" linkedin,twitter,facebook,instagram, " +
	" (select COALESCE(ARRAY_AGG(job.id), ARRAY[]::uuid[]) from job where job.company_id = company.user_id) " +
	" FROM public.company " +
	" INNER JOIN public.industry ON industry.id = company.industry " +
	" INNER JOIN public.company_size ON company_size.id = company.company_size " +
	" INNER JOIN public.webuser ON webuser.id = company.user_id " +
	" WHERE company.user_id = $1 "
const updateCompanyProfileSQL = "UPDATE public.company SET company_name = $1, founded = $2, description=$3, " +
	" phone=$4,website=$5, linkedin=$6, twitter=$7, facebook=$8, instagram=$9, industry=$10, location=$11, company_size=$12 where user_id = $13 "
const companyStatisticsSQL = " select " +
	" (select count(id) from job where status = 2 AND company_id = $1) as nbActiveJob, " +
	" (select count(id) from job where status = any (ARRAY[2, 3]) AND company_id = $1) as nbTotaljob, " +
	" (select COALESCE(sum(viewed), 0) from job where company_id = $1) as nbTotalJobViews, " +
	" (select viewed from company where user_id = $1) as nbProfileView, " +
	" (select COALESCE(sum(redirection), 0) from job where company_id = $1) as nbRedirection "

	/******************************************************************************
	 ** JOBS
	 **/

const insertJobSQL = "INSERT INTO public.job " +
	" (id, public_id, company_id, " +
	"	position, description, url_or_mail_apply, " +
	" contrat, experience, worktime, salary, regional, full_remote, expire_date, status) " +
	" VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, to_timestamp($13, 'YYYY-MM-DD hh24:mi:ss'), $14) RETURNING job.id, job.public_id"

// job update
const updateJobSQL = "UPDATE public.job SET status = $13," +
	" public_id = $1, position = $2, description = $3, url_or_mail_apply = $4, contrat = $5, experience = $6, worktime = $7, salary = $8, regional = $9, full_remote = $10, expire_date = to_timestamp($11, 'YYYY-MM-DD hh24:mi:ss') " +
	" where job.id = $12"
const archiveJobSQL = "UPDATE public.job SET status = 3 where job.id = $1"
const unarchiveJobSQL = "UPDATE public.job SET status = 2 where job.id = $1"
const updateExpiredJobStatusSQL = "UPDATE public.job SET status = 3 where job.expire_date >= now()"

// job preview
const getJobPreviewSQL = " SELECT job.id, job.public_id, job.position, substring(job.description, 1, $2), job.full_remote, job.contrat, job.regional, job.experience, job.worktime, job.featured, false, '', post_date, company_id, '', " +
	" 0 as sourceaggregation, ( select COALESCE(array_agg(job_tag.tag_id), ARRAY[]::integer[]) from job_tag where job_tag.job_id = job.id ) " +
	" from job where job.id = any ( $1 ) AND status = 2 "

// const getCompanyAvailablesJobsIDSQL = " SELECT job.id " +
// 	" from job where company_id = $1  AND status = 2"

// const getAvailablesJobsIDSQL = " SELECT job.id from job where status = 2"

// const getArchivedJobSQL = " SELECT job.id, job.public_id, job.position, substring(job.description, 1, $2), job.full_remote, job.contrat, job.regional, job.featured, false, '', post_date, company_id, '', " +
// 	" 0 as sourceaggregation, ( select COALESCE(array_agg(job_tag.tag_id), ARRAY[]::integer[]) from job_tag where job_tag.job_id = job.id ) " +
// 	" from job where job.id = any ( $1 ) AND status = 3"

// similar job previews
// TODO we compare only tags for the moment
const getSimilarJobsSQL = " select distinct id from job " +
	" inner join job_tag on job_id = job.id " +
	" where job_tag.tag_id = any (select tag_id from job_tag where job_id = $1 )  " +
	" and job_tag.job_id != $1 and status = 2 LIMIT $2"

// 1 online job
const getOnlineJobSQL = "SELECT job.id, job.public_id, job.company_id, job.position, job.description, url_or_mail_apply, post_date, expire_date,  " +
	" salary, job.contrat, job.worktime, job.experience, job.regional, " +
	" full_remote, featured, viewed, ( select COALESCE(array_agg(job_tag.tag_id), ARRAY[]::integer[]) from job_tag where job_tag.job_id = job.id ) " +
	" FROM job " +
	" WHERE job.public_id = $1 AND status = 2 LIMIT 1 "
const getJobSQL = "SELECT job.id, job.public_id, job.company_id, job.position, job.description, url_or_mail_apply, post_date, expire_date,  " +
	" salary, job.contrat, job.worktime, job.experience, job.regional, " +
	" full_remote, featured, viewed, ( select COALESCE(array_agg(job_tag.tag_id), ARRAY[]::integer[]) from job_tag where job_tag.job_id = job.id ) " +
	" FROM job " +
	" WHERE job.public_id = $1 LIMIT 1 "

// const getBabyJobSQL = "SELECT job.id, job.public_id, job.company_id, job.position, job.description, url_or_mail_apply, post_date, expire_date,  " +
// 	" salary, job.contrat, job.worktime, job.experience, job.regional, " +
// 	" full_remote, featured, viewed, ( select COALESCE(array_agg(job_tag.tag_id), ARRAY[]::integer[]) from job_tag where job_tag.job_id = job.id ) " +
// 	" FROM job " +
// 	" WHERE job.status = 0 and company_id = $1 LIMIT 1 "

// const peekBabyJobSQL = "SELECT id FROM job WHERE job.status = ANY (ARRAY[0, 1]) and job.company_id = $1 LIMIT 1"
const getJobStatusSQL = "SELECT id FROM job where job.public_id = $1 AND job.company_id = $2 AND job.status = $3 LIMIT 1"

const getJobStatForCompanySQL = "SELECT job.id, job.public_id, job.position, job.contrat, job.regional, full_remote, post_date, expire_date, " +
	" featured, viewed, redirection, status" +
	" FROM job " +
	" WHERE job.company_id = $1 ORDER BY status DESC, expire_date ASC"

// get apply data
const getJobURLMailApplySQL = "select id,url_or_mail_apply from job where public_id = $1 limit 1"

// job stats
const incrementJobViewCountSQL = " UPDATE public.job SET viewed = viewed + 1 WHERE id = $1 "
const incrementJobRedirectionCountSQL = " UPDATE public.job SET redirection = redirection + 1 WHERE id = $1 "

// disable
const disableJobSQL = "UPDATE public.job SET status = 4 where id = $1"
const deleteJobSQL = "DELETE FROM public.job where id = $1"

/*****************************************************************************
 ** AGGREGATE
 **/
const insertAggregateJobSQL = " INSERT INTO aggregate_job (id,public_id,source,company_name,contrat,experience,worktime,full_remote,location,position,description,url,posted_date,expire_date) " +
	" VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING id "
const checkAggregateJobAlreadyExistSQL = " select true from aggregate_job where public_id = $1 LIMIT 1 "
const getAggregateURLSQL = "select url from aggregate_job where public_id = $1 LIMIT 1 "

/******************************************************************************
 ** NEWSLETTER
 **/

const createNewletterSub = "insert into public.newsletter_subscription (email, params, regularity, activation_code) VALUES ($1, $2, $3, $4) RETURNING id "
const deleteNewletterSub = "delete from public.newsletter_subscription where activation_code = $1"
const getSubcriptionsPeriodicitySQL = " select sub.id, sub.email, sub.regularity from public.newsletter_subscription sub " +
	" inner join public.newsletter_regularity reg on reg.id = sub.regularity " +
	" where reg.id = $1 "
const createNewsletterQueueItemSQL = "INSERT INTO newsletter_queue (email,title,content_html,content_text) " +
	" VALUES ($1,$2,$3,$4) RETURNING newsletter_queue.id"
const getNewletterQueueSQL = "select ID, Email, Title, content_html, content_text, created_at, sent from public.newsletter_queue where sent = false"
const getMailInQueueSQL = "select ID, Email, Title, content_html, content_text, created_at, sent from public.newsletter_queue where ID = $1"
const sentQueueItemSQL = " update public.newsletter_queue set sent = true , sent_time = now() where id = $1 "
const comfirmItemSQL = "update public.newsletter_subscription set active = true where activation_code = $1 "

/*****
** LOGS
**/
// const insertLogSQL = "insert into private.logs (message, level, date) VALUES ($1, $2, $3)"

const insertLogSQL = "INSERT INTO private.logs " +
	" (date, msg, module, lvl, caller, section, controller, method, action_path, name, error, controller_type, elapsed, json) " +
	" VALUES " +
	" ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) "

// debug only
const getActivationCode = "select activation_code from public.newsletter_subscription where email = $1 and active = false limit 1"

// const getCompanyIDsSQL = "select user_id, id, public_id, company_name from company where user_id = $1 LIMIT 1"

// const getCompanyJobsSQL = " SELECT job.id, job.public_id, job.position, " +
// 	" company.company_name AS company_name, webuser.avatar AS avatar, location_town.label AS company_location, " +
// 	" job_contrat.label AS contrat, full_remote AS full_remote, job_time.label AS work_regularity, salary, " +
// 	" post_date AS since, featured AS featured, job.viewed " +
// 	" FROM public.job job " +
// 	" INNER JOIN company ON job.company_id = company.user_id " +
// 	" INNER JOIN location_town ON company.location = location_town.id " +
// 	" INNER JOIN job_contrat ON job.contrat = job_contrat.id " +
// 	" INNER JOIN job_time ON job.worktime = job_time.id " +
// 	" INNER JOIN webuser ON webuser.id = company.user_id " +
// 	" WHERE company_id = $1 AND validated = true AND expire_date >= now() "
// const getCompanyArchivedJobsSQL = " SELECT job.id, job.public_id, job.position,job.external_job, job.url, " +
// 	" company.company_name AS company_name, webuser.avatar AS avatar, location_town.label AS company_location, " +
// 	" job_contrat.label AS contrat, full_remote AS full_remote, job_time.label AS work_regularity, salary, " +
// 	" expire_date AS since, featured AS featured, job.viewed " +
// 	" FROM public.job job " +
// 	" INNER JOIN company ON job.company_id = company.user_id " +
// 	" INNER JOIN location_town ON company.location = location_town.id " +
// 	" INNER JOIN job_contrat ON job.contrat = job_contrat.id " +
// 	" INNER JOIN job_time ON job.worktime = job_time.id " +
// 	" INNER JOIN webuser ON webuser.id = company.user_id " +
// 	" WHERE company_id = $1 AND validated = true AND expire_date < now() "

// keyword filter
// const getJobsListKeywordSQL = " SELECT job.public_id, job.position, job.external_job, job.url, company.company_name AS company_name, webuser.avatar AS avatar, location_town.label AS company_location, " +
// 	" full_remote AS full_remote, job_time.label AS work_regularity, salary, " +
// 	" post_date AS since, featured AS featured, job.viewed  " +
// 	" FROM job " +
// 	" INNER JOIN company ON job.company_id = company.user_id " +
// 	" INNER JOIN location_town ON company.location = location_town.id " +
// 	" INNER JOIN job_time ON job.worktime = job_time.id " +
// 	" INNER JOIN webuser ON webuser.id = company.user_id " +
// 	" WHERE validated = true AND expire_date >= now() " +
// 	" and job.position ILIKE $1 OR job.description ILIKE $1 " +
// 	" limit $2 offset $3 "

// company filter
// const getAllCompanyJobsSQL = "SELECT job.id, job.public_id, job.company_id, job.position, job.description, url_or_mail_apply, post_date, expire_date,  " +
// 	" salary, job.contrat, job.worktime, job.experience, job.regional, " +
// 	" full_remote, featured, viewed, ( select COALESCE(array_agg(job_tag.tag_id), ARRAY[]::integer[]) from job_tag where job_tag.job_id = job.id ) as tagIDs " +
// 	" FROM job " +
// 	" WHERE job.company_id = $1 AND validated = true ORDER BY expire_date ASC"
