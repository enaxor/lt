package dbmodels

import (
	"time"
)

// ApplicationsCandidateView store an application data for a specific job, for a specific candidate
// "application focused view"
// type ApplicationsCandidateView struct {
// 	// job data
// 	JobID            guuid.UUID
// 	JobPublicKey     string
// 	JobPosition      string
// 	JobDescription   string
// 	JobValidatedDate time.Time
// 	JobExpireDate    time.Time
// 	JobFeatured      bool
// 	// application data
// 	AppliStatusLabel      string
// 	AppliStatusUpdateDate time.Time
// 	AppliMessage          string
// 	// company data
// 	CompanyID     string
// 	CompanyName   string
// 	CompanyAvatar string
// 	// chat
// 	CandidateMessages []ChatMessage
// 	CompanyMessages   []ChatMessage
// }

// JobsCompanyView view and its status
type JobsCompanyView struct {
	// job data
	JobID            int
	JobPublicKey     string
	JobPosition      string
	JobDescription   string
	JobValidatedDate time.Time
	JobExpireDate    time.Time
	JobFeatured      bool
	// application data
	NbApplication         int
	AppliStatusUpdateDate time.Time
	AppliMessage          string
	// company data
	CompanyID     string
	CompanyName   string
	CompanyAvatar string
	// chat
	// CandidateMessages []ChatMessage
	// CompanyMessages   []ChatMessage
}

// ApplicationsCompanyView store all applications data for a specific job, for a specific comapny
// "job focused view"
// type ApplicationsCompanyView struct {
// 	Name     string
// 	Prenom   string
// 	Email    string
// 	CvPath   string // path of an uploaded cv
// 	Message  string
// 	IsMember bool // true if a candidate profile is available
// 	Date     time.Time
// }

// // SearchLocation of an user
// type SearchLocation struct {
// 	ID            string // string because departement id can be a varchar
// 	PostalCode    string
// 	Label         string
// 	IsDepartement bool // if true, searchLocation is a department ID
// 	RadiusID      int  // cant be null -> only in selected town
// }

/*******************************/

// type job struct {
// 	id               uint
// 	publicKey        string
// 	employerID       uint
// 	contrat          uint
// 	experience       uint
// 	remoteRegularity uint
// 	worktime         uint
// 	position         string
// 	description      string
// 	location         uint
// 	skills           string
// 	salaryRange      string
// 	validated        bool
// 	postDate         time.Time
// 	expireDate       time.Time
// 	validatedDate    time.Time
// }

// CandidateProfile show applicants detail of a job
// todo does not handle account less apply
// type CandidateProfile struct {
// 	UserID string
// 	// common data
// 	Email              string
// 	Avatar             string
// 	SubscriptionDate   string
// 	LastConnectionDate string
// 	// candidate data
// 	Nom                     string
// 	Prenom                  string
// 	SeekJobRemoteRegularity string
// 	SeekJobExperience       string
// 	SeekJobTime             string
// 	SeekJobContrat          string
// 	SeekJobLocation         string
// 	Cv                      string
// }
