package dbmodels

import (
	"time"
)

// NewsletterSubscription handle a newletter sub
type NewsletterSubscription struct {
	Email      string
	ID         int
	Regularity int
}

// // SuggestedJobMailing stub
// type SuggestedJobMailing struct {
// 	UserID guuid.UUID
// 	Email  string
// 	Prenom string
// }

// MailQueueItem handle mail queue db data
type MailQueueItem struct {
	ID          int64
	Email       string
	Title       string
	ContentHTML string
	ContentText string
	CreatedAt   time.Time
	Sent        bool
	SentTime    *time.Time
}
