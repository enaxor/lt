package dbmodels

import (
	"time"

	guuid "github.com/google/uuid"
)

// Webuser handle common data for a user (candidate or employer)
type Webuser struct {
	ID                 guuid.UUID
	Email              string
	Password           string
	Avatar             string
	EmailValidated     bool
	DisabledAccount    bool
	SubscriptionDate   time.Time
	LastConnectionDate time.Time
}

// Tag is exported to controllers
type Tag struct {
	ID    int64
	Label string
	Count int64
}

// Regional is used to locate a job
// @see table job_regional
type Regional struct {
	ID    int64
	Label string
}

// UserLocation data entered by an user
type UserLocation struct {
	MainLocation      Location // Can be a deptartment, town or agglo ID
	SearchLocationAll []int64  // all the town ids computed using ^this datas^ (all town in a dept or all town in the radius of)
}

// Location contain all the location data
type Location struct {
	ID            int64
	PostalCode    string
	Label         string
	IsDepartement bool
	LocationLat   string // filled only for the AroundMe option
	LocationLon   string // filled only for the AroundMe option
	RadiusID      int64  // cant be null -> only in selected town form
}

// Industry of a company
type Industry struct {
	ID    int64
	Label string
}

// ApplyStatus closed,new,pending,accepted,refused
type ApplyStatus struct {
	ID    int64
	Label string
}

// ChatMessage store a message between users not used anymore 20201226
// type ChatMessage struct {
// 	ID           int64
// 	ApplicantID  string
// 	CompanyID    string
// 	RelatedJobID string
// 	Content      string
// 	Viewed       bool
// 	Date         time.Time
// }

// SocialApp Skype/Mattermost/Mail
type SocialApp struct {
	ID    int64
	Label string
}

// JobContrat cdi/cdd/freelance
type JobContrat struct {
	ID    int64
	Label string
}

// JobExperience junior/intermediate/senior/expert/master
type JobExperience struct {
	ID    int64
	Label string
}

// JobWorkTime temps plein/partiel
type JobWorkTime struct {
	ID    int64
	Label string
}

// Langage English,Mandarin,Spanish...
type Langage struct {
	ID    int64
	Label string
}

// JobSalary < 20 000
type JobSalary struct {
	ID    int64
	Label string
}
