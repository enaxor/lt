package dbmodels

import guuid "github.com/google/uuid"

// Company data of a Company profile
type Company struct {
	// basic company data
	Webuser     Webuser
	PublicID    string
	Name        string
	CompanySize CompanySize
	Description string
	Industry    Industry
	Location    Location
	Founded     int
	// social
	Phone     string
	Website   string
	Linkedin  string
	Twitter   string
	Facebook  string
	Instagram string
	// apps to remote working
	SocialApps    []SocialApp
	AvailableJobs []JobPreview
	Tags          []Tag
	// assets
	Logo []byte // only used on uploading
	// stats
	Stats CompanyStat
}

// CompanyStat stats about a c
type CompanyStat struct {
	// profile
	ProfileViewCount         int
	ProfileCompletionPercent int
	ProfileNameFilled        bool
	// jobs
	JobActiveCount      int
	JobTotalCount       int
	JobTotalViewCount   int
	JobRedirectionCount int
}

// CompanyPreview minimal data of a company
type CompanyPreview struct {
	Webuser           Webuser
	PublicID          string
	Name              string
	Location          Location
	AvailableJobCount int
	Tags              []Tag
}

// CompanyMinimal minimal info to use a company filter
type CompanyMinimal struct {
	DbID     int64
	ID       guuid.UUID
	PublicID string
	Name     string
}

// CompanySize of a company
type CompanySize struct {
	ID    int64
	Label string
}
