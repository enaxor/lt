package dbmodels

import (
	"errors"
	"lt/app/utils"
	"strconv"
	"time"

	guuid "github.com/google/uuid"
)

// Job containt job data
type Job struct {
	// private data
	ID guuid.UUID
	// public job data
	PublicID    string
	Position    string
	Description string
	// apply info
	URLMailApply string
	Since        time.Time
	SinceStr     string
	Until        time.Time
	// indicator
	Salary     int
	Contrat    JobContrat
	Experience JobExperience
	Regional   Regional
	WorkTime   JobWorkTime
	Tags       []Tag
	FullRemote bool
	Featured   bool
	// stats
	ViewCount int // number of job page showed
	// similar job data
	SimilarJobs []JobPreview
	// company
	Company Company
}

// JobStat (use only on company dashboard)
type JobStat struct {
	// private data
	ID guuid.UUID
	// public job data
	PublicID   string
	Position   string
	Since      time.Time
	Until      time.Time
	Contrat    JobContrat
	Regional   Regional
	FullRemote bool
	// management
	Featured bool
	Status   int
	// stats
	ViewCount     int // number of job page showed
	RedirectCount int
}

// JobPreview minimal data for a job
type JobPreview struct {
	ID              guuid.UUID
	PublicID        string
	Position        string
	Description     string
	FullRemote      bool
	Featured        bool
	Since           time.Time
	SinceStr        string
	Tags            []Tag
	Contrat         JobContrat
	Xp              JobExperience
	Regional        Regional
	WorkTime        JobWorkTime
	Company         CompanyPreview
	IsAggregate     bool
	AggregateURL    string
	AggregateSource int
}

// AggregateJob to store aggregate job from external sites / commun object
type AggregateJob struct {
	ID              guuid.UUID
	PublicID        string
	Source          int
	CompanyName     string
	Contrat         int
	Experience      int
	Worktime        int
	FullRemote      bool
	LocationID      int64 // real ID of a town in db (or 0)
	Position        string
	Description     string
	URL             string
	Tags            []string
	PublicationDate time.Time
	ExpirationDate  time.Time
}

// AggregateNbSource keep this update
var AggregateNbSource = 6

// Validate an aggregate job
func (cj *AggregateJob) Validate() bool {
	loc, _ := time.LoadLocation("UTC")
	expiresAt := time.Now().In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
	// ID
	if len(cj.ID) == 0 {
		utils.ManageLog(utils.Info, utils.Database, "AggregateJob.Validate", "1", errors.New("DROPPED new aggregate job [REASON = 'ID is empty']"), "Source", strconv.Itoa(cj.Source), "Position", cj.Position)
		return false
	}
	// 	Source
	if cj.Source <= 0 || int(cj.Source) > AggregateNbSource {
		utils.ManageLog(utils.Info, utils.Database, "AggregateJob.Validate", "1", errors.New("DROPPED new aggregate job [REASON = 'wrong source']"), "Source", strconv.Itoa(cj.Source), "Position", cj.Position)
		return false
	}
	// CompanyName
	if len(cj.CompanyName) == 0 {
		utils.ManageLog(utils.Info, utils.Database, "AggregateJob.Validate", "1", errors.New("DROPPED new aggregate job [REASON = 'company name is empty']"), "Source", strconv.Itoa(cj.Source), "Position", cj.Position)
		return false
	}
	// Contrat
	// Experience
	// Worktime
	// RemoteRegularity
	// LocationID
	// Position
	if len(cj.Position) == 0 {
		utils.ManageLog(utils.Info, utils.Database, "AggregateJob.Validate", "1", errors.New("DROPPED new aggregate job [REASON = 'position is empty']"), "Source", strconv.Itoa(cj.Source), "Position", cj.Position)
		return false
	}
	// Description
	// URL
	if len(cj.URL) == 0 {
		utils.ManageLog(utils.Info, utils.Database, "AggregateJob.Validate", "1", errors.New("DROPPED new aggregate job [REASON = 'No external URL']"), "Source", strconv.Itoa(cj.Source), "Position", cj.Position)
		return false
	}
	// Tags
	// Date
	if expiresAt.Before(cj.PublicationDate) {
		utils.ManageLog(utils.Info, utils.Database, "AggregateJob.Validate", "1", errors.New("DROPPED new aggregate job [REASON = 'No publication date']"), "Source", strconv.Itoa(cj.Source), "Position", cj.Position)
		return false
	}
	return true
}
