package db

import (
	"database/sql"
	"fmt"
	"lt/app/utils"
	"regexp"
	"strconv"
	"strings"
	"time"

	guuid "github.com/google/uuid"
	// https://github.com/xeonx/timeago/blob/master/timeago.go
)

const publicIDURLMaxLen = 24
const commonCharToCleanURL = "-"

// GenerateIDForNewJob @param: position @return uuid and publicID
func GenerateIDForNewJob(jobPosition string, uniqueUUID guuid.UUID) string {
	// position process
	// escape and replace by -
	cleanPosition := SanitizeStringReplaceWith(jobPosition, publicIDURLMaxLen, commonCharToCleanURL)
	// concat UUID
	var ret strings.Builder
	ret.WriteString(cleanPosition)
	ret.WriteString("-")
	ret.WriteString(uniqueUUID.String())
	// debug
	manageInfoLog("GenerateIDForNewJob", "1", "result", ret.String())
	return ret.String()
}

// GenerateIDForNewAggregateJob @param: position @return uuid and publicID for an aggregate job
func generateIDForNewAggregateJob(source int, position string) (guuid.UUID, string) {
	// position process
	// escape and replace by -
	publicID := SanitizeStringReplaceWith(position, publicIDURLMaxLen, commonCharToCleanURL)
	// uuid process
	// il est tres possible vu la source qu'on se retrouve avec des jobs existants deja en database
	// on peut les detecter easy grace a la publicID qui fait une simple concaténation de la source (int)
	// et le hash de position
	var uniqueID strings.Builder
	uniqueID.WriteString(strconv.Itoa(source))
	uniqueID.WriteString("-")
	uniqueID.WriteString(publicID)
	newUUID := guuid.NewMD5(guuid.NameSpaceURL, []byte(uniqueID.String())) // md5Position := md5.Sum([]byte(sb.String()))
	// concat UUID
	var ret strings.Builder
	ret.WriteString(newUUID.String())
	ret.WriteString("-")
	ret.WriteString(publicID)
	// debug
	// fmt.Println("generateIDForNewAggregateJob processed", "result", ret.String(), "newUUID", newUUID)
	return newUUID, ret.String()
}

// createPublicKey public key to fill unique url
// func createPublicKey(jobPosition string) string {
// 	var str strings.Builder
// 	// escape and replace by -
// 	publicKey := sanitizeStringReplaceWith(jobPosition, commonCharToCleanURL)
// 	// convert it to a rune to change size
// 	publicKeyRune := []rune(publicKey)
// 	// limit size
// 	var maxlen int
// 	if publicKeyURLMaxLen > len(publicKey) {
// 		maxlen = len(publicKey)
// 	} else {
// 		maxlen = publicKeyURLMaxLen
// 	}
// 	publicKey = string(publicKeyRune[0:maxlen])
// 	// Convert it to a rune
// 	publicKey = string([]rune(publicKey))
// 	// lowercase
// 	publicKey = strings.ToLower(publicKey)
// 	// add salt "-97845"
// 	publicKey = strings.TrimSuffix(publicKey, commonCharToCleanURL)
// 	str.WriteString(publicKey)
// 	str.WriteString("-")
// 	for i := 0; i < 5; i++ {
// 		str.WriteByte(NumSource[rand.Intn(len(NumSource))])
// 	}
// 	// and voila
// 	return str.String()
// }

// SanitizeStringReplaceWith remove all not in a-z, A-Z and replace by replaceWith
func SanitizeStringReplaceWith(notEscaped string, maxSize int, replaceWith string) string {
	var str string
	// lower
	notEscaped = strings.ToLower(notEscaped)
	// trim
	notEscaped = strings.TrimSpace(notEscaped)
	// remove all not in a-z, A-Z
	cleanChars := regexp.MustCompile("[^a-zA-Z]+")
	str = cleanChars.ReplaceAllString(notEscaped, replaceWith)
	// limit size
	if len(str) > maxSize {
		// convert it to a rune to change size
		strRune := []rune(str)
		str = string(strRune[0:maxSize])
	}
	// delete possible last tokens
	str = strings.Trim(str, replaceWith)
	// ok
	return str
}

// time to 2016-11-28 06:36:38+01
func toDatabaseTimestamp(t time.Time) string {
	return fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())
}

func convertMapSQLNullStringValueToString(m map[string]sql.NullString) map[string]string {
	var ret = make(map[string]string)
	var value string
	for k, v := range m {
		value = ""
		if v.Valid { // check if string is not null
			value = v.String
			ret[k] = value
		}
	}
	return ret
}

// Int64ArrayToString convert an int array to a string delimited by a comma
func int64ArrayToString(a []int64) string {
	b := ""
	for _, v := range a {
		if len(b) > 0 {
			b += ","
		}
		b += strconv.FormatInt(v, 10)
	}

	return b
}

// Contains return true if item is in slice
func contains(item string, slice []string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}

// error logger
func manageErrorLog(err error, action, method string, param ...string) {
	utils.ManageLog(utils.Error, utils.Database, method, action, err, param...)
}

func manageInfoLog(action, method string, param ...string) {
	utils.ManageLog(utils.Info, utils.Database, method, action, nil, param...)
}
