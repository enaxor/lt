package db

/**
 ** DATABASE credentials
 **/

const dbHost = "localhost"
const dbName = "lt"
const dbUser = "shgz"
const dbPassword = "azeqsdwxc"
const dbPort = "5432"

/* Le système métrique n'est ce pas */
const (
	_      = iota
	KB int = 1 << (10 * iota)
	MB
	GB
)

// NumSource available number
const NumSource = "0123456789"

/**
 ** RETURN CODES
 **/

// CompletedWithNoError all fine
const CompletedWithNoError = 0

// UncoverableDatabaseError undetermined fatal errpr
const UncoverableDatabaseError = 500

// NoItemFound empty result error
const NoItemFound = 404

// ItemAlreadyExist duplicate row error
const ItemAlreadyExist = 1

// MaxDescriptionCharPreviewJob max char to keep from a jobPreview Description
const MaxDescriptionCharPreviewJob = 200

var pqErrCodesMap = map[string]int{
	"23505": ItemAlreadyExist,
}

// JobStatusCreated step 1
const JobStatusCreated = 0

// JobStatusPending step 2
const JobStatusPending = 1 // need your validation

// JobStatusOnline step 3
const JobStatusOnline = 2

// JobStatusArchived step 4
// WARN: queries.go have hardcoded values
const JobStatusArchived = 3

// JobStatusDeleted step 5
const JobStatusDeleted = 4

// JobStatusUnvalidated need a edition
const JobStatusUnvalidated = 5

// nb tag max on a job
const JobMaximumTag = 20

// Removed UserTypeApplicant int to determine user type
// const UserTypeApplicant = 1

// UserTypeCompany int to determine user type
const UserTypeCompany = 2

// FilterType handle filters on a jobs list requeest
type FilterType int

// List of different filter type
const (
	TagsSearchF FilterType = 1 // string array / assuming already escaped
	// only logged user can filter by it
	LocationTownF     FilterType = 2  // int array
	LocationDeptF     FilterType = 3  // int array
	ExperienceF       FilterType = 4  // int array
	ContratF          FilterType = 5  // int array
	WorktimeF         FilterType = 6  // int array
	RemoteF           FilterType = 15 // int array
	SalaryF           FilterType = 7  // int array
	TagsF             FilterType = 8  // int array
	SocialAppsF       FilterType = 9  // int array
	LangageF          FilterType = 10 // int array
	FeaturedF         FilterType = 11 // boolean
	SuggestedJobOnlyF FilterType = 12 // string (applicant_id)
	SavedJobOnlyF     FilterType = 13 // string (applicant_id)
	CompanyF          FilterType = 14 // string (public_id)
)

// RequestFilter a filter
type RequestFilter struct {
	FilterType FilterType
	FString    string
	FIntArray  []int64
	FInt       int64
	FBoolean   bool
}

// JobListRequest handle a list request with pagination and multiple filter
type JobListRequest struct {
	IndexStart int
	IndexEnd   int
	Filters    []RequestFilter
}

// COMPLETED_WITH_NO_ERROR all ok
// const errorCodeDbEmailAlreadyExist = 199
// const errorCodeDbEmailNotExist = 200
// const errorUserNotExist = 201
// const errorJobAlreadyExist = 202
// const errorSkillAlreadyExist = 203
// const errorCodeDbItemUpdate = 204
// const errorLocationNotExist = 205
// const errorIndustryNotExist = 206
// const errorSkillNotExist = 207
// const errorCodeDbDeleteFailed = 208
// const errorCodeDbInsertFailed = 209
// const errorCodeDbItemNotExist = 210
// const errorCodeDbInsertDuplicate = 211

// // ErrorCodes containt message associated with error code
// var ErrorCodes = map[int]string{ // TODO amiliorer le systeme d'errerur ?
// 	errorCodeDbEmailAlreadyExist: "Cet email existe déjà.",
// 	errorCodeDbEmailNotExist:     "Utilisateur inconnu.",
// 	errorUserNotExist:            "Utilisateur inconnu.",
// 	errorJobAlreadyExist:         "Une annonce avec ce job existe déjà.",
// 	errorSkillAlreadyExist:       "Skill Link already exist",
// 	errorCodeDbItemUpdate:        "L'update a échoué.",
// 	errorLocationNotExist:        "La location n'existe pas",
// 	errorIndustryNotExist:        "L'industry n'existe pas",
// 	errorSkillNotExist:           "Aucune compétances trouvées",
// 	errorCodeDbDeleteFailed:      "La supression a échouée",
// 	errorCodeDbInsertFailed:      "L'ajout de l'element a échoué",
// 	errorCodeDbItemNotExist:      "Objet non trouvé",
// 	errorCodeDbInsertDuplicate:   "L'element existe déjà.",
// }

/* DEPRECATED SQL queries */

// list all online job
// const getJobsListSQL = " SELECT job.public_id, job.position, job.external_job, job.url, company.company_name AS company_name, webuser.avatar AS avatar, location_town.label AS company_location, " +
// 	" job_remote_regularity.label AS remote_regularity, job_time.label AS work_regularity, salary, " +
// 	" validated_date AS since, featured AS featured " +
// 	" FROM job " +
// 	" INNER JOIN company ON job.company_id = company.user_id " +
// 	" INNER JOIN location_town ON company.location = location_town.id " +
// 	" INNER JOIN job_remote_regularity ON job.remote_regularity = job_remote_regularity.id " +
// 	" INNER JOIN job_time ON job.worktime = job_time.id " +
// 	" INNER JOIN webuser ON webuser.id = company.user_id " +
// 	" WHERE validated_date IS NOT NULL AND validated = true AND expire_date >= now() " +
// 	" limit $1 offset $2 "

// const getCandidateSQL = " select " +
// 	" applicant.nomade AS Nomade, " +
// 	" applicant.seek_job_deptlocation AS location_dept, " +
// 	" applicant.seek_job_townlocation AS location_town, " +
// 	" (select array_agg(id) from public.location_town where code_postal LIKE concat(COALESCE(applicant.seek_job_deptlocation, -1), '%')) AS SearchTownDept, " +
// 	" (select array_agg(subtown.id) FROM location_town subtown " +
// 	" WHERE acos(sin(radians(location_town.lat)) * sin(radians(lat)) + cos(radians(location_town.lat))  " +
// 	" * cos(radians(lat)) * cos(radians(lon) - (radians(location_town.lon))))	* 6371 <= location_town_available_radius.km ) AS SearchTownRadius, " +
// 	" (select array_agg(time_id) from public.applicant_seek_time where applicant_seek_time.applicant_id = $1) AS SearchTime, " +
// 	" (select array_agg(contrat_id) from public.applicant_seek_contrat where applicant_seek_contrat.applicant_id = $1) AS SearchContrat, " +
// 	" (select array_agg(experience_id) from applicant_seek_experience where applicant_seek_experience.applicant_id = $1) AS SearchXp, " +
// 	" (select array_agg(langage_id) from applicant_langage where applicant_langage.applicant_id = $1) AS Langages " +
// 	" (select array_agg(skill_id) from applicant_langage where applicant_skill.applicant_id = $1) AS Skills " +
// 	" from public.applicant applicant " +
// 	" inner join public.webuser webuser ON webuser.id = applicant.user_id  " +
// 	" left join public.location_town ON location_town.id = applicant.seek_job_townlocation " +
// 	" left join public.location_department ON location_department.id = applicant.seek_job_deptlocation " +
// 	" left join public.location_town_available_radius ON location_town_available_radius.id = applicant.seek_job_location_radius " +
// 	" where applicant.user_id = $1 "
// TODO DOUBLON with ^

/********** READ */

// job

// job.PublicKey, &job.Position, &job.IsExternalJob, &job.URLExternal,
// 			&job.Company.Name, &job.Company.AvatarPath, &job.Company.Location.Label, &job.RemoteRegularity, &job.WorkRegularity,
// 			&job.SalaryRange, &job.Since, &job.Featured

// applicant

// user list
// const getListUserWithTagSQL = " select user_id,nom,prenom,description,nomade,wuser.avatar," +
// 	" (select xp.id from applicant_seek_experience " +
// 	" inner join job_experience xp on xp.id = applicant_seek_experience.experience_id " +
// 	" where applicant_seek_experience.applicant_id = user_id order by id DESC limit 1) as idexp, " +
// 	" (select COALESCE(ARRAY_AGG(applicant_skill.skill_id), ARRAY[]::INTEGER[]) from public.applicant_skill " +
// 	" where applicant_skill.applicant_id = user_id ) as idskill " +
// 	" from applicant appli INNER JOIN public.webuser wuser ON wuser.id = appli.user_id "

/********** CREATE */

// job

// company

// candidates

/********** UPDATE */

// job

// candidate

/********** DELETE */

// job

// ************* OLD
// const getApplicantsForJobSQL = " select nom,prenom,email,cv_file_path,message,false,date " +
// 	" FROM public.job_application_accountless " +
// 	" WHERE job_id = $1 " +
// 	" UNION " +
// 	" select cand.nom,cand.prenom,wuser.email,cand.cv_file_path,appli.message,true,appli.date " +
// 	" FROM public.job_application_account appli " +
// 	" INNER JOIN public.webuser wuser ON wuser.id = appli.applicant_id " +
// 	" INNER JOIN public.applicant cand ON cand.user_id = appli.applicant_id " +
// 	" WHERE job_id = $1 "

// TODO: see if data can be retrieved in only one sql req
// const getCompanyDataSQL = "SELECT user_id,company_name,description,website,linkedin,twitter,facebook,instagram,founded FROM public.company WHERE user_id = $1 limit 1"
// const getCompanyIndustryDataSQL = "SELECT industry_id, label from company_industry INNER JOIN industry ON industry.id = industry_id WHERE user_id = $1"
// const getCompanyAppsDataSQL = "SELECT social_app_id,label FROM public.company_social_app INNER JOIN social_app ON social_app.id = social_app_id WHERE user_id = $1"
// const getCompanySizeDataSQL = "SELECT company_size, label from company INNER JOIN company_size ON company_size.id = company_size WHERE user_id = $1 limit 1"
// const getCompanyLocationDataSQL = "SELECT id,code_postal,label, lat, lon from public.company INNER JOIN location_town ON company.location = location_town.id WHERE user_id = $1 limit 1"

// " SELECT wuser.id, wuser.email, wuser.avatar, wuser.subscription_date, wuser.last_connection_date, " +
// 	" cand.nom, cand.prenom, 0, 0, 0, 0, 0, cand.cv_file_path, " +
// 	" appli.message, appli.date " +
// 	" FROM public.job_application appli " +
// 	" INNER JOIN public.webuser wuser ON wuser.id = appli.applicant_id " +
// 	" INNER JOIN public.applicant cand ON cand.user_id = appli.applicant_id " +
// 	" WHERE appli.job_id = $1 "

// const getCandidateProfileFromUserID = " select applicant.user_id AS UserID, applicant.nom AS Nom, applicant.prenom AS Prenom, applicant.description AS Description, applicant.nomade AS Nomade, " +
// 	" applicant.seek_job_deptlocation AS location_dept, " +
// 	" location_department.code_postal AS location_dept_cp, " +
// 	" location_department.label AS location_dept_label, " +
// 	" (select array_agg(id) from public.location_town " +
// 	" where code_postal LIKE concat(applicant.seek_job_deptlocation, '%')) AS SearchTownDept, " +
// 	" applicant.seek_job_townlocation AS location_town, " +
// 	" location_town.code_postal AS location_town_cp, " +
// 	" location_town.label AS location_town_label, " +
// 	" applicant.seek_job_location_radius AS SearchRadiusID, " +
// 	" (select array_agg(subtown.id) FROM location_town subtown " +
// 	" WHERE acos(sin(radians(location_town.lat)) * sin(radians(lat)) + cos(radians(location_town.lat))  " +
// 	" * cos(radians(lat)) * cos(radians(lon) - (radians(location_town.lon))))	* 6371 <= location_town_available_radius.km ) AS SearchTownRadius, " +
// 	" (select array_agg(id) from public.job_time  " +
// 	" inner join public.applicant_seek_time ON applicant_seek_time.time_id = job_time.id  " +
// 	" where applicant_seek_time.applicant_id = $1) AS Disponibility, " +
// 	" (select array_agg(id) from public.job_contrat " +
// 	" inner join public.applicant_seek_contrat ON applicant_seek_contrat.contrat_id = job_contrat.id  " +
// 	" where applicant_seek_contrat.applicant_id = $1) AS SearchContrat, " +
// 	" (select array_agg(id) from public.job_experience " +
// 	" inner join public.applicant_seek_experience ON applicant_seek_experience.experience_id = job_experience.id  " +
// 	" where applicant_seek_experience.applicant_id = $1) AS SearchXp, " +
// 	" (select array_agg(id) from public.social_app  " +
// 	" inner join public.applicant_social_app ON applicant_social_app.social_app_id = social_app.id  " +
// 	" where applicant_social_app.user_id = $1) AS UsingApps, " +
// 	" applicant.Website AS Website, applicant.Linkedin AS Linkedin, applicant.Facebook AS Facebook, applicant.Twitter AS Twitter, applicant.Instagram AS Instagram,  " +
// 	" webuser.avatar AS Avatar " +
// 	" from public.applicant applicant  " +
// 	" inner join public.webuser webuser ON webuser.id = applicant.user_id " +
// 	" inner join public.applicant_seek_time applicant_jobtimes ON applicant_jobtimes.applicant_id = applicant.user_id  " +
// 	" inner join public.job_time job_time ON job_time.id = applicant_jobtimes.time_id  " +
// 	" left join public.location_town ON location_town.id = applicant.seek_job_townlocation " +
// 	" left join public.location_department ON location_department.id = applicant.seek_job_deptlocation " +
// 	" inner join public.location_town_available_radius ON location_town_available_radius.id = applicant.seek_job_location_radius  " +
// 	" where applicant.user_id = $1 "
