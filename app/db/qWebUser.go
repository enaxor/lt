package db

import (
	"database/sql"
	"errors"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"math"

	"github.com/revel/revel"

	guuid "github.com/google/uuid"
	"github.com/lib/pq"
)

/********** WEBUSER */

// InsertNewUser insert a new user. you will need to check if input will not create a Email violation before calling this
func InsertNewUser(email string, password string) (guuid.UUID, int) {
	var userid guuid.UUID
	// prepare
	tx, _ := db.Begin()
	stmt, err := db.Prepare(insertNewUserSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run query
	row := tx.Stmt(stmt).QueryRow(UserTypeCompany, email, password)
	// read
	err = row.Scan(&userid)
	if err != nil {
		// check for duplicated error
		if pqerr, ok := err.(*pq.Error); ok {
			if string(pqerr.Code) == "23505" {
				return userid, pqErrCodesMap["23505"]
			}
		}
		manageErrorLog(err, "scan", "InsertNewUser_0", "input userType", utils.ToString(UserTypeCompany), "input email", email, "error", err.Error())
		tx.Rollback()
		return userid, UncoverableDatabaseError
	}
	// create associated user type
	stmta, err := db.Prepare(insertNewCompany) // company is the new default
	if err != nil {
		panic(err)
	}
	defer stmta.Close()
	_, err = tx.Stmt(stmta).Exec(userid)
	if err != nil {
		manageErrorLog(err, "query", "InsertNewUser_1", "input userType", utils.ToString(UserTypeCompany), "input email", email, "error", err.Error())
		tx.Rollback()
		return userid, UncoverableDatabaseError
	}
	// commit
	if err := tx.Commit(); err != nil {
		manageErrorLog(err, "commit", "InsertNewUser_2", "input userType", utils.ToString(UserTypeCompany), "input email", email, "error", err.Error())
		tx.Rollback()
		return userid, UncoverableDatabaseError
	}
	return userid, CompletedWithNoError
}

// CreatePasswordResetRequest stub
func CreatePasswordResetRequest(email string, requestID guuid.UUID) int {
	// prepare
	stmta, err := db.Prepare(createPasswordResetRequestSQL)
	if err != nil {
		panic(err)
	}
	defer stmta.Close()
	res, err := stmta.Exec(email, requestID)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Database, "CreatePasswordResetRequest", "query", err, "input email", email, "input requestID", requestID.String())
		return UncoverableDatabaseError
	}
	if nbRowAffected, err := res.RowsAffected(); err != nil && nbRowAffected != 1 {
		utils.ManageLog(utils.Error, utils.Database, "CreatePasswordResetRequest", "scan", err, "input email", email, "input requestID", requestID.String())
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// GetUserIDPasswordResetRequest stub
func GetUserIDPasswordResetRequest(requestID guuid.UUID) (guuid.UUID, int) {
	var ret guuid.UUID
	// prepare
	stmt, err := db.Prepare(getUserIDPasswordResetRequestSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run query
	row := stmt.QueryRow(requestID)
	// read
	ret, err = scanUUIDRow(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		utils.ManageLog(utils.Error, utils.Database, "GetUserIDPasswordResetRequest", "scan", err, "input requestID", requestID.String())
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// get userIDs that have expired request and delete codes
func ExpireOldPasswordRequestRequest() int {
	var IDs []guuid.UUID
	// prepare
	stmt, err := db.Prepare(getUserIDExpiredPasswordResetRequestSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query()
	if err != nil {
		manageErrorLog(err, "query", "ExpireOldPasswordRequestRequest_0")
		return UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	IDs, err = scanUUIDRows(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return NoItemFound
		}
		manageErrorLog(err, "scan", "ExpireOldPasswordRequestRequest_1")
		return UncoverableDatabaseError
	}
	// delete found request
	for _, v := range IDs {
		DeletePasswordResetRequest(v)
	}
	// ok
	return CompletedWithNoError
}

// DeletePasswordResetRequest stub
func DeletePasswordResetRequest(userID guuid.UUID) {
	stmt, err := db.Prepare(deletePasswordResetRequestSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// check
	res, err := stmt.Exec(userID)
	if err != nil {
		utils.ManageLog(utils.Crit, utils.Database, "DeletePasswordResetRequest", "scan", err, "userID", userID.String(), "res", utils.ToString(res))
	}
	utils.ManageLog(utils.Info, utils.Database, "DeletePasswordResetRequest", "query", errors.New("removed Old Password RequestID for userID"), "userID", userID.String())
}

// PeekWebUser check user/password
func PeekWebUser(email string, password string) (guuid.UUID, int) {
	stmt, err := db.Prepare(peekWebUserInfoSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// check
	row := stmt.QueryRow(email, password)
	// scan return
	var userUUID guuid.UUID
	err = row.Scan(&userUUID)
	if err != nil {
		if err == sql.ErrNoRows {
			return userUUID, NoItemFound
		}
		manageErrorLog(err, "scan", "PeekWebUser", "input email", email)
		return userUUID, UncoverableDatabaseError
	}
	return userUUID, CompletedWithNoError
}

// GetWebUser retrieve a webuser data by unique ID
// does not retrieve password (will be empty)
func GetWebUser(userID guuid.UUID) (dbmodels.Webuser, int) {
	var ret dbmodels.Webuser
	// prepare
	stmt, err := db.Prepare(getWebUserSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// execute
	row := stmt.QueryRow(userID)
	// scan
	ret, err = scanUserDataRow(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound // user does not exist
		}
		manageErrorLog(err, "scan", "GetWebUser", "input userID", userID.String())
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetWebUserEmail retrieve a webuser data by email
func GetWebUserEmail(email string) *dbmodels.Webuser {
	var ret *dbmodels.Webuser
	// prepare
	stmt, err := db.Prepare(getWebUserEmailSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// execute
	row := stmt.QueryRow(email)
	// scan
	userResult, err := scanUserDataRow(row)
	if err != nil {
		if err != sql.ErrNoRows {
			manageErrorLog(err, "scan", "GetWebUserEmail", "input email", email)
		}
		return nil
	}
	ret = &userResult
	return ret
}

// GetPercentProfileFilled return a percentage of an user profile completness
// return % && namecompleted
func GetPercentProfileFilled(userID guuid.UUID) (int, bool) {
	revel.AppLog.Info("GetPercentProfileFilled>>", "userID", userID.String())
	var profilepercent float64
	namecompleted := true
	var iterate float64
	profilepercent = 0.0
	company, err := GetCompany(userID)
	if err != CompletedWithNoError {
		return 0.0, false
	}
	iterate = (100 / 11.0)
	if company.Webuser.Avatar != "default.png" {
		profilepercent = profilepercent + iterate
	}
	if len(company.Name) > 0 {
		profilepercent = profilepercent + iterate
	} else {
		namecompleted = false
	}
	if company.Location.ID != 0 {
		profilepercent = profilepercent + iterate
	}
	if company.Founded != 0 {
		profilepercent = profilepercent + iterate
	}
	if company.Industry.ID != 0 {
		profilepercent = profilepercent + iterate
	}
	if len(company.Description) > 0 {
		profilepercent = profilepercent + iterate
	}
	if len(company.Website) > 0 {
		profilepercent = profilepercent + iterate
	}
	if len(company.Linkedin) > 0 {
		profilepercent = profilepercent + iterate
	}
	if len(company.Twitter) > 0 {
		profilepercent = profilepercent + iterate
	}
	if len(company.Facebook) > 0 {
		profilepercent = profilepercent + iterate
	}
	if len(company.Instagram) > 0 {
		profilepercent = profilepercent + iterate
	}
	revel.AppLog.Info("GetPercentProfileFilled>>", "userID", utils.ToString(int(math.RoundToEven(profilepercent))), "namecompleted", utils.ToString(namecompleted))
	return int(math.RoundToEven(profilepercent)), namecompleted
}

// UpdateWebUserAvatar Update WebUser Todo update email
func UpdateWebUserAvatar(userID guuid.UUID, avatar string) int {
	stmt, err := db.Prepare(updateWebUserAvatarSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(avatar, userID)
	if err != nil {
		manageErrorLog(err, "query", "UpdateWebUserAvatar", "input userID", userID.String(), "input avatar", avatar)
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// UpdateUserEmail update webbuser email
func UpdateUserEmail(userID guuid.UUID, email string) int {
	stmt, err := db.Prepare(updateWebUserEmailSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(email, userID)
	if err != nil {
		manageErrorLog(err, "query", "UpdateUserEmail", "input userID", userID.String(), "input email", email)
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// UpdateUserPassword update webuser password
func UpdateUserPassword(userID guuid.UUID, password string) int {
	stmt, err := db.Prepare(updateWebUserPasswordSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(password, userID)
	if err != nil {
		manageErrorLog(err, "query", "UpdateUserPassword", "input userID", userID.String(), "input password", password)
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// DeleteUser delete an user (company or candidat)
// modify contraint to another common user 'none'
func DeleteUser(userID guuid.UUID) int {
	stmt, err := db.Prepare(deleteUserSQL)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec(userID)
	if err != nil {
		manageErrorLog(err, "query", "DeleteUser", "input userID", userID.String())
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// // UpdateUserPrivacy update privacy settings
// func UpdateUserPrivacy(userID guuid.UUID, privacy bool) int {
// 	stmt, err := db.Prepare(updateWebUserPrivacySQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	_, err = stmt.Exec(privacy, userID)
// 	if err != nil {
// 		manageErrorLog(err, "query", "UpdateUserPrivacy", "input userID", userID.String(), "input password", marshalObject(privacy))
// 		return UncoverableDatabaseError
// 	}
// 	return CompletedWithNoError
// }

// ID                 string
// UserType           uint
// Email              string
// Password           string
// Avatar             string
// ProfileViewCount   int
// Private            bool
// EmailValidated     bool
// DisabledAccount    bool
// SubscriptionDate   time.Time
// LastConnectionDate time.Time

// UpdateUser update a webuser data USE WITH CAUTION
// 1. GetWebUser(id) -> 2. Change data -> 3. UpdateUser(user)
// func UpdateUser(user dbmodels.Webuser) int {
// 	stmt, err := db.Prepare(updateWebUserSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	_, err = stmt.Exec(user.ID, user.Email, user.Avatar, user.ProfileViewCount, user.Private, user.EmailValidated, user.DisabledAccount, user.LastConnectionDate)
// 	if err != nil {
// 		manageErrorLog(err, "query", "UpdateUser", "input user", structToString(user))
// 		return UncoverableDatabaseError
// 	}
// 	return CompletedWithNoError
// }
