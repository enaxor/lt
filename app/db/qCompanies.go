package db

import (
	"database/sql"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strconv"
	"strings"

	guuid "github.com/google/uuid"
	"github.com/lib/pq"
)

/********** COMPANIES */

// func GetCompanyUserIDFromCompanyPublicID(publicID string) (guuid.UUID, int) {
// 	var ret guuid.UUID

// 	stmt, err := db.Prepare(getCompanyUserIDFromCompanyPublicIDSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	// query
// 	row := stmt.QueryRow(publicID)
// 	// scan
// 	ret, err = scanUUIDRow(row)
// 	if err != nil {
// 		if err == sql.ErrNoRows {
// 			return ret, NoItemFound
// 		}
// 		manageErrorLog(err, "scan", "GetCompanyUserIDFromCompanyPublicID_1")
// 		return ret, UncoverableDatabaseError
// 	}
// 	// ok
// 	return ret, CompletedWithNoError
// }

// GetCompanies get a list for all company.
// these datas need to be cached and rendered little by little
func GetCompanies(limitCompany, limitTags int) ([]dbmodels.CompanyPreview, int) {
	var ret []dbmodels.CompanyPreview

	stmt, err := db.Prepare(getCompaniesPreviewSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row, err := stmt.Query(limitCompany, limitTags)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Database, "GetCompanies_0", "query", err)
		manageErrorLog(err, "query", "GetCompanies_0")
		return nil, UncoverableDatabaseError
	}
	// scan
	ret, err = scanCompanyPreviewRows(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetCompanies_1")
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetCompanyPreview retrieve light company data
func GetCompanyPreview(inputID guuid.UUID) (dbmodels.CompanyPreview, int) {
	var ret dbmodels.CompanyPreview
	// prepare
	stmt, err := db.Prepare(getCompanyPreviewSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(inputID)
	// scan
	ret, err = scanCompanyPreviewRow(row)
	if err != nil {
		manageErrorLog(err, "scan", "GetCompanyPreview_1", "inputID", inputID.String())
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetCompanyMinimalPublicID minial info about a company
func GetCompanyMinimalPublicID(publicID string) (dbmodels.CompanyMinimal, int) {
	var ret dbmodels.CompanyMinimal
	// prepare
	stmt, err := db.Prepare(getCompanyMinimalPublicIDSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(publicID)
	// scan
	ret, err = scanCompanyMinimal(row)
	if err != nil {
		manageErrorLog(err, "query", "GetCompanyMinimalPublicID", "publicID", publicID)
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetCompanyMinimal minial info about a company
func GetCompanyMinimal(userID guuid.UUID) (dbmodels.CompanyMinimal, int) {
	var ret dbmodels.CompanyMinimal
	// prepare
	stmt, err := db.Prepare(getCompanyMinimalSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(userID)
	// scan
	ret, err = scanCompanyMinimal(row)
	if err != nil {
		manageErrorLog(err, "query", "GetCompanyMinimal", "userID", userID.String())
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetCompany retrieve company data
// @param userID
func GetCompany(userID guuid.UUID) (dbmodels.Company, int) {
	var ret dbmodels.Company
	// prepare
	stmt, err := db.Prepare(getCompanySQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(userID)
	// scan
	ret, err = scanCompany(row)
	if err != nil {
		manageErrorLog(err, "query", "GetCompany_0", "userID", userID.String())
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// UpdateCompanyAppsProfile update Apps of a company
func UpdateCompanyAppsProfile(userID guuid.UUID, apps []int64) int {
	tx, _ := db.Begin() // BEGIN
	// statements
	stmtDelete, err := db.Prepare(deleteCompanyAppsProfileSQL)
	if err != nil {
		panic(err)
	}
	defer stmtDelete.Close()
	// delete all apps
	_, err = tx.Stmt(stmtDelete).Exec(userID)
	// insert new apps
	stmtInsert, err := db.Prepare(insertCompanyAppsProfileSQL)
	if err != nil {
		panic(err)
	}
	defer stmtInsert.Close()
	// re create apps
	txstmlt := tx.Stmt(stmtInsert)
	for _, v := range apps {
		if _, err = txstmlt.Exec(userID, v); err != nil {
			manageErrorLog(err, "query", "UpdateCompanyAppsProfile_0", "userID", userID.String(), "apps", utils.ToString(apps))
			tx.Rollback()
			return UncoverableDatabaseError
		}
	}
	if err := tx.Commit(); err != nil {
		manageErrorLog(err, "commit", "UpdateCompanyAppsProfile_0", "userID", userID.String(), "apps", utils.ToString(apps))
		tx.Rollback()
		return UncoverableDatabaseError
	}

	return CompletedWithNoError
}

// type companyIDs struct {
// 	UserID   guuid.UUID
// 	ID       int64
// 	PublicID string
// 	Name     string
// }

// func getCompanyIDs(userID guuid.UUID) (*companyIDs, int) {
// 	var ret companyIDs
// 	stmt, err := db.Prepare(getCompanyIDsSQL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer stmt.Close()
// 	// run query
// 	row := stmt.QueryRow(userID)
// 	// scan
// 	err = row.Scan(&ret.UserID, &ret.ID, &ret.PublicID, &ret.Name)
// 	if err != nil {
// 		if err == sql.ErrNoRows {
// 			return nil, NoItemFound
// 		}
// 		manageErrorLog(err, "scan", "getCompanyIDs_0_1", "userID", userID.String())
// 		return nil, UncoverableDatabaseError
// 	}
// 	return &ret, CompletedWithNoError
// }

// GetUserID return an userID from its company public profile
// retrieve unique userID from a company
// en fait l'url contient un public_id plutot que l'uuid qui est moins readeable (et privé)
// ce mecanisme implique aussi que les company ait renseigné a minima leur name
// pour que leur profile soit affiché
func GetUserID(publicID string) (guuid.UUID, int) {
	var ret guuid.UUID
	stmt, err := db.Prepare(getUserIDSQL)
	if err != nil {
		panic(err)
	}
	// query
	row := stmt.QueryRow(publicID)
	// scan
	ret, err = scanUUIDRow(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetUserID", "publicID", publicID)
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

func isPublicIDExist(userID guuid.UUID, publicID string) (bool, int) {
	stmt, err := db.Prepare(isCompanyPublicIDAlreadyExistSQL)
	if err != nil {
		panic(err)
	}
	// query
	row := stmt.QueryRow(userID, publicID)
	// scan
	retint, err := scanRowForInteger(row)
	if err != nil || retint != 1 {
		if err == sql.ErrNoRows {
			return false, CompletedWithNoError
		}
		manageErrorLog(err, "scan", "isPublicIDExist", "userID", userID.String(), "publicID", publicID)
		return false, UncoverableDatabaseError
	}
	// exist
	return true, CompletedWithNoError
}

// UpdateCompanyProfile update basic data about a company
func UpdateCompanyProfile(userID guuid.UUID, name string, founded int, description, phone, website, linkedin, twitter, facebook, instagram string, industry, location, companySize int64) int {
	tx, _ := db.Begin() // BEGIN
	newPublicID := name
	// if company name has been modified, we need to change publicID too
	companyIDs, errint := GetCompanyMinimal(userID)
	// company non trouvé ou company name inchangé
	if errint == NoItemFound || !strings.EqualFold(companyIDs.Name, name) {
		// update public id, managing duplicate
		exist, errint := isPublicIDExist(userID, name)
		if errint != CompletedWithNoError {
			return errint
		}
		if exist {
			// add id after company name to build public_id
			newPublicID = utils.Join(newPublicID, "-", utils.InterfaceToString(companyIDs.ID))
		}
		// update publicID
		stmt, err := db.Prepare(updateCompanyPublicIDSQL)
		if err != nil {
			panic(err)
		}
		defer stmt.Close()
		// run
		_, err = tx.Stmt(stmt).Exec(newPublicID, userID)
		if err != nil {
			manageErrorLog(err, "query", "UpdateCompanyProfile_0", "userID", userID.String(), "name", name, "companyIDs", utils.ToString(companyIDs))
			tx.Rollback()
			return UncoverableDatabaseError
		}
	}
	// to title
	name = strings.Title(name)
	// update others data
	stmt, err := db.Prepare(updateCompanyProfileSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = tx.Stmt(stmt).Exec(name, founded, description, phone, website, linkedin, twitter, facebook, instagram, industry, location, companySize, userID)
	if err != nil {
		manageErrorLog(err, "query", "UpdateCompanyProfile_1", "userID", userID.String(), "name", name, "founded", utils.ToString(founded), "description", description, "website", website, "linkedin", linkedin,
			"twitter", twitter, "facebook", facebook, "instagram", instagram, "industry", utils.ToString(industry), "location", utils.ToString(location),
			"companySize", utils.ToString(companySize))
		tx.Rollback()
		return UncoverableDatabaseError
	}
	// AFTER / commit
	if err := tx.Commit(); err != nil {
		manageErrorLog(err, "commit", "UpdateCompanyProfile_2", "userID", userID.String(), "name", name, "founded", utils.ToString(founded), "description", description, "website", website, "linkedin", linkedin,
			"twitter", twitter, "facebook", facebook, "instagram", instagram, "industry", utils.ToString(industry), "location", utils.ToString(location),
			"companySize", utils.ToString(companySize))
		tx.Rollback()
		return UncoverableDatabaseError
	}
	// ok
	return CompletedWithNoError
}

// GetCompanyStats return stats of jobs that are currently online or published by a company
//  nbActiveJob Nombre de job posté en ligne de la company
//  nbTotalJob nombre de job de la company posté en tout
//  nbTotalJobView  nombre de visites de tous les jobs de la company
//  nbProfileView  nombre de visite sur le profile de la company
//  nbRedirection nombre d'application possible de tout les jobs de la company
func GetCompanyStats(companyID guuid.UUID) dbmodels.CompanyStat {
	var ret dbmodels.CompanyStat
	// prepare
	stmt, err := db.Prepare(companyStatisticsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row := stmt.QueryRow(companyID)
	err = row.Scan(&ret.JobActiveCount, &ret.JobTotalCount, &ret.JobTotalViewCount, &ret.ProfileViewCount, &ret.JobRedirectionCount)
	if err != nil {
		manageErrorLog(err, "scan", "CountOnlineCompanyJob", "companyID", companyID.String())
	}
	ret.ProfileCompletionPercent, ret.ProfileNameFilled = GetPercentProfileFilled(companyID)
	// ok
	return ret
}

// SCANS
// UserID             string
// Nom                string
// Prenom             string
// Description        string
// Nomade             bool              // default false

// SearchLocation     db.SearchLocation // Profile location can be null (nomade)
// SearchTag        []db.Tag        // json
// Disponibility      []int64
// SearchContrat      []int64
// SearchXp           []int64
// AvailableLocations []int64 // containt all the potentials job location chosed by user. can be null (-> all locations)
// UsingApps          []int64

// Website            string
// Linkedin           string
// Facebook           string
// Twitter            string
// Instagram          string

// AvatarPath         string
// CvPath             string

// pour une row, besoin de check sql.ErrNoRows
func scanCompany(row *sql.Row) (dbmodels.Company, error) {
	var ret dbmodels.Company
	var err error
	var jobIDsStr pq.StringArray // list of job id posted by the company
	var socialApps pq.Int64Array // list of social app used by the company
	// nullable strings
	// var website sql.NullString
	// var linkedin sql.NullString
	// var twitter sql.NullString
	// var facebook sql.NullString
	// var instagram sql.NullString
	var founded string
	var publicID sql.NullString

	err = row.Scan(&ret.Webuser.ID, &publicID, &ret.Webuser.Avatar, &ret.Industry.ID, &ret.Industry.Label,
		&socialApps, &ret.CompanySize.ID, &ret.CompanySize.Label, &ret.Location.ID,
		&ret.Name, &founded, &ret.Description, &ret.Phone, &ret.Website, &ret.Linkedin, &ret.Twitter, &ret.Facebook,
		&ret.Instagram, &jobIDsStr)
	if err != nil && err != sql.ErrNoRows {
		return ret, err
	}
	// publicID
	if publicID.Valid {
		ret.PublicID = publicID.String
	}
	// get jobs preview
	var jobIDs []guuid.UUID
	for _, v := range jobIDsStr {
		jobuuid, _ := guuid.Parse(v) // can't fail as data come from the holy db
		jobIDs = append(jobIDs, jobuuid)
	}
	ret.AvailableJobs, _ = GetJobsPreview(jobIDs)
	// get location
	ret.Location, _ = GetLocation(ret.Location.ID)
	// get social apps
	ret.SocialApps, _ = getSocialApps(socialApps)
	// tags
	ret.Tags, _ = getCompanyTags(ret.Webuser.ID)
	// set strings
	if len(founded) > 0 {
		ret.Founded, err = strconv.Atoi(founded)
		if err != nil {
			manageErrorLog(err, "scan", "scanCompany_0", "Company", utils.ToString(ret.PublicID), "founded", founded)
		}
	}
	// if website.Valid {
	// 	ret.Website = website.String
	// }
	// if linkedin.Valid {
	// 	ret.Linkedin = linkedin.String
	// }
	// if twitter.Valid {
	// 	ret.Twitter = twitter.String
	// }
	// if facebook.Valid {
	// 	ret.Facebook = facebook.String
	// }
	// if instagram.Valid {
	// 	ret.Instagram = instagram.String
	// }
	return ret, err
}

func scanCompanyPreviewRows(rows *sql.Rows) ([]dbmodels.CompanyPreview, error) {
	var ret []dbmodels.CompanyPreview
	var tmp dbmodels.CompanyPreview
	var locationID int64
	var tagIDs pq.Int64Array
	var publicID sql.NullString
	var err error
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&tmp.Webuser.ID, &publicID, &tmp.Name, &locationID, &tmp.Webuser.SubscriptionDate, &tmp.Webuser.Avatar, &tmp.AvailableJobCount, &tagIDs)
		if err != nil {
			return ret, err
		}
		if publicID.Valid {
			tmp.PublicID = publicID.String
		}
		tmp.Location, _ = GetLocation(locationID)
		tmp.Tags, _ = GetTags(tagIDs)
		ret = append(ret, tmp)
	}
	return ret, err
}

func scanCompanyPreviewRow(row *sql.Row) (dbmodels.CompanyPreview, error) {
	var ret dbmodels.CompanyPreview
	var locationID int64
	var publicID sql.NullString
	err := row.Scan(&ret.Webuser.ID, &publicID, &ret.Name, &locationID, &ret.AvailableJobCount, &ret.Webuser.Avatar)
	if err != nil {
		return ret, err
	}
	if publicID.Valid {
		ret.PublicID = publicID.String
	}
	ret.Location, _ = GetLocation(locationID)
	return ret, err
}

func scanCompanyMinimal(row *sql.Row) (dbmodels.CompanyMinimal, error) {
	var ret dbmodels.CompanyMinimal
	var publicID sql.NullString
	var name sql.NullString
	err := row.Scan(&ret.DbID, &ret.ID, &publicID, &name)
	if publicID.Valid {
		ret.PublicID = publicID.String
	}
	if name.Valid {
		ret.Name = name.String
	}
	return ret, err
}
