package db

import (
	"database/sql"
	"lt/app/db/dbmodels"

	guuid "github.com/google/uuid"
)

func scanUserDataRow(row *sql.Row) (dbmodels.Webuser, error) {
	var ret dbmodels.Webuser
	err := row.Scan(&ret.ID, &ret.Email, &ret.Avatar, &ret.EmailValidated, &ret.DisabledAccount, &ret.SubscriptionDate, &ret.LastConnectionDate)
	return ret, err
}

func scanRowForInteger(row *sql.Row) (int, error) {
	ret := 0
	err := row.Scan(&ret)
	if err != nil {
		return 0, err
	}
	return ret, err
}

func scanRowForInteger64(row *sql.Row) (int64, error) {
	var ret int64
	err := row.Scan(&ret)
	if err != nil {
		return 0, err
	}
	return ret, err
}

func scanRowsForInteger(row *sql.Rows) (int, error) {
	var err error
	ret := 0
	defer row.Close()
	if row.Next() {
		err = row.Scan(&ret)
		if err != nil {
			return ret, err
		}
	}
	return ret, err
}

func scanRowsForInteger64(row *sql.Rows) (int64, error) {
	var err error
	var ret int64
	ret = 0
	defer row.Close()
	if row.Next() {
		err = row.Scan(&ret)
		if err != nil {
			return ret, err
		}
	}
	return ret, err
}

func scanRowsForInteger64Array(rows *sql.Rows) ([]int64, error) {
	var err error
	var ret []int64
	defer rows.Close()
	for rows.Next() {
		var tmp int64
		err = rows.Scan(&tmp)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, err
}

func scanRowForString(row *sql.Row) (string, error) {
	var err error
	ret := ""
	err = row.Scan(&ret)
	if err != nil {
		return ret, err
	}
	return ret, err
}

func scanUUIDRow(row *sql.Row) (guuid.UUID, error) {
	var ret guuid.UUID
	err := row.Scan(&ret)
	if err != nil {
		return ret, err
	}
	return ret, err
}

// duplicate this to I can use Query() insteady of QueryRow
func scanUniqueUUIDRows(rows *sql.Rows) (guuid.UUID, error) {
	var err error
	var ret guuid.UUID
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&ret)
		return ret, err
	}
	return ret, err
}

func scanUUIDRows(rows *sql.Rows) ([]guuid.UUID, error) {
	var err error
	var ret []guuid.UUID
	defer rows.Close()
	for rows.Next() {
		var tmp guuid.UUID
		err = rows.Scan(&tmp)
		if err != nil {
			return ret, err
		}
		ret = append(ret, tmp)
	}
	return ret, err
}

func scanCompanyIndustryRow(rows *sql.Rows) ([]dbmodels.Industry, error) {
	var err error
	var ret []dbmodels.Industry
	var tmp dbmodels.Industry
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return nil, err
		}
		ret = append(ret, tmp)
	}
	return ret, err
}

func scanUserTagsRows(rows *sql.Rows) ([]dbmodels.Tag, error) {
	var err error
	var ret []dbmodels.Tag
	var tmp dbmodels.Tag
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return nil, err
		}
		// capitalise label
		// tmp.Label = strings.Title(tmp.Label)
		ret = append(ret, tmp)
	}
	return ret, err
}

func scanIDLabel(row *sql.Row) (int64, string, error) {
	var id int64
	var label string
	err := row.Scan(&id, &label)
	return id, label, err
}

// func scanRowsForString(row *sql.Rows) (string, error) {
// 	var err error
// 	ret := ""
// 	defer row.Close()
// 	if row.Next() {
// 		err = row.Scan(&ret)
// 		if err != nil {
// 			return ret, err
// 		}
// 	}
// 	return ret, err
// }

// func scanApplicationsCandidateRows(rows *sql.Rows) ([]dbmodels.ApplicationsCandidateView, error) {
// 	var err error
// 	var ret []dbmodels.ApplicationsCandidateView
// 	defer rows.Close()
// 	for rows.Next() {
// 		var tmp dbmodels.ApplicationsCandidateView
// 		err := rows.Scan(&tmp.JobID, &tmp.JobPublicKey, &tmp.JobPosition, &tmp.JobDescription, &tmp.JobValidatedDate, &tmp.JobExpireDate, &tmp.JobFeatured,
// 			&tmp.AppliStatusLabel, &tmp.AppliStatusUpdateDate, &tmp.AppliMessage,
// 			&tmp.CompanyID, &tmp.CompanyName, &tmp.CompanyAvatar)
// 		if err != nil {
// 			return nil, err
// 		}
// 		ret = append(ret, tmp)
// 	}
// 	return ret, err
// }

/**
 ** MESSAGES SCANS
 **/
// func scanMessagesRows(rows *sql.Rows) ([]dbmodels.ChatMessage, error) {
// 	var err error
// 	var ret []dbmodels.ChatMessage
// 	defer rows.Close()
// 	for rows.Next() {
// 		var tmp dbmodels.ChatMessage
// 		err := rows.Scan(&tmp.ID, &tmp.ApplicantID, &tmp.CompanyID, &tmp.RelatedJobID, &tmp.Content, &tmp.Viewed, &tmp.Date)
// 		if err != nil {
// 			return nil, err
// 		}
// 		ret = append(ret, tmp)
// 	}
// 	return ret, err
// }

// // LOCATIONS
// var availableLocations []int64
// if !nomade {
// 	searchLocation.RadiusID = searchRadiusID
// 	if locationTown.Valid {
// 		// town chooser
// 		availableLocations = searchtownradius
// 		searchLocation.ID = convertStringToInt64(locationTown.String) // can't fail assuming db's data are perfect
// 		searchLocation.PostalCode = locationTownCp.String
// 		searchLocation.Label = locationTownLabel.String
// 		searchLocation.IsDepartement = false
// 	} else if locationDept.Valid {
// 		// dept chooser
// 		availableLocations = searchtowndept
// 		searchLocation.ID = convertStringToInt64(locationDept.String) // can't fail assuming db's data are perfect
// 		searchLocation.PostalCode = locationDeptCp.String
// 		searchLocation.Label = locationDeptLabel.String
// 		searchLocation.IsDepartement = true
// 	} // else user did not update profile, no location data entered
// 	ret.SearchLocation, _ = GetLocations(availableLocations)
// }
// // else no location to process as nomade == true

// func scanUserDataRows(row *sql.Rows) (dbmodels.Webuser, error) {
// 	var ret dbmodels.Webuser
// 	defer row.Close()
// 	if row.Next() {
// 		err := row.Scan(&ret.ID, &ret.Email, &ret.Avatar, &ret.UserType, &ret.Private)
// 		if err != nil {
// 			return ret, err
// 		}
// 	}
// 	return ret, nil
// }
