package db

import (
	"database/sql"
	"lt/app/db/dbmodels"

	"github.com/lib/pq"
)

// getSocialApps retrieve multiple social apps
func getSocialApps(inputID []int64) ([]dbmodels.SocialApp, int) {
	var ret []dbmodels.SocialApp
	// prepare
	stmt, err := db.Prepare(getSocialAppsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(pq.Array(inputID))
	if err != nil {
		manageErrorLog(err, "query", "getSocialApps_0", "inputID", int64ArrayToString(inputID))
		return nil, UncoverableDatabaseError
	}
	defer rows.Close()
	// scan
	ret, err = scanSocialApps(rows)
	if err != nil {
		manageErrorLog(err, "scan", "getSocialApps_1", "inputID", int64ArrayToString(inputID))
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

/** SCANS */

func scanSocialApps(rows *sql.Rows) ([]dbmodels.SocialApp, error) {
	var ret []dbmodels.SocialApp
	defer rows.Close()
	for rows.Next() {
		var tmp dbmodels.SocialApp
		err := rows.Scan(&tmp.ID, &tmp.Label)
		if err != nil {
			return nil, err
		}
		ret = append(ret, tmp)
	}
	return ret, nil
}
