package db

import (
	"database/sql"
	"fmt"
	"lt/app/utils"

	//"github.com/lib/pq"
	_ "github.com/lib/pq"
	"github.com/revel/revel"
	"github.com/ventu-io/go-shortid"
)

// Postgres Database handler
var db *sql.DB
var sid *shortid.Shortid

// Caching bool to enable or disable caching for debug purpose
var Caching = true

// InitDB open a db instance
func InitDB() {
	var err error

    url := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable", dbUser, dbPassword, dbHost, dbPort, dbName)
	//dbinfo := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable", dbHost, dbUser, dbName)
	db, err = sql.Open("postgres", url)
	if err != nil {
		utils.ManageLog(utils.Panic, utils.Database, "InitDB", "sql.Open", err, "url", url)
		panic(err)
	}
	// sid generator
	sid, err = shortid.New(1, shortid.DefaultABC, 71421)
	if err != nil {
		panic(err)
	}
	// caching on/off
	Caching = revel.Config.BoolDefault("cache.enable", true)
}
