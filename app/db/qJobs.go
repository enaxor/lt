package db

import (

	//"github.com/lib/pq"

	"database/sql"
	"errors"
	"fmt"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strconv"
	"strings"

	guuid "github.com/google/uuid"
	"github.com/lib/pq"
)

/********** JOBS CRUD MANAGEMENT */

// UpdateJob update a job and it's status (jobStatus -> 1 (pending)
func UpdateJob(jobUUID guuid.UUID,
	position, description, URLMailApply, expireDate string,
	tags []dbmodels.Tag,
	contrat, experience, worktime, salary, regionals int,
	fullRemote bool, jobStatus int) (guuid.UUID, string, int) {
	tx, _ := db.Begin()
	// recreate public ID
	publicID := GenerateIDForNewJob(position, jobUUID)
	// delete then insert new experiences
	stmt, err := db.Prepare(updateJobSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// add job
	_, err = tx.Stmt(stmt).Exec(publicID, position, description, URLMailApply, contrat, experience, worktime, salary, regionals, fullRemote, expireDate, jobUUID, jobStatus)
	if err != nil {
		manageErrorLog(err, "query", "UpdateJob_0", "jobUUID", jobUUID.String(), "publicID", publicID, "position", position, "description", "description len", strconv.Itoa(len(description)), "URLMailApply", URLMailApply,
			"contrat", utils.ToString(contrat), "experience", utils.ToString(experience), "worktime", utils.ToString(worktime), "salary", utils.ToString(salary), "regionals", utils.ToString(regionals),
			"fullRemote", utils.ToString(fullRemote), "expireDate", expireDate, "tags", utils.ToString(tags), "jobStatus", utils.ToString(jobStatus))
		tx.Rollback()
		return jobUUID, publicID, UncoverableDatabaseError
	}
	// commit
	if err := tx.Commit(); err != nil {
		manageErrorLog(err, "commit", "UpdateJob_2", "jobUUID", jobUUID.String(), "publicID", publicID, "position", position, "description", "description len", strconv.Itoa(len(description)), "URLMailApply", URLMailApply,
			"contrat", utils.ToString(contrat), "experience", utils.ToString(experience), "worktime", utils.ToString(worktime), "salary", utils.ToString(salary), "regionals", utils.ToString(regionals),
			"fullRemote", utils.ToString(fullRemote), "expireDate", expireDate, "tags", utils.ToString(tags), "jobStatus", utils.ToString(jobStatus))
		tx.Rollback()
		return jobUUID, publicID, UncoverableDatabaseError
	}
	// tags
	if err := insertJobTag(jobUUID, tags); err != CompletedWithNoError {
		manageErrorLog(nil, "query", "UpdateJob_1 >> addTagsToJob", "jobUUID", jobUUID.String(), "tags", utils.ToString(tags))
		return jobUUID, publicID, UncoverableDatabaseError
	}
	// all done
	return jobUUID, publicID, CompletedWithNoError
}

// AddJob Create a new job in db
func AddJob(employerID, jobUUID guuid.UUID,
	position, description, URLMailApply, expireDate string,
	tags []dbmodels.Tag,
	contrat, experience, worktime, salary, regionals int,
	fullRemote bool) (guuid.UUID, string, int) {
	var retUUID guuid.UUID
	var retPublicID string
	tx, _ := db.Begin()
	// create public ID
	publicID := GenerateIDForNewJob(position, jobUUID)
	// prepare query
	stmt, err := db.Prepare(insertJobSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// run query
	row := tx.Stmt(stmt).QueryRow(jobUUID, publicID, employerID,
		position, description, URLMailApply,
		contrat, experience, worktime, salary, regionals,
		fullRemote, expireDate, JobStatusPending)
	// get new job id
	err = row.Scan(&retUUID, &retPublicID)
	if err != nil {
		manageErrorLog(err, "query", "AddJob_0", "jobID", jobUUID.String(), "publicID", publicID, "employerID", employerID.String(),
			"position", position, "description", "description len", strconv.Itoa(len(description)), "URLMailApply", URLMailApply,
			"contrat", utils.ToString(contrat), "experience", utils.ToString(experience), "worktime", utils.ToString(worktime), "salary", utils.ToString(salary), "regionals", utils.ToString(regionals),
			"fullRemote", utils.ToString(fullRemote), "expireDate", expireDate, "tags", utils.ToString(tags))
		tx.Rollback() // abord
		return retUUID, retPublicID, UncoverableDatabaseError
	}
	// commit
	if err := tx.Commit(); err != nil {
		manageErrorLog(err, "commit", "AddJob_2", "jobID", jobUUID.String(), "publicID", publicID, "employerID", employerID.String(),
			"position", position, "description", "description len", strconv.Itoa(len(description)), "URLMailApply", URLMailApply,
			"contrat", utils.ToString(contrat), "experience", utils.ToString(experience), "worktime", utils.ToString(worktime), "salary", utils.ToString(salary), "regionals", utils.ToString(regionals),
			"fullRemote", utils.ToString(fullRemote), "expireDate", expireDate, "tags", utils.ToString(tags))
		tx.Rollback() // abord
		return retUUID, retPublicID, UncoverableDatabaseError
	}

	// Tags link after commit so job uuid exist to keep db consistency
	// limit tags to 20
	if len(tags) > JobMaximumTag {
		tags = tags[:JobMaximumTag]
	}
	if err := insertJobTag(jobUUID, tags); err != CompletedWithNoError {
		manageErrorLog(nil, "query", "AddJob_3 >> addTagsToJob", "jobID", jobUUID.String(), "tags", utils.ToString(tags))
		return jobUUID, publicID, UncoverableDatabaseError
	}
	// Ok
	return retUUID, retPublicID, CompletedWithNoError
}

// ArchiveJob stub
// archiveStatus: false is online, true if offline
func ArchiveJob(jobUUID guuid.UUID, archiveStatus bool) int {
	sqlToRun := unarchiveJobSQL
	if archiveStatus {
		sqlToRun = archiveJobSQL
	}
	// prepare
	stmt, err := db.Prepare(sqlToRun)
	if err != nil {
		manageErrorLog(err, "prepare", "ArchiveJob_0", "jobUUID", jobUUID.String())
		return UncoverableDatabaseError
	}
	// query
	res, err := stmt.Exec(jobUUID)
	if err != nil {
		manageErrorLog(err, "query", "ArchiveJob_1", "jobUUID", jobUUID.String())
		return UncoverableDatabaseError
	}
	nbRowArchived, err := res.RowsAffected()
	if err != nil {
		manageErrorLog(err, "query", "ArchiveJob_2", "jobUUID", jobUUID.String())
		return UncoverableDatabaseError
	}
	if nbRowArchived != 1 {
		manageErrorLog(err, "query", "ArchiveJob_3", "jobUUID", jobUUID.String(), "nbRowArchived", utils.ToString(nbRowArchived))
		return NoItemFound
	}
	return CompletedWithNoError
}

// AddAggregateJobs create multiples jobs from an aggregator
// local aggregates function only
// this function must not appear in controller api
// in case of error, return array of inserted jobs
func AddAggregateJobs(jobs []dbmodels.AggregateJob) ([]guuid.UUID, int) {
	var ret []guuid.UUID
	// prepare
	stmtadd, err := db.Prepare(insertAggregateJobSQL)
	if err != nil {
		panic(err)
	}
	defer stmtadd.Close()
	for _, job := range jobs {
		// validate
		if job.Validate() {
			// create public ID
			inputJobID, jobPublicID := generateIDForNewAggregateJob(job.Source, job.Position)
			// insert in db
			row, err := stmtadd.Query(inputJobID, jobPublicID, job.Source, job.CompanyName, job.Contrat, job.Experience, job.Worktime, job.FullRemote, job.LocationID, job.Position,
				job.Description, job.URL, job.PublicationDate, job.ExpirationDate)
			if err != nil {
				if pqerr, ok := err.(*pq.Error); ok {
					errorCode := string(pqerr.Code)
					nonFatalErrorCodeList := []string{"23505"} // error code for 'already exist'
					if contains(errorCode, nonFatalErrorCodeList) {
						// all good i guess... job already exists
						continue
					}
				}
				manageErrorLog(err, "query", "AddAggregateJobs_0", "inputJobID", inputJobID.String(), "jobPublicID", jobPublicID, "job", job.PublicID) // utils.ToString(job))
				continue
			}
			// get new job id
			inputJobIDInserted, err := scanUniqueUUIDRows(row)
			if err != nil {
				if pqerr, ok := err.(*pq.Error); ok {
					errorCode := string(pqerr.Code)
					nonFatalErrorCodeList := []string{"23505"} // error code for 'already exist'
					if contains(errorCode, nonFatalErrorCodeList) {
						continue
					}
				}
				manageErrorLog(err, "scan", "AddAggregateJobs_2", "inputJobID", inputJobID.String(), "jobPublicID", jobPublicID, "job", utils.ToString(job))
				return ret, UncoverableDatabaseError
			}
			// TAGS
			// limit tags to 20
			if len(job.Tags) > JobMaximumTag {
				job.Tags = job.Tags[:JobMaximumTag]
			}
			for _, tag := range job.Tags {
				tagID, err := CreateTagIfNotExist(tag, true)
				if err != CompletedWithNoError {
					continue
				}
				// create tag link
				addAggregateJobTagLink(inputJobIDInserted, tagID)
			}
			ret = append(ret, inputJobID)
			fmt.Printf("Created new aggregate job publicID: %s \n", inputJobIDInserted)
		}
	}
	// all done
	return ret, CompletedWithNoError
}

// PeekJobByIDCompanyStatus Check if a job with this company + status exist
func PeekJobByIDCompanyStatus(jobPublicID string, companyID guuid.UUID, status int) (*guuid.UUID, int) {
	// prepare
	stmt, err := db.Prepare(getJobStatusSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(jobPublicID, companyID, status)
	ret, err := scanUUIDRow(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, CompletedWithNoError
		}
		manageErrorLog(err, "scan", "PeekJobIDCompanyStatus", "jobPublicID", jobPublicID, "companyID", companyID.String(), "status", utils.ToString(status))
		return nil, UncoverableDatabaseError
	}
	// ok
	return &ret, CompletedWithNoError
}

// GetJob Read a single job
func GetJob(jobPublicID string, onlineOnly bool) (dbmodels.Job, int) {
	var ret dbmodels.Job
	sqlreq := getJobSQL
	if onlineOnly {
		sqlreq = getOnlineJobSQL
	}
	// prepare
	stmt, err := db.Prepare(sqlreq)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(jobPublicID)
	// scan
	ret, err = scanJobRow(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetJob_0", "jobPublicID", jobPublicID)
		return ret, UncoverableDatabaseError
	}
	// now get company data
	errint := 0
	if ret.Company, errint = GetCompany(ret.Company.Webuser.ID); errint != 0 {
		manageErrorLog(err, "scan", "GetJob_1", "id", jobPublicID, "ret", utils.ToString(ret))
		return ret, UncoverableDatabaseError
	}
	// ok
	return ret, CompletedWithNoError
}

// GetSimilarJobs Get Similar Jobs
// we dont return any error value for this falc field
func GetSimilarJobs(jobID guuid.UUID) []dbmodels.JobPreview {
	var ret []dbmodels.JobPreview
	// prepare
	stmt, err := db.Prepare(getSimilarJobsSQL)
	if err != nil {
		manageErrorLog(err, "prepare", "GetSimilarJobs_0", "jobID", jobID.String())
		return ret
	}
	defer stmt.Close()
	// run
	rows, err := stmt.Query(jobID, 3)
	if err != nil {
		manageErrorLog(err, "prepare", "GetSimilarJobs_1", "jobID", jobID.String())
		return ret
	}
	// scan
	uuids, err := scanUUIDRows(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret
		}
		manageErrorLog(err, "query", "GetSimilarJobs_2", "jobID", jobID.String())
		return ret
	}
	ret, _ = GetJobsPreview(uuids)
	// ok
	return ret
}

// GetAggregateJobLink get link for redirect
func GetAggregateJobLink(jobPublicID string) (string, int) {
	var ret string
	stmt, err := db.Prepare(getAggregateURLSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow(jobPublicID)
	// scan
	ret, err = scanRowForString(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetAggregateJobLin_0", "jobPublicID", jobPublicID)
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetJobURLMailApply get apply data for a job
// take a public ID as paramater
func GetJobURLMailApply(jobPublicID string) (guuid.UUID, string, int) {
	var uuid guuid.UUID
	var applydata string
	// prepare
	stmt, err := db.Prepare(getJobURLMailApplySQL)
	if err != nil {
		manageErrorLog(err, "prepare", "GetJobURLMailApply_0", "jobID", jobPublicID)
		return uuid, applydata, UncoverableDatabaseError
	}
	defer stmt.Close()
	// run
	row := stmt.QueryRow(jobPublicID)
	// scan
	err = row.Scan(&uuid, &applydata)
	if err != nil {
		if err == sql.ErrNoRows {
			return uuid, applydata, NoItemFound
		}
		manageErrorLog(err, "query", "GetJobURLMailApply_1", "jobID", jobPublicID)
		return uuid, applydata, UncoverableDatabaseError
	}
	// ok
	return uuid, applydata, CompletedWithNoError
}

// GetJobs take a request, return a list of job
func GetJobs(request JobListRequest, permissive bool) ([]dbmodels.JobPreview, int, int) {

	// generate queries
	customJobsListSQL, param, customJobsCountSQL, paramCount, generror := generateJobsQuery(request, permissive)
	if generror != CompletedWithNoError {
		manageErrorLog(nil, "generateJobsQuery", "GetJobs_0", "generror", strconv.Itoa(generror))
		return nil, 0, UncoverableDatabaseError
	}
	// debug
	// if revel.DevMode {
	// 	utils.ManageLog(utils.Debug, utils.Database, "GetJobs", "GetJobs_1.generateJobsQuery", nil, "request", utils.ToString(request), "permissive", utils.ToString(permissive),
	// 		"param", utils.ToString(param), "customJobsCountSQL", customJobsCountSQL, "paramCount", utils.ToString(paramCount), "customJobsListSQL", customJobsListSQL)
	// }

	// prepare query
	stmtJobs, err := db.Prepare(customJobsListSQL)
	if err != nil {
		manageErrorLog(err, "prepare", "GetJobs_1", "param", utils.ToString(param), "customJobsListSQL", customJobsListSQL)
		return nil, 0, UncoverableDatabaseError
	}
	defer stmtJobs.Close()
	// run query
	rows, err := stmtJobs.Query(param...)
	if err != nil {
		manageErrorLog(err, "query", "GetJobs_2", "param", utils.ToString(param), "customJobsListSQL", customJobsListSQL)
		return nil, 0, UncoverableDatabaseError
	}
	defer rows.Close()
	// read result
	ret, err := scanJobPreviewRows(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, 0, NoItemFound
		}
		manageErrorLog(err, "scan", "GetJobs_3", "param", utils.ToString(param), "customJobsListSQL", customJobsListSQL)
		return nil, 0, UncoverableDatabaseError
	}
	// prepare counting total matched result for paging...
	stmtCount, err := db.Prepare(customJobsCountSQL)
	if err != nil {
		manageErrorLog(err, "customgen", "GetJobs_4", "customJobsCountSQL", customJobsListSQL)
		return nil, 0, UncoverableDatabaseError
	}
	defer stmtCount.Close()
	// run count query
	row := stmtCount.QueryRow(paramCount...)
	// read result
	var retcount int
	err = row.Scan(&retcount)
	if err != nil {
		manageErrorLog(err, "scan", "GetJobs_5", "customJobsCountSQL", customJobsListSQL)
		return nil, 0, UncoverableDatabaseError
	}
	return ret, retcount, CompletedWithNoError
}

// GetJobsPreview return minimal data about a list of job
// @param job IDs
func GetJobsPreview(jobIDs []guuid.UUID) ([]dbmodels.JobPreview, int) {
	ret := []dbmodels.JobPreview{}
	// prepare
	stmtJobs, err := db.Prepare(getJobPreviewSQL)
	if err != nil {
		manageErrorLog(err, "prepare", "GetJobsPreview_0", "input jobIDs", utils.ToString(jobIDs))
		return ret, UncoverableDatabaseError
	}
	defer stmtJobs.Close()
	// run queries
	rows, err := stmtJobs.Query(pq.Array(jobIDs), MaxDescriptionCharPreviewJob)
	if err != nil {
		manageErrorLog(err, "query", "GetJobsPreview_1", "input jobIDs", utils.ToString(jobIDs))
		return ret, UncoverableDatabaseError
	}
	defer rows.Close()
	// read result
	ret, err = scanJobPreviewRows(rows)
	if err != nil {
		if err == sql.ErrNoRows {
			return ret, NoItemFound
		}
		manageErrorLog(err, "scan", "GetJobsPreview_2", "input jobIDs", utils.ToString(jobIDs))
		return ret, UncoverableDatabaseError
	}
	return ret, CompletedWithNoError
}

// GetJobsCompanyDashboard obtain a list of detailed jobs for company dashboard (pending + online + expired)
func GetJobsCompanyDashboard(companyID guuid.UUID) ([]dbmodels.JobStat, []dbmodels.JobStat, []dbmodels.JobStat, int) {
	var pendingJob []dbmodels.JobStat
	var onlineJob []dbmodels.JobStat
	var archivedJob []dbmodels.JobStat
	// prepare
	stmt, err := db.Prepare(getJobStatForCompanySQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(companyID)
	if err != nil {
		manageErrorLog(err, "query", "GetJobsCompanyDashboard_0", "companyID", companyID.String())
		return nil, nil, nil, UncoverableDatabaseError
	}
	// scan
	jobs, err := scanJobStatRows(rows)
	if err != nil && err != sql.ErrNoRows {
		manageErrorLog(err, "scan", "GetJobsCompanyDashboard_1", "companyID", companyID.String())
		return nil, nil, nil, UncoverableDatabaseError
	}
	// remove expired jobs
	for _, v := range jobs {
		if v.Status == 1 {
			pendingJob = append(pendingJob, v)
		} else if v.Status == 2 {
			onlineJob = append(onlineJob, v)
		} else if v.Status == 3 {
			archivedJob = append(archivedJob, v)
		} // else: on skip les job deleted
	}
	// ok
	return pendingJob, onlineJob, archivedJob, CompletedWithNoError
}

// IncrementJobRedirectionCount +1 to redirection count field of a job
func IncrementJobRedirectionCount(jobID guuid.UUID) {
	stmt, err := db.Prepare(incrementJobRedirectionCountSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	_, err = stmt.Exec(jobID)
	if err != nil {
		manageErrorLog(err, "query", "IncrementJobRedirectionCount", "jobID", jobID.String())
	}
}

// IncrementJobViewCount +1 to view count field of a job
func IncrementJobViewCount(jobID guuid.UUID) {
	stmt, err := db.Prepare(incrementJobViewCountSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	_, err = stmt.Exec(jobID)
	if err != nil {
		manageErrorLog(err, "query", "IncrementJobViewCount", "jobID", jobID.String())
	}
}

// DisableJob make a job unavailable to consult, but still usable for stats.
func DisableJob(jobID guuid.UUID) int {
	stmt, err := db.Prepare(disableJobSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(jobID)
	if err != nil {
		manageErrorLog(err, "query", "DisableJob", "jobID", jobID.String())
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// DeleteJob Deprecated delete a job
// /!\ Do not use (unit test purpose)
// Use DisableJob(id) instead
func DeleteJob(jobID guuid.UUID) int {
	stmt, err := db.Prepare(deleteJobSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(jobID)
	if err != nil {
		manageErrorLog(err, "query", "DeleteJob", "jobID", jobID.String())
		return UncoverableDatabaseError
	}
	return CompletedWithNoError
}

// generateJobsQuery generate froms craft a sql string with parameter list
// to get a job list
func generateJobsQuery(request JobListRequest, permissive bool) (string, []interface{}, string, []interface{}, int) {
	var unionJobsSB strings.Builder
	var innerJoinSB strings.Builder
	var whereSqlsb strings.Builder
	var fieldSelectionSB strings.Builder
	var countSelectionSB strings.Builder

	fieldSelectionSB.WriteString("select jobs.id,jobs.public_id,position,jobs.description,full_remote,contrat,regional,experience,worktime,featured,aggregateJob,aggregateURL,pub_date,company_id,jobs.company_name,sourceaggregation,tags from ")
	countSelectionSB.WriteString("select count(*) from ") // Remove ORDER BY/LIMIT/OFFSET when using this

	// select count(*) from
	// union of aggregate, external and internal jobs
	unionJobsSB.WriteString(" ( (SELECT ij.id,ij.public_id,0 as sourceAggregation,ij.company_id,company.location,company.company_name,ij.contrat,ij.regional,ij.experience,ij.worktime,ij.position," +
		" ij.description,ij.full_remote,false as aggregateJob,'' as aggregateURL,ij.salary,ij.post_date as pub_date,ij.featured, " +
		" (select COALESCE(array_agg(job_tag.tag_id),ARRAY[]::integer[]) from job_tag where job_tag.job_id = ij.id) as tags " +
		" FROM job ij INNER JOIN company ON company.user_id = company_id where ij.post_date IS NOT NULL AND ij.status = 2 " +
		" ) UNION ( " +
		" SELECT ej.id,ej.public_id,ej.source,'77777777-0000-7777-0000-777777777777' as company_id,location,ej.company_name,ej.contrat,0 as regional, ej.experience, " +
		" ej.worktime,ej.position,ej.description,full_remote,true as aggregateJob,ej.url as aggregateURL,0 as salary,ej.posted_date, false as featured, " +
		" (select COALESCE(array_agg(aggregate_job_tag.tag_id),ARRAY[]::integer[]) from aggregate_job_tag where aggregate_job_tag.aggregate_job_id = ej.id) as tags " +
		" FROM aggregate_job ej ) ) jobs ") // where ej.expire_date >= now()

	innerJoinSB.WriteString(" INNER JOIN company ON company.user_id = company_id  ")

	// parameter management
	var sqlParam []interface{}
	sqlParamIdx := 1

	// where
	for _, filter := range request.Filters {

		// get the first filter to write in whereSqlsb: where
		// get another filter to write in whereSqlsb: and / or
		if sqlParamIdx == 1 {
			whereSqlsb.WriteString(" where ")
		} else {
			// by pass for tags
			if permissive {
				whereSqlsb.WriteString(" or ")
			} else {
				whereSqlsb.WriteString(" and ")
			}
		}

		switch filter.FilterType {
		case TagsSearchF:
			// // convertion int->string de l'index
			// sqlParamIdxStr := strconv.Itoa(sqlParamIdx)
			// whereSqlsb.WriteString(" ( jobs.position ILIKE $" + sqlParamIdxStr + " OR jobs.description ILIKE $" + sqlParamIdxStr + " ) ")
			// //	whereSqlsb.WriteString(" job.position ~* '.*( $" + sqlParamIdxStr + " ).*' OR job.description  ~* '.*($" + sqlParamIdxStr + ").*'") // a test with regex
			// sqlParam = append(sqlParam, "%"+filter.FString+"%")
			// // keep an index management
			// sqlParamIdx++

			whereSqlsb.WriteString(" tags @> ($" + strconv.Itoa(sqlParamIdx) + ") ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++

		case LocationTownF:
			innerJoinSB.WriteString(" INNER JOIN location_town ON jobs.location = location_town.id  ")
			whereSqlsb.WriteString(" location_town.id = any ( $" + strconv.Itoa(sqlParamIdx) + " ) ") // todo innerjoin departement (what ids do we get, radius ?)
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case LocationDeptF:
			innerJoinSB.WriteString(" INNER JOIN location_town ON jobs.location = location_town.id  ")
			whereSqlsb.WriteString(" location_town.code_postal ~ '^(  || $")
			whereSqlsb.WriteString(strconv.Itoa(sqlParamIdx))
			whereSqlsb.WriteString(" || )' ")
			sqlParam = append(sqlParam, filter.FString) // 01|02 list of dept separated by pipe
			sqlParamIdx++
		case ExperienceF:
			whereSqlsb.WriteString(" experience = any  ($" + strconv.Itoa(sqlParamIdx) + ") ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case ContratF:
			whereSqlsb.WriteString(" contrat = any  ($" + strconv.Itoa(sqlParamIdx) + ") ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case WorktimeF:
			innerJoinSB.WriteString(" INNER JOIN job_time ON jobs.worktime = job_time.id ")
			whereSqlsb.WriteString(" worktime = any ( $" + strconv.Itoa(sqlParamIdx) + " ) ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case SalaryF:
			whereSqlsb.WriteString(" jobs.salary >= $" + strconv.Itoa(sqlParamIdx))
			sqlParam = append(sqlParam, filter.FInt)
			sqlParamIdx++
		case TagsF:
			whereSqlsb.WriteString(" tags @> ($" + strconv.Itoa(sqlParamIdx) + ") ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case SocialAppsF:
			innerJoinSB.WriteString(" INNER JOIN company_social_app ON company_id = company_social_app.user_id ")
			whereSqlsb.WriteString(" company_social_app.social_app_id = any  ($" + strconv.Itoa(sqlParamIdx) + ") ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case LangageF:
			innerJoinSB.WriteString(" INNER JOIN job_langage ON job_langage.job_id = jobs.id ")
			whereSqlsb.WriteString(" job_langage.langage_id = any  ($" + strconv.Itoa(sqlParamIdx) + ") ")
			sqlParam = append(sqlParam, pq.Array(filter.FIntArray))
			sqlParamIdx++
		case FeaturedF:
			whereSqlsb.WriteString(" featured = $" + strconv.Itoa(sqlParamIdx))
			sqlParam = append(sqlParam, filter.FBoolean)
			sqlParamIdx++
		case SuggestedJobOnlyF:
			innerJoinSB.WriteString(" INNER JOIN applicant_suggested_job ON applicant_suggested_job.job_id = jobs.id ")
			whereSqlsb.WriteString(" applicant_suggested_job.applicant_id = $" + strconv.Itoa(sqlParamIdx) + " ")
			sqlParam = append(sqlParam, filter.FString)
			sqlParamIdx++
		case SavedJobOnlyF:
			innerJoinSB.WriteString(" INNER JOIN applicant_saved_job ON applicant_saved_job.job_id = jobs.id ") // internal && aggregate jobs can't share same ids
			whereSqlsb.WriteString(" applicant_saved_job.applicant_id = $" + strconv.Itoa(sqlParamIdx) + " ")
			sqlParam = append(sqlParam, filter.FString)
			sqlParamIdx++
		case RemoteF:
			whereSqlsb.WriteString(" full_remote = $" + strconv.Itoa(sqlParamIdx) + " ")
			sqlParam = append(sqlParam, filter.FInt)
			sqlParamIdx++
		case CompanyF:
			whereSqlsb.WriteString(" company.public_id = $" + strconv.Itoa(sqlParamIdx) + " ")
			sqlParam = append(sqlParam, filter.FString)
			sqlParamIdx++
		default:
			utils.ManageLog(utils.Panic, utils.Database, "generateJobsQuery", "FilterType", errors.New("unknown filter type"), "filter.FilterType", utils.ToString(filter.FilterType))
			panic("generateJobsQuery FilterType inconnu")
		}

	}

	// backup param data here to avoid limiting for sql counting all matched result
	sqlParamCount := sqlParam

	var orderLimitSqlsb strings.Builder
	// order by featured
	orderLimitSqlsb.WriteString(" ORDER BY featured DESC, pub_date DESC ")
	// limit
	orderLimitSqlsb.WriteString(" offset $" + strconv.Itoa(sqlParamIdx))
	sqlParam = append(sqlParam, request.IndexStart)
	sqlParamIdx++
	orderLimitSqlsb.WriteString(" limit $" + strconv.Itoa(sqlParamIdx))
	sqlParam = append(sqlParam, request.IndexEnd)
	sqlParamIdx++

	// concat full query
	var allSqlsb strings.Builder
	allSqlsb.WriteString(fieldSelectionSB.String())
	allSqlsb.WriteString(unionJobsSB.String())
	allSqlsb.WriteString(innerJoinSB.String())
	allSqlsb.WriteString(whereSqlsb.String())
	allSqlsb.WriteString(orderLimitSqlsb.String())
	// concat same sql to count total values
	var selectCountAllSqlsb strings.Builder
	selectCountAllSqlsb.WriteString(countSelectionSB.String())
	selectCountAllSqlsb.WriteString(unionJobsSB.String())
	selectCountAllSqlsb.WriteString(innerJoinSB.String())
	selectCountAllSqlsb.WriteString(whereSqlsb.String())

	return allSqlsb.String(), sqlParam, selectCountAllSqlsb.String(), sqlParamCount, CompletedWithNoError
}

/*** SCANS **/

func scanJobRow(row *sql.Row) (dbmodels.Job, error) {
	var job dbmodels.Job
	job = dbmodels.Job{}
	var tagIDs pq.Int64Array
	err := row.Scan(&job.ID, &job.PublicID, &job.Company.Webuser.ID, &job.Position, &job.Description, &job.URLMailApply, &job.Since, &job.Until,
		&job.Salary, &job.Contrat.ID, &job.WorkTime.ID, &job.Experience.ID, &job.Regional.ID,
		&job.FullRemote, &job.Featured, &job.ViewCount, &tagIDs)
	if err != nil {
		return job, err
	}
	// get company
	job.Company, _ = GetCompany(job.Company.Webuser.ID)
	// get tags
	job.Tags, _ = GetTags(tagIDs)
	// get contrat
	job.Contrat, _ = GetContrat(job.Contrat.ID)
	// get regional
	job.Regional, _ = GetRegional(job.Regional.ID)
	// get worktime
	job.WorkTime, _ = GetJobTime(job.WorkTime.ID)
	// redeable time
	job.SinceStr = utils.HumanizeTime(job.Since)
	// get experience
	job.Experience, _ = getJobExperience(job.Experience.ID)
	return job, err
}

func scanJobStatRows(rows *sql.Rows) ([]dbmodels.JobStat, error) {
	jobs := []dbmodels.JobStat{}
	var err error
	var job dbmodels.JobStat
	for rows.Next() {
		err := rows.Scan(&job.ID, &job.PublicID, &job.Position, &job.Contrat.ID, &job.Regional.ID, &job.FullRemote, &job.Since, &job.Until, &job.Featured, &job.ViewCount,
			&job.RedirectCount, &job.Status)
		if err != nil {
			return nil, err
		}
		// get contrat
		job.Contrat, _ = GetContrat(job.Contrat.ID)
		// get regional
		job.Regional, _ = GetRegional(job.Regional.ID)
		// ok
		jobs = append(jobs, job)
	}
	return jobs, err
}

// scan jobs preview
func scanJobPreviewRows(rows *sql.Rows) ([]dbmodels.JobPreview, error) {
	jobs := []dbmodels.JobPreview{}
	var err error
	for rows.Next() {
		var job dbmodels.JobPreview
		var campagnyID guuid.UUID
		var tagIDs pq.Int64Array
		var contratID int64
		var xpID int64
		var regionalID int64
		var workimeID int64
		err := rows.Scan(&job.ID, &job.PublicID, &job.Position, &job.Description, &job.FullRemote, &contratID, &regionalID, &xpID, &workimeID, &job.Featured,
			&job.IsAggregate, &job.AggregateURL, &job.Since, &campagnyID, &job.Company.Name, &job.AggregateSource, &tagIDs)
		if err != nil {
			return nil, err
		}
		// get tags
		job.Tags, _ = GetTags(tagIDs)
		// get contrat
		job.Contrat, _ = GetContrat(contratID)
		// get regional
		job.Regional, _ = GetRegional(regionalID)
		// get time
		job.WorkTime, _ = GetJobTime(workimeID)
		// get xp
		job.Xp, _ = getJobExperience(xpID)
		// format since
		job.SinceStr = utils.HumanizeTime(job.Since)
		// company
		if job.IsAggregate {
			switch job.AggregateSource {
			case 1: // EmploiStore
				job.Company.Webuser.Avatar = "poleemploi.png"
			case 2: // StackOverflow
				job.Company.Webuser.Avatar = "stackoverflow.png"
			case 3: // RemoteOk
				job.Company.Webuser.Avatar = "remoteok.png"
			case 4: // Indeed
				job.Company.Webuser.Avatar = "indeed.png"
			case 5: // LinuxJobs
				job.Company.Webuser.Avatar = "linuxjobs.png"
			case 6: // RemotiveJobs
				job.Company.Webuser.Avatar = "default.png"
			default:
				utils.ManageLog(utils.Error, utils.Database, "scanJobPreviewRows", "job.AggregateSource", errors.New("encountered an unknown Aggregation code"), "job.AggregateSource", strconv.Itoa(job.AggregateSource), "job.ID", job.ID.String())
				job.Company.Webuser.Avatar = "default.png"
			}
		} else {
			job.Company, _ = GetCompanyPreview(campagnyID)
		}
		jobs = append(jobs, job)
	}
	return jobs, err
}
