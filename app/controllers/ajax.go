package controllers

import (
	"errors"
	"lt/app/controllers/uimodels"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"net/url"
	"strconv"
	"time"

	"github.com/revel/revel"
)

type xajaLocationResult struct {
	Items []dbmodels.Location
}

type xajaTagResult struct {
	Items []dbmodels.Tag
}

type xajaIndustryResult struct {
	Items []dbmodels.Industry
}

func (c App) LazyLoadingJobs() revel.Result {

	// ystr := c.Params.Query.Get("index")
	// y := utils.StringToIntOrZero(ystr)
	// update jobs infinite page index
	// c.Session["jidx"]
	// user session track
	userSession := c.getSessionUserOrCreate()
	//var queryFilter uimodels.JobsInputSearchFilters

	queryFilter := &uimodels.JobsInputSearchFilters{}
	_, err := c.Session.GetInto("queryFilter", queryFilter, false)
	if err != nil {
		utils.ManageLog(utils.Crit, utils.Controller, "LazyLoadingJobs", "1", err)
	}

	// skip initial job loading to load the nexts
	if queryFilter.P == 0 {
		queryFilter.P = 1
	}
	if queryFilter.JobIndexStart == 0 {
		queryFilter.JobIndexStart = maxJobShowedInJobList
	}

	indexStart := queryFilter.P * queryFilter.JobIndexStart
	indexEnd := queryFilter.P*queryFilter.JobIndexStart + maxJobShowedInJobList

	// if c.ViewArgs["queryFilter"] != nil {
	// 	queryFilter = c.Session["queryFilter"]  c.ViewArgs["queryFilter"].(uimodels.JobsInputSearchFilters)
	var pageData uimodels.JobsPageData
	var errint int
	pageData.Jobs, pageData.TotalItem, errint = getJobsList(*queryFilter, &userSession.ID, indexStart, indexEnd)
	if errint != 0 {
		utils.ManageLog(utils.Crit, utils.Controller, "LazyLoadingJobs", "2", errors.New("can't retrieve Jobs list"), "errint", strconv.Itoa(errint))
	}
	c.ViewArgs["Jobs"] = pageData.Jobs

	utils.ManageLog(utils.Debug, utils.Controller, "LazyLoadingJobs", "3", nil, "queryFilter", utils.ToString(queryFilter))

	// update queryfilter
	queryFilter.P = queryFilter.P + 1
	c.Session["queryFilter"] = queryFilter

	return c.RenderTemplate("templates/jobPreviewItemRange.html")
}

// todo test this
// XajaCreateNewletterSub new sub
func (c App) XajaCreateNewletterSub(mail string, params string, periodicity int, terms bool) revel.Result {
	// metrics
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "XajaCreateNewletterSub", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// check session
	userSession := c.getSessionUserOrFail()
	if userSession == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "XajaCreateNewletterSub", "1", errors.New("an user without session tried to use this ajax function"), "params", params, "periodicity", strconv.Itoa(periodicity))
		return c.Forbidden("Forbidden")
	}
	// add sub
	var newSubscription uimodels.NewsletterSubscription
	newSubscription.Email = mail
	newSubscription.Periodicity = periodicity
	newSubscription.Terms = terms
	// parse params
	m, err := url.ParseQuery(params)
	if err == nil {
		params := parseJobsParameter(m, userSession.Connected)
		newSubscription.Params = params.ToString()
	}
	// validate
	newSubscription.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		utils.ManageLog(utils.Warn, utils.Controller, "XajaCreateNewletterSub", "2", errors.New("error at validation"), "newslettersubscription", utils.ToString(newSubscription), "errors", utils.ToString(c.Validation.Errors))
		return c.RenderJSON(false)
	}
	// Ok, insert new sub
	resultok, errorval := sendConfirmationNewletterSubcription(newSubscription)
	if errorval != nil || !resultok {
		c.Flash.Error(errorval.Message)
		c.Validation.Keep()
		c.FlashParams()
		utils.ManageLog(utils.Error, utils.Controller, "XajaCreateNewletterSub", "3", errors.New("error while inserting new email subscription"), "newslettersubscription", utils.ToString(newSubscription), "errors", utils.ToString(c.Validation.Errors))
		return c.RenderJSON(false)
	}
	return c.RenderJSON(true)
}

// XajaGetLocationList get an async location list
func (c App) XajaGetLocationList(keyword string) revel.Result {
	// metrics
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "XajaGetLocationList", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	if c.getSessionUserOrFail() == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "XajaGetLocationList", "1", errors.New("an user without session tried to use this ajax function"), "keyword", keyword)
		return c.Forbidden("Forbidden")
	}
	matchingLocations := searchLocations(keyword)
	// ok
	var ret xajaLocationResult
	ret.Items = matchingLocations
	return c.RenderJSON(ret)
}

// XajaGetIndustryList get an async industry list
func (c App) XajaGetIndustryList(keyword string) revel.Result {
	// metrics
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "XajaGetIndustryList", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	if c.getSessionUserOrFail() == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "XajaGetIndustryList", "1", errors.New("an user without session tried to use this ajax function"), "keyword", keyword)
		return c.Forbidden("Forbidden")
	}
	matchingIndustries := searchIndustry(keyword)
	// ok
	var ret xajaIndustryResult
	ret.Items = matchingIndustries
	return c.RenderJSON(ret)
}

// XajaGetTagList get an async tag list
func (c App) XajaGetTagList(keyword string) revel.Result {
	// metrics
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "XajaGetTagList", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	if c.getSessionUserOrFail() == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "XajaGetTagList", "1", errors.New("an user without session tried to use this ajax function"), "keyword", keyword)
		return c.Forbidden("Forbidden")
	}
	matchingTags := searchTags(keyword)
	// ok
	var ret xajaTagResult
	ret.Items = matchingTags
	revel.AppLog.Info(">>>>>>>>> XajaGetTagList", "keyword", keyword, "ret", utils.ToString(ret))
	return c.RenderJSON(ret)
}

// XajaGetLocationAllList get an async location list from input that can be a postal code/town/dept
func (c App) XajaGetLocationAllList(keyword string) revel.Result {
	// metrics
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "XajaGetLocationAllList", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	userSession := c.getSessionUserOrFail()
	if userSession == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "XajaGetLocationAllList", "1", errors.New("an user without session tried to use this ajax function"), "keyword", keyword)
		return c.Forbidden("Forbidden")
	}
	matchingLocations := searchLocations(keyword)
	// ok
	return c.RenderJSON(matchingLocations)
}

// XajaGetTownListNearMe get a list of town Near Me. input = lat && lon
func (c App) XajaGetTownListNearMe(lat, lon string) revel.Result {
	// metrics
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "XajaGetTownListNearMe", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	userSession := c.getSessionUserOrFail()
	if userSession == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "XajaGetLocationAllList", "1", errors.New("an user without session tried to use this ajax function"), "lat", lat, "lon", lon)
		return c.Forbidden("Forbidden")
	}
	// check input
	if len(lat) == 0 || len(lon) == 0 {
		return nil
	}
	matchingLocations := searchLocationNearMe(lat, lon)
	// ok
	return c.RenderJSON(matchingLocations)
}
