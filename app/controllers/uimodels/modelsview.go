package uimodels

import (
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"net/url"
	"strconv"

	guuid "github.com/google/uuid"
)

// DEPRECATED AddJobPageData handle data send to client for this page
type AddJobPageData struct {
	User   UserSession
	Step   int          // Computed next Step
	NewJob NewJob       // copy cache of new job
	Job    dbmodels.Job // used to preview
}

// JobCreationPageData handle data send to client for this page
type JobCreationPageData struct {
	User            UserSession
	NewJob          NewJob       // copy cache of new job
	PreviousBabyJob dbmodels.Job // TODO merge with newJob
}

// JobPreviewPageData handle data sent to client for this page
type JobPreviewPageData struct {
	User UserSession
	// Company dbmodels.Company
	Job dbmodels.Job
}

// IndexPageData handle data send to client for this page
// type IndexPageData struct {
// 	User UserSession
// 	Jobs []dbmodels.JobPreview
// 	// job list pagination mangement TODO pager object
// 	TotalItem int
// 	Page      int
// 	TotalPage int
// 	HasPrev   bool
// 	HasNext   bool
// 	// podium
// 	TopCompanies []dbmodels.CompanyPreview
// }

// AboutPageData handle data send to client for this page
type AboutPageData struct {
	User UserSession
}

// JobPageData handle data send to client for this pagee
type JobPageData struct {
	User UserSession
	Job  *dbmodels.Job
}

// LoginPageData handle data send to client for this page
type LoginPageData struct {
	User UserSession
}

// PasswordResetPageData handle data send to client for this page
type PasswordResetPageData struct {
	User                UserSession
	PasswordRequestUUID guuid.UUID
}

// SocialPageData handle data send to client for this page
type SocialPageData struct {
	User           UserSession
	CompanyProfile *dbmodels.Company
}

// ListSocialPageData handle data send to client for this page
type ListSocialPageData struct {
	User      UserSession
	Companies []dbmodels.CompanyPreview
}

// ProfilePageData handle data send to client for this page
type ProfilePageData struct {
	User           UserSession
	CompanyProfile dbmodels.Company
}

// CandidateApplicationPageData handle data send to client for this page
type CandidateApplicationPageData struct {
	User UserSession
	// CandidateApplication []dbmodels.ApplicationsCandidateView
}

// CompanySpaceData SpaceData handle data send to client for this page
type CompanySpaceData struct {
	User UserSession
	// stats
	Stats dbmodels.CompanyStat
	// jobs
	Jobs CompanyJobsData
}

// CompanyJobsData container for all type of jobs for a company
type CompanyJobsData struct {
	PendingJobs  []dbmodels.JobStat
	OnlineJobs   []dbmodels.JobStat
	ArchivedJobs []dbmodels.JobStat
	Crfsc        string
}

// SubscribePageData handle data send to client for this page
type SubscribePageData struct {
	User         UserSession
	TopCompanies []dbmodels.CompanyPreview
}

// JobsPageData handle data send to client for this page
type JobsPageData struct {
	Jobs []dbmodels.JobPreview
	User UserSession
	// job list pagination mangement TODO pager object
	TotalItem int
	Page      int
	TotalPage int
	HasPrev   bool
	HasNext   bool
	// Most Used Tags
	MostUsedTags []dbmodels.Tag
}

// RecruitPageData handle data send to client for this page
type RecruitPageData struct {
	User UserSession
}

// MessagePageData handle data send to client for this page
type MessagePageData struct {
	User UserSession
}

// JobsInputSearchFilters filter handler
// containt data about filtering jobs page
// deserialised and mapped from json at page loading
type JobsInputSearchFilters struct {
	Q             []int64 // id of tags in search
	Contrat       []int64
	Xp            []int64
	Time          []int64
	Tag           []int64
	SalaryMin     int   // minimum salary per year
	Nomad         int   // same
	LocationID    int64 // only ID (front query)
	Radius        int
	Ronly         int                     // SuggestedJobsOnly
	Sonly         int                     // SavedFilterQuery
	P             int                     // Page
	JobIndexStart int                     // Job index
	Company       dbmodels.CompanyMinimal // public ID of a company
	// Server response
	Location dbmodels.Location // IDS and label (server response)
	Contrats []dbmodels.JobContrat
	Times    []dbmodels.JobWorkTime
	Xps      []dbmodels.JobExperience
	Tags     []dbmodels.Tag
}

// ToString of type JobsInputSearchFilters
func (s JobsInputSearchFilters) ToString() string {
	//var values url.Values = make(map[int]string)
	values := make(url.Values)
	// rewrite parameters for security
	if len(s.Q) > 0 {
		values.Set("Q", utils.Int64ArrayToString(s.Q, "+"))
	}
	if len(s.Contrat) > 0 {
		values.Set("contrat", utils.Int64ArrayToString(s.Contrat, "+"))
	}
	if len(s.Xp) > 0 {
		values.Set("xp", utils.Int64ArrayToString(s.Xp, "+"))
	}
	if len(s.Time) > 0 {
		values.Set("time", utils.Int64ArrayToString(s.Time, "+"))
	}
	if s.SalaryMin > 0 {
		values.Set("salary", strconv.Itoa(s.SalaryMin))
	}
	if len(s.Tag) > 0 {
		values.Set("tag", utils.Int64ArrayToString(s.Tag, "+"))
	}
	if s.Nomad > 0 {
		values.Set("nomad", strconv.Itoa(s.Nomad))
	}
	if s.LocationID > 0 {
		values.Set("location", strconv.FormatInt(s.LocationID, 10))
	}
	if s.Radius > 0 {
		values.Set("radius", strconv.Itoa(s.Radius))
	}
	if s.Ronly == 1 {
		values.Set("ronly", "1")
	}
	if s.Sonly == 1 {
		values.Set("sonly", "1")
	}
	if len(s.Company.PublicID) > 0 {
		values.Set("Company", utils.ToString(s.Company))
	}
	// ok
	utils.ManageLog(utils.Debug, utils.Controller, "JobsInputSearchFilters", "ToString", nil, "input", utils.ToString(s), "result", values.Encode())
	return values.Encode()
}
