package uimodels

import (
	"lt/app/db/dbmodels"
	"regexp"
	"strings"
	"time"

	guuid "github.com/google/uuid"
	"github.com/revel/revel"
)

// used to calculate image size
const (
	_      = iota
	kb int = 1 << (10 * iota)
	mb
	gb
)

/**
 * OTHERS MODELS
 */

// UserSession server side handling for connected or not connected users
type UserSession struct {
	SessionToken string
	// userId of user in db if connected == true
	ID    guuid.UUID // todo it's a private data, check if not leeked in htmls (voir le supprimer)
	Email string
	// path of avatar
	Avatar string
	// true if logged user
	Connected bool
}

// GetUniqueID return sessionToken for unknow user, ID from DB otherwise
func (user *UserSession) GetUniqueID() string {
	if user.Connected {
		return user.ID.String()
	}
	return user.SessionToken
}

// ToString debug info about a webuser
func (user *UserSession) ToString() string {
	var sb strings.Builder
	if user.Connected {
		sb.WriteString("Connected User with user ID = '" + user.ID.String() + "', sessionToken = '" + user.SessionToken + "'")
	} else {
		sb.WriteString("Disconnected User with sessionToken = '" + user.SessionToken + "'")
	}
	return sb.String()
}

// login

// UserLogin simple user model used to log user
type UserLogin struct {
	Email    string
	Password string
	Crfsc    string
}

// Validate user login
func (user *UserLogin) Validate(v *revel.Validation) {
	v.Required(user.Email).Message("Email is a mandatory field")
	v.Required(user.Password).Message("Password is a mandatory field")
}

// WebUserPasswordSetting handle data to change an account password
type WebUserPasswordSetting struct {
	Password                string // actual Password
	NewPassword             string
	NewPasswordConfirmation string
	Crfsc                   string
}

// Validate webuser password change
func (webuser *WebUserPasswordSetting) Validate(v *revel.Validation) {
	v.Required(webuser.Password)
	v.Required(webuser.NewPassword)
	v.Required(webuser.NewPasswordConfirmation)
	v.Required(webuser.NewPasswordConfirmation == webuser.NewPassword).
		Message("Les 2 mots de passe ne correspondent pas.").Key("webUserPasswordSetting.NewPasswordConfirmation")
}

// WebUserEmailSetting handle data to change an account email
type WebUserEmailSetting struct {
	Email string
	Crfsc string
}

// Validate user email
func (user *WebUserEmailSetting) Validate(v *revel.Validation) {
	v.Required(user.Email).Message("Email is a mandatory field")
}

/**
 * UI TARGET MODELS
 */

// Application / postulation
// type Application struct {
// 	// common application data
// 	JobID                     guuid.UUID
// 	JobPublicID               string
// 	CandidateLettreMotivation string // falcultatif
// 	IsConnectedApplication    bool

// 	// connected Application
// 	// before any application, candidate must
// 	// indicate data about nom,prenom,email adress, resume
// 	UserID guuid.UUID

// 	// no account Application
// 	CandidateNom        string
// 	CandidatePrenom     string
// 	CandidateEmail      string
// 	CandidateResumePath string
// 	CandidateResume     []byte
// }

// // ToString debug info about an application
// func (application *Application) ToString() string {
// 	var sb strings.Builder
// 	sb.WriteString("JobPublicID [" + application.JobPublicID + "]")
// 	sb.WriteString("JobID [" + application.JobID.String() + "]")
// 	sb.WriteString("IsConnectedApplication [" + strconv.FormatBool(application.IsConnectedApplication) + "]")
// 	sb.WriteString("CandidateLettreMotivation [" + application.CandidateLettreMotivation + "]")
// 	sb.WriteString("UserID [" + application.UserID.String() + "]")
// 	sb.WriteString("CandidateNom [" + application.CandidateNom + "]")
// 	sb.WriteString("CandidatePrenom [" + application.CandidatePrenom + "]")
// 	sb.WriteString("CandidateEmail [" + application.CandidateEmail + "]")
// 	sb.WriteString("CandidateResumePath [" + application.CandidateResumePath + "]")
// 	sb.WriteString("CandidateResume len [" + strconv.Itoa(len(application.JobPublicID)) + "]")
// 	return sb.String()
// }

// Validate a new Application
// func (application *Application) Validate(v *revel.Validation) {
// 	v.Required(application.JobPublicID).Message("JobPublicID is a mandatory field")
// 	v.Required(application.JobID).Message("Job not found")
// 	if application.IsConnectedApplication == false {
// 		// no account apply
// 		v.Required(application.CandidateNom).Message("CandidateNom is a mandatory field")
// 		v.MinSize(application.CandidateNom, 3).Message("Check Name")
// 		v.Required(application.CandidatePrenom).Message("CandidatePrenom is a mandatory field")
// 		v.MinSize(application.CandidatePrenom, 3).Message("Check Prenom")
// 		v.Required(application.CandidateEmail).Message("CandidateEmail is a mandatory field")
// 		v.Email(application.CandidateEmail).Message("Check Email")
// 		// resume document format
// 		// v.MinSize(application.CandidateResume, 1).Message("CandidateResume is a mandatory field")
// 		if len(application.CandidateResume) != 0 {
// 			// get mime type
// 			typef, err := utils.GetFileContentTypeBuffer(application.CandidateResume)
// 			// check mime type
// 			if err != nil || utils.MimeTypeIsDocument(typef) == false {
// 				v.Errors = append(v.Errors, &revel.ValidationError{Key: "application.CandidateResume", Message: "Ce format de CV n'est pas supporté."})
// 			}
// 		}
// 	} else {
// 		v.Required(application.UserID).Message("UserID is a mandatory field")
// 	}
// }

// NewsletterSubscription abonnement custom mail
type NewsletterSubscription struct {
	// can be a new or not new user
	Email           string
	Periodicity     int
	Params          string
	Terms           bool
	ActivationGUUID *guuid.UUID
	// front input tostringfy
	QueryFilter JobsInputSearchFilters
}

// Validate a new Newsletter Subscription
func (subscription *NewsletterSubscription) Validate(v *revel.Validation) {
	v.Required(subscription.Email).Message("Renseigner une adresse email")
	v.Email(subscription.Email).Message("Renseigner une adresse email valide")
	v.Required(subscription.Terms == true).Message("Vous devez accepter les conditions d'utilisations")
}

// Subscription exported to revel
type Subscription struct {
	Email           string
	Password        string
	PasswordConfirm string
	Crfsc           string
}

// Validate a new subscription
func (subscription *Subscription) Validate(v *revel.Validation) {
	// email verification
	v.Required(subscription.Email).Message("Renseigner une adresse email")
	v.Email(subscription.Email).Message("Renseigner une adresse email valide")
	v.MinSize(subscription.Email, 4).Message("Renseigner une adresse email valide")
	v.MaxSize(subscription.Email, 255).Message("Renseigner une adresse email valide")
	// password
	v.Required(subscription.Password).Message("Type a Password")
	v.MaxSize(subscription.Password, 24).Message("Invalid Password should be > 6 && < 24")
	v.MinSize(subscription.Password, 6).Message("Invalid Password should be > 6 && < 24")
	// todo check password strengh
	// password confirm
	v.Required(subscription.PasswordConfirm).Message("Retype a Password")
	v.Required(subscription.PasswordConfirm == subscription.Password).Message("Les mots de passe ne sont pas identiques.")
	// User Type should be always OK
	// v.Check(subscription.UserType,
	// 	revel.Required{},
	// )
}

// ResetPassword exported to revel
type ResetPassword struct {
	Email string
	Crfsc string
}

// Validate a new Password Reset
func (resetpassword *ResetPassword) Validate(v *revel.Validation) {
	v.Required(resetpassword.Email)
	v.Email(resetpassword.Email).Message("Renseigner une adresse email valide")
}

// ResetingPassword exported to revel
type ResetingPassword struct {
	ID              string
	Password        string
	PasswordConfirm string
	Crfsc           string
}

// Validate a new Password Reset
func (resetinpassword *ResetingPassword) Validate(v *revel.Validation) {
	v.Required(resetinpassword.ID)
	v.Required(resetinpassword.Password)
	v.Required(resetinpassword.PasswordConfirm)
	v.Required(resetinpassword.Password == resetinpassword.PasswordConfirm).Message("Les mots de passe ne sont pas identiques.")
}

// func (v *Validation) IsTrue(obj interface{}, n int) *ValidationResult {

// 	if obj == true {
// 		v.ValidationResult(true)
// 	}
// 	vr := v.ValidationResult(false)
// 	vr.Error = err
// 	return vr
// }

// EmailPattern rgx check email is valid
var EmailPattern = regexp.MustCompile("^[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?$")

// URLPattern rgx check url is valid
var URLPattern = regexp.MustCompile(`^((((https?|ftps?|gopher|telnet|nntp)://)|(mailto:|news:))(%[0-9A-Fa-f]{2}|[-()_.!~*';/?:@#&=+$,A-Za-z0-9\p{L}])+)([).!';/?:,][[:blank:]])?$`)

var PhonePattern = regexp.MustCompile(`^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$`)

// NewJob job
type NewJob struct {
	UserID       guuid.UUID
	Position     string
	Description  string
	URLMailApply string
	ExpireDate   string
	Tagstr       string
	Tags         []dbmodels.Tag
	Contrat      int
	Experience   int
	Worktime     int
	Regionals    int
	Salary       int
	FullRemote   bool
	SubmitButton string
	Crfsc        string
}

// Validate a new job
func (newJob *NewJob) Validate(v *revel.Validation) {
	v.Required(newJob.UserID)
	v.Required(newJob.Experience).Message("Veillez indiquer l'experience du candidat.")
	v.Required(newJob.Position).Message("Veillez indiquer le titre de l'emploi.")
	v.MaxSize(newJob.Position, 200)
	v.MinSize(newJob.Position, 10).Message("Le titre de l'annonce est trop courte (10 catactères minimum)")
	v.Required(newJob.Description).Message("Description is a mandatory field")
	v.MaxSize(newJob.Description, 10000).Message("La description de l'annonce est trop longue (10000 catactères maximum)")
	v.MinSize(newJob.Description, 20).Message("La description de l'annonce est trop courte (20 catactères minimum)")
	v.Required(newJob.URLMailApply).Message("Vous devez spécifier un lien ou un mail de contact pour les candidats.")
	if !URLPattern.MatchString(newJob.URLMailApply) && !EmailPattern.MatchString(newJob.URLMailApply) {
		v.Error("Vous devez spécifier un lien valide ou un mail de contact valide pour les candidats.")
	}
	// to sync with db datas
	v.Min(newJob.Contrat, 0).Message("Données de formulaire invalide.").Key("newJob.Contrat")
	v.Max(newJob.Contrat, 7).Message("Données de formulaire invalide.").Key("newJob.Contrat")
	v.Min(newJob.Experience, 0).Message("Données de formulaire invalide.").Key("newJob.Experience")
	v.Max(newJob.Experience, 5).Message("Données de formulaire invalide.").Key("newJob.Experience")
	v.Min(newJob.Worktime, 0).Message("Données de formulaire invalide.")
	v.Max(newJob.Worktime, 2).Message("Données de formulaire invalide.")
	v.Min(newJob.Regionals, 0).Message("Données de formulaire invalide.")
	v.Max(newJob.Regionals, 3).Message("Données de formulaire invalide.")
}

// CompanyProfile used to update a company profil
type CompanyProfile struct {
	UserID      guuid.UUID
	Name        string
	Logo        []byte
	Location    dbmodels.Location
	Industry    dbmodels.Industry
	CompanySize dbmodels.CompanySize
	Founded     int
	Description string
	Phone       string
	Website     string
	Linkedin    string
	Facebook    string
	Twitter     string
	Instagram   string
	Apps        []int64
	Avatar      string
	Crfsc       string
}

// Validate a new company update request
func (companyProfile *CompanyProfile) Validate(v *revel.Validation) {
	v.Required(companyProfile.UserID)
	if len(companyProfile.Phone) > 0 {
		if !PhonePattern.MatchString(companyProfile.Phone) {
			v.Error("Numéro de téléphone non valide. Formats acceptés: +919367788755 ; 8989829304 ; +16308520397 ; 786-307-3615")
		}
	}
	if len(companyProfile.Website) > 0 {
		v.URL(companyProfile.Website).Message("Veuillez indiquez un lien valide")
	}
	t := time.Now().Year()
	v.Max(companyProfile.Founded, t)
	v.Min(companyProfile.Founded, 1900)
}

// DeleteAccount stub
type DeleteAccount struct {
	Crfsc string
}

// PostCanceljob stub
type PostCanceljob struct {
	JobID string
	Crfsc string
}

// PostArchivejob stub
type PostArchivejob struct {
	JobID string
	Crfsc string
}

// PostUnarchivejob stub
type PostUnarchivejob struct {
	JobID string
	Crfsc string
}

// PostDeletejob stub
type PostDeletejob struct {
	JobID string
	Crfsc string
}

// CandidateProfile is used to update a candidate profile

// func (v *CandidateProfile) ToString() string {
// 	nikita := CandidateProfile{}
// 	copier.Copy(&nikita, &v)
// 	nikita.Avatar = nil
// 	res2B := marshalObject(nikita)
// 	return string(res2B)
// }

// image upload
// if candidateProfile.HasUploadedAvatar {
// 	v.Required(candidateProfile.Avatar)
// 	_, format, err := image.DecodeConfig(bytes.NewReader(candidateProfile.Avatar))
// 	v.Required(err == nil).Message("JPEG or PNG file format is expected (err 1)").Key("CandidateProfile.Avatar")
// 	v.Required(format == "jpeg" || format == "png").Message("JPEG or PNG file format is expected (err 2)").Key("CandidateProfile.Avatar")
// 	//	v.Required(conf.Height >= 150 && conf.Width >= 150).Message("Minimum allowed resolution is 150x150px (err 3)").Key("CandidateProfile.Avatar")
// }

// if candidateProfile.HasUploadedCV {
// 	v.Required(candidateProfile.CV)
// }

// v.Required(CandidateProfile.Nomade).Message("Nomade is a mandatory field")

// v.MinSize(companyProfile.Logo, 2*kb).Message("Minimum a file size of 2KB expected") not used cause optional field
// v.MaxSize(companyProfile.Logo, 2*mb).Message("File cannot be larger than 2MB")
