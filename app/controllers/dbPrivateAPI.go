package controllers

import (
	"bytes"
	"image"
	"lt/app/ajobs"
	"lt/app/controllers/uimodels"
	"lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"net/url"
	"strconv"

	"github.com/revel/revel"

	guuid "github.com/google/uuid"
)

func incrementJobView(jobID guuid.UUID) {
	go db.IncrementJobViewCount(jobID)
}

func getTags(tagIDs []int64) []dbmodels.Tag {
	ret, errint := db.GetTags(tagIDs)
	if errint == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Warn, utils.Controller, "getTags", "1", nil, "errint", strconv.Itoa(errint), "tagIDs", utils.ToString(tagIDs))
	}
	return ret
}

func getMostUsedTags() []dbmodels.Tag {
	ret, errint := db.GetMostUsedTags(maxTagsToShowInQuerySearch)
	if errint == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Warn, utils.Controller, "getMostUsedTags", "1", nil, "errint", strconv.Itoa(errint))
	}
	return ret
}

func getLocation(locationID int64) dbmodels.Location {
	location, errint := db.GetLocation(locationID)
	if errint == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Warn, utils.Controller, "getLocation", "1", nil, "errint", strconv.Itoa(errint), "locationID", utils.ToString(locationID))
	}
	return location
}

func getContrats(contratIDs []int64) []dbmodels.JobContrat {
	ret, errint := db.GetContrats(contratIDs)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "getContrats", "1", nil, "errint", strconv.Itoa(errint), "contratIDs", utils.ToString(contratIDs))
	}
	return ret
}
func getJobTime(timesIDs []int64) []dbmodels.JobWorkTime {
	ret, errint := db.GetJobTimes(timesIDs)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "getJobTime", "1", nil, "errint", strconv.Itoa(errint), "timesIDs", utils.ToString(timesIDs))
	}
	return ret
}
func getExperiences(experienceIDs []int64) []dbmodels.JobExperience {
	ret, errint := db.GetJobExperiences(experienceIDs)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "getExperiences", "1", nil, "errint", strconv.Itoa(errint), "experienceIDs", utils.ToString(experienceIDs))
	}
	return ret
}

// @return CompanyStat
// ProfileViewCount         int nombre de visite sur le profile de la company
// ProfileCompletionPercent int
// ProfileNameFilled        bool
// JobActiveCount           int Nombre de job posté en ligne de la company
// JobTotalCount            int nombre de job de la company posté en tout
// JobTotalViewCount        int  nombre de visites de tous les jobs de la company
// JobRedirectionCount      int nombre d'application possible de tout les jobs de la company
func companyJobStat(companyID guuid.UUID) dbmodels.CompanyStat {
	return db.GetCompanyStats(companyID)
}

func (c App) validateAvatar(avatar []byte, key string) []byte {
	// validate avatar
	var normalizedAvatar []byte
	if len(avatar) != 0 {
		// size
		if len(avatar) > 10*mb {
			c.Validation.ValidationResult(false).Key(key).Message("File cannot be larger than 10MB")
		} else {
			conf, _, err := image.DecodeConfig(bytes.NewReader(avatar))
			if err != nil {
				c.Validation.ValidationResult(false).Key(key).Message("Incorrect file format")
			} else if conf.Height < 16 && conf.Width < 16 {
				c.Validation.ValidationResult(false).Key(key).Message("Minimum allowed resolution is 16x16px")
			} else {
				// resize and convert new picture
				normalizedAvatar, err = utils.NormalizeImagePng(avatar, logoResizeWidth)
				if err != nil {
					c.Validation.ValidationResult(false).Key(key).Message("JPEG or PNG file format is expected")
				}
				return normalizedAvatar
			}
		}
	}
	return nil
}

// create a new newsletter sub
func sendConfirmationNewletterSubcription(newSubscription uimodels.NewsletterSubscription) (bool, *InterfaceError) {
	var errorval *InterfaceError
	newSubscription.ActivationGUUID, errorval = InsertNewletterSub(newSubscription)
	if errorval != nil {
		return false, errorval
	}
	// already exist
	if newSubscription.ActivationGUUID == nil {
		return true, nil
	}
	// prepare mail
	mailItem, err := ajobs.ConfirmSubscriptionEmailPrepare(newSubscription.ActivationGUUID.String(), newSubscription.Email, newSubscription.Periodicity)
	if err != nil {
		return false, errorval
	}
	// send mail
	ajobs.SendMailWithParams(*mailItem, true)
	return true, nil
}

func parseJobsParameter(values url.Values, connected bool) uimodels.JobsInputSearchFilters {
	if revel.DevMode {
		utils.ManageLog(utils.Debug, utils.Controller, "parseJobsParameter", "1", nil, "input values", values.Encode(), "input connected", utils.ToString(connected))
	}
	// params container
	var queryFilter uimodels.JobsInputSearchFilters
	// // url = /foo?sort=asc&active=1
	queryFilter.Q = utils.ParseQueryIntegerArray(values.Get("q"), " ")
	queryFilter.Contrat = utils.ParseQueryIntegerArray(values.Get("contrat"), " ")
	queryFilter.Xp = utils.ParseQueryIntegerArray(values.Get("xp"), " ")
	queryFilter.Time = utils.ParseQueryIntegerArray(values.Get("time"), " ")
	// queryFilter.SalaryMin = utils.StringToIntOrZero(values.Get("salary"))
	queryFilter.Tag = utils.ParseQueryIntegerArray(values.Get("tag"), " ")
	// queryFilter.Nomad = utils.StringToIntOrZero(values.Get("nomad"))
	// queryFilter.LocationID = utils.ConvertStringToInt64(values.Get("location")) // id, isdept
	// queryFilter.Radius = utils.StringToIntOrZero(values.Get("radius"))
	queryFilter.Company.PublicID = values.Get("c")
	if connected {
		queryFilter.Ronly = utils.StringToIntOrZero(values.Get("ronly")) // recommended
		queryFilter.Sonly = utils.StringToIntOrZero(values.Get("sonly")) // saved
	}
	queryFilter.P = utils.StringToIntOrZero(values.Get("p"))
	// set page 1 par defaut
	// if queryFilter.P <= 0 {
	// 	queryFilter.P = 1
	// }
	// fill locations data to render page (id, isdept, cp, label)
	// queryFilter.Location = getLocation(queryFilter.LocationID)
	// fill contrat data to render page (id, name)
	queryFilter.Contrats = getContrats(queryFilter.Contrat)
	// fill Times
	queryFilter.Times = getJobTime(queryFilter.Time)
	// fill Xps
	queryFilter.Xps = getExperiences(queryFilter.Xp)
	// fill tag
	queryFilter.Tags = getTags(queryFilter.Tag)
	// fill company
	if len(queryFilter.Company.PublicID) != 0 {
		company, errint := db.GetCompanyMinimalPublicID(queryFilter.Company.PublicID)
		if errint == db.CompletedWithNoError {
			queryFilter.Company = company
		}
	}
	// retrieve page index paramater
	//queryFilter.JobIndexStart = ((queryFilter.P * maxJobShowedInJobList) - maxJobShowedInJobList)
	// ok
	if revel.DevMode {
		utils.ManageLog(utils.Debug, utils.Controller, "parseJobsParameter", "2", nil, "input values", values.Encode(), "input connected", utils.ToString(connected), "result queryFilter", utils.ToString(queryFilter))
	}
	return queryFilter
}
