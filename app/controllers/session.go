package controllers

import (
	"errors"
	"lt/app/controllers/uimodels"
	"lt/app/utils"
	"time"

	guuid "github.com/google/uuid"
	"github.com/revel/revel/cache"
)

// DEBUG: set to true - automaticaly log a debug user
var bypasslogin = false

// DEBUG: set to true - let unit test work
var bypassCRFSCheck = true

// userSessionExpirationMin more minutes
// func (c App) refeshUserSession() {
// 	userSession := c.getSessionUser()
// 	cache.Set(c.Session.ID(), userSession, userSessionExpirationMin*time.Minute)
// }
func (c App) replaceSessionAvatarPath(logoPath string) {
	userSession := c.getSessionUserOrCreate()
	if userSession.Connected {
		userSession.Avatar = logoPath
		cache.Set(c.Session.ID(), userSession, userSessionExpirationMin*time.Minute)
	}
	// else do nothing
}

// get request, or create. refresh all the same
func (c App) getSessionUserOrCreate() uimodels.UserSession {
	var userSession uimodels.UserSession
	err := cache.Get(c.Session.ID(), &userSession)
	if err != nil {

		// v DEBUG
		if bypasslogin {
			guuiddebug, _ := guuid.ParseBytes([]byte("da8ab055-1309-4385-a15f-da1ecbd49362"))
			userdebug := getUserData(guuiddebug)
			if userdebug == nil {
				utils.ManageLog(utils.Error, utils.Controller, "getSessionUserOrCreate", "1", errors.New("debug user did not log"), "guuiddebug", guuiddebug.String())
			} else {
				userSession.Connected = true
				userSession.Email = userdebug.Email
				userSession.ID = userdebug.ID
				userSession.SessionToken = c.Session.ID()
				userSession.Avatar = userdebug.Avatar
			}
		}
		// ^ DEBUG

		cache.Set(c.Session.ID(), userSession, userSessionExpirationMin*time.Minute)
		return userSession
	}
	// refresh session
	cache.Set(c.Session.ID(), userSession, userSessionExpirationMin*time.Minute)

	return userSession
}

// post requests
func (c App) getSessionUserOrFail() *uimodels.UserSession {
	var userSession uimodels.UserSession
	err := cache.Get(c.Session.ID(), &userSession)
	if err != nil {
		// no session
		return nil
	}

	// v DEBUG
	if bypasslogin {
		guuiddebug, _ := guuid.ParseBytes([]byte("da8ab055-1309-4385-a15f-da1ecbd49362"))
		userdebug := getUserData(guuiddebug)
		if userdebug == nil {
			utils.ManageLog(utils.Error, utils.Controller, "getSessionUserOrCreate", "1", errors.New("debug user did not log"), "guuiddebug", guuiddebug.String())
		} else {
			userSession.Connected = true
			userSession.Email = userdebug.Email
			userSession.ID = userdebug.ID
			userSession.SessionToken = c.Session.ID()
			userSession.Avatar = userdebug.Avatar
		}
	}
	// ^ DEBUG

	// refresh session
	cache.Set(c.Session.ID(), userSession, userSessionExpirationMin*time.Minute)

	return &userSession
}

// connectUser return a front connected user session
// param: uuid of the user in DB
func (c App) connectUser(userID guuid.UUID) {
	// get data from db
	user := getUserData(userID)
	if user != nil {
		user.SessionToken = c.Session.ID()
		user.Connected = true
		// set session
		cache.Set(c.Session.ID(), *user, userSessionExpirationMin*time.Minute)
		utils.ManageLog(utils.Debug, utils.Controller, "connectUser", "new connection", nil, "userID", userID.String())
	} else {
		utils.ManageLog(utils.Crit, utils.Controller, "connectUser", "2", errors.New("can not connect user with unknown id"), "userID", userID.String())
	}
}

// log out an user
func (c App) disconnectUser() {
	cache.Delete(c.Session.ID())
}

func (c App) addCRFSC(code string) string {
	// cfrs
	crfsc := guuid.New()
	cache.Set(c.Session.ID()+"-"+code+"-crfsc", crfsc.String(), userSessionExpirationMin*time.Minute)
	c.ViewArgs["crfsc"] = crfsc
	return crfsc.String()
}

func (c App) checkCRFSC(code, input string) bool {
	if bypassCRFSCheck {
		return true
	}
	var crfsc string
	sessionCode := c.Session.ID() + "-" + code + "-crfsc"
	err := cache.Get(sessionCode, &crfsc)
	cache.Delete(sessionCode)
	if err != nil {
		utils.ManageLog(utils.Warn, utils.Controller, "checkCRFSC", "user failed cfrs", nil, "sessionCode", sessionCode, "crfsc", crfsc, "received", input)
		return false
	}
	if input != crfsc {
		utils.ManageLog(utils.Warn, utils.Controller, "checkCRFSC", "user failed cfrs", nil, "sessionCode", sessionCode, "crfsc", crfsc, "received", input)
		return false
	}
	return true
}
