package controllers

import (
	"encoding/json"
	"errors"
	"lt/app/controllers/uimodels"
	"lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strconv"
	"strings"
	"time"

	guuid "github.com/google/uuid"
	_ "github.com/lib/pq" // blank import needed
	"github.com/revel/revel"
)

/*********************
	HIGH LEVEL INTERFACE
	- check inputs
	- convert to ui data
	- cache results
*********************/

// USERS

// InterfaceError handle a ierror
type InterfaceError struct {
	Severity int
	Message  string
}

// LogUser permit user login
func logUser(user *uimodels.UserLogin) (guuid.UUID, *revel.ValidationError) {
	var userID guuid.UUID
	userID, errint := db.PeekWebUser(user.Email, user.Password)
	if errint != db.CompletedWithNoError {
		if errint == db.UncoverableDatabaseError {
			utils.ManageLog(utils.Error, utils.Controller, "logUser", "PeekWebUser", nil, "errint", strconv.Itoa(errint), "user.Email", user.Email)
			return userID, &revel.ValidationError{Key: "all", Message: "Une erreur est survenue"}
		}
		return userID, &revel.ValidationError{Key: "all", Message: "Identifiants incorrects"}
	}
	return userID, nil
}

// InsertUser New subcription
func insertUser(subscription *uimodels.Subscription) (guuid.UUID, *InterfaceError) {
	var userID guuid.UUID
	userID, errint := db.InsertNewUser(subscription.Email, subscription.Password)
	if errint != db.CompletedWithNoError {
		if errint == db.ItemAlreadyExist {
			return userID, &InterfaceError{Severity: 0, Message: "Un compte avec cette adresse mail existe déjà."}
		}
		if errint == db.UncoverableDatabaseError {
			utils.ManageLog(utils.Error, utils.Controller, "insertUser", "InsertNewUser", nil, "errint", strconv.Itoa(errint), "user.Email", subscription.Email)
		}
		return userID, &InterfaceError{Severity: 0, Message: "Une erreur est survenue"}
	}
	utils.ManageLog(utils.Info, utils.Controller, "insertUser", "InsertNewUser", nil, "new user Email", subscription.Email)
	return userID, nil
}

// insertBabyJob add new job
func insertBabyJob(newJob *uimodels.NewJob) int {
	// compte a new expire date
	newJob.ExpireDate = utils.CreateExpireDateFromNow(jobDaysUntilExpiration)
	// insert
	UUID, publicID, errint := db.AddJob(newJob.UserID, guuid.New(), newJob.Position, newJob.Description, newJob.URLMailApply,
		newJob.ExpireDate, newJob.Tags, newJob.Contrat, newJob.Experience, newJob.Worktime, newJob.Salary,
		newJob.Regionals, newJob.FullRemote)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Error, utils.Controller, "insertUser", "InsertNewUser", nil, "errint", strconv.Itoa(errint), "user.ID", newJob.UserID.String(), "Position", newJob.Position)
	} else {
		utils.ManageLog(utils.Info, utils.Controller, "insertBabyJob", "AddJob", nil, "inserted job's UUID", UUID.String(), "inserted job's publicID", publicID)
	}
	return errint
}

// InsertNewletterSub insertNewletterSub
func InsertNewletterSub(subcription uimodels.NewsletterSubscription) (*guuid.UUID, *InterfaceError) {
	subID, err := db.CreateNewletterSub(subcription.Email, subcription.Params, subcription.Periodicity)
	if err != db.CompletedWithNoError {
		if err == db.ItemAlreadyExist {
			// sub already exist
			return nil, nil
		}
		return nil, &InterfaceError{Severity: 0, Message: "Une erreur est survenue. Vérifier qu'une subscription n'existe pas déjà."}
	}
	return subID, nil
}

func updateUserSession(userID guuid.UUID, ret uimodels.UserSession) {
	webuser, err := db.GetWebUser(userID)
	if err != db.CompletedWithNoError {
		return
	}
	ret.ID = webuser.ID
	ret.Email = webuser.Email
	ret.Avatar = webuser.Avatar
}

func getUserData(userID guuid.UUID) *uimodels.UserSession {
	var ret uimodels.UserSession
	webuser, err := db.GetWebUser(userID)
	if err != db.CompletedWithNoError {
		return nil
	}
	ret.ID = webuser.ID
	ret.Email = webuser.Email
	ret.Avatar = webuser.Avatar
	return &ret
}

func getTopCompanies(limit int) []dbmodels.CompanyPreview {
	ret, errint := db.GetCompanies(limit, maxTagsShowedInCompaniesList)
	if errint == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "getTopCompanies", "1", errors.New("can't retrieve companies list"), "errint", strconv.Itoa(errint))
	}
	return ret
}

func checkUserExists(userID guuid.UUID) bool {
	_, err := db.GetWebUser(userID)
	return err == db.CompletedWithNoError
}

// change profile picture for an user
// param: userID, binary of file, old path to delete previous pic
// return new path
func (c App) updateAvatar(userID guuid.UUID, normalizedAvatar []byte, oldPath string) string {
	if len(normalizedAvatar) > 0 {
		// create new path
		logoPath := userID.String() + "_" + time.Now().Format("20060102150405") + ".jpg"
		// write byte to disk
		utils.WriteFileContent(normalizedAvatar, logoDirectory+logoPath)
		// update new avatar path for user
		if err := db.UpdateWebUserAvatar(userID, logoPath); err != db.CompletedWithNoError {
			// can not update db, reverting to older avatar
			utils.ManageLog(utils.Error, utils.Controller, "updateAvatar", "1", errors.New("can not update db, reverting to older avatar"), "userID", userID.String(), "normalizedAvatarSize", strconv.Itoa(len(normalizedAvatar)), logoPath, logoPath)
			// delete new pic
			utils.DeleteFile(logoPath)
			// revert to original
			return oldPath
		}
		// delete previous file
		if strings.Compare(oldPath, "default.png") != 0 {
			utils.DeleteFile(logoDirectory + oldPath)
		}
		return logoPath
	}
	// sinon on garde l'original
	return oldPath
}

func updateUserPassword(userID guuid.UUID, newPassword string) int {
	if !checkUserExists(userID) {
		return db.NoItemFound
	}
	return db.UpdateUserPassword(userID, newPassword)
}

// TODO not this way to change email
// - create entry in db (email2 ?)
// - send confirmation email to new email
// - change email2 to email 1 if clic on verification link
func updateUserEmail(userID guuid.UUID, newEmail string) int {
	if !checkUserExists(userID) {
		return db.NoItemFound
	}
	return db.UpdateUserEmail(userID, newEmail)
}

// COMPANY

// get company profile from PublicID
func getCompanyProfileFromPublicID(publicID string) (dbmodels.Company, int) {
	var ret dbmodels.Company
	userID, errint := db.GetUserID(publicID)
	if errint != db.CompletedWithNoError {
		return ret, errint
	}
	return getCompanyProfile(userID)
}

// get company profile from webuser ID
func getCompanyProfile(userID guuid.UUID) (dbmodels.Company, int) {
	ret, errint := db.GetCompany(userID)
	if errint == db.CompletedWithNoError {
		// get stats
		ret.Stats = companyJobStat(userID)
		// paillette
		ret.Name = strings.Title(ret.Name)
		ret.Location.Label = strings.Title(ret.Location.Label)
	}
	return ret, db.CompletedWithNoError
}

// update a copany profile
func updateCompanyProfile(companyProfile uimodels.CompanyProfile) int {
	// maj Apps
	errint := db.UpdateCompanyAppsProfile(companyProfile.UserID, companyProfile.Apps)
	if errint != db.CompletedWithNoError {
		return errint
	}
	// maj other profile  stuff
	return db.UpdateCompanyProfile(companyProfile.UserID, companyProfile.Name, companyProfile.Founded, companyProfile.Description, companyProfile.Phone, companyProfile.Website,
		companyProfile.Linkedin, companyProfile.Twitter, companyProfile.Facebook, companyProfile.Instagram,
		companyProfile.Industry.ID, companyProfile.Location.ID, companyProfile.CompanySize.ID)
}

// AJAX
func searchLocations(keyword string) []dbmodels.Location {
	matchingLocations, err := db.SearchLocations(keyword, maxItemAutocomplete)
	if err == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "searchLocations", "1", errors.New("failed to retrieve location list"), "keyword", keyword)
	}
	return matchingLocations
}
func searchIndustry(keyword string) []dbmodels.Industry {
	matchingIndustries, err := db.GetIndustryFromKeyword(keyword, maxItemAutocomplete)
	if err == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "searchIndustry", "1", errors.New("failed to retrieve industry list"), "keyword", keyword)
	}
	return matchingIndustries
}
func searchTags(keyword string) []dbmodels.Tag {
	matchingTags, err := db.GetTagsFromKeyword(keyword, true, maxItemAutocomplete)
	if err == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "searchTags", "1", errors.New("failed to retrieve tag list"), "keyword", keyword)
	}
	return matchingTags
}
func searchLocationNearMe(lat, lon string) []dbmodels.Location {
	matchingLocations, err := db.GetLocationNearMe(lat, lon)
	if err == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "searchLocationNearMe", "1", errors.New("failed to retrieve location list"), "lat", lat, "lon", lon)
	}
	return matchingLocations
}

func getJobsForCompany(companyID guuid.UUID) ([]dbmodels.JobStat, []dbmodels.JobStat, []dbmodels.JobStat) {
	pending, online, archived, err := db.GetJobsCompanyDashboard(companyID)
	if err == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "getJobsForCompany", "1", errors.New("failed to retrieve company's jobs list"), "companyID", companyID.String())
	}
	return pending, online, archived
}

// JOBS
// InputLocation   []int64
// InputRadiusID   int64
// InputContrat    []int64
// InputExperience []int64
// InputSalary     []int64
// InputTime       []int64
// InputRemote     []int64
// InputSorting    []int64
func getJobsList(inputData uimodels.JobsInputSearchFilters, user *guuid.UUID, indexStart, indexEnd int) ([]dbmodels.JobPreview, int, int) {
	var req db.JobListRequest
	req.IndexStart = indexStart
	req.IndexEnd = indexEnd
	req.Filters = []db.RequestFilter{}
	// prepare filter
	// if len(inputData.Q) != 0 {
	// 	// if similar to a knew tab
	// 	if tag, err := db.GetTagsFromKeyword(inputData.Q, false, 1); err == db.CompletedWithNoError && len(tag) > 0 {
	// 		inputData.Tag = append(inputData.Tag, tag[0].ID)
	// 	} else {
	// 		// or add keyword filter
	// 		req.Filters = append(req.Filters, db.RequestFilter{
	// 			FilterType: db.KeywordF,
	// 			FString:    strings.ReplaceAll(inputData.Q, " ", "|"),
	// 		})
	// 	}
	// }
	revel.AppLog.Error(">>>>>>>>> reveive jobs inputData.Q", "inputData.Q", utils.ToString(inputData.Q))
	if len(inputData.Q) > 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.TagsSearchF,
			FIntArray:  inputData.Q,
		})
	}
	if len(inputData.Contrat) > 0 {
		// add filter
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.ContratF,
			FIntArray:  inputData.Contrat,
		})
	}
	if len(inputData.Tag) > 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.TagsF,
			FIntArray:  inputData.Tag,
		})
	}
	if inputData.LocationID != 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.LocationTownF,
			FInt:       inputData.LocationID,
		})
	}
	if len(inputData.Xp) > 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.ExperienceF,
			FIntArray:  inputData.Xp,
		})
	}
	if inputData.SalaryMin > 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.SalaryF,
			FInt:       int64(inputData.SalaryMin),
		})
	}
	if len(inputData.Time) > 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.WorktimeF,
			FIntArray:  inputData.Time,
		})
	}
	if inputData.Nomad == 1 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.RemoteF,
			FBoolean:   true, // 100% nomade
		})
	}
	if inputData.Ronly > 0 {
		// add filter
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.SuggestedJobOnlyF,
			FString:    user.String(),
		})
	}
	if inputData.Sonly > 0 {
		// add filter
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.SavedJobOnlyF,
			FString:    user.String(),
		})
	}
	if len(inputData.Company.PublicID) > 0 {
		req.Filters = append(req.Filters, db.RequestFilter{
			FilterType: db.CompanyF,
			FString:    inputData.Company.PublicID,
		})
	}
	// request
	return db.GetJobs(req, false)
}

func getAggregateJobLink(jobPublicID string) string {
	if len(jobPublicID) != 0 {
		if url, err := db.GetAggregateJobLink(jobPublicID); err == 0 {
			return url
		}
	}
	return ""
}

// view a job
func getJobData(jobPublicID string) *dbmodels.Job {
	// check ID
	if len(jobPublicID) == 0 {
		return nil
	}
	// get job data
	job, errint := db.GetJob(jobPublicID, true)
	if errint != db.CompletedWithNoError {
		if errint == db.UncoverableDatabaseError {
			utils.ManageLog(utils.Error, utils.Controller, "getJobData", "1", errors.New("can't retrieve job data"), "errint", strconv.Itoa(errint))
		}
		return nil
	}
	// get company
	job.Company, errint = db.GetCompany(job.Company.Webuser.ID)
	if errint != db.CompletedWithNoError {
		if errint == db.UncoverableDatabaseError {
			utils.ManageLog(utils.Error, utils.Controller, "getJobData", "2", errors.New("can't retrieve company data"), "errint", strconv.Itoa(errint))
		}
		return nil
	}
	// get similar job
	job.SimilarJobs = db.GetSimilarJobs(job.ID)
	// all good
	return &job
}

func getTagsFromJSON(inputstr string) []dbmodels.Tag {
	ret := []dbmodels.Tag{}
	if len(inputstr) == 0 {
		return ret
	}
	err := json.Unmarshal([]byte(inputstr), &ret)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Controller, "getTagsFromJSON", "unmarshal", err, "inputstr", inputstr)
	}
	return ret
}
