package controllers

import (
	"errors"
	_ "image/jpeg" // meh
	_ "image/png"
	"lt/app/ajobs"
	"lt/app/controllers/uimodels"
	"lt/app/db"
	"lt/app/routes"
	"lt/app/utils"
	"strconv"
	"strings"
	"time"

	guuid "github.com/google/uuid"
	"github.com/revel/revel"
)

// PostLogin process the login
func (c App) PostLogin(user *uimodels.UserLogin) revel.Result {
	c.getSessionUserOrCreate()
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostLogin", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// csfs
	if !c.checkCRFSC("login", user.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostLogin", "Crfsc", errors.New("crfsc verification failed"), "user.Email", user.Email)
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// validation
	user.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Warn, utils.Controller, "PostLogin", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors), "user.Email", user.Email)
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Subscribe)
	}
	// form OK, let's check if user exist in db
	userID, errorval := logUser(user)
	if errorval != nil {
		c.Validation.Errors = append(c.Validation.Errors, errorval)
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Subscribe)
	}
	c.connectUser(userID)
	return c.Redirect(App.Dashboard)
}

// PostLogout Handle users logout
func (c App) PostLogout() revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostLogout", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	c.disconnectUser()
	c.Flash.Out["info"] = "Vous avez été déconnecté."
	return c.Redirect(App.Subscribe)
}

// PostDeleteAccount delete an user account
func (c App) PostDeleteAccount(deleteAccount *uimodels.DeleteAccount) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostDeleteAccount", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Error("Vous devez être connecté pour effectuer cette action.")
		return c.Redirect(App.Subscribe)
	}
	// csfs
	if !c.checkCRFSC("settings", deleteAccount.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostDeleteAccount", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	if userSession.Connected {
		webuser, err := db.GetWebUser(userSession.ID)
		if err != 0 {
			c.Flash.Out["info"] = "Vous êtes déconnecté."
			return c.Redirect(App.Subscribe)
		}
		// deletion
		db.DeleteUser(webuser.ID)
		c.Flash.Success("Votre compte a bien été supprimé. Bonne route, kenavo !")
		c.disconnectUser()
		return c.Redirect(routes.App.Success())
	}
	return c.Redirect(App.Jobs)
}

// PostCreateAccount process user subscription
func (c App) PostCreateAccount(subscription *uimodels.Subscription) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostCreateAccount", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	userSession := c.getSessionUserOrFail()
	if userSession.Connected {
		// already logged
		return c.Redirect(App.Dashboard)
	}
	// if userSession == nil || !userSession.Connected {
	// 	c.Flash.Error("Vous devez être connecté pour effectuer cette action.")
	// 	return c.Redirect(App.Subscribe)
	// }
	// csfs
	if !c.checkCRFSC("subscribe", subscription.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostCreateAccount", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// validate
	subscription.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Info, utils.Controller, "PostCreateAccount", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors), "user.ID", userSession.ID.String())
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Subscribe)
	}
	// Ok, insert new user
	userID, errorval := insertUser(subscription)
	if errorval != nil {
		c.Flash.Error(errorval.Message)
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Subscribe)
	}
	// Ok, connect the created user
	c.connectUser(userID)
	c.Flash.Success("Bienvenue ! Afin de publier votre première annonce, renseignez d'abord le nom de votre entreprise sur la page de profile en cliquant sur 'Publier un emploi'")
	return c.Redirect(App.Dashboard)
}

// PostResetPassword stub
func (c App) PostResetPassword(resetPassword *uimodels.ResetPassword) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostResetPassword", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	userSession := c.getSessionUserOrCreate()
	// already logged
	if userSession.Connected {
		return c.Redirect(App.Dashboard)
	}
	// csfs
	if !c.checkCRFSC("reset", resetPassword.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostResetPassword", "Crfsc", errors.New("crfsc verification failed"), "resetPassword.Email", resetPassword.Email)
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// validate
	resetPassword.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Info, utils.Controller, "PostResetPassword", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors))
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.ResetPassword)
	}
	// demande valide, on check si l'email est dans la DB
	user := db.GetWebUserEmail(resetPassword.Email)
	if user == nil || !user.EmailValidated {
		c.Flash.Out["info"] = "Email inconnu ou non validé." // todo remove this sensitive data ?
	} else {
		c.Flash.Out["info"] = "Vous allez recevoir un mail s'il y a un compte enregistré à cette adresse."

		// UUID specifique
		requestUUID := guuid.New()
		if errint := db.CreatePasswordResetRequest(user.Email, requestUUID); errint != db.CompletedWithNoError {
			c.Flash.Error("Une erreur s'est produit lors de la réinitialisation du mot de passe. Merci de contacter le support.")
			return c.Redirect(App.ResetPassword)
		}

		if err := ajobs.SendUserResetPassword(user.Email, requestUUID.String()); err != nil {
			c.Flash.Error("Une erreur s'est produit lors de la réinitialisation du mot de passe. Merci de contacter le support.")
		}
	}
	return c.Redirect(App.ResetPassword)
}

// PostResetingPassword stub
func (c App) PostResetingPassword(resetingPassword *uimodels.ResetingPassword) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostResetingPassword", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	c.getSessionUserOrCreate()
	// csfs
	if !c.checkCRFSC("reseting", resetingPassword.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostResetingPassword", "Crfsc", errors.New("crfsc verification failed"), "resetingPassword", utils.ToString(resetingPassword))
		c.disconnectUser()
		return c.Redirect(App.ResetPassword)
	}
	// validate
	resetingPassword.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Info, utils.Controller, "PostResetingPassword", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors), "resetingPassword.ID", resetingPassword.ID)
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.ResetingPassword)
	}
	requestUUID, err := guuid.Parse(resetingPassword.ID)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Controller, "PostResetingPassword", "guuid.Parse", err, "Validation.Errors", utils.ToString(c.Validation.Errors), "resetingPassword.ID", resetingPassword.ID)
		return c.Redirect(App.ResetingPassword)
	}
	// actually reset password
	userID, errint := db.GetUserIDPasswordResetRequest(requestUUID)
	if errint != db.CompletedWithNoError {
		c.Flash.Error("Une erreur s'est produite lors de la réinitialisation du mot de passe. Merci de contacter le support.")
		return c.Redirect(App.ResetPassword)
	}
	errint = db.UpdateUserPassword(userID, resetingPassword.Password)
	if errint != db.CompletedWithNoError {
		c.Flash.Error("Une erreur s'est produite lors de la réinitialisation du mot de passe. Merci de contacter le support.")
		return c.Redirect(App.ResetPassword)
	}
	// clear reset password code
	db.DeletePasswordResetRequest(userID)
	c.Flash.Out["info"] = "Nouveau mot de passe enregistré."
	return c.Redirect(App.Subscribe)
}

// PostAddNewJob process adding of new job ===================================
func (c App) PostAddNewJob(newJob *uimodels.NewJob) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostAddNewJob", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	// csfs
	if !c.checkCRFSC("newJob", newJob.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostAddNewJob", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	newJob.UserID = userSession.ID
	// parse form
	c.Request.ParseForm()
	// fill tags
	tagStr := utils.ArrayToString(c.Request.Form["newJob.Tagstr"])
	if len(tagStr) < 50*db.JobMaximumTag { // protection against too much json data sent by client
		newJob.Tags = getTagsFromJSON(tagStr)
	}
	// validate form
	newJob.Validate(c.Validation)
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Info, utils.Controller, "PostAddNewJob", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors))
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.JobCreation)
	}
	errorval := insertBabyJob(newJob)
	if errorval != db.CompletedWithNoError {
		c.Flash.Error("Une erreur est survenue lors de la prise en compte du nouvel emploi. Veuillez nous excuser pour la gène occasionnée.")
		return c.Redirect(routes.App.Oops())
	}
	c.Flash.Out["info"] = "Merci ! Votre annonce est bien enregistrée et sera en ligne dès que la modération l'aura validée."
	return c.Redirect(App.Dashboard)
}

// PostCancelJob cancel a pending job
func (c App) PostCancelJob(postCanceljob *uimodels.PostCanceljob) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostCancelJob", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// check usersession exist
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	if !c.checkCRFSC("dashboard", postCanceljob.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostCancelJob", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// check pending job exist
	jobID, errint := db.PeekJobByIDCompanyStatus(postCanceljob.JobID, userSession.ID, db.JobStatusPending)
	if errint != db.CompletedWithNoError || jobID == nil {
		utils.ManageLog(utils.Warn, utils.Controller, "PostCancelJob", "1", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de l'annulation.")
		return c.Redirect(App.Dashboard)
	}
	// checks passed
	if err := db.DisableJob(*jobID); err != db.CompletedWithNoError {
		c.Flash.Error("Une erreur s'est produite lors de l'annulation.")
		return c.Redirect(App.Dashboard)
	}
	// ok
	return c.Redirect(App.Dashboard)
}

// PostArchiveJob archive a job
func (c App) PostArchiveJob(postArchivejob *uimodels.PostArchivejob) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostArchiveJob", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// check usersession exist
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	if !c.checkCRFSC("dashboard", postArchivejob.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostArchiveJob", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// check job exist
	job, errint := db.GetJob(postArchivejob.JobID, true)
	if errint != db.CompletedWithNoError || job.Company.Webuser.ID != userSession.ID {
		utils.ManageLog(utils.Warn, utils.Controller, "PostArchiveJob", "1", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de l'archivage.")
		return c.Redirect(App.Dashboard)
	}
	// checks passed
	if err := db.ArchiveJob(job.ID, true); err != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "PostArchiveJob", "2", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de l'archivage.")
		return c.Redirect(App.Dashboard)
	}
	return c.Redirect(App.Dashboard)
}

// PostUnArchiveJob stub
func (c App) PostUnArchiveJob(postUnarchivejob *uimodels.PostUnarchivejob) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostUnArchiveJob", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// check usersession exist
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.disconnectUser()
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	if !c.checkCRFSC("dashboard", postUnarchivejob.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUnArchiveJob", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// check job exist
	job, errint := db.GetJob(postUnarchivejob.JobID, false)
	if errint != db.CompletedWithNoError || job.Company.Webuser.ID != userSession.ID {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUnArchiveJob", "1", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de l'unarchivage.")
		return c.Redirect(App.Dashboard)
	}
	if errint := db.ArchiveJob(job.ID, false); errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Error, utils.Controller, "PostUnArchiveJob", "2", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de l'unarchivage.")
		return c.Redirect(App.Dashboard)
	}
	// done
	return c.Redirect(App.Dashboard)
}

// PostDeleteJob stub
func (c App) PostDeleteJob(postDeletejob *uimodels.PostDeletejob) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostDeleteJob", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// get userProfile
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	if !c.checkCRFSC("dashboard", postDeletejob.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostDeleteJob", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// check job exist
	job, errint := db.GetJob(postDeletejob.JobID, false)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "PostDeleteJob", "1", errors.New("failed to delete job given an unknown ID"), "errint", strconv.Itoa(errint), "job's ID", postDeletejob.JobID, "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de la suppression de l'emploi.")
		return c.Redirect(App.Dashboard)
	}
	// security check userID == job.CompanyID
	if userSession.ID != job.Company.Webuser.ID {
		utils.ManageLog(utils.Warn, utils.Controller, "PostDeleteJob", "2", errors.New("wrong user/not owner tried to disable a job"), "user.ID", userSession.ID.String(), "owner: job.Company.Webuser.ID", job.Company.Webuser.ID.String())
		c.Flash.Error("Une erreur s'est produite lors de la suppression de l'emploi.")
		return c.Redirect(App.Dashboard)
	}
	// disabling
	errint = db.DisableJob(job.ID)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "PostDeleteJob", "3", nil, "errint", strconv.Itoa(errint))
		c.Flash.Error("Une erreur s'est produite lors de la suppression de l'emploi.")
		return c.Redirect(App.Dashboard)
	}
	// done
	return c.Redirect(App.Dashboard)
}

// PostUpdateCompanyProfile process a company profile update
func (c App) PostUpdateCompanyProfile(companyProfile *uimodels.CompanyProfile) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostUpdateCompanyProfile", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// get userProfile
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	if !c.checkCRFSC("profile", companyProfile.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUpdateCompanyProfile", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	companyProfile.UserID = userSession.ID
	/*
	 * Parse form
	 */
	c.Request.ParseForm()
	// revel fix checkbox
	companyProfile.Apps = utils.ParseStringArrayToInt64Array(c.Request.Form["companyProfile.Apps"])
	// capitals
	companyProfile.Name = strings.Title(companyProfile.Name)
	// avatar
	c.Params.Bind(&companyProfile.Logo, "companyProfile.Logo") // revel file upload fix
	/*
	 * Validation
	 */
	// validate avatar
	normalizedAvatar := c.validateAvatar(companyProfile.Logo, "companyProfile.Logo")
	// revel validation
	companyProfile.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUpdateCompanyProfile", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors), "user.ID", userSession.ID.String())
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Profile)
	}
	/*
	 * Update
	 */
	// maj avatar (db + current session)
	c.replaceSessionAvatarPath(c.updateAvatar(userSession.ID, normalizedAvatar, userSession.Avatar))

	// maj others data
	errint := updateCompanyProfile(*companyProfile)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Error, utils.Controller, "PostUpdateCompanyProfile", "1", nil, "errint", strconv.Itoa(errint))
		c.Flash.Error("Une erreur s'est produite lors de la mise à jour du profile.")
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Profile)
	}
	// all good
	return c.Redirect(App.Profile)
}

// PostUpdateUserPassword update an user password
func (c App) PostUpdateUserPassword(webUserPasswordSetting *uimodels.WebUserPasswordSetting) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostUpdateUserPassword", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// get userProfile
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	// crfsc
	if !c.checkCRFSC("settings", webUserPasswordSetting.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUpdateUserPassword", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// parse form
	c.Request.ParseForm()
	// check new password
	webUserPasswordSetting.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUpdateUserPassword", "validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors), "user.ID", userSession.ID.String())
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Settings)
	}
	// check password
	userID, err := db.PeekWebUser(userSession.Email, webUserPasswordSetting.Password)
	if err != db.CompletedWithNoError {
		c.Validation.Errors = append(c.Validation.Errors, &revel.ValidationError{Key: "webUserPasswordSetting.Password", Message: "Le mot de passe est invalide."})
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Settings)
	}
	// champ OK, update password
	errint := updateUserPassword(userID, webUserPasswordSetting.NewPassword)
	if errint == db.NoItemFound {
		c.Flash.Error("La page que vous cherchez n'est visible que par les membres connectés.")
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	} else if errint == db.UncoverableDatabaseError {
		utils.ManageLog(utils.Error, utils.Controller, "PostUpdateUserPassword", "1", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur est survenue. Le mot de passe est resté inchangé.")
	} else {
		c.Flash.Success("Mot de passe change avec succes.")
	}
	return c.Redirect(App.Settings)
}

// PostUpdateUserEmail update an user email
func (c App) PostUpdateUserEmail(webUserEmailSetting uimodels.WebUserEmailSetting) revel.Result {
	if revel.DevMode {
		defer utils.ManageLog(utils.Info, utils.Controller, "PostUpdateUserEmail", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// get userProfile
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	// crfsc
	if !c.checkCRFSC("settings", webUserEmailSetting.Crfsc) {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUpdateUserEmail", "Crfsc", errors.New("crfsc verification failed"), "user.ID", userSession.ID.String())
		c.disconnectUser()
		return c.Redirect(App.Subscribe)
	}
	// parse form
	c.Request.ParseForm()
	// check new password
	webUserEmailSetting.Validate(c.Validation)
	// Handle errors
	if c.Validation.HasErrors() {
		utils.ManageLog(utils.Warn, utils.Controller, "PostUpdateUserEmail", "Validation", nil, "Validation.Errors", utils.ToString(c.Validation.Errors), "user.ID", userSession.ID.String())
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Settings)
	}
	// champ OK, update password
	errint := updateUserEmail(userSession.ID, webUserEmailSetting.Email)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Error, utils.Controller, "PostUpdateUserEmail", "1", nil, "errint", strconv.Itoa(errint), "user.ID", userSession.ID.String())
		c.Flash.Error("Une erreur est survenue. L'email est resté inchangé.")
	} else {
		// todo mail de confirmation
		c.Flash.Success("Nouveau mail sauvegardé. Le changement n'est pas encore effectif: un mail de confirmation vous a été envoyé.")
	}
	// done
	return c.Redirect(App.Settings)
}
