package controllers

import (
	"errors"
	"lt/app/ajobs"
	"lt/app/controllers/uimodels"
	"lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/routes"
	"lt/app/utils"
	"net/url"
	"strconv"
	"strings"
	"time"

	guuid "github.com/google/uuid"
	. "github.com/gorilla/feeds"
	"github.com/revel/modules/jobs/app/jobs"
	"github.com/revel/revel"
	"github.com/revel/revel/cache"
)

// async job for mailing

// assync job to delete disable job (status deleted)

// job: add location for not 100% remote

// todo search form : location or 100% remote / remove salary filter

// en fait on peut faire marché un compte sans avoir forcement validé son mail, OU SI ? et demander le code de verif en js
// mais il sera delete apres 1 temps et plusieurs warning TODO)

// todo  recherche cooki fié

// todo roll back commit sql req partially done (can't open that much connection on async job massive load)

// much caching todo

// todo remove visit for candidate but implement message (1 way msg) . they can respond externally

// todo demander a valider l'adresse mail avant de poster une annonce (company) ou de souscrire a une newsletter (candidate)

// todo encard candidat : vous avez vu ces jobs (historique)

// todo javascript encrypted contact

// todo timer et logger tout ce qui se passe (+ metrics)

// App main revel controller
type App struct {
	*revel.Controller
}

/*
**
** BASIC PAGES
**
 */

// /debugshit stub
func (c App) DebugPage() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "DebugPage", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// jobs.Now(ajobs.FeedDBWithLogs{})
	// test job
	jobs.Now(ajobs.AggregateJobFromFeed{})
	// test mail
	// jobs.Now(asyncjobs.DailyNewsletter{})
	// render page
	return c.Render()
}

// About page for contact legal and description
func (c App) About() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "About", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.AboutPageData
	// user session track
	userSession := c.getSessionUserOrCreate()
	pageData.User = userSession
	// render page
	return c.Render(pageData)
}

// RedirectAggregateJob send the user on ths aggregate link page
func (c App) RedirectAggregateJob(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "RedirectAggregateJob", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	c.getSessionUserOrCreate()
	// get job
	url := getAggregateJobLink(id)
	if len(url) == 0 {
		utils.ManageLog(utils.Warn, utils.Controller, "RedirectAggregateJob", "1", errors.New("cet emploi agregé n'existe pas, a été archivé ou pourvu"), "id", id)
		c.Flash.Error("Cet emploi agregé n'existe pas, a été archivé ou pourvu.")
		return c.Redirect(routes.App.Oops())
	}
	// todo add a view to the aggregate job ?
	return c.Redirect(url) // change "/" to url for prod
}

// RedirectApplicationLink send link to mail or website to apply
func (c App) RedirectApplicationLink(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "RedirectApplicationLink", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	utils.ManageLog(utils.Debug, utils.Controller, "RedirectApplicationLink", "", nil)
	// user session refresh
	c.getSessionUserOrCreate()
	// get apply link
	idUUID, URLMailApply, errint := db.GetJobURLMailApply(id)
	if errint != db.CompletedWithNoError {
		utils.ManageLog(utils.Warn, utils.Controller, "RedirectApplicationLink", "1", errors.New("could not find apply link from publicID"), "publicID", id)
		return c.NotFound("Invalid job id #2")
	}
	var URLRedirect = ""
	if uimodels.URLPattern.MatchString(URLMailApply) {
		URLRedirect = URLMailApply
	} else if uimodels.EmailPattern.MatchString(URLMailApply) {
		// fix alias '+'
		URLMailApply = strings.Replace(URLMailApply, "+", "%2b", -1)
		URLRedirect = "mailto:"
		URLRedirect = utils.Join(URLRedirect, URLMailApply)
	} else {
		utils.ManageLog(utils.Error, utils.Controller, "RedirectApplicationLink", "2", errors.New("malformed URLMailApply for job"), "jobUUID", idUUID.String(), "malformed URLMailApply", URLMailApply)
		return c.NotFound("Invalid job id #3")
	}
	// +1 redirection count
	db.IncrementJobRedirectionCount(idUUID)
	// Ok
	return c.Redirect(URLRedirect)
}

// Job show a single job
func (c App) Job(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Job", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.JobPageData
	// user session track
	userSession := c.getSessionUserOrCreate()
	pageData.User = userSession
	// get job
	job := getJobData(id)
	if job == nil {
		pageData.Job = nil
		c.ViewArgs["jobID"] = id
		return c.Render(pageData)
	}
	// cache session to count page view
	viewed := 0
	if err := cache.Get("job_"+id+"_viewuser_"+userSession.GetUniqueID()+"count", &viewed); err != nil {
		incrementJobView(job.ID)
		go cache.Set("job_"+id+"_viewuser_"+userSession.GetUniqueID()+"count", 1, 60*time.Minute)
	}
	// prepare page data
	pageData.Job = job
	return c.Render(pageData)
}

// JobsFeed a rss job feed
func (c App) JobsFeed() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "JobsFeed", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// parse params
	var queryFilter uimodels.JobsInputSearchFilters
	// // url = /foo?sort=asc&active=1
	queryFilter.Q = utils.ParseQueryIntegerArray(c.Params.Query.Get("q"), " ")
	queryFilter.Contrat = utils.ParseQueryIntegerArray(c.Params.Query.Get("contrat"), " ")
	queryFilter.Xp = utils.ParseQueryIntegerArray(c.Params.Query.Get("xp"), " ")
	queryFilter.Time = utils.ParseQueryIntegerArray(c.Params.Query.Get("time"), " ")
	queryFilter.SalaryMin = utils.StringToIntOrZero(c.Params.Query.Get("salary"))
	queryFilter.Tag = utils.ParseQueryIntegerArray(c.Params.Query.Get("tag"), " ")
	queryFilter.Nomad = utils.StringToIntOrZero(c.Params.Query.Get("nomad"))
	queryFilter.LocationID = utils.ConvertStringToInt64(c.Params.Query.Get("location")) // id, isdept
	queryFilter.Radius = utils.StringToIntOrZero(c.Params.Query.Get("radius"))
	queryFilter.P = utils.StringToIntOrZero(c.Params.Query.Get("p"))
	// set page 1 par defaut
	if queryFilter.P <= 0 {
		queryFilter.P = 1
	}
	// set user filter false if user is not connecter
	queryFilter.Ronly = 0
	queryFilter.Sonly = 0
	// fill locations data to render page (id, isdept, cp, label)
	queryFilter.Location = getLocation(queryFilter.LocationID)
	// fill contrat data to render page (id, name)
	queryFilter.Contrats = getContrats(queryFilter.Contrat)
	// fill Times
	queryFilter.Times = getJobTime(queryFilter.Time)
	// fill Xps
	queryFilter.Xps = getExperiences(queryFilter.Xp)
	// fill tag
	queryFilter.Tags = getTags(queryFilter.Tag)
	// retrieve corresponding jobs
	var pageData uimodels.JobsPageData
	pageData.Jobs, _, _ = getJobsList(queryFilter, nil, 0, 20)

	now := time.Now()

	feed := &Feed{
		Title:       "Emploi Nomade RSS",
		Link:        &Link{Href: "https://emploinomade.fr"},
		Description: "Trouver l'emploi nomade de vos rêves",
		Author:      &Author{Name: "Emploi Nomade", Email: "contact@emploinomade.fr"},
		Created:     now,
		Copyright:   strconv.Itoa(now.Year()) + " © Emploi Nomade",
	}
	feed.Items = []*Item{}

	for _, v := range pageData.Jobs {
		tmp := &Item{
			Title:       v.Position,
			Link:        &Link{Href: "http://localhost:9000/job/" + v.PublicID},
			Description: utils.PreviewText(v.Description, 400),
			Author:      &Author{Name: "Emploi Nomade", Email: "contact@emploinomade.fr"},
			Created:     now,
		}
		feed.Items = append(feed.Items, tmp)
	}

	rss, _ := feed.ToRss()
	return c.RenderText(rss) // return c.RenderXML(rss)
}

// Jobs browse a list of job
func (c App) Jobs() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Jobs", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	userSession := c.getSessionUserOrCreate()
	var queryFilter uimodels.JobsInputSearchFilters
	m, err := url.ParseQuery(c.Params.Query.Encode())
	if err == nil {
		c.Params.Values = m
		queryFilter = parseJobsParameter(m, userSession.Connected)
	}
	// retrieve corresponding jobs
	var pageData uimodels.JobsPageData
	var errint int
	pageData.Jobs, pageData.TotalItem, errint = getJobsList(queryFilter, &userSession.ID, queryFilter.JobIndexStart, maxJobShowedInJobList)
	if errint != 0 {
		utils.ManageLog(utils.Crit, utils.Controller, "Jobs", "1", errors.New("can't retrieve Jobs list"), "errint", strconv.Itoa(errint))
	}
	// request fill
	pageData.Page = queryFilter.P
	pageData.HasPrev, pageData.HasNext, pageData.TotalPage = pager(queryFilter.P, pageData.TotalItem)
	pageData.User = userSession
	// cached tags as an help-search
	pageData.MostUsedTags = getMostUsedTags()
	// refill Filter
	c.ViewArgs["queryFilter"] = queryFilter
	// store query for ajax inifinite page index
	c.Session["queryFilter"] = queryFilter

	revel.AppLog.Info(">>>>>>>>> reveive jobs query", "query", utils.ToString(queryFilter))
	// render page
	return c.Render(pageData)
}

// // Login log an user
// func (c App) Login() revel.Result {
// 	// metrics
// 	if utils.MetricsMonitoring {
// 		defer utils.ManageLog(utils.Info, utils.Controller, "Login", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
// 	}
// 	var pageData uimodels.LoginPageData
// 	// user session track
// 	userSession := c.getSessionUserOrCreate()
// 	// already logged
// 	if userSession.Connected {
// 		return c.Redirect(App.Dashboard)
// 	}
// 	pageData.User = userSession
// 	c.ViewArgs["pageData"] = pageData
// 	// cfrs
// 	c.addCRFSC("login")
// 	// render page
// 	return c.Render(pageData)
// }

// Subscribe permit subscription
func (c App) Subscribe() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Subscribe", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.SubscribePageData
	// user session track
	userSession := c.getSessionUserOrCreate()
	pageData.User = userSession
	// already logged
	if userSession.Connected {
		c.ViewArgs["pageData"] = pageData
		return c.Redirect(App.Dashboard)
	}
	// cfrs
	c.addCRFSC("subscribe")
	// podium
	pageData.TopCompanies = getTopCompanies(3)
	// render page
	return c.Render(pageData)
}

// ResetPassword stub
func (c App) ResetPassword() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "ResetPassword", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.PasswordResetPageData
	// user session track
	userSession := c.getSessionUserOrCreate()
	// already logged
	if userSession.Connected {
		return c.Redirect(App.Dashboard)
	}
	// csfs
	c.addCRFSC("reset")
	pageData.User = userSession
	// render page
	return c.Render(pageData)
}

// ResetingPassword stub
func (c App) ResetingPassword(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "ResetingPassword", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.PasswordResetPageData
	// user session track
	pageData.User = c.getSessionUserOrCreate()
	// check if userID valid && exist
	requestUUID, err := guuid.Parse(id)
	if err != nil {
		// todo monitor this error
		return c.NotFound("id invalid")
	}
	companyUUID, errint := db.GetUserIDPasswordResetRequest(requestUUID)
	if errint != db.CompletedWithNoError {
		return c.NotFound("id invalid2")
	}
	// csfs
	c.addCRFSC("reseting")
	// ok
	pageData.User.ID = companyUUID
	pageData.PasswordRequestUUID = requestUUID
	// render page
	return c.Render(pageData)
}

// Company show profile of a company
func (c App) Company(publicid string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Company", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.SocialPageData
	// user session refresh
	pageData.User = c.getSessionUserOrCreate()
	// we receive a readable public_id (a string) from this url (not a guuid)
	profile, errint := getCompanyProfileFromPublicID(publicid)
	if errint != 0 {
		utils.ManageLog(utils.Warn, utils.Controller, "Company", "1", errors.New("can't find profile of a company"), "Company Public ID", publicid, "errint", strconv.Itoa(errint))
		// render page
		pageData.CompanyProfile = nil
		return c.Render(pageData)
	}
	// oki
	pageData.CompanyProfile = &profile
	// render page
	return c.Render(pageData)
}

// CompanyJobs a company's jobs /company/:id/jobs
func (c App) CompanyJobs(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "CompanyJobs", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.ListSocialPageData
	// user session track
	userSession := c.getSessionUserOrCreate()
	pageData.User = userSession
	return c.Render(pageData)
}

// Companies show a list of all company
func (c App) Companies() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Companies", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var pageData uimodels.ListSocialPageData
	// user session track
	userSession := c.getSessionUserOrCreate()
	pageData.User = userSession
	// get companies
	pageData.Companies = getTopCompanies(maxCompaniesShowedInCompaniesList)
	// ok
	return c.Render(pageData)
}

/*
**
** LOGGED ONLY COMMONS PAGES
**
 */

// Dashboard show the user space
func (c App) Dashboard() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Dashboard", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session check
	userSession := c.getSessionUserOrCreate()
	if !userSession.Connected {
		c.Flash.Error("La page que vous cherchez n'est visible que par les membres connectés.")
		return c.Redirect(App.Subscribe)
	}
	if userSession.Connected {
		var pageData uimodels.CompanySpaceData
		// csfs
		pageData.Jobs.Crfsc = c.addCRFSC("dashboard")
		// get data about user
		updateUserSession(userSession.ID, userSession)
		// userSession.ProfileCompletness = db.GetPercentProfileFilled(userSession.ID)
		pageData.User = userSession
		// stats
		pageData.Stats = companyJobStat(userSession.ID)
		// jobs
		pageData.Jobs.PendingJobs, pageData.Jobs.OnlineJobs, pageData.Jobs.ArchivedJobs = getJobsForCompany(userSession.ID)
		return c.Render(pageData)
	}
	return c.Redirect(App.Subscribe)
}

// Profile edit an user profile
func (c App) Profile(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Profile", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Error("La page que vous cherchez n'est visible que par les membres connectés.")
		return c.Redirect(App.Subscribe)
	}
	if userSession.Connected {
		// csfs
		c.addCRFSC("profile")
		var pageData uimodels.ProfilePageData
		// user session track
		pageData.User = *userSession
		// prefill CompanyProfile
		companyProfile, errint := getCompanyProfile(pageData.User.ID)
		if errint != db.CompletedWithNoError {
			utils.ManageLog(utils.Error, utils.Controller, "Profile", "1", errors.New("weird unknown company"), "Company Public ID", pageData.User.ID.String(), "errint", strconv.Itoa(errint))
			c.disconnectUser()
			return c.Redirect(App.Subscribe)
		}
		pageData.CompanyProfile = companyProfile
		c.ViewArgs["companyProfile"] = pageData.CompanyProfile
		// get all industry to show
		c.ViewArgs["companyIndustry"] = db.GetAllIndustry()
		return c.Render(pageData)
	}
	return c.Redirect(App.Subscribe)
}

// Settings settings page (change password, manage data, delete account)
func (c App) Settings() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Settings", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	userSession := c.getSessionUserOrCreate()
	if userSession.Connected {
		var pageData uimodels.ProfilePageData
		// user session track
		pageData.User = userSession
		// csfs
		c.addCRFSC("settings")
		return c.Render(pageData)
	}
	return c.Redirect(App.Subscribe)
}

/*
**
** COMPANY PAGES
**
 */

// JobCreation page to add a job
func (c App) JobCreation() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "JobCreation", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	var pageData uimodels.JobCreationPageData
	// user session track
	pageData.User = *userSession
	// if user did not fill its profile at minimum level, redirect to profile page
	_, namefilled := db.GetPercentProfileFilled(pageData.User.ID)
	if !namefilled {
		c.Flash.Out["info"] = "Veillez renseigner le nom de votre entreprise avant de publier une annonce"
		return c.Redirect(App.Profile)
	}
	// csfs
	c.addCRFSC("newJob")
	// if yes, fill page data with job data (post: will update baby job in db)
	return c.Render(pageData)
}

// JobPreview page to preview the job being created
func (c App) JobPreview() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "JobPreview", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// user session track
	userSession := c.getSessionUserOrFail()
	if userSession == nil || !userSession.Connected {
		c.Flash.Out["info"] = "Votre session a expiré, veuillez vous reconnecter."
		return c.Redirect(App.Subscribe)
	}
	var pageData uimodels.JobPreviewPageData
	// user session track
	pageData.User = *userSession
	// company data
	var err int
	pageData.Job.Company, err = db.GetCompany(userSession.ID)
	if err != db.CompletedWithNoError {
		utils.ManageLog(utils.Error, utils.Controller, "JobPreview", "1", errors.New("failed to get company from userSession.ID"), "userSession.ID", userSession.ID.String())
		c.Flash.Error("Une erreur s'est produite.")
		return c.Redirect(routes.App.Oops())
	}
	// fill GET datas
	pageData.Job.Position = c.Params.Query.Get("position")
	pageData.Job.Description = c.Params.Query.Get("description")
	pageData.Job.Salary = utils.StringToIntOrZero(c.Params.Query.Get("salary"))
	pageData.Job.Contrat.Label = c.Params.Query.Get("contrat")
	pageData.Job.FullRemote = (utils.StringToIntOrZero(c.Params.Query.Get("remote")) == 1)
	pageData.Job.WorkTime.Label = c.Params.Query.Get("worktime")
	pageData.Job.Experience.Label = c.Params.Query.Get("experience")
	for _, v := range strings.Split(c.Params.Query.Get("tags"), ",") {
		v = strings.Trim(v, " ")
		if len(v) != 0 {
			var tag dbmodels.Tag
			tag.Label = v
			pageData.Job.Tags = append(pageData.Job.Tags, tag)
		}
	}
	pageData.Job.Regional, _ = db.GetRegional(utils.ConvertStringToInt64(c.Params.Query.Get("regional")))
	pageData.Job.Company.Webuser.Avatar = pageData.User.Avatar
	return c.Render(pageData)
}

// NOT IN USE JobUpdate stub TODO
func (c App) JobUpdate() revel.Result {
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "JobUpdate", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	return c.Render()
}

/*
**
** TOOLS PAGES
**
 */

// NewsletterConfirm Newsletter Confirmation page
// /newsletter/confirm/fc3f300e-24a0-43b0-a1b2-fd51119db066
func (c App) NewsletterConfirm(id string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "NewsletterConfirm", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// parse input id
	activationCodeUUID, err := guuid.Parse(id)
	if err != nil {
		c.Flash.Error("Requête invalide.")
		return c.Redirect(routes.App.Oops())
	}
	errint := db.ActivateNewsletter(activationCodeUUID)
	if errint != 0 {
		c.Flash.Error("Ce code d'activation est invalide.")
		return c.Redirect(routes.App.Oops())
	}
	c.Flash.Success("Merci pour votre inscription. Vous aurez bientôt des nouveaux emplois de notre part dans votre boîte mail !")
	return c.Redirect(routes.App.Success())
}

// NewsletterUnsubscribe for unsub
func (c App) NewsletterUnsubscribe(activationcode string) revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "NewsletterUnsubscribe", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// session
	c.getSessionUserOrCreate()
	// check uuid code
	checkedCode, err := guuid.Parse(activationcode)
	if err != nil {
		return c.Redirect(App.NotFound)
	}
	// delete sub
	errint := db.DeleteNewletterSub(checkedCode)
	if errint != 0 {
		c.Flash.Error("Requête invalide.")
		return c.Redirect(routes.App.Oops())
	}
	c.Flash.Success("Vous avez bien été désincrit(e). Merci et bonne route !")
	return c.Redirect(routes.App.Success())
}

/*
**
** OTHERS PAGES
**
 */

// Oops emet une page d'error style "something went wrong"
// A alimenter avec flash cookies puis redirect:
// c.Flash.Error(msg)
// return c.Redirect(routes.App.Oops())
func (c App) Oops() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Oops", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// refresh session
	userSession := c.getSessionUserOrCreate()
	if len(c.Flash.Data["error"]) == 0 {
		// no error
		return c.Redirect(App.Jobs)
	}
	var pageData uimodels.MessagePageData
	pageData.User = userSession
	return c.Render(pageData)
}

// Success emet une page de validation
func (c App) Success() revel.Result {
	// metrics
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Controller, "Success", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// refresh session
	userSession := c.getSessionUserOrCreate()
	if len(c.Flash.Data["success"]) == 0 {
		// no message
		return c.Redirect(App.Jobs)
	}
	var pageData uimodels.MessagePageData
	pageData.User = userSession
	return c.Render(pageData)
}
