package controllers

import "math"

const (
	_      = iota
	kb int = 1 << (10 * iota)
	mb
	gb
)

const maxItemAutocomplete = 10

const maxJobShowedInJobList = 20

const maxCompaniesShowedInCompaniesList = 50

const maxTagsShowedInCompaniesList = 4

const jobDaysUntilExpiration = 90

const userSessionExpirationMin = 300

const maxIndustryByCompany = 5

const maxTagsToShowInQuerySearch = 15

const logoDirectory = "public/img/avatars/"
const logoResizeWidth = 250

const resumeDirectory = "public/doc/resumes/"

// tools

func pager(index, nbTotalResult int) (bool, bool, int) {
	var hasPrev bool
	var hasNext bool
	var totalPages int

	hasPrev = (index > 1)
	totalPages = int(math.Round(float64(nbTotalResult) / float64(maxJobShowedInJobList)))
	hasNext = index < totalPages

	return hasPrev, hasNext, totalPages
}
