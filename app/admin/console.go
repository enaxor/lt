package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strconv"
	"strings"

	guuid "github.com/google/uuid"
	"github.com/lib/pq"
	"github.com/ventu-io/go-shortid"
)

const dbHost = "localhost"
const dbName = "lt"
const dbUser = "shgz"
const dbPassword = ""

var db *sql.DB
var sid *shortid.Shortid

const colorReset = "\033[0m"
const colorRed = "\033[31m"
const colorGreen = "\033[32m"
const colorYellow = "\033[33m"
const colorBlue = "\033[34m"
const colorPurple = "\033[35m"
const colorCyan = "\033[36m"
const colorWhite = "\033[37m"

func main() {
	initDB()

	// main menu
	fmt.Println("Hello")
menu:
	fmt.Println("1 == Logs Stats")
	fmt.Println("2 == Launch dev Tests")
	fmt.Println("3 == Manage pending jobs")
	fmt.Println("q == Quit")

	fmt.Println("-------------------------")
	// Taking input from user
	var input string
	fmt.Scanln(&input)
	// check input
	switch input {
	case "1":
		ret := getTodaysLogsStat()
		fmt.Println("count INFO", ret.Info)
		fmt.Println("count DEBUG", ret.Debug)
		fmt.Println("count WARN", ret.Warn)
		fmt.Println("count ERROR", ret.Error)
	readlog:
		fmt.Println("See logs ? (INFO|DEBUG|WARN|ERROR)")
		var logLevelInput string
		fmt.Scanln(&logLevelInput)
		var color string
		switch logLevelInput {
		case "INFO":
			color = colorWhite
		case "DEBUG":
			color = colorCyan
		case "WARN":
			color = colorYellow
		case "ERROR":
			color = colorRed
		default:
			// back to menu
			goto menu
		}
		fmt.Println(color)
		for _, msg := range getTodaysLogsByLevel(logLevelInput) {
			fmt.Println("> ", msg.Message)
			fmt.Print("... and ", msg.Count, " more identical log\n\n")
		}
		fmt.Println(colorReset)
		goto readlog
	case "2":
		// launch revel test . dev
		out, err := exec.Command("/home/shgz/devel/go/bin/revel", "test", "/home/shgz/devel/go/src/lt", "dev").Output()
		if err != nil {
			panic(err)
		}
		_, err = ioutil.ReadFile("./test-results/cat result.failed")
		if err != nil {
			fmt.Println(colorRed, "TESTS FAILED", colorReset)
			fmt.Println("See tests logs ? (y/*)")
			var readLogInput string
			fmt.Scanln(&readLogInput)
			switch readLogInput {
			case "y":
				fmt.Println("out", string(out))
			}
		} else {
			fmt.Println(colorGreen, "TESTS PASSED", colorReset)
		}
	case "3":
		pendingJobCount := getPendingJobsCount()
		fmt.Println("count pending jobs = ", pendingJobCount)
		if pendingJobCount > 0 {
			fmt.Println("Start review ? (y/*)")
			fmt.Scanln(&input)
			switch input {
			case "y":
				// get pendings job
				pendingJobs := getPendingJobs()
				// foreach job
				for _, job := range pendingJobs {
					fmt.Println(job.toString())
					// validate / invalidate
					fmt.Println("Valide this job ? (y/n)")
					var validationInput string
					fmt.Scanln(&validationInput)
					switch validationInput {
					case "y":
						jobValidation(job.ID, true)
					case "n":
						jobValidation(job.ID, false)
					default:
						fmt.Println(colorRed + "Aborted." + colorReset)
						goto menu
					}
				}
				fmt.Println(colorGreen + "All jobs reviewed." + colorReset)
			}
		}
	case "q":
		return
	default:
		fmt.Println("unknow input")
	}
	fmt.Println("-------------------------")
	// return to menu
	goto menu
}

func initDB() {
	_ = pq.Efatal // fix a weird connection problem
	var err error
	dbinfo := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable", dbHost, dbUser, dbName)
	db, err = sql.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}
	// sid generator
	sid, err = shortid.New(1, shortid.DefaultABC, 71421)
	if err != nil {
		panic(err)
	}
}

// LogsStat stub
type logsStat struct {
	Debug int // 1
	Info  int // 2
	Warn  int // 3
	Error int // 4
}

const getTodayLogStatSQL = "select " +
	"(select count(*) from private.logs where lvl = 'INFO'), " +
	"(select count(*) from private.logs where lvl = 'DEBUG'), " +
	"(select count(*) from private.logs where lvl = 'WARN'), " +
	"(select count(*) from private.logs where lvl = 'ERROR')"

func getTodaysLogsStat() *logsStat {
	var ret logsStat
	// prepare
	stmt, err := db.Prepare(getTodayLogStatSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow()
	// scqn
	err = row.Scan(&ret.Info, &ret.Debug, &ret.Warn, &ret.Error)
	if err != nil {
		fmt.Println("GetTodaysLogsStat err", err)
		return nil
	}
	return &ret
}

// MessageLogs stub
type messageLogs struct {
	Message string
	Count   int
}

// "select message from private.logs where message ->> 'lvl' = $1"
// CONCAT(message ->> 'msg' , message ->> 'error') as error

const getTodaysLogsByLevelSQL = " select (select json from private.logs where id = ids[1]), mcount from " +
	" ( select msg as msg, array_agg(id) AS ids, count(*) AS mcount from private.logs where lvl = $1 GROUP BY msg) lv "

func getTodaysLogsByLevel(level string) []messageLogs {
	var ret []messageLogs
	// prepare
	stmt, err := db.Prepare(getTodaysLogsByLevelSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query(level)
	if err != nil {
		panic(err)
	}
	// scqn
	for rows.Next() {
		var msg messageLogs
		err = rows.Scan(&msg.Message, &msg.Count)
		if err != nil {
			panic(err)
		}
		ret = append(ret, msg)
	}
	return ret
}

const getPendingJobsCountSQL = "select count(*) from job where job.status = 1"

// getPendingJobs stub
func getPendingJobsCount() int {
	var ret int
	// prepare
	stmt, err := db.Prepare(getPendingJobsCountSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	row := stmt.QueryRow()
	// scqn
	err = row.Scan(&ret)
	if err != nil {
		fmt.Println("GetTodaysLogsStat err", err)
		return 0
	}
	return ret
}

// pov com job
type job struct {
	// private data
	ID guuid.UUID
	// public job data
	PublicID    string
	Position    string
	Description string
	// apply info
	URLMailApply string
	Since        string
	// indicator
	Salary     string
	Contrat    string
	Experience string
	Regional   string
	WorkTime   string
	Tags       []string
	FullRemote string
	Featured   string
	// company
	Company string
}

// ToString debug info about a webuser
func (j *job) toString() string {
	var sb strings.Builder
	sb.WriteString("-------------------------\n")
	sb.WriteString("Job ID = '" + j.ID.String() + "'\n")
	sb.WriteString("Job PublicID = '" + j.PublicID + "'\n")
	sb.WriteString("Job Position = '" + j.Position + "'\n")
	sb.WriteString("Job Description = '" + j.Description + "'\n")
	sb.WriteString("Job URLMailApply = '" + j.URLMailApply + "'\n")
	sb.WriteString("Job Since = '" + j.Since + "'\n")
	sb.WriteString("Job Salary = '" + j.Salary + "'\n")
	sb.WriteString("Job Contrat = '" + j.Contrat + "'\n")
	sb.WriteString("Job Experience = '" + j.Experience + "'\n")
	sb.WriteString("Job Regional = '" + j.Regional + "'\n")
	sb.WriteString("Job WorkTime = '" + j.WorkTime + "'\n")
	sb.WriteString("Job Tags = " + strconv.Itoa(len(j.Tags)) + " ")
	for _, tag := range j.Tags {
		sb.WriteString(tag + ",")
	}
	sb.WriteString("\n")
	sb.WriteString("Job FullRemote = '" + j.FullRemote + "'\n")
	sb.WriteString("Job Featured = '" + j.Featured + "'\n")
	sb.WriteString("Job Company = '" + j.Company + "'\n")
	sb.WriteString("-------------------------\n")
	return sb.String()
}

const getPendingJobsSQL = " select " +
	" id, " +
	" public_id,position, description,  " +
	" url_or_mail_apply,post_date,  " +
	" salary,contrat,experience,regional,worktime,  " +
	" COALESCE((select array_agg(tag.label) from tag where tag.id = ANY((select COALESCE(array_agg(job_tag.tag_id),ARRAY[]::integer[]) from job_tag where job_tag.job_id = mj.id)::int[])),ARRAY[]::varchar[]) as tags,  " +
	" full_remote,featured,  " +
	" (select company_name from company where company.user_id = company_id)  " +
	" from job mj where mj.status = 1"

// getPendingJobs stub
func getPendingJobs() []job {
	var ret []job
	// prepare
	stmt, err := db.Prepare(getPendingJobsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	rows, err := stmt.Query()
	if err != nil {
		panic(err)
	}
	// scqn
	for rows.Next() {
		var j job
		var tagsstr pq.StringArray
		err = rows.Scan(&j.ID, &j.PublicID, &j.Position, &j.Description, &j.URLMailApply, &j.Since, &j.Salary, &j.Contrat, &j.Experience, &j.Regional, &j.WorkTime, &tagsstr, &j.FullRemote, &j.Featured, &j.Company)
		if err != nil {
			panic(err)
		}
		j.Tags = tagsstr
		ret = append(ret, j)
	}
	return ret
}

const valideJobsSQL = "UPDATE job set status = $2 where job.ID = $1"

func jobValidation(jobID guuid.UUID, validated bool) {
	status := 5
	if validated {
		status = 2
	}
	// prepare
	stmt, err := db.Prepare(valideJobsSQL)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	// query
	res, err := stmt.Exec(jobID, status)
	if err != nil {
		panic(err)
	}
	if nbr, err := res.RowsAffected(); err != nil || nbr != 1 {
		fmt.Println("Bad result affected rows = ", nbr)
		panic(err)
	}
	// all good
}
