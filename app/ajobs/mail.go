package ajobs

import (
	"fmt"
	db "lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/utils"

	"github.com/revel/revel"
	"gopkg.in/gomail.v2"
)

// TODO omg i think this need to be in a async job

// SendMailWithParams previously named after PrepareMailAndSendItNowIfYouWant
func SendMailWithParams(mail dbmodels.MailQueueItem, sendNow bool) {
	id, err := db.CreateMailToQueue(mail)
	if err != 0 {
		fmt.Println("ERROR SendMailWithParams while creating new mail", "mail", utils.ToString(mail), "sendNow", sendNow, "err", err)
	} else if sendNow {
		fmt.Println("INFO SendMailWithParams OK will send mail in q with id", id)
		SendMail(id)
	}
}

// SendAllMail send all mails waiting in database Q
func SendAllMail() {
	// select mail in q
	q := db.GetNewletterQueue()
	// send a message for each message in q
	for _, mail := range q {
		SendMail(mail.ID)
	}
}

// SendMail send a specific mail in database Q
// param: id of mail in db
func SendMail(mailID int64) {
	// get mail data
	mail := db.GetMailInQueue(mailID)
	// validate mail to send
	if mail == nil || mail.Sent {
		// already sent or ID does not exist
		fmt.Println("INFO sendMail encoutered invalid mail id:", utils.ToString(mailID))
		return
	}
	// create message and send it
	m := gomail.NewMessage()
	m.SetHeader("From", gandiEmailID)
	m.SetHeader("To", mail.Email)
	m.SetHeader("Subject", mail.Title)
	m.SetBody("text/plain", mail.ContentText)
	m.AddAlternative("text/html", mail.ContentHTML)

	if revel.DevMode {
		fmt.Println(">>>>>>>> MOCK MAILLING: mail sent", utils.ToString(mail))
	} else {
		// send it
		d := gomail.NewDialer(gandiMailServer, mailSendSMTPPort, gandiEmailID, gandiMailPwd)
		err := d.DialAndSend(m)
		if err != nil {
			fmt.Println("ERROR while sending a mail method=SendMail", "mail", utils.ToString(mail), "err", err.Error())
			return
		}
		fmt.Println("INFO sendMail sent", "mail", utils.ToString(mail.Email))
	}
	// set message to "sent"
	err := db.SetNewletterItemSent(mail.ID)
	if err != nil {
		fmt.Println("ERROR setting mail sent to true", "mail.ID", utils.ToString(mailID))
	}
}
