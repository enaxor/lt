package aggregator

import (
	"encoding/xml"
	"sort"
	"strings"
	"time"
)

const (
	// EmploiStore 1
	EmploiStore int = 1
	// StackOverflow 2
	StackOverflow int = 2
	// RemoteOk 3
	RemoteOk int = 3
	// Indeed 4
	Indeed int = 4
	// LinuxJobs 5
	LinuxJobs int = 5
	// RemotiveJob 6
	RemotiveJob int = 6
)

func cleanTags(inputTags []string) []string {
	var ret []string
	for _, vtag := range inputTags {
		tag := strings.ToLower(strings.TrimSpace(vtag))
		if len(tag) != 0 {
			ret = append(ret, tag) // rewrite tag, trim and lowered
		}
	}
	// alpha sort
	sort.Strings(ret)
	return ret
}

type RemotiveJobsFeed struct {
	Legal_notice string            `json:"0-legal-notice"`
	Job_count    int               `json:"job_count"`
	Jobs         []RemotiveJobFeed `json:"jobs"`
}

type RemotiveJobFeed struct {
	ID                          int      `json:"id"`
	URL                         string   `json:"url"`
	Title                       string   `json:"title"`
	Company_name                string   `json:"company_name"`
	Category                    string   `json:"category"`
	Tags                        []string `json:"tags"`
	Job_type                    string   `json:"job_type"`
	Publication_date            string   `json:"publication_date"`
	Candidate_required_location string   `json:"candidate_required_location"`
	Salary                      string   `json:"salary"`
	Description                 string   `json:"description"`
	Company_logo_url            string   `json:"company_logo_url"`
}

// EmploiStoreFeed to deserialise emploi-store feed
type EmploiStoreFeed struct {
	FiltresPossibles []struct {
		Agregation []struct {
			NbResultats    int    `json:"nbResultats"`
			ValeurPossible string `json:"valeurPossible"`
		} `json:"agregation"`
		Filtre string `json:"filtre"`
	} `json:"filtresPossibles"`
	Resultats []struct {
		AccessibleTH       bool   `json:"accessibleTH"`
		Alternance         bool   `json:"alternance"`
		Appellationlibelle string `json:"appellationlibelle"`
		Competences        []struct {
			Code     string `json:"code,omitempty"`
			Exigence string `json:"exigence"`
			Libelle  string `json:"libelle"`
		} `json:"competences"`
		Contact struct {
			Coordonnees1 string `json:"coordonnees1"`
			Courriel     string `json:"courriel"`
			Nom          string `json:"nom"`
		} `json:"contact"`
		DateCreation                time.Time `json:"dateCreation"`
		DeplacementCode             string    `json:"deplacementCode"`
		DeplacementLibelle          string    `json:"deplacementLibelle"`
		Description                 string    `json:"description"`
		DureeTravailLibelle         string    `json:"dureeTravailLibelle"`
		DureeTravailLibelleConverti string    `json:"dureeTravailLibelleConverti"`
		Entreprise                  struct {
			Description string `json:"description"`
			Nom         string `json:"nom"`
		} `json:"entreprise"`
		ExperienceExige   string `json:"experienceExige"`
		ExperienceLibelle string `json:"experienceLibelle"`
		Formations        []struct {
			CodeFormation  string `json:"codeFormation"`
			Commentaire    string `json:"commentaire"`
			DomaineLibelle string `json:"domaineLibelle"`
			Exigence       string `json:"exigence"`
			NiveauLibelle  string `json:"niveauLibelle"`
		} `json:"formations"`
		ID          string        `json:"id"`
		Intitule    string        `json:"intitule"`
		Langues     []interface{} `json:"langues"`
		LieuTravail struct {
			CodePostal string  `json:"codePostal"`
			Commune    string  `json:"commune"`
			Latitude   float64 `json:"latitude"`
			Libelle    string  `json:"libelle"`
			Longitude  float64 `json:"longitude"`
		} `json:"lieuTravail"`
		NatureContrat string `json:"natureContrat"`
		NombrePostes  int    `json:"nombrePostes"`
		OrigineOffre  struct {
			Origine     string        `json:"origine"`
			Partenaires []interface{} `json:"partenaires"`
			URLOrigine  string        `json:"urlOrigine"`
		} `json:"origineOffre"`
		QualificationCode        string `json:"qualificationCode"`
		QualificationLibelle     string `json:"qualificationLibelle"`
		QualitesProfessionnelles []struct {
			Description string `json:"description"`
			Libelle     string `json:"libelle"`
		} `json:"qualitesProfessionnelles"`
		RomeCode    string `json:"romeCode"`
		RomeLibelle string `json:"romeLibelle"`
		Salaire     struct {
			Commentaire string `json:"commentaire"`
			Complement1 string `json:"complement1"`
			Complement2 string `json:"complement2"`
		} `json:"salaire"`
		SecteurActivite        string `json:"secteurActivite"`
		SecteurActiviteLibelle string `json:"secteurActiviteLibelle"`
		TrancheEffectifEtab    string `json:"trancheEffectifEtab"`
		TypeContrat            string `json:"typeContrat"`
		TypeContratLibelle     string `json:"typeContratLibelle"`
	} `json:"resultats"`
}

// SORss to deserialise stackoverflow rss feed
type SORss struct {
	XMLName     xml.Name  `xml:"rss"`
	Version     string    `xml:"version,attr"`
	Channel     SOChannel `xml:"channel"`
	Description string    `xml:"description"`
	Title       string    `xml:"title"`
	Link        string    `xml:"link"`
}

// SOChannel to deserialise stackoverflow rss feed
type SOChannel struct {
	XMLName     xml.Name `xml:"channel"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	Items       []SOItem `xml:"item"`
}

// SOItem to deserialise stackoverflow rss feed
type SOItem struct {
	XMLName     xml.Name `xml:"item"`
	Author      SOAuthor `xml:"author"`
	Categories  []string `xml:"category,attr"`
	Title       string   `xml:"title"`
	Name        string   `xml:"name,attr"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	PubDate     string   `xml:"pubDate"`
	GUID        string   `xml:"guid"`
	Updated     string   `xml:"updated,attr"`
}

type SOAuthor struct {
	Name string `xml:"name"`
}

// IndeedFeed to deserialise indeed feed
type IndeedFeed struct {
	Version           int    `json:"version"`
	Query             string `json:"query"`
	Location          string `json:"location"`
	PaginationPayload string `json:"paginationPayload"`
	Dupefilter        bool   `json:"dupefilter"`
	Highlight         bool   `json:"highlight"`
	TotalResults      int    `json:"totalResults"`
	Start             int    `json:"start"`
	End               int    `json:"end"`
	PageNumber        int    `json:"pageNumber"`
	EstimatedRevenue  int    `json:"estimatedRevenue"`
	Results           []struct {
		Jobtitle              string `json:"jobtitle"`
		Company               string `json:"company"`
		City                  string `json:"city"`
		State                 string `json:"state"`
		Country               string `json:"country"`
		Language              string `json:"language"`
		FormattedLocation     string `json:"formattedLocation"`
		Source                string `json:"source"`
		Date                  string `json:"date"`
		Snippet               string `json:"snippet"`
		URL                   string `json:"url"`
		Onmousedown           string `json:"onmousedown"`
		Jobkey                string `json:"jobkey"`
		Sponsored             bool   `json:"sponsored"`
		Expired               bool   `json:"expired"`
		IndeedApply           bool   `json:"indeedApply"`
		FormattedLocationFull string `json:"formattedLocationFull"`
		FormattedRelativeTime string `json:"formattedRelativeTime"`
		Stations              string `json:"stations"`
		RefNum                string `json:"refNum"`
		AdvertiserID          int    `json:"advertiserId,omitempty"`
	} `json:"results"`
}

// RemoteOKFeed to deserialise remoteOK feed
type RemoteOKFeed []struct {
	Legal       string   `json:"legal,omitempty"`
	Slug        string   `json:"slug,omitempty"`
	ID          string   `json:"id,omitempty"`
	Epoch       string   `json:"epoch,omitempty"`
	Date        string   `json:"date,omitempty"`
	Company     string   `json:"company,omitempty"`
	CompanyLogo string   `json:"company_logo,omitempty"`
	Position    string   `json:"position,omitempty"`
	Tags        []string `json:"tags,omitempty"`
	Description string   `json:"description,omitempty"`
	URL         string   `json:"url,omitempty"`
	Original    bool     `json:"original,omitempty"`
	Verified    bool     `json:"verified,omitempty"`
	Logo        string   `json:"logo,omitempty"`
}
