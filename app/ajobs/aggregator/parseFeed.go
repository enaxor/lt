package aggregator

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/microcosm-cc/bluemonday"
)

// todo https://www.apec.fr/partenariats-offres-demploi-cadres.html#/fluxXml

// true: prend les chop sur les data de debug en local
// false: prend les jobs sur les url internet TODO
const debugMockURL = false

var bluemondayPolicy *bluemonday.Policy

// Aggregate process
func Aggregate() {
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Ajobs, "Aggregate", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	var jobs []dbmodels.AggregateJob
	bluemondayPolicy = bluemonday.UGCPolicy()
	// parse
	// getIndeedOauth()
	jobs = append(jobs, parseStackoverflow()...)
	// jobs = append(jobs, parseEmploiStore()...)
	// jobs = append(jobs, parseIndeed()...)
	jobs = append(jobs, parseRemoteOK()...)
	jobs = append(jobs, parseLinuxJobs()...)
	jobs = append(jobs, parseRemotiveJob()...)
	// ok
	db.AddAggregateJobs(jobs)
}

func parseEmploiStore() []dbmodels.AggregateJob {
	var ret []dbmodels.AggregateJob
	loc, _ := time.LoadLocation("UTC")
	jsonFile, err := os.Open("./wip/lt-aggregator/emploi-store.json")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	// var jobs []VanillaJob
	var feeds EmploiStoreFeed
	json.Unmarshal(byteValue, &feeds)
	for _, v := range feeds.Resultats {
		// fill data
		var tmp dbmodels.AggregateJob
		tmp.Source = EmploiStore
		// CompanyName
		if len(v.Entreprise.Nom) == 0 {
			tmp.CompanyName = "Pôle Emploi"
		} else {
			tmp.CompanyName = strings.ToLower(v.Entreprise.Nom)
		}
		// Contrat
		switch v.TypeContrat {
		case "CDI":
			tmp.Contrat = 1
		case "CDD":
			tmp.Contrat = 2
		case "MIS": // mission interim
			tmp.Contrat = 5
		case "CCE": // commercial
			tmp.Contrat = 0
		case "LIB": // freelance / indepandant
			tmp.Contrat = 3
		default:
			fmt.Printf("Unknown contrat %s.\n", v.TypeContrat)
			tmp.Contrat = 0
		}
		// Experience
		switch v.ExperienceExige {
		case "D":
			tmp.Experience = 1
		case "S":
			tmp.Experience = 2
		case "E":
			tmp.Experience = 3
		default:
			fmt.Printf("Unknown experience %s.\n", v.TypeContrat)
		}
		// Worktime
		switch v.DureeTravailLibelleConverti {
		case "Temps plein":
			tmp.Worktime = 1
		case "Non renseigné":
			tmp.Worktime = 1 // default to temps plein
		case "Temps partiel":
			tmp.Worktime = 2
		default:
			fmt.Printf("Unknown worktime %s.\n", v.DureeTravailLibelleConverti)
		}
		// RemoteRegularity
		tmp.FullRemote = false // i don't know
		// LocationID
		if len(v.LieuTravail.CodePostal) > 1 {
			// call locations from db
			location, errint := db.GetLocationCodePostal(v.LieuTravail.CodePostal)
			if errint == 0 {
				tmp.LocationID = location.ID
			}
		} else {
			tmp.LocationID = 0 // undeterminate
		}
		// Position
		tmp.Position = strings.TrimSpace(v.Intitule)
		// Description
		tmp.Description = bluemondayPolicy.Sanitize(v.Description)
		// URL
		tmp.URL = v.OrigineOffre.URLOrigine
		// Tags
		for _, tag := range v.Formations {
			tmp.Tags = append(tmp.Tags, tag.DomaineLibelle)
		}
		tmp.Tags = cleanTags(tmp.Tags)
		// dates
		tmp.PublicationDate = v.DateCreation
		tmp.ExpirationDate = tmp.PublicationDate.In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
		//  ok
		ret = append(ret, tmp)

		// debug
		// tmp.Description = strconv.Itoa(len(tmp.Description))
		// str, _ := json.Marshal(tmp)
		// fmt.Println("EmploiStore Marshal: " + string(str))
	}
	return ret
}

func parseIndeed() []dbmodels.AggregateJob {
	var ret []dbmodels.AggregateJob
	loc, _ := time.LoadLocation("UTC")
	jsonFile, err := os.Open("./wip/lt-aggregator/indeed.json")
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var feeds IndeedFeed
	json.Unmarshal(byteValue, &feeds)
	for _, v := range feeds.Results {
		if v.Expired {
			continue
		}
		// fill data
		var tmp dbmodels.AggregateJob
		tmp.Source = Indeed
		// CompanyName
		tmp.CompanyName = strings.TrimSpace(v.Company)
		if len(tmp.CompanyName) == 0 {
			tmp.CompanyName = "Indeed Jobs"
		}
		// Contrat
		// Experience
		// Worktime
		tmp.Worktime = 1 // we can legitimatly assume that all of these job are full time
		// RemoteRegularity
		tmp.FullRemote = false // indeed result can contain non-100% remote jobs
		// LocationID
		tmp.LocationID = 0 // undeterminate
		// Position
		tmp.Position = strings.TrimSpace(v.Jobtitle)
		// Description
		tmp.Description = bluemondayPolicy.Sanitize(v.Snippet)
		// URL
		tmp.URL = v.URL
		// Tags
		// Dates
		tmp.PublicationDate, err = time.Parse("Mon, 2 Jan 2006 15:04:05 GMT", v.Date)
		if err != nil {
			fmt.Println("parseIndeed unparsable date: ", err.Error(), " : ", v.Date)
			continue
		}
		tmp.ExpirationDate = tmp.PublicationDate.In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
		// ok
		ret = append(ret, tmp)
		// debug
		// tmp.Description = strconv.Itoa(len(tmp.Description))
		// str, _ := json.Marshal(tmp)
		// fmt.Println("Indeed Marshal: " + string(str))
	}
	return ret
}

const linuxJobsremoteKeyword = "(télétravail)"

// https://www.linuxjobs.fr/rss
func parseLinuxJobs() []dbmodels.AggregateJob {
	var ret []dbmodels.AggregateJob
	var err error
	loc, _ := time.LoadLocation("UTC")
	var byteValue []byte
	// get data
	if debugMockURL {
		byteValue = retrieveDataFromFile("./wip/lt-aggregator/linuxjobs.xml")
	} else {
		byteValue = retrieveDataFromURL("https://www.linuxjobs.fr/rss")
	}
	rss := SORss{}
	xml.Unmarshal(byteValue, &rss)
	for _, item := range rss.Channel.Items {
		// fill data
		var tmp dbmodels.AggregateJob
		tmp.Source = LinuxJobs
		// CompanyName
		tmp.CompanyName = "Linux Jobs"
		// Contrat
		// Experience
		// Worktime
		tmp.Worktime = 1 // we can legitimatly assume that all of these job are full time
		// RemoteRegularity
		tmp.FullRemote = true // linuxjobs feed is already filtered by full remote !
		// LocationID
		tmp.LocationID = 0 // undeterminate
		// Position
		tmp.Position = strings.TrimSpace(item.Title)
		if !strings.Contains(tmp.Position, linuxJobsremoteKeyword) {
			// not télétravail - drop
			fmt.Println("linuxjobs no ", linuxJobsremoteKeyword, " in title [", tmp.Position, "] : dropped.")
			continue
		}
		tmp.Position = strings.Replace(tmp.Position, linuxJobsremoteKeyword, "", 1)
		// Description
		tmp.Description = bluemondayPolicy.Sanitize(item.Description)
		// URL
		tmp.URL = item.Link
		// Tags
		tmp.Tags = cleanTags(item.Categories)
		// Dates
		tmp.PublicationDate, err = time.Parse("Mon, 2 Jan 2006 15:04:05 -0700", item.PubDate)
		if err != nil {
			fmt.Println("linuxjobs unparsable date: ", err.Error(), " : ", item.PubDate)
			continue
		}
		tmp.ExpirationDate = tmp.PublicationDate.In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
		//  ok
		ret = append(ret, tmp)
	}
	return ret
}

func parseStackoverflow() []dbmodels.AggregateJob {
	var ret []dbmodels.AggregateJob
	var err error
	loc, _ := time.LoadLocation("UTC")
	var byteValue []byte
	// get data
	if debugMockURL {
		byteValue = retrieveDataFromFile("./wip/lt-aggregator/stackoverflow.xml")
	} else {
		byteValue = retrieveDataFromURL("http://stackoverflow.com/jobs/feed?l=france&r=true")
	}
	rss := SORss{}
	xml.Unmarshal(byteValue, &rss)
	for _, item := range rss.Channel.Items {
		// fill data
		var tmp dbmodels.AggregateJob
		tmp.Source = StackOverflow
		// CompanyName
		tmp.CompanyName = strings.TrimSpace(item.Author.Name)
		// Contrat
		// Experience
		// Worktime
		tmp.Worktime = 1 // we can legitimatly assume that all of these job are full time
		// RemoteRegularity
		tmp.FullRemote = true // stackoveflow feed is already filtered by full remote !
		// LocationID
		tmp.LocationID = 0 // undeterminate
		// Position
		// remove "(allows remote)"
		position := strings.ReplaceAll(item.Title, "(allows remote)", "")
		// remove "()"
		position = strings.ReplaceAll(position, "()", "")
		// trimspace
		tmp.Position = strings.TrimSpace(position)
		// Description
		tmp.Description = bluemondayPolicy.Sanitize(item.Description)
		// URL
		tmp.URL = item.Link
		// Tags
		tmp.Tags = cleanTags(item.Categories)
		// Dates
		tmp.PublicationDate, err = time.Parse("Mon, 2 Jan 2006 15:04:05 Z", item.PubDate)
		if err != nil {
			fmt.Println("parseStackoverflow unparsable date: ", err.Error(), " : ", item.PubDate)
			continue
		}
		tmp.ExpirationDate = tmp.PublicationDate.In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
		//  ok
		ret = append(ret, tmp)

		// debug
		// tmp.Description = strconv.Itoa(len(tmp.Description))
		// str, _ := json.Marshal(tmp)
		// fmt.Println("parseStackoverflow Marshal: " + string(str))
	}
	return ret
}

func parseRemoteOK() []dbmodels.AggregateJob {
	var ret []dbmodels.AggregateJob
	loc, _ := time.LoadLocation("UTC")
	var byteValue []byte
	// get data
	if debugMockURL {
		byteValue = retrieveDataFromFile("./wip/lt-aggregator/remoteok.json")
	} else {
		byteValue = retrieveDataFromURL("https://remoteok.io/api")
	}
	var feeds RemoteOKFeed
	json.Unmarshal(byteValue, &feeds)
	for _, v := range feeds {
		// fill data
		var tmp dbmodels.AggregateJob
		tmp.Source = RemoteOk
		// CompanyName
		tmp.CompanyName = strings.TrimSpace(v.Company)
		if len(tmp.CompanyName) == 0 {
			tmp.CompanyName = "RemoteOK"
		}
		// Contrat
		// Experience
		// Worktime
		tmp.Worktime = 1 // we can legitimatly assume that all of these job are full time
		// RemoteRegularity
		tmp.FullRemote = true // remoteOK publish remote only
		// LocationID
		tmp.LocationID = 0 // undeterminate
		// Position
		tmp.Position = strings.TrimSpace(v.Position)
		// Description
		tmp.Description = bluemondayPolicy.Sanitize(v.Description)
		// URL
		tmp.URL = v.URL
		// Tags
		tmp.Tags = cleanTags(v.Tags)
		// Dates
		if len(v.Epoch) == 0 {
			// remoteOK first item is empty
			continue
		}
		epoch, err := strconv.ParseInt(v.Epoch, 10, 64)
		if err != nil {
			fmt.Println("remoteok unparsable date: ", err.Error(), " : ", v.Date)
			continue
		}
		tmp.PublicationDate = time.Unix(epoch, 0)
		tmp.ExpirationDate = tmp.PublicationDate.In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
		// ok
		ret = append(ret, tmp)

		// debug
		// tmp.Description = strconv.Itoa(len(tmp.Description))
		// str, _ := json.Marshal(tmp)
		// fmt.Println("RemoteOK Marshal: " + string(str))
	}
	return ret
}

func parseRemotiveJob() []dbmodels.AggregateJob {

	//	RemotiveJob
	//	https://remotive.io/api/remote-jobs

	var ret []dbmodels.AggregateJob
	loc, _ := time.LoadLocation("UTC")
	var byteValue []byte
	// get data
	if debugMockURL {
		byteValue = retrieveDataFromFile("./wip/lt-aggregator/remotivejob.json")
	} else {
		byteValue = retrieveDataFromURL("https://remotive.io/api/remote-jobs")
	}
	var feeds RemotiveJobsFeed
	err := json.Unmarshal(byteValue, &feeds)
	if err != nil {
		fmt.Println("err unmarshal", err)
		fmt.Println("DATA==========================================================================")
		fmt.Println(string(byteValue))
		fmt.Println("==========================================================================DATA")
		return nil
	}
	for i, v := range feeds.Jobs {
		// fill data
		var tmp dbmodels.AggregateJob
		tmp.Source = RemotiveJob
		// CompanyName
		tmp.CompanyName = strings.TrimSpace(v.Company_name)
		if len(tmp.CompanyName) == 0 {
			tmp.CompanyName = "RemotiveJob"
		}
		// Contrat
		if v.Job_type == "contract" {
			tmp.Contrat = 6
		} else if v.Job_type == "freelance" {
			tmp.Contrat = 3
		} else if v.Job_type == "internship" {
			tmp.Contrat = 4
		} else if v.Job_type == "other" || len(v.Job_type) == 0 {
			tmp.Contrat = 0
		} else if v.Job_type == "part_time" {
			tmp.Worktime = 2
		} else if v.Job_type == "full_time" {
			// Worktime
			tmp.Worktime = 1 // we can legitimatly assume that all of these job are full time
		} else {
			fmt.Println("parseRemotiveJob found uk Job_type == " + v.Job_type)
		}
		// Experience
		// RemoteRegularity
		tmp.FullRemote = true // RemotiveJob publish remote only
		// Position
		tmp.Position = strings.TrimSpace(v.Title)
		// LocationID
		if strings.HasSuffix(v.Candidate_required_location, "Only") && v.Candidate_required_location != "France Only" {
			//	fmt.Println("RemotiveJob [", tmp.Position, "] location contain xx Only [", v.Candidate_required_location, "] : dropped.")
			continue
		}
		tmp.LocationID = 0 // undeterminate
		// Description
		tmp.Description = bluemondayPolicy.Sanitize(v.Description)
		// URL
		tmp.URL = v.URL
		// Tags
		tmp.Tags = cleanTags(v.Tags)
		if len(v.Category) != 0 {
			tmp.Tags = append(tmp.Tags, v.Category)
		}
		// Dates
		t, err := time.Parse("2006-01-02T15:04:05", v.Publication_date)
		if err != nil {
			fmt.Println("err time.Pars", err)
			return nil
		}
		tmp.PublicationDate = t
		tmp.ExpirationDate = tmp.PublicationDate.In(loc).Add(24 * 30 * 3 * time.Hour) // 90 jours max
		// ok
		ret = append(ret, tmp)
		if i == 5 {
			break
		}
	}
	return ret
}

/*
** TOOLS
 */

func retrieveDataFromURL(URL string) []byte {
	// Get the data
	resp, err := http.Get(URL)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer resp.Body.Close()
	// read data
	byteValue, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Println("RETRIEVED retrieveDataFromURL from ", URL, "len", len(byteValue))
	return byteValue
}

func retrieveDataFromFile(path string) []byte {
	// Open the xmlFile
	xmlFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	// defer the closing of xmlFile so that we can parse it.
	defer xmlFile.Close()
	byteValue, err := ioutil.ReadAll(xmlFile) // Unmarshal takes a []byte and fills the rss struct with the values found in the xmlFile
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fmt.Println("RETRIEVED retrieveDataFromFile from ", path, "len", len(byteValue))
	return byteValue
}

/*
** TESTING
 */

func getCall(method, url, token string) error {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return fmt.Errorf("call Got error %s", err.Error())
	}
	//req.Header.Add("Authorization", "Bearer "+bearer)
	var bearer = "Bearer " + token
	req.Header.Add("Authorization", bearer)

	fmt.Println("call request == ", req)

	response, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("call Got error %s", err.Error())
	}
	defer response.Body.Close()

	//fmt.Println("result a", utils.ToString(response))
	fmt.Println("result b", response)
	fmt.Println("result c", utils.ToString(response.Body))
	fmt.Println("result e", response.Body)

	// Read body from response
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("result f Error reading response. ", err)
	}

	fmt.Printf("result G %s\n", body)

	return nil
}

type emploistoreresponseauth struct {
	Access_token string
	Expires_in   int
	Scope        string
	Token_type   string
}

// DISCONTINUED x_x
func getIndeedOauth() {
	client := &http.Client{
		Timeout: time.Second * 10,
	}

	data := url.Values{
		"client_id":     {"3ca4598bb41f0efd5737c310819a0fda4f89d6206180f8aec6fce29cbe3434f0"},
		"client_secret": {"JFosKJWxaSVS9S2r9AwCI9AUtQgKXUszpwP9Ouc45b5OhaetS7S5tg37BVdwz5qm"},
		"grant_type":    {"client_credentials"},
		//	"scope":         {"employer_access"},
	}

	req, err := http.NewRequest("POST", "https://apis.indeed.com/oauth/v2/tokens", strings.NewReader(data.Encode()))
	if err != nil {
		fmt.Errorf("1 getIndeedOauth Got error %s", err.Error())
		return
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")

	fmt.Println("call request == ", req)

	response, err := client.Do(req)
	if err != nil {
		fmt.Errorf("2 getIndeedOauth Got error %s", err.Error())
		return
	}
	fmt.Println("resp.All", response)
	fmt.Println("resp.Body", utils.ToString(response.Body))
	fmt.Println("result b", response)
	fmt.Println("result c", utils.ToString(response.Body))
	fmt.Println("result e", response.Body)
	// Read body from response
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("result f Error reading response. ", err)
	}
	fmt.Printf("result G %s\n", body)
	// result G
	// {"access_token":"eyJraWQiOiI2ZmZmYjVlOS03Zjc0LTQ4YjItYjRmMS03ZmY0Njc4MGI0ZjEiLCJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiJ9.eyJzdWIiOiJhcHA6M2NhNDU5OGJiNDFmMGVmZDU3MzdjMzEwODE5YTBmZGE0Zjg5ZDYyMDYxODBmOGFlYzZmY2UyOWNiZTM0MzRmMCIsImFwcF9hY2NvdW50IjoiNDBkZjZkODdlNDliY2FkZiIsImF6cCI6IjNjYTQ1OThiYjQxZjBlZmQ1NzM3YzMxMDgxOWEwZmRhNGY4OWQ2MjA2MTgwZjhhZWM2ZmNlMjljYmUzNDM0ZjAiLCJzY29wZSI6ImVtcGxveWVyX2FjY2VzcyIsImlzcyI6Imh0dHBzOlwvXC9zZWN1cmUuaW5kZWVkLmNvbSIsImV4cCI6MTYyMDY0OTMwMiwiaWF0IjoxNjIwNjQ1NzAyfQ.rz7gLWxpnD3xjFQWwMfvPNXIxjwFgLM89Wx3wzUcrCjmwK_o78YRL5-Ttl7pjip9knhTwMOnIzM1U_KMDLenfg",
	// "scope":"employer_access",
	// "token_type":"Bearer",
	//"expires_in":3600}

	// decode resp
	var res emploistoreresponseauth
	json.NewDecoder(response.Body).Decode(&res)
	fmt.Println("indeed access token ", res.Access_token)

}

// STAND BY - need verif
// URLs
// https://pole-emploi.io/data/documentation/utilisation-api-pole-emploi/generer-access-token
// https://pole-emploi.io/data/api/offres-emploi?tabgroup-api=documentation&doc-section=api-doc-section-rechercher-par-crit%C3%A8res
// https://pole-emploi.io/login
func getPoleEmploiOauth() {
	data := url.Values{
		"grant_type":    {"client_credentials"},
		"client_id":     {"PAR_dailycheckremote_9429d15801242be9be618c1524993f2baf2a75c372bf3ca0182e30b815a6166e"},
		"client_secret": {"57b7850d820400de760d1d64b7c9ff47abb2d0d49e1f6f73c74ef7630b2d102e"},
		"scope":         {"api_offresdemploiv2%20application_PAR_dailycheckremote_9429d15801242be9be618c1524993f2baf2a75c372bf3ca0182e30b815a6166e%20o2dsoffre"}, // {"application_PAR_dailycheckremote_9429d15801242be9be618c1524993f2baf2a75c372bf3ca0182e30b815a6166e%20api_offresdemploiv2"}, // {"api_offresdemploiv2"},
	}

	resp, err := http.PostForm("https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire", data)

	if err != nil {
		fmt.Println("err = ", err)
		return
	}

	fmt.Println("resp.All", resp)
	fmt.Println("resp.Body", utils.ToString(resp.Body))

	var res emploistoreresponseauth
	json.NewDecoder(resp.Body).Decode(&res)
	fmt.Println("resp 3", utils.ToString(res)) // resp 3 {"access_token":"jxcriMLaxQB8vqNXQ6hdgFbQnL0","expires_in":1499,"scope":"api_offresdemploiv2","token_type":"Bearer"}
	fmt.Println("=====================================================================================")
	// we have the token, we can REQ
	err = getCall("GET", "https://api.emploi-store.fr/partenaire/offresdemploi/v2/offres/search?motsCles=teletravail", res.Access_token)
	if err != nil {
		fmt.Println("err d = ", err)
		return
	}

}
