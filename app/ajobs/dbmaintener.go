package ajobs

import db "lt/app/db"

// UpdateExpiredJob maintain data
type UpdateExpiredJob struct {
}

type ExpirePasswordResetCode struct {
}

// Run job that set expiry to true
func (e UpdateExpiredJob) Run() {
	db.UpdateExpiredJobs()
}

func (e ExpirePasswordResetCode) Run() {
	db.ExpireOldPasswordRequestRequest()
}
