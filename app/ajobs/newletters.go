package ajobs

import (
	"bytes"
	"fmt"
	htpl "html/template"
	"lt/app/ajobs/aggregator"
	db "lt/app/db"
	"lt/app/db/dbmodels"
	"lt/app/utils"
	"strconv"
	ttpl "text/template" // Rename for differentiate with html/template
	"time"

	guuid "github.com/google/uuid"
)

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Newsletter //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// AggregateJobFromFeed launch agregation process
type AggregateJobFromFeed struct {
}

// DailyNewsletter job handler
type DailyNewsletter struct {
}

// WeeklyNewsletter job handler
type WeeklyNewsletter struct {
}

// SendSubscriptionComfirmation sub confirm
type SendSubscriptionComfirmation struct {
	ID         string
	Name       string
	Regularity string // 1: daily / 2 weekly / 3 monthly
	Email      string
}

// Run job aggregation
func (e AggregateJobFromFeed) Run() {
	aggregator.Aggregate()
}

// Run sent DailyNewsletter
func (e DailyNewsletter) Run() {
	// query DB for last 1 day new job
	subscribers := db.SelectSubscriber(dailyNewsletter)
	// create and queue mail
	queueNewletter(subscribers)
	// send mails
	// DEBUG MODE sendQueuedMails()
}

// Run sent WeeklyNewsletter
func (e WeeklyNewsletter) Run() {
	// query DB for last 1 day new job
	subscribers := db.SelectSubscriber(weeklyNewsletter)
	// create and queue mail
	queueNewletter(subscribers)
	// we add weekly candidate automatic mail
	// candidateSubscribers := db.SelectCandidatesSugg()
	// // create and queue mail
	// queueJobSuggestionMail(candidateSubscribers)
	// send mails
	// DEBUG MODE sendQueuedMails()
}

// SendUserResetPassword send simple email with userID to he can acess resetpageID
func SendUserResetPassword(email, id string) error {
	var mail dbmodels.MailQueueItem
	// basic data
	mail.Title = "EmploiNomade.fr: Réinitialiser votre mot de passe"
	mail.Sent = false
	// Prepare template param
	params := make(map[string]interface{})
	params["email"] = email
	params["confirm_url"] = domainName + "/reseting-password/" + id
	textPart, err := executeTextTemplate(resetPasswordEmailTextTemplate, params)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Controller, "SendUserResetPassword", "executeTextTemplate", err, "email", email, "id", id)
		return err
	}
	htmlPart, err := executeHTMLTemplate(resetPasswordEmailHTMLTemplate, params)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Controller, "SendUserResetPassword", "executeHTMLTemplate", err, "email", email, "id", id)
		return err
	}
	mail.ContentText = textPart
	mail.ContentHTML = htmlPart
	// send mail
	SendMailWithParams(mail, true)
	return nil
}

// ConfirmSubscriptionEmailPrepare Mail de confirmation: subscription
func ConfirmSubscriptionEmailPrepare(id, email string, regularity int) (*dbmodels.MailQueueItem, error) {
	var mail dbmodels.MailQueueItem
	// basic data
	mail.Email = email
	mail.Title = "LeTeletravail.fr: Validez votre inscription à la newsletter"
	mail.Sent = false
	// Prepare template param
	params := make(map[string]interface{})
	if regularity == 1 {
		params["regularity"] = "journalière"
	} else if regularity == 3 {
		params["regularity"] = "mensuelle"
	} else {
		params["regularity"] = "hebdomadaire"
	}
	params["confirm_url"] = domainName + "/newsletter/confirm/" + id
	textPart, err := executeTextTemplate(confirmSubscriptionTextTemplate, params)
	if err != nil {
		return nil, err
	}
	htmlPart, err := executeHTMLTemplate(confirmSubscriptionHTMLTemplate, params)
	if err != nil {
		return nil, err
	}
	mail.ContentText = textPart
	mail.ContentHTML = htmlPart
	return &mail, nil
}

// func queueJobSuggestionMail(suggs []dbmodels.SuggestedJobMailing) {
// 	var mailsToQueue = []dbmodels.MailQueueItem{}
// 	for _, suggestion := range suggs {
// 		if len(suggestion.UserID) > 0 {
// 			// get job ids
// 			ids := db.GetPopulatedJobID(suggestion.UserID)
// 			// get jobs preview
// 			jobspreview, err := db.GetJobsPreview(ids)
// 			if err != 0 {
// 				continue // cancel
// 			}
// 			// prepare sugg
// 			mails := generateApplicantNewsletter(suggestion.UserID, suggestion.Email, suggestion.Prenom, jobspreview)
// 			if mails != nil {
// 				// add to queue
// 				mailsToQueue = append(mailsToQueue, *mails)
// 			}
// 		}
// 	}
// 	db.CreateMailsToQueue(mailsToQueue)
// }

func queueNewletter(sub []dbmodels.NewsletterSubscription) {
	var mailsToQueue = []dbmodels.MailQueueItem{}
	for _, subscriber := range sub {

		// standars newsletter
		// get last daily new job
		// todo, grab from cache
		req := db.JobListRequest{IndexStart: 0, IndexEnd: 10, Filters: nil}
		jobspreview, _, err := db.GetJobs(req, true)
		if err != 0 {
			continue // cancel
		}
		// prepare newsletter
		mails := generateCustomNewsletter(subscriber.ID, subscriber.Email, jobspreview)
		if mails != nil {
			// add to queing
			mailsToQueue = append(mailsToQueue, *mails)
		}
	}
	db.CreateMailsToQueue(mailsToQueue)
}

func generateCustomNewsletter(subID int, email string, jobs []dbmodels.JobPreview) *dbmodels.MailQueueItem {
	nbJob := len(jobs)
	if nbJob == 0 {
		// no job (and chocolate) for you
		return nil
	}
	var err error
	ret := dbmodels.MailQueueItem{}
	// basic data
	ret.Email = email
	ret.Sent = false
	ret.SentTime = nil
	// time data
	ret.CreatedAt = time.Now()
	// template data
	params := make(map[string]interface{})
	params["jobs"] = jobs
	params["subscribtion_id"] = subID
	params["mail"] = email
	// titre
	nbJobStr := strconv.Itoa(nbJob)
	if nbJob == 1 {
		ret.Title = "Votre alerte Emploi nomade: " + nbJobStr + " nouvel emploi disponible !"
	} else {
		ret.Title = "Votre alerte Emploi nomade: " + nbJobStr + " nouvels emplois disponibles !"
	}
	// body
	ret.ContentHTML, err = executeHTMLTemplate(newsletterHTMLTemplate, params)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	ret.ContentText, err = executeTextTemplate(newsletterTextTemplate, params)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	return &ret
}

func generateApplicantNewsletter(UserID guuid.UUID, email string, prenom string, jobs []dbmodels.JobPreview) *dbmodels.MailQueueItem {
	nbJob := len(jobs)
	if nbJob == 0 {
		// no job (and chocolate) for you
		return nil
	}
	var err error
	ret := dbmodels.MailQueueItem{}
	// basic data
	ret.Email = email
	ret.Sent = false
	ret.SentTime = nil
	// time data
	ret.CreatedAt = time.Now()
	// template data
	params := make(map[string]interface{})
	params["jobs"] = jobs
	params["user_id"] = UserID.String()
	params["mail"] = email
	// titre
	nbJobStr := strconv.Itoa(nbJob)
	if nbJob == 1 {
		ret.Title = "Emploi nomade: " + nbJobStr + " job qui pourrait vous interesser !"
	} else {
		ret.Title = "Emploi nomade: " + nbJobStr + " jobs qui pourraient vous interesser !"
	}
	// body
	ret.ContentHTML, err = executeHTMLTemplate(newsletterHTMLTemplate, params)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	ret.ContentText, err = executeTextTemplate(newsletterTextTemplate, params)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	return &ret
}

// ExecuteTextTemplate Execute une template text
func executeTextTemplate(templateText string, params map[string]interface{}) (string, error) {
	t, err := ttpl.New("template_name").Parse(templateText)
	if err != nil {
		return "", err
	}
	var output bytes.Buffer
	err = t.Execute(&output, params)
	if err != nil {
		return "", err
	}
	return output.String(), nil
}

// ExecuteHTMLTemplate Execute une template html
func executeHTMLTemplate(templateHTML string, params map[string]interface{}) (string, error) {
	t, err := htpl.New("template_name").Parse(templateHTML)
	if err != nil {
		return "", err
	}
	var output bytes.Buffer
	err = t.Execute(&output, params)
	if err != nil {
		return "", err
	}
	return output.String(), nil
}

// Run instant job to confirm sub
// func (e SendSubscriptionComfirmation) Run() {
// 	message, err := ConfirmSubscriptionEmailPrepare(e.Id, e.Name, e.Regularity)
// 	if err != nil {
// 		revel.AppLog.Error("Unable to prepare ConfirmEmail message,", err)
// 		return
// 	}
// 	SendMailOrLogError(revel.AppLog.Error, message, e.Email)
// }

// func testMail() {
// 	m := gomail.NewMessage()
// 	m.SetHeader("From", gandiEmailID)
// 	m.SetHeader("To", "shgz@rxsoft.eu")
// 	m.SetHeader("Subject", "Hello!")
// 	m.SetBody("text/html", "Hello <b>Bob</b> and <i>Cora</i>!")

// 	d := gomail.NewDialer(gandiMailServer, 587, gandiEmailID, gandiMailPwd)

// 	// Send the email to Bob, Cora and Dan.
// 	if err := d.DialAndSend(m); err != nil {
// 		fmt.Println(err.Error())
// 		panic(err)
// 	}
// 	//	SendMailOrLogError(revel.ERROR, "message", "rsp@rxsoft.eu")
// }

