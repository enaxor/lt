package ajobs

import (
	"github.com/revel/modules/jobs/app/jobs"
	"github.com/revel/revel"
)

func init() {
	revel.OnAppStart(func() {
		jobs.Schedule("@midnight", UpdateExpiredJob{})        // Run once a day, midnight
		jobs.Schedule("@midnight", ExpirePasswordResetCode{}) // Run at night, expire 24h old password request
		jobs.Schedule("0 0 11 * * *", DailyNewsletter{})      // Run once a day, at 11am
		jobs.Schedule("0 0 11 * * 1", WeeklyNewsletter{})     // Run once on monday, at 11am
		jobs.Schedule("0 0 * * * *", FeedDBWithLogs{})        // Run once an hour
		// jobs.Now(populateCache{})
	})
}
