package ajobs

import (
	"bufio"
	"lt/app/db"
	"lt/app/utils"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"
)

const logsDir = "./log/"
const archivesLogsDir = "./log/archives/"
const logsFileName = "lt-logs-*.json"

// const logPathPathTMP = "./logrevelTMP.json"

// FeedDBWithLogs insert logs from revel json log file to postgresql private.logs table
type FeedDBWithLogs struct {
}

// Run to insert logs from revel json log file to postgresql private.logs table
func (e FeedDBWithLogs) Run() {
	if utils.MetricsMonitoring {
		defer utils.ManageLog(utils.Info, utils.Ajobs, "FeedDBWithLogs.Run", "TimeTrack", nil, "elapsed", strconv.FormatInt(utils.TimeTrack(time.Now()).Nanoseconds(), 10))
	}
	// get the logs files to process
	files, err := filepath.Glob(logsDir + logsFileName)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "filepath.Glob", err, "path", logsDir+logsFileName)
		return
	}
	// read log file
	lines, err := parseFiles(files)
	if err != nil {
		utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "parseFiles", err, "path", logsDir+logsFileName, "len(files)", strconv.Itoa(len(files)))
		return
	}
	logsize := len(lines)
	var nbAdded int64
	if logsize > 0 {
		if added, success := db.InsertLog(lines); success {
			nbAdded = added
			// write all processed log to another unique file
			toarchivefilename, err := writeNewFIle(lines)
			if err != nil {
				utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "writeNewFIle", err, "toarchivefilename", toarchivefilename, "full path", logsDir+toarchivefilename, "len lines", strconv.Itoa(len(files)))
				return
			}
			// copy to archive
			oldpath := logsDir + toarchivefilename
			newpath := archivesLogsDir + toarchivefilename
			err = os.Rename(oldpath, newpath)
			if err != nil {
				utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "os.Rename", err, "old path", oldpath, "archives path", newpath)
				return
			}
			// compress
			if err := exec.Command("gzip", archivesLogsDir+toarchivefilename).Run(); err != nil {
				utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "gzip", err, "path", archivesLogsDir+toarchivefilename)
				return
			}
		} else {
			utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "InsertLog", nil, "no-intersed lines", strconv.Itoa(logsize), "no-intersed files", strconv.Itoa(len(files)))
			return
		}
	}
	// All done, we can delete thoses files
	for _, file := range files {
		if err := os.Remove(file); err != nil {
			utils.ManageLog(utils.Error, utils.Ajobs, "FeedDBWithLogs.Run", "os.Remove", err, "file to delete", file)
			return
		} else {
			utils.ManageLog(utils.Debug, utils.Ajobs, "FeedDBWithLogs.Run", "os.Remove", nil, "deleted file", file)
		}
	}
	utils.ManageLog(utils.Debug, utils.Ajobs, "FeedDBWithLogs.Run", "InsertLog", nil, "inserted logs", strconv.FormatInt(nbAdded, 10), "on", strconv.Itoa(logsize))
}

// write data to a new file and return filename
func writeNewFIle(data []string) (string, error) {
	toarchivefilename := "lt-logs-" + time.Now().Format("2006-01-02_15:04:05") + ".json"
	f, err := os.Create(logsDir + toarchivefilename)
	if err != nil {
		return "", err
	}
	defer f.Close()
	for _, v := range data {
		_, err := f.WriteString(v)
		if err != nil {
			return "", err
		}
	}
	return toarchivefilename, nil
}

func parseFiles(files []string) ([]string, error) {
	var all_lines []string
	for _, f := range files {
		lines, err := scanLines(f)
		if err != nil {
			return nil, err
		}
		all_lines = append(all_lines, lines...)
	}
	return all_lines, nil
}

func scanLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, nil
}
