package ajobs

const domainName = "http://localhost:9000" // "https://leteletravail.fr"
const dailyNewsletter = 1
const weeklyNewsletter = 2
const monthlyNewsletter = 3
const maxJobPreviewByEmail = 77
const maxLenJobPreviewContent = 300

/* APPLY LINK MAIL */
// const APPLY_MAIL_SUBJECT = "Candidature suite á l'annonce d'emploi {{.position}} sur leTélétravail.fr"
// const APPLY_MAIL_BODY = "Madame, Monsieur,\n\nEn réponse à votre annonce d'emploi [ {{.position}} ] à l'adresse suivante:\n\nhttp://leteletravail.fr/job/{{.public_id}}\n\nj'ai le plaisir de me porter candidat et à ce titre je vous prie de trouver ci-joint mon CV, ainsi que ma lettre de motivation.\n\nDans l'attente de votre rencontre, je vous prie de croire, Madame, Monsieur, à l'assurance de mes salutations distinguées.\n\nCordialement"

/* ROBOTS.TXT */
const robotsTxt = "User-agent: *\nDisallow: /"

// todo Si cette annonce plus d'actualité, cliquez sur le lien de suppression suivant ou contactez-nous pour rectifier vos coordonnées.
const enabledEmailTextTemplate string = `votre annonce est en ligne!`
const enabledEmailHTMLTemplate string = `votre annonce est en ligne!`

const confirmEmailHTMLTemplate string = `
<body>
<h1>Bonjour,</h1>

<p>Votre annonce est prête a être publiée.

<p>Veuillez vous rendre a l'adresse si dessous pour finalisez votre annonce !<br>
<a href="{{.confirm_url}}">{{.confirm_url}}</a>

Bonne journée,<br>
L'équipe de LeTeletravail.fr
</body>
`

const newsletterTextTemplate string = `
Bonjour,

De nouvelles annonces en télétravail ont été publiée(s) sur letélétravail.fr:

{{range .jobs}}
{{.Position}} chez {{.Company.Name}} (Il y a {{.Since}})
Lire plus en cliquant sur ce lien http://localhost/job/{{.PublicID}}
{{end}}

Bonne journée,
L'équipe de LeTeletravail.fr

--
Vous vous êtes incrit(e) à cette alerte via http://leteletravail.fr avec l'adresse {{.mail}}
Recevoir ces alertes tous les jours: http://leteletravail.fr/newsletter/change/{{.subscribtion_id}}?&regularity=daily
Recevoir ces alertes toutes les semaines: http://leteletravail.fr/newsletter/change/{{.subscribtion_id}}?&regularity=weekly
Recevoir ces alertes tous les mois: http://leteletravail.fr/newsletter/change/{{.subscribtion_id}}?&regularity=monthly
Vous déinscrire de cette alerte: http://leteletravail.fr/newsletter/unsubscribe/{{.subscribtion_id}}
Ne plus recevoir d'emails de notre part: http://leteletravail.fr/newsletter/unsubscribe-all/{{.subscribtion_id}}
`

const newsletterHTMLTemplate string = `
<body>
Bonjour,
<p>
De nouvelles annonces en télétravail ont été publiée(s) sur letélétravail.fr:<br>
</p>
{{range .jobs -}}
* <b>{{.Position}}</b> chez <b>{{.Company.Name}}</b> <i>(Il y a {{.Since}})</i><br>
<a href="http://localhost/job/{{.PublicID}}">Lire plus</a>
<br><br>
{{- end}}
<p>
Bonne journée,<br>
L'équipe de LeTeletravail.fr
</p>
<br>--<br>
Vous vous êtes incrit(e) à cette alerte via <a href="http://leteletravail.fr">leteletravail.fr</a> avec l'adresse {{.mail}} <br>
<a href="http://leteletravail.fr/newsletter/change/{{.subscribtion_id}}?&regularity=1">Recevoir ces alertes tous les jours</a> <br>
<a href="http://leteletravail.fr/newsletter/change/{{.subscribtion_id}}?&regularity=2">Recevoir ces alertes toutes les semaines</a> <br>
<a href="http://leteletravail.fr/newsletter/change/{{.subscribtion_id}}?&regularity=3">Recevoir ces alertes tous les mois</a> <br>
<a href="http://leteletravail.fr/newsletter/email/unsubscribe/{{.subscribtion_id}}">Vous déinscrire de cette alerte</a> <br>
<a href="http://leteletravail.fr/newsletter/unsubscribe-all/{{.subscribtion_id}}">Ne plus recevoir d'emails de notre part</a> <br>
</body>
`

// newsletter confirm text
const confirmSubscriptionTextTemplate string = `
Bonjour,

Merci de valider votre inscription à l'alerte {{.regularity}} d'annonces d'emploi en télétravail en visitant le lien ci dessous:
{{.confirm_url}}


Merci d'ignorer ce mail si vous n'avez pas fait cette demande.

Bonne journée,
L'équipe EmploiNomade.fr
`

// newsletter confirm html
const confirmSubscriptionHTMLTemplate string = `
<body>
Bonjour,<br>

<p>Valider votre inscription à l'alerte {{.regularity}} d'annonces d'emploi en télétravail en visitant le lien ci dessous:
{{.confirm_url}}</p>
<p>Merci d'ignorer ce mail si vous n'avez pas fait cette demande.</p>
<br>
<br>
Bonne journée,<br>
L'équipe EmploiNomade.fr
</body>
`

// reset password
const resetPasswordEmailTextTemplate string = `
Bonjour {{.email}},
vous avez fait une demande de modification de mot de passe.
Merci de suivre le lien suivant pour procéder au changement: {{.confirm_url}} 
Ce lien est disponible 48h.
Si vous n'avez pas fait cette demande, vous pouvez ignorer ce mail.

Bonne journée,
L'équipe EmploiNomade.fr
`
const resetPasswordEmailHTMLTemplate string = `
<body>
Bonjour {{.email}},<br>
<p>vous avez fait une demande de modification de mot de passe.</p>
<p>Merci de suivre le lien suivant pour procéder au changement: {{.confirm_url}}</p>
<p>Ce lien est disponible 48h.</p>
<p>Si vous n'avez pas fait cette demande, vous pouvez ignorer ce mail.</p>
<br>
<br>
Bonne journée,<br>
L'équipe EmploiNomade.fr
</body>
`

///////////////////////////////////////////////////////////////////////////////////
// Mis ca part ici pour les function soit plus lisible la haut
// Ca peut aussi etre dans un fichier.
// ANCHOA
// const confirmEmailTextTemplate string = `
// Bonjour,

// Votre annonce "{{.position}}" est prête a être publiée.

// Veuillez vous rendre a l'adresse si dessous pour finalisez votre annonce !
// {{.confirm_url}}

// Bonne journée,
// L'équipe de LeTeletravail.fr

// `

const amoureuse string = "RORO"
