$(document).ready(function () {
	// message close
	$('.message .close')
		.on('click', function () {
			$(this)
				.closest('.message')
				.transition('fade')
				;
	});
	//sidebar
	// fix menu when passed
	$('.masthead')
	.visibility({
		once: false,
		onBottomPassed: function () {
			$('.fixed.menu').transition('fade in');
		},
		onBottomPassedReverse: function () {
			$('.fixed.menu').transition('fade out');
		}
	});
	// create sidebar and attach to menu open
	$('.ui.sidebar')
		.sidebar('attach events', '.toc.item');
	// just a moving title
	$('#waving-title').transition('bounce', '2000ms')
});