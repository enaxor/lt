// SEMANTIC UI


$(document).ready(function () {
	// pagination control
	// $('.jobsPagination').click(function () {
	// 	let pagenum = $(this).data("page")
	// 	console.log("pagenum=" + pagenum)
	// 	// $("#pageNumberHidden").val(pagenum)
	// 	$("#jobs-form-filter").submit(); return false;
	// });
	// location fix on clear
	$("#searchBox_Location").keyup(function () {
		if ($(this).val().length == 0) {
			$("#searchBox_HiddenLocationID").val('')
		}
	});

	// Sub Validations
	$('#formvaltest')
		.form({
			fields: {
				subnemail: {
					rules: [
						{
							type: 'email',
						}
					]
				},
				subperiodicity: {
					rules: [
						{
							type: 'empty'
						}
					]
				},
				subterms: {
					rules: [
						{
							type: 'empty'
						}
					]
				},
			}
		});
	$('#formvalconnected')
		.form({
			fields: {
				subperiodicityc: {
					rules: [
						{
							type: 'empty'
						}
					]
				},
			}
		});
	$("#mail-sub").on("click", function () {
		showSubscriptionModal();
	});
	$("#list-mail-sub").on("click", function () {
		showSubscriptionModal();
	});
	function showSubscriptionModal() {
		$('#mail-sub-modal')
			.modal({
				closable: false,
				duration: 0,
				onApprove: function () {
					if (!$('#formvaltest').form('is valid')) {
						return false;
					} else {
						return subcribeNewcomer()
					}
				}
			}).modal('show');
	}

	// geo TODO
	$("#location-near-me").click(function () {
		console.log('hehe')
		function success(position) {
			const latitude = position.coords.latitude;
			const longitude = position.coords.longitude;
			console.log("browser latitude=" + latitude + ", longitude=" + longitude)
			// get town list
			$.ajax({
				type: "POST",
				url: "/handleajaxgettownlistnearme",
				data: {
					lat: latitude,
					lon: longitude
				},
				beforeSend: function () {
					// todo icon loading
				},
				complete: function () {
					// todo
				},
				success: function (data) {
					// todo
					console.log(data)
				}
			});
		}
		function error() {
			console.log("Unable to retrieve your location")
		}
		if (!navigator.geolocation) {
			console.log("Geolocation is not supported by your browser")
		} else {
			navigator.geolocation.getCurrentPosition(success, error);
		}
	});
	// wandering tag filter
	$(".job-tag").click(function () {
		let id = $(this).data("tagid")
		let label = $(this).data("taglabel")
		addTag(id, label)
	});
});


// OLDER

var locationsSearch = []
let chipsTpl = template`<div data-chipsid=${0} class="chip bg-primary">${1}<a onclick='removeChips(${0},"${2}");return false;' class="btn btn-clear" href="#" aria-label="Close" role="button"></a></div>`
let salaryChipsTpl = template`<div data-chipsid=${0} class="chip bg-primary">${1}<a onclick='updateSalary(0);return false;' class="btn btn-clear" href="#" aria-label="Close" role="button"></a></div>`


$(document).ready(function () {
	// newletter
	$("#newslettersubscription_Params").val(location.search.substring(1));

	var inputTextHandler = $("#searchBox_Location")
	var peekBoxHandler = $("#location-suggestion")
	var loadingIcon = $("#location-suggestion-loading")
	var locationNearMeIcon = $("#location-near-me")
	var hiddenJSONResultHandler = $("#searchBox_HiddenLocationID")

	// work time
	// $('select[name="time-less"]').on('change', function () {
	// 	$("#jobs-form-filter").submit()
	// });
	// // contratf
	// $('select[name="contrat-less"]').on('change', function () {
	// 	$("#jobs-form-filter").submit()
	// });
	// // xp
	// $('select[name="xp-less"]').on('change', function () {
	// 	$("#jobs-form-filter").submit()
	// });
	// remote
	$('input[type=checkbox][name="remote-less"]').on('change', function () {
		let name = $(this).data("name");
		if ($(this).prop('checked') == true) {
			addChips(0, name, "remote")
		} else {
			removeChips(0, "remote");
		}
	});
	// salary
	$('#salary-range-input').on('change', function () {
		updateSalary($(this).val())
	});
	// $('#salary-less').on('keyup', function () {
	// 	updateSalary($(this).val())
	// });

	// location methods
	generateSuggestion = function (data) {
		var sb = "<ul class='menu'>"
		// define template
		let itemTpl = template`<li class='menu-item'><a href='#' onclick='selectFromSuggestion(${0},"${1}");return false;' title="${1}"><div class='tile-content'><small class="label">${2}</small> ${1}</div></a></li>`;
		$.each(data, function (_, value) {
			//	sb += itemTpl(value.ID, value.Label.replace("'", "&#39;"), value.PostalCode)
		});
		sb += "</ul>"
		return sb;
	}
	removeChipslocation = function (_id) {
		// update hidden input
		for (index = 0; index < locationsSearch.length; index++) {
			if (locationsSearch[index].id == _id) {
				locationsSearch.splice(index, 1)
			}
		}
		inputTextHandler.val('')	// clear input
		hiddenJSONResultHandler.val(JSON.stringify(locationsSearch));		// write json
		// remove chips
		$('#location-chips .chip[data-chipsid=' + _id + ']').remove();
	}
	// suggestion selection
	selectFromSuggestion = function (_id, _name) {
		// check exist
		for (index = 0; index < locationsSearch.length; index++) {
			if (locationsSearch[index].id == _id) {
				// do nothing
				return
			}
		}
		// update hidden input
		locationsSearch.push({ id: _id, name: _name })
		inputTextHandler.val('')	// clear input
		hiddenJSONResultHandler.val(JSON.stringify(locationsSearch));		// write json
		// add chips
		$('#location-chips').append(chipsTpl(_id, _name, "location"));
		// hide suggestion
		peekBoxHandler.hide();
		peekBoxHandler.empty();
	}
	// load suggestion
	processingPeekBox = false;
	inputTextHandler.keyup(function () {
		var keyword = $(this).val();
		if (keyword.length >= 1 && processingPeekBox != true) {
			$.ajax({
				type: "POST",
				url: "/handleajaxgetlocationlist",
				data: 'keyword=' + keyword,
				beforeSend: function () {
					processingPeekBox = true;
					loadingIcon.show();
				},
				complete: function () {
					loadingIcon.hide();
					processingPeekBox = false;
				},
				success: function (data) {
					if (data != null && data.length != 0) {
						var genx = generateSuggestion(data);
						peekBoxHandler.html(genx)
						peekBoxHandler.show();
					}
				}
			});
		}

	});
});



// Submitting filters to get another jobs page
function onJobsFilterSubmit(form) {
	// loader
	$("#filtering-status-icon").removeClass("right")
	$("#filtering-status-icon").removeClass("arrow")
	$("#filtering-status-icon").addClass("notched")
	$("#filtering-status-icon").addClass("circle")
	$("#filtering-status-icon").addClass("loading")
	// placeholders
	// $("#jobs-container-placeholder").removeClass("d-invisible")
	// $("#jobs-container-actual-jobs").addClass("d-hidden")

	$('body')
		.toast({
			message: 'Chargement...',
			class: 'inverted grey',
			position: 'top center',
			showProgress: 'bottom',
			classProgress: 'red',
			progressUp: true
		})
		;

	// parameter
	var searchParams = new URLSearchParams();
	// page
	// searchParams.set("p", $("#pageNumberHidden").val());
	// query
	var searchQuery = $("select[name='query-tags']").val();
	// var searchQuery = form.elements["query-less"].value
	console.log("searchQuery", searchQuery)
	if (searchQuery.length != 0) searchParams.set("q", searchQuery);
	// InputLocation string
	// var locationID = $("#searchBox_HiddenLocationID").val()
	// if (locationID > 0) searchParams.set("location", locationID);
	// // radius
	// var radius = $("input[name='location-radius-less']").val()
	// if (radius > 0) searchParams.set("radius", radius);
	// contrat
	var inputContrat = $("select[name='contrat-less']").val()
	if (inputContrat.length > 0) searchParams.set("contrat", genPlusParameter(inputContrat));
	// xp
	var checkedxp = $("select[name='xp-less']").val()
	if (checkedxp.length > 0) searchParams.set("xp", genPlusParameter(checkedxp));
	// salary
	// var inputSalaryMin = $("input[name='salary-less']").val()
	// if (inputSalaryMin != 0) searchParams.set("salary", inputSalaryMin);
	// time
	var checkedTime = $("select[name='time-less']").val()
	if (checkedTime.length > 0) searchParams.set("time", genPlusParameter(checkedTime));
	// InputRemote 0/1
	// if ($("input[name = 'remote-less']").prop("checked") == true) {
	// 	searchParams.set("nomad", 1);
	// }

	// TODO refaire full process to find glitch about Q / tag search

	// keyword tags
	var checkedKeywordTag = getSelectedTagIDs("tags")
	if (checkedKeywordTag.length > 0) searchParams.set("tag", genPlusParameter(checkedKeywordTag));
	// company tags
	var checkedCompanyTag = getSelectedTagIDs("company")
	if (checkedCompanyTag.length > 0) searchParams.set("c", checkedCompanyTag);
	// bool
	// if ($("input[name = 'SuggestedJobsOnly']").prop("checked") == true) {
	// 	searchParams.set("ronly", 1);
	// }
	// http://localhost:9000/jobs?p=1&order=1&ronly=1&sonly=1&aonly=1p=1&q=az&order=1
	// todo searchParams store in couki
	console.log(">> submit = ", searchParams.toString())
	var newURL = window.location.pathname.split("/").pop() + "?" + searchParams.toString() + "#jobs";
	// window.location.href = newURL;
	// document.location.href = newURL, true;
	// window.location = newURL;
	window.location.replace(newURL);
	return false
}

// !!! TODO KEYWORD == tag, tags(id,name,type) cleanup

/*** CHIPS GENERATION ***/

updateSalary = function (value) {
	$('#salary-less').val(value)
	$("#salary-range-input").val(value)
	removeChips(0, "salary", false);
	if (value > 0) {
		$('#salary-chips').append(salaryChipsTpl(0, ">= " + value + " €/year", "salary"));
	}
}

function removeChips(id, typeStr, update = true) {
	console.log("removeChips", id, "-", typeStr)
	if (typeStr == "remote") {
		// uncheck single box
		$('input[name = remote-less]').prop('checked', false);
	} else {
		// uncheck box
		$('input[name = ' + typeStr + '-less][value = ' + id + ']').prop('checked', false);
	}
	// delete chips
	$('#' + typeStr + '-chips .chip[data-chipsid=' + id + ']').remove();
	// update result
	// if (update) {
	// 	$("#jobs-form-filter").submit()
	// }
}

function addChips(id, name, typeStr) {
	console.log("add chipd name =", name)
	// check box
	$('input[name = ' + typeStr + '-less][value=' + id + ']').prop('checked', true);
	// add chips
	$('#' + typeStr + '-chips').append(chipsTpl(id, name, typeStr));
	// update result
	// $("#jobs-form-filter").submit()
}

// Subscription

function subcribeConnected() {
	console.log("subcribeConnected")
	if ($('#formvalconnected').form('is valid')) {
		console.log("subcribeConnected valid")
		var periodicityQuery = $("#subperiodicityc").val()
		var paramsQuery = location.search.substring(1)
		var processing = false
		if (!processing) {
			console.log("conected")
			$.ajax({
				type: "POST",
				url: "/handlecreatenewlettersubconnected",
				data: { params: paramsQuery, periodicity: periodicityQuery },
				beforeSend: function () {
					processing = true;
				},
				error: function () {
					processing = false;
					$('body')
						.toast({
							class: 'error',
							message: `Une erreur est survenue (c1).`
						});
				},
				success: function (data) {
					if (data == true) {
						$('body')
							.toast({
								title: 'Merci pour votre subscription',
								message: "L'incription à la newsletter a bien été prise en compte."
							});
						processing = false;
						return true;
					} else {
						$('body')
							.toast({
								class: 'error',
								message: `Une erreur est survenue (c2).`
							});
						processing = false;
						return false
					}
				}
			});
		}
	} else {
		console.log("subcribeConnected novalid")
		return false
	}
}

function subcribeNewcomer() {
	if ($('#formvaltest').form('is valid')) {
		var emailQuery = $('#subnemail').val()
		var periodicityQuery = $("#subperiodicity").val()
		var termsQuery = $('#subterms').prop("checked")
		var paramsQuery = location.search.substring(1)
		var processing = false

		if (!processing) {
			console.log("not conected")
			$.ajax({
				type: "POST",
				url: "/handlecreatenewlettersub",
				data: { mail: emailQuery, params: paramsQuery, periodicity: periodicityQuery, terms: termsQuery },
				beforeSend: function () {
					processing = true;
				},
				error: function () {
					processing = false;
					$('body')
						.toast({
							class: 'error',
							message: `Une erreur est survenue (1).`
						});
				},
				success: function (data) {
					if (data == true) {
						$('body')
							.toast({
								title: 'Merci pour votre subscription',
								message: 'Un email de confirmation va vous être envoyé !'
							});
						processing = false;
						return true;
					} else {
						$('body')
							.toast({
								class: 'error',
								message: `Une erreur est survenue (2).`
							});
						processing = false;
						return false
					}
				}
			});
		}
	} else {
		return false
	}
}

$(document).ready(function () {
	var win = $(window);
	var doc = $("#jobs-container-actual-jobs");
	var loader = $('#loading')
	var exhaust_msg = $('#job-page-end')
	var loading = false;
	var exhaust = $("#exhaust-div").data("exhaust")
	// Each time the user scrolls
	win.scroll(function () {
		// End of the document reached?
		if (exhaust == false && (doc.height() - win.height() <= win.scrollTop())) {
			loader.show();
			if (loading == false) {
				loading = true;
				$.ajax({
					url: '/lljobs',
					data: jQuery.param({ index: $("#jobs-container-actual-jobs").data("index") }),
					dataType: 'html',
					success: function (html) {
						$("#jobs-container-actual-jobs").data('index', $("#jobs-container-actual-jobs").data("index") + 1);
						$('#jobs-container-actual-jobs').append(html);
						loader.hide();
						loading = false;
						if (!html) {
							exhaust = true
							exhaust_msg.show();
							$('#job-page-end .ghost.icon')
								.transition({
									animation: 'jiggle',
									interval: 200
								})
								;
						}
					}
				});
			}
		}
	});
});



	// console.log("checkedContrat=", checkedContrat)
	// // $(form).append('<input type="text" name="contrat" value=' + checkedContrat + ' />');
	// //var serialform0 = $(form).serializeArray()
	// //console.log("serialform0=", serialform0)
	// //	$(form).append('contrats', checkedContrat);
	// //	serialform.contrat = checkedContrat

	// console.log("serialform1=", serialform1)

	// var data = JSON.stringify(serialform1);

	// console.log("onJobsFilterSubmit 1", data);

	// $.each(serialform1, function (key, value) {
	// 	console.log(key + ": " + value.name + " = " + value.value);
	// });
	// jobsQuery.SuggestedJobsOnly = $(form)[0]

	// selectMultiFromSuggestion = function (id, name) {
	// 	// fill inputs
	// 	hiddenJSONResultHandler.val(id);
	// 	inputTextHandler.val(name);
	// 	// hide suggestion
	// 	peekBoxHandler.hide();
	// 	peekBoxHandler.empty();
	// }
	// var genericCompletion = new LocationGenericCompletion(inputTextHandler, peekBoxHandler, loadingIcon, inputRadiusHandler, locationNearMeIcon, hiddenJSONResultHandler)
	// inputTextHandler.keyup(function () {
	// 	genericCompletion.ajaxCompletion(this)
	// });



	// class LocationGenericCompletion {

	// 	constructor(_inputTextHandler, _peekBoxHandler, _peekBoxloadingIcon, _inputRadiusHandler, _locationNearMeIcon, _hiddenJSONResultHandler) {
	// 		// user input
	// 		this.inputTextHandler = _inputTextHandler
	// 		this.inputRadiusHandler = _inputRadiusHandler
	// 		// peekbox
	// 		this.peekBoxHandler = _peekBoxHandler
	// 		this.peekBoxloadingIcon = _peekBoxloadingIcon
	// 		this.processingPeekBox = false
	// 		// around me
	// 		this.locationNearMeIcon = _locationNearMeIcon
	// 		// input send to server
	// 		this.hiddenJSONResultHandler = _hiddenJSONResultHandler



	// 	}


	// generateSuggestion = function (data) {
	// 	var sb = "<ul class='menu'>"
	// 	for (var i = 0; i < data.length; i++) {
	// 		var id = data[i].ID;
	// 		var label = data[i].Label;
	// 		// start string builder
	// 		sb += "<li class='menu-item'>"
	// 		// a
	// 		sb += "<a href='#' onclick='$(this).selectFromSuggestion(" + id + ",\"" + label + "\");return false;' title='" + label + "'>"
	// 		// div complete
	// 		sb += "<div class='tile tile-centered'><div class='tile-content'>" + label + "</div></div>"
	// 		// end string builder
	// 		sb += "</a></li>"
	// 	}
	// 	sb += "</ul>"
	// 	return sb;
	// }





	// updateChips = function () {
	// 	// define template
	// 	let chipsTpl = template`<div data-chipsid=${0} class="chip bg-primary">${1}<a onclick='removeChips(${0});return false;' class="btn btn-clear" href="#" aria-label="Close" role="button"></a></div>`
	// 	// let chipsTpl = template`<span data-chipsid=${0} class="chip">${1}<a  onclick='removeChips(${0});return false;' class="btn btn-clear" href="#" aria-label="Close" role="button"></a></span>`;
	// 	// clear
	// 	locationChipsContainer.empty()
	// 	// render html
	// 	$.each(locationsSearch, function (_, value) {
	// 		locationChipsContainer.append(chipsTpl(value.id, value.name))
	// 	});
	// 	// clear input
	// 	inputTextHandler.val('')
	// 	// write json
	// 	hiddenJSONResultHandler.val(JSON.stringify(locationsSearch));
	// }
	// addChipsloc = function (_id, _name) {
	// 	// check exist
	// 	for (index = 0; index < locationsSearch.length; index++) {
	// 		if (locationsSearch[index].id == _id) {
	// 			// do nothing
	// 			return
	// 		}
	// 	}
	// 	var location = { id: _id, name: _name }
	// 	locationsSearch.push(location)
	// 	updateChips()
	// }