$(document).ready(function () {

	// form validation
	$('#form-add-job')
		.form({
			fields: {
				newJob_Position: {
					rules: [
						{
							type: 'empty',
							prompt : 'Vous devez renseigner un titre'
						},
						{
							type: 'minLength[10]',
							prompt : 'Le titre de votre emploi est trop court (10 caractères min.)'
						}
					]
				},
				newJob_Regionals: {
					rules: [
						{
							type: 'empty',
							prompt : 'Vous devez renseigner une location'
						}
					]
				},
				newJob_Description: {
					rules: [
						{
							type: 'empty',
							prompt : 'Vous devez renseigner une description pour votre emploi'
						},
						{
							type: 'maxLength[10000]',
							prompt : 'Votre description est trop longue'
						},
						{
							type: 'minLength[20]',
							prompt : 'La description de votre emploi est trop courte'
						}
					]
				},
				newJob_Experience: {
					rules: [
						{
							type: 'empty',
							prompt : "Vous devez renseigner un niveau d'experience"
						}
					]
				},
				newJob_URLMailApply: {
					rules: [
						{
							type: 'empty',
							prompt : "Vous devez renseigner un moyen de vous contacter"
						}
					]
				},
			}
		})
		;
	// TODO rules email + url
});



function onJobAddSubmit(form) {
	console.log("onJobAddSubmit", form)
	$("#newJob_Tagstr").val(JSON.stringify(getSelectedTags()))
	return true
}

function openPreview() {
	var searchParams = new URLSearchParams();

	searchParams.set("position", $("#newJob_Position").val());

	// searchParams.set("job_description", $("#newJob_Description").val());

	searchParams.set("salary", $("#newJob_Salary").val());

	$('.contrat_item').each(function () {
		if ($(this).data('value') == $("#newJob_Contrat").val()) {
			searchParams.set("contrat", $(this).html());
		}
	});

	$('.remote_item').each(function () {
		if ($(this).data('value') == $("#newJob_FullRemote").val()) {
			searchParams.set("remote", $(this).html());
		}
	});

	$('.regional_item').each(function () {
		if ($(this).data('value') == $("#newJob_Regionals").val()) {
			searchParams.set("regional", $(this).data('value'));
		}
	});

	if ($("#newJob_FullRemote").is(':checked')) {
		searchParams.set("remote", 1);
	} else {
		searchParams.set("remote", 0);
	}

	$('.worktime_item').each(function () {
		if ($(this).data('value') == $("#newJob_Worktime").val()) {
			searchParams.set("worktime", $(this).html());
		}
	});

	$('.item_experience').each(function () {
		if ($(this).data('value') == $("#newJob_Experience").val()) {
			searchParams.set("experience", $(this).html());
		}
	});

	searchParams.set("description", $("#newJob_Description").val());

	var tags = getSelectedTags()
	let urltags = ""
	for (let i = 0; i < tags.length; i++) {
		if (i == 0) {
			urltags += tags[i].label
		} else {
			urltags += "," + tags[i].label
		}
	}
	searchParams.set("tags", urltags);

	var newURL = "http://localhost:9000/jobpreview" + "?" + searchParams.toString();
	window.open(newURL);
}