class Supercomplete {
    constructor() {
        this.userTextInput = $("#companyProfile_Location_Label")
        this.hiddenInputID = $("#companyProfile_Location_ID")
        this.suggestionBox = $("#location-peek-box")
        this.suggestionLoadingIcon = $("#location-peek-box-loading")
        this.processingLocations = false
    }
    selectFromSuggestion(id, name) {
        // fill inputs
        this.hiddenInputID.val(id);
        this.userTextInput.val(name);
        // hide suggestion
        $(this.suggestionBox).hide();
        $(this.suggestionBox).empty();
    }
    ajaxCompletion(inputElement) {
        var keyword = $(inputElement).val();
        console.log("keyup :", keyword)
        var that = this;
        if (keyword.length >= 1 && that.processingLocations != true) {
            $.ajax({
                type: "POST",
                url: "/handleajaxgetlocationlist",
                data: 'keyword=' + keyword,
                beforeSend: function () {
                    that.processingLocations = true;
                    $(that.suggestionLoadingIcon).show();
                },
                complete: function () {
                    $(that.suggestionLoadingIcon).hide();
                    that.processingLocations = false;
                },
                success: function (data) {
                    if (data != null) {
                        console.log(">> launch ", $(that.hiddenInputID))
                        var genx = that.generateSuggestion(data);
                        console.log(genx)
                        $(that.suggestionBox).html(genx);
                        $(that.suggestionBox).show();
                    }
                }
            });
        }
    }
    generateSuggestion(data) {
        var sb = "<ul class='menu'>"
        for (var i = 0; i < data.length; i++) {
            var id = data[i].ID;
            var label = data[i].Label;
            // start string builder
            sb += "<li class='menu-item'>"
            // a
            sb += "<a href='#' onclick='$(this).selectFromSuggestion(" + id + ",\"" + label + "\");return false;' title='" + label + "'>"
            // div complete
            sb += "<div class='tile tile-centered'><div class='tile-content'>" + label + "</div></div>"
            // end string builder
            sb += "</a></li>"
        }
        sb += "</ul>"
        return sb;
    }
}

$(document).ready(function () {
    var r = new Supercomplete()
    //var soc = Object.create(Supercomplete);
    // var userTextInput = $("#companyProfile_Location_Label")
    $('#companyProfile_Location_Label').keyup(function () {
        r.ajaxCompletion(this)
    });
});


// var Supercomplete = {
//     userTextInput: $("#companyProfile_Location_Label"),
//     hiddenInputID: $("#companyProfile_Location_ID"),
//     suggestionBox: $("#location-peek-box"),
//     suggestionLoadingIcon: $("#location-peek-box-loading"),
//     processingLocations: false,
//     ajaxCompletion: function () {
//         if ($(this).val().length >= 1 && processingLocations != true) {
//             $.ajax({
//                 type: "POST",
//                 url: "/handleajaxgetlocationlist",
//                 data: 'keyword=' + $(this).val(),
//                 beforeSend: function () {
//                     processingLocations = true;
//                     suggestionLoadingIcon.show();
//                 },
//                 complete: function () {
//                     suggestionLoadingIcon.hide();
//                     processingLocations = false;
//                 },
//                 success: function (data) {
//                     if (data != null) {
//                         suggestionBox.html(createLocationItem(data));
//                         suggestionBox.show();
//                     }
//                 }
//             });
//         }
//     },
//     selectLocation: function (id, name) {
//         // fill inputs
//         hiddenInputID.val(id);
//         userTextInput.val(name);
//         // hide suggestion
//         suggestionBox.hide();
//         suggestionBox.empty();
//     },
//     generateSuggestion: function (data) {
//         var sb = "<ul class='menu'>"
//         for (var i = 0; i < data.length; i++) {
//             var id = data[i].ID;
//             var label = data[i].Label;
//             // start string builder
//             sb += "<li class='menu-item'>"
//             // a
//             sb += "<a href='#' onclick='selectFromSuggestion(" + id + ",\"" + label + "\");return false;' title='" + label + "'>"
//             // div complete
//             sb += "<div class='tile tile-centered'><div class='tile-content'>" + label + "</div></div>"
//             // end string builder
//             sb += "</a></li>"
//         }
//         sb += "</ul>"
//         return sb;
//     }
// };

// var box = {
//     config: {
//         color: 'red'
//     },
//     init: function (config) {
//         $.extend(this.config, config);
//         return this
//     }
// };

// console.log(box) // ok
// var myBox = box.init({
//     color: 'blue'
// });
// console.log(myBox) // UNDEF ????



// OLD

// var DEBUG = true;

// // init dynamic vars
// var inputLocation = $("#companyProfile_Location_Label");
// var inputLocationHidden = $("#companyProfile_Location_ID");
// var inputLocationSuggestion = $("#location-peek-box");
// var inputLocationSuggestionLoading = $("#location-peek-box-loading");
// var processingLocations = false

// // dynamic funcs

// function createLocationItem(data) {
//     var sb = "<ul class='menu'>"
//     for (var i = 0; i < data.length; i++) {
//         var id = data[i].ID;
//         var label = data[i].Label;
//         // start string builder
//         sb += "<li class='menu-item'>"
//         // a
//         sb += "<a href='#' onclick='selectLocation(" + id + ",\"" + label + "\");return false;' title='" + label + "'>"
//         // div complete
//         sb += "<div class='tile tile-centered'><div class='tile-content'>" + label + "</div></div>"
//         // end string builder
//         sb += "</a></li>"
//     }
//     sb += "</ul>"
//     return sb;
// }

// function selectLocation(id, name) {
//     // fill inputs
//     inputLocationHidden.val(id);
//     inputLocation.val(name);
//     // hide suggestion
//     inputLocationSuggestion.hide();
//     inputLocationSuggestion.empty();
// }

// $(document).ready(function () {
//     inputLocation.keyup(function () {
//         if ($(this).val().length >= 1 && processingLocations != true) {
//             $.ajax({
//                 type: "POST",
//                 url: "/handleajaxgetlocationlist",
//                 data: 'keyword=' + $(this).val(),
//                 beforeSend: function () {
//                     processingLocations = true;
//                     inputLocationSuggestionLoading.show();
//                 },
//                 complete: function () {
//                     inputLocationSuggestionLoading.hide();
//                     processingLocations = false;
//                 },
//                 success: function (data) {
//                     if (data != null) {
//                         inputLocationSuggestion.html(createLocationItem(data));
//                         inputLocationSuggestion.show();
//                     }
//                 }
//             });
//         }
//     });
// });


// old

// var DEBUG = true;
// var locationProcessing = false

// // dynamic vars
// var inputLocation = $("#companyProfile_Location_Label");
// var inputLocationSuggestion = $("#location-peek-box");
// var inputLocationHidden = $("#companyProfile_Location_ID");

// // dynamic funcs

// function selectCountry(id, name) {
//     console.log("selectCountry", id, name)
//     inputLocationHidden.val(id);
//     inputLocation.val(name);
//     inputLocationSuggestion.hide();
//     inputLocationSuggestion.empty();
// }

// $(document).ready(function () {
//     inputLocation.keyup(function () {
//         if ($(this).val().length >= 1 && locationProcessing != true) {
//             $.ajax({
//                 type: "POST",
//                 url: "/handleajaxgetlocationlist",
//                 data: 'keyword=' + $(this).val(),
//                 beforeSend: function () {
//                     locationProcessing = true
//                     inputLocation.css("background", "#FFFFF00 url('/public/img/loading.gif') right 0px no-repeat");
//                     inputLocationSuggestion.show();
//                 },
//                 complete: function () {
//                     locationProcessing = false;
//                 },
//                 success: function (data) {
//                     if (data != null) {
//                         const tul = $('<ul>', {
//                                 class: "locationList"
//                             })
//                             .append(
//                                 data.map(country =>
//                                     $("<li>")
//                                     .attr('onClick', 'selectCountry("' + country.ID + '", "' + country.Label + '")')
//                                     .append($("<a>").text(country.Label))
//                                 )
//                             );
//                         inputLocation.css("background", "#FFFFF00");
//                         inputLocationSuggestion.html(tul);
//                         inputLocationSuggestion.show();
//                     } else {
//                         inputLocationSuggestion.hide();
//                         inputLocationSuggestion.empty();
//                     }
//                 }
//             });
//         } else {
//             inputLocationSuggestion.hide();
//             inputLocationSuggestion.empty();
//         }
//     });
// });