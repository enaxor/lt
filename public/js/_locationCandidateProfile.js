var DEBUG = true;

// dynamic vars
var inputLocation = $("#location-selector .location-input-all");
var inputLocationSuggestion = $("#location-selector .location-suggestion");
var inputLocationRadius = $('#candidateProfile_Location_SearchRadiusLocationID')

// dynamic funcs
function hideSuggestions() {
	inputLocationSuggestion.hide();
	inputLocationSuggestion.empty();
	inputLocation.css("background", "#FFF");
}

function selectCountry(id, cp, isDept, label) {
	inputLocation.val(label);
	hideSuggestions();
	searchLocationHandler.ID = id
	searchLocationHandler.PostalCode = cp
	searchLocationHandler.IsDepartement = isDept
	searchLocationHandler.Label = label
	refreshLocationHandlerInput()
}

$(document).ready(function () {
	// vars
	var processingLocationSuggestion = false;

	// static basic funcs
	function showSuggestionsReady(htmlContent) {
		const tul = $('<ul>', {
			class: "location-suggestion-list menu"
		}).append(
			htmlContent.map(country =>
				$("<li>").attr('onClick', 'selectCountry(' + country.ID + ', "' + country.PostalCode + '", ' + country.IsDepartement + ', "' + country.Label + '")').append($("<a>").text(country.PostalCode + ", " + country.Label))
			)
		);

		inputLocation.css("background", "#FFF");
		inputLocationSuggestion.html(tul);
		inputLocationSuggestion.show();
	}

	function processingSuggestions() {
		inputLocation.css("background", "#FFF url('/public/img/loading.gif') no-repeat scroll right 0px / 21px auto");
	}

	// static specific funcs
	function onLocationFormKeyUp() {
		if ($(this).val().length >= 1 && processingLocationSuggestion != true) {
			ajaxGetLocationAllList($(this).val());
		} else {
			hideSuggestions();
		}
	}

	// static ajax funcs
	function ajaxGetTownListNearMeCall(latitude, longitude) {
		$.ajax({
			type: "POST",
			url: "/handleajaxgettownlistnearme",
			data: {
				lat: latitude,
				lon: longitude
			},
			beforeSend: function () {
				processingLocationSuggestion = true
				processingSuggestions();
			},
			success: function (data) {
				if (DEBUG) console.log(data)
				if (data != null && data.length != 0) {
					showSuggestionsReady(data);
					processingLocationSuggestion = false
				} else {
					hideSuggestions();
					processingLocationSuggestion = false;
				}
			}
		});
	}

	function ajaxGetLocationAllList(keyword) {
		$.ajax({
			type: "POST",
			url: "/handleajaxgetlocationalllist",
			data: 'keyword=' + keyword,
			beforeSend: function () {
				processingLocationSuggestion = true
				processingSuggestions();
			},
			success: function (data) {
				if (DEBUG) console.log(data)
				if (data != null) {
					showSuggestionsReady(data);
					processingLocationSuggestion = false
				} else {
					hideSuggestions();
					processingLocationSuggestion = false;
				}
			}
		});
	}

	function geoFindMe() {

		function success(position) {
			const latitude = position.coords.latitude;
			const longitude = position.coords.longitude;
			if (DEBUG) console.log("browser latitude=" + latitude + ", longitude=" + longitude)
			// get town list
			ajaxGetTownListNearMeCall(latitude, longitude);
		}

		function error() {
			$.growl.error({
				message: "Unable to retrieve your location"
			});
		}

		if (!navigator.geolocation) {
			$.growl.error({
				message: "Geolocation is not supported by your browser"
			});
		} else {
			navigator.geolocation.getCurrentPosition(success, error);
		}
	}

	// query selectors
	document.querySelector('#location-selector .location-near-me').addEventListener('click', geoFindMe);
	document.querySelector('#location-selector .location-input-all').addEventListener("keyup", onLocationFormKeyUp);

	inputLocationRadius.change(function () {
		searchLocationHandler.RadiusID = parseInt($(this).val())
		console.log("parsed location == ", searchLocationHandler.RadiusID)
		refreshLocationHandlerInput()
	});

	// events
	$("input[id='candidateProfile.Nomade']:checkbox").change(function () {
		if ($(this).is(':checked')) {
			$('#candidate_location_selection').hide();
		} else {
			$('#candidate_location_selection').show();
		}
	});
});

window.onload = function () { }

// Retrieve location data from user input

function InitLocation() {
	if (searchLocationHandler != null && searchLocationHandler.ID.length != 0) {
		$('#candidateProfile_Location').val(searchLocationHandler.Label)
		//inputLocationRadius.val(searchLocationHandler.RadiusID)
		//refreshLocationRadiusContainer();
	}
}

function refreshLocationHandlerInput() {
	refreshLocationRadiusContainer();
	$('#candidateProfile_HiddenJsonLocation').val(JSON.stringify(searchLocationHandler))
}

function refreshLocationRadiusContainer() {
	if (searchLocationHandler.IsDepartement == true) {
		console.log("set to 0")
		searchLocationHandler.RadiusID = 0
		$('#locationRadiusContainer').hide()
	} else {
		$('#locationRadiusContainer').show()
	}
}