// semantic
$(document).ready(function () {
	$('.activating.element')
		.popup()
		;
	$('.label')
		.popup({
			position: 'left center',
			delay: {
				show: 0,
				hide: 0
			}
		})
		;
	$('.ui.dropdown').dropdown({
		duration: 0,
	});
	$('select.dropdown').dropdown({
		duration: 0,
	});
	$('.ui.radio.checkbox').checkbox();
	$('.ui.checkbox').checkbox();
	// accordion
	$('.ui.accordion').accordion({ exclusive: false });

	// location autocomplete
	$('.location-search').search({
		minCharacters: 1,
		apiSettings: {
			url: '/handleajaxgetlocationlist?keyword={query}',
		},
		fields: {
			results: 'Items',
			title: 'Label',
			description: 'PostalCode',
		},
		onSelect: function (result, _) {
			$("#searchBox_HiddenLocationID").val(result.ID)
			return true
		},
	});

	// industry
	$('.industry-search').search({
		minCharacters: 1,
		apiSettings: {
			url: '/handleajaxgetindustrylist?keyword={query}',
		},
		fields: {
			results: 'Items',
			title: 'Label',
		},
		onSelect: function (result, _) {
			$("#searchBox_HiddenIndustryID").val(result.ID)
			return true
		},
	});

});


// generic tools

function template(strings, ...keys) {
	return (function (...values) {
		let dict = values[values.length - 1] || {};
		let result = [strings[0]];
		keys.forEach(function (key, i) {
			let value = Number.isInteger(key) ? values[key] : dict[key];
			result.push(value, strings[i + 1]);
		});
		return result.join('');
	});
}

// Post to the provided URL with the specified parameters.
function postJSON(path, parameters) {
	var form = $('<form></form>');

	form.attr("method", "post");
	form.attr("action", path);

	var field = $('<input></input>');
	field.attr("type", "hidden");
	field.attr("name", "json");
	field.attr("value", JSON.stringify(parameters));
	form.append(field)

	// The form needs to be a part of the document in
	// order for us to be able to submit it.
	$(document.body).append(form);
	form.submit();
}

function goBack() {
	window.history.back();
}

function genParameter(param, values) {
	if (param.length == 0) {
		param = values
	} else {
		param = param + " " + values
	}
	return param
}

function genPlusParameter(array) {
	var ret = ""
	for (let i = 0; i < array.length; ++i) {
		if (i == 0) {
			ret += array[i]
		} else {
			ret += " " + array[i]
		}
	}
	return ret
}

function openModal(modalID) {
	$('#' + modalID).addClass("active")
}
function closeModal(modalID) {
	$('#' + modalID).removeClass("active")
}