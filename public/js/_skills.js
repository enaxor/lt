let tagTpl = template`<a data-tagid=${0} nohref onClick="$(this).remove();updateTagstr();" class="ui large teal skilltag basic label">${1}</a>`

let tagSearchTpl = template`<a data-tooltip="Supprimer" data-position="bottom center" data-tagid=${0} data-tagtype="tags" nohref onClick="$(this).remove();" class="ui large teal skilltag basic label">#${1}</a>&nbsp;<div class="ui small bottom center popup">Supprimer</div>`

let maxTag = 20

$(document).ready(function () {

	// tag algo job creation
	$('#skill-search').search({
		minCharacters: 1,
		clearable: true,
		selectFirstResult: true,
		apiSettings: {
			url: '/handleajaxgettaglist?keyword={query}',
		},
		fields: {
			results: 'Items',
			title: 'Label',
		},
		onSelect: function (res, _) {
			if (getSelectedTags().length < maxTag) {
				addTag(res.ID, res.Label)
			} else {
				toastTagError()
			}
			return true; // false: prevent dropdown closing and futher operation
		},
		showNoResults: false	
	});
	$("#skill-force-add-btn").click(function () {
		if (getSelectedTags().length < maxTag) {
			addTag(0, $("#skill-search-input").val())
		} else {
			toastTagError()
		}
	});

	
	// tag algo search jobs
	$('#job-tag-search').search({
		type: 'customType', 
		templates: {	
			customType: function(response){
				ret = ""
			console.log("special", response)
			console.log("res", response.Items[0].Label)
			for (i = 0 ; i < response.Items.length ; i++) {
				ret += '<a class="item result active"><div class="content"><div class="title">#' + response.Items[i].Label + '</div></div></a>'
			}
			return ret
		}},		
		minCharacters: 1,
		clearable: true,
		selectFirstResult: true,
		apiSettings: {
			url: '/handleajaxgettaglist?keyword={query}',
		},
		fields: {
			results: 'Items',
			title: 'Label',
		},
		onSelect: function (res, _) {
			if (getSelectedTags().length < maxTag) {
				addSearchTag(res.ID, res.Label)
			} else {
				toastTagError()
			}
			return true; // false: prevent dropdown closing and futher operation
		},
		showNoResults: false,
		
	});
	$("#skill-force-add-btn").click(function () {
		if (getSelectedTags().length < maxTag) {
			addSearchTag(0, $("#skill-search-input").val())
		} else {
			toastTagError()
		}
	});	

});



function toastTagError() {
	$('body')
		.toast({
			class: 'info',
			message: maxTag + ' #tags maximum atteints',
			showProgress: 'bottom',
			classProgress: 'teal'
		});
}

function addTag(id, label) {
	if (label.length > 0 && !isTagExist(id, label)) {
		$("#job-tag-handler").append(tagTpl(id, label))
		$("#skill-search-input").val('')
		updateTagstr()
	}
}

function addSearchTag(id, label) {
	if (label.length > 0 && !isTagExist(id, label)) {
		$("#job-tag-handler").append(tagSearchTpl(id, label))
		$("#skill-search-input").val('')
		updateTagstr()
	}
}

function isTagExist(id, label) {
	let tags = getSelectedTags()
	for (let i = 0; i < tags.length; i++) {
		if (tags[i].label.toLowerCase() === label.toLowerCase()) {
			return true
		}
		if (id != 0 && tags[i].id === id) {
			return true
		}
	}
	return false
}
function updateTagstr() {
	let tags = getSelectedTags()
	$("#candidateProfile_Tagstr").val(JSON.stringify(tags))
}
function getSelectedTags() {

	let ret = []
	$("#job-tag-handler .skilltag").each(function (_) {
		let tag = {}
		tag.id = $(this).data("tagid")
		tag.label = $(this).text()
		ret.push(tag)
	});
	return ret
}
function getSelectedTagIDs(tagtype) {
	let ret = []
	$("#job-tag-handler a").each(function (_) {
		let id = $(this).data("tagid")
		if ($(this).data("tagtype") == tagtype) {
			ret.push(id)
		}
	});
	return ret
}

