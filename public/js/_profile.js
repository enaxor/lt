
$(document).ready(function () {
	// avatar
	function refreshLogo(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#avatar-preview').css('background-image', 'url(' + e.target.result + ')');
				$('#avatar-preview').hide();
				$('#avatar-preview').fadeIn(610);
			}
			reader.readAsDataURL(input.files[0]);
			// toast
			$('body')
				.toast({
					message: "N'oublier pas de sauvegarder vos préférences !"
				});
		}
	}
	$("#companyProfile_Logo").change(function () {
		refreshLogo(this);
	});
	$("#candidateProfile_Avatar").change(function () {
		refreshLogo(this);
	});
	// cv
	$("#deleteCV").click(function () {
		$("#candidateProfile_HasDeletedCV").val(true)
		$("#cvinput").show()
		$("#cvmanage").hide()
	});
	$('#cvinputtext, .ui.button', '#cvinput').on('click', function (e) {
		$('#candidateProfile_CV', $(e.target).parents()).click();
	});
	$('#candidateProfile_CV', '#cvinput').on('change', function (e) {
		var name = e.target.files[0].name;
		$('#cvinputtext', $(e.target).parent()).val(name);
	});

});