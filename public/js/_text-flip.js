var intervalCount = 0
var refreshIntervalId = setInterval(function () {
	$('.text-flip :first-child').addClass('flip-first');
	$('.text-flip :nth-child(2)').addClass('flip-second');

	setTimeout(function () {
		$('.text-flip :nth-child(2)').removeClass('flip-second');
		$('.text-flip :last-child').prependTo('.text-flip');
		$('.text-flip :nth-child(2)').removeClass('flip-first');
	}, 4500)

	// stop after x iterations
	if (intervalCount > 4) {
		clearInterval(refreshIntervalId);
	}
	intervalCount++;
}, 4000);