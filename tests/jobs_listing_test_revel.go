package tests

import "net/url"

// TestJobs test listing jobs with multiples parameters
func (t *JobsListing) TestJobs() {
	// POST no parameters REMOVED
	// t.PostForm("/jobs", url.Values{})
	// t.AssertOk()
	// GET no parameters
	t.Get("/jobs?")
	t.AssertOk()
	// all parameters
	var params url.Values = url.Values{
		"p":        []string{"1"},
		"q":        []string{"design"},
		"location": []string{"42"},
		"radius":   []string{"2"},
		"contrat":  []string{"2+3"},
		"xp":       []string{"1+3"},
		"salary":   []string{"1000"},
		"time":     []string{"1"},
		"nomad":    []string{"1"},
		"tag":      []string{"1+3+4"},
		"ronly":    []string{"1"},
		"sonly":    []string{"1"},
	}
	t.Get("/jobs?" + params.Encode())
	t.AssertOk()
	// GET bad parameters
	t.Get("/jobs?meh=lol&bloup=blip")
	t.AssertOk()
}

// TestJobsPage test jobs pages
func (t *JobsListing) TestJobsPage() {
	// list job
	println("========================================== TestJobsPage.list job")
	t.Get("/job")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	t.Get("/jobs")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	// list jobs of a company
	println("========================================== TestJobsPage.list jobs of a company")
	t.Get("/company/da8ab055-1309-4385-a15f-da1ecbd49362/jobs")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	// 1 job
	println("========================================== TestJobsPage.1 job")
	t.Get("/job/editor-engineer-at-slite-d3609625-b6ab-47ed-8bf7-0f39e4db7490")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	// 1 unexistant job
	println("========================================== TestJobsPage.1 unexistant job")
	t.Get("/job/meh123")
	t.AssertContains("job-a42dd19g-error")
	t.AssertContentType("text/html; charset=utf-8")
	// 1 expired job
	println("========================================== TestJobsPage.1 expired job")
	t.Get("/job/java-application-development-manager-at-equian-1dda8204-709f-4661-8d0d-e9a4104d358c")
	t.AssertContains("job-a42dd19g-error")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestSimpleRedirectionPages stub
func (t *JobsListing) TestSimpleRedirectionPages() {

	// wrong agregate id
	println("========================================== TestSimpleRedirectionPages.wrong agregate id")
	t.Get("/ajob/meh")
	t.AssertContains("oops-8827e258-error")
	t.AssertContentType("text/html; charset=utf-8")

	// redirection (i don't want to send shit)
	// println("========================================== TestJobsPage.redirection")
	// t.Get("/ajob/a707f3a3-b6d7-32b2-86ee-3229339ddf73-d-veloppeur-full-stack-j")
	// t.AssertNotFound() // old stuff, old 404
	// t.AssertContentType("text/html; charset=utf-8")
	// t.Get("/ajob/meh123")
	// t.AssertNotFound()
	// t.AssertContentType("text/html; charset=utf-8")
	// t.Get("/job/postuler/editor-engineer-at-slite-d3609625-b6ab-47ed-8bf7-0f39e4db7490")
	// // t.AssertNotFound() whatever to what test url return
	// t.AssertContentType("text/html; charset=UTF-8") // UTF caps for exeample.com mon cher
	// t.Get("/job/postuler/meh123")
	// t.AssertNotFound()
	// t.AssertContentType("text/html; charset=utf-8")
}
