package tests

import "github.com/revel/revel/testing"

/** pense bête des codes de page et d'erreur **/
// about-3f6537ca
// subscribe-fce9eacd
// user-d3b8fca8
// users-21f1a40d
// login-e5097dab
// recruit-d8959382 to relete
// companies-61c26c62
// dashboard-3e4472d5
// newjob-8aa1a140
// profile-9b67cd20
// oops-8827e258
// company-8d82df06
// success-2c8c13e1
// companyjobs-3975db6a
// settings-74ffdca7
// jobs-a40dc07f
// job-a40dc07f
// 404-b6avvq4g

// App just simple page
type App struct {
	testing.TestSuite
}

// AccountManagement tests related to account settings, login / logout, subscribe
type AccountManagement struct {
	testing.TestSuite
}

// CandidateMatching tests related to visiting candidate profile, match candidate for a job
type CandidateMatching struct {
	testing.TestSuite
}

// JobManagement tests related to job creation, deletion, saving, activation, ré-activation
type JobManagement struct {
	testing.TestSuite
}

// JobsListing tests related to job list, filter job, matched job, saved jobs, all kind of filters
type JobsListing struct {
	testing.TestSuite
}

// NewsletterManagement tests related to newsletter subscription, unsubscription, send of mcoked mails
type NewsletterManagement struct {
	testing.TestSuite
}

// ProfileManagement tests related to profile update (candidate + company)
type ProfileManagement struct {
	testing.TestSuite
}
