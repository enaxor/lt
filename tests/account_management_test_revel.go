package tests

import (
	"bytes"
	"net/url"

	"lt/app/db"

	"github.com/go-playground/form"
)

/*
** TEST ABOUT ACCOUNT CREATION / CONNECTION / DELETION
 */

// logs
const emailApplicant = "rsp+lt_test_applicant@rxsoft.eu"
const passwordApplicant = "azeqsdwxc"

var encoder *form.Encoder

// Before Create account and login
func (t *AccountManagement) Before() {
}

// After Delete account and logout
func (t *AccountManagement) After() {
	if user := db.GetWebUserEmail("temporary_lt_email@rxsoft.eu"); user != nil {
		db.DeleteUser(user.ID)
	}
	if user := db.GetWebUserEmail(emailApplicant); user != nil {
		db.DeleteUser(user.ID)
	}
}

// TestCreateDeleteAccount stub
func (t *AccountManagement) TestCreateDeleteAccount() {
	//println(string(t.ResponseBody))
	println("========================================== TestCreateDeleteAccount.createAccount")
	createAccount(t, emailApplicant, passwordApplicant)
	println("========================================== TestCreateDeleteAccount.login")
	login(t, emailApplicant, passwordApplicant)
	// delete account
	println("========================================== TestCreateDeleteAccount.deleteAccount")
	deleteAccount(t, emailApplicant, passwordApplicant)
}

// TestSettingPage test settings page
func (t *AccountManagement) TestSettingPage() {
	// predelete
	println("========================================== TestSettingPage.predelete")
	// deleteAccount(t, "temporary_lt_email@rxsoft.eu", passwordApplicant)
	// deleteAccount(t, emailApplicant, passwordApplicant)

	// recreate
	println("========================================== TestSettingPage.recreate")
	createAccount(t, emailApplicant, passwordApplicant)

	// login
	println("========================================== TestSettingPage.login")
	login(t, emailApplicant, passwordApplicant)

	// go to settings
	println("========================================== TestSettingPage.go to settings")
	t.Get("/space/settings")
	t.AssertOk()
	t.AssertContains("settings-74ffdca7")
	t.AssertNotContains("settings-74ffdca7-error")
	t.AssertContentType("text/html; charset=utf-8")

	// change user email POST /postsettingemail App.PostUpdateUserEmail
	println("========================================== TestSettingPage.change user email")
	settingEmailData := url.Values{}
	newEmail := "temporary_lt_email@rxsoft.eu"
	settingEmailData.Set("webUserEmailSetting.Email", newEmail)
	t.PostForm("/postsettingemail", settingEmailData)
	t.AssertOk()
	t.AssertContains("settings-74ffdca7") // todo code d'erreur
	t.AssertNotContains("settings-74ffdca7-error")

	// relogin
	println("========================================== TestSettingPage.relogin")
	logout(t)
	login(t, newEmail, passwordApplicant)

	// change user password POST /postsettingpassword App.PostUpdateUserPassword
	println("========================================== TestSettingPage.change user password ")
	settingPasswordData := url.Values{}
	// incorrect changes of password
	settingPasswordData.Set("webUserPasswordSetting.Password", passwordApplicant)
	settingPasswordData.Set("webUserPasswordSetting.NewPassword", "")
	settingPasswordData.Set("webUserPasswordSetting.NewPasswordConfirmation", "")
	t.PostForm("/postsettingpassword", settingPasswordData)
	t.AssertOk()
	t.AssertContains("settings-74ffdca7-error")
	settingPasswordData.Set("webUserPasswordSetting.Password", "badpassword")
	settingPasswordData.Set("webUserPasswordSetting.NewPassword", "badpassword")
	settingPasswordData.Set("webUserPasswordSetting.NewPasswordConfirmation", "badpassword")
	t.PostForm("/postsettingpassword", settingPasswordData)
	t.AssertOk()
	t.AssertContains("settings-74ffdca7-error")
	t.AssertContentType("text/html; charset=utf-8")
	newPassword := "meh"
	settingPasswordData.Set("webUserPasswordSetting.Password", passwordApplicant)
	settingPasswordData.Set("webUserPasswordSetting.NewPassword", newPassword)
	settingPasswordData.Set("webUserPasswordSetting.NewPasswordConfirmation", newPassword)
	t.PostForm("/postsettingpassword", settingPasswordData)
	t.AssertOk()
	t.AssertContains("settings-74ffdca7")
	t.AssertNotContains("settings-74ffdca7-error")

	// 	log out
	println("========================================== TestSettingPage.log out")
	logout(t)

	// test old password
	println("========================================== TestSettingPage.test old password")
	loginData := url.Values{}
	loginData.Set("user.Email", emailApplicant)
	loginData.Set("user.Password", passwordApplicant)
	t.Get("/login")
	t.PostForm("/handlelogin", loginData)
	t.AssertOk()
	t.AssertContains("login-e5097dab-error")

	// revert password
	println("========================================== TestSettingPage.revert password")
	loginData.Set("user.Email", newEmail)
	loginData.Set("user.Password", newPassword)
	t.Get("/login")
	t.PostForm("/handlelogin", loginData)
	t.AssertOk()
	t.AssertContains("dashboard-3e4472d5")
	println("TestSettingPage will delete now")

	// clean test account
	println("========================================== TestSettingPage.clean test account")
	deleteAccount(t, newEmail, newPassword)
}

func createAccount(t *AccountManagement, email, password string) {
	//testing.NewTestSuite().Client.Get("")
	subData := url.Values{}
	subData.Set("subscription.Email", email)
	subData.Set("subscription.Password", password)
	subData.Set("subscription.PasswordConfirm", password)
	subData.Set("subscription.Terms", "true")
	// visit sub page
	t.Get("/subscribe")
	t.AssertOk()
	t.AssertContains("subscribe-fce9eacd")
	t.AssertContentType("text/html; charset=utf-8")
	// create account
	t.PostForm("/handlecreateaccount", subData)
	t.AssertOk()
	t.AssertContains("dashboard-3e4472d5")
	t.AssertContentType("text/html; charset=utf-8")
	// log out
	logout(t)
}

func login(t *AccountManagement, email, password string) {
	// visit login page
	t.Get("/login")
	t.AssertContains("login-e5097dab")
	t.AssertContentType("text/html; charset=utf-8")
	// correct login
	loginData := url.Values{}
	loginData.Set("user.Email", email)
	loginData.Set("user.Password", password)
	// log in
	t.PostForm("/handlelogin", loginData)
	t.AssertOk()
	t.AssertContains("dashboard-3e4472d5")
	t.AssertContentType("text/html; charset=utf-8")
}

func logout(t *AccountManagement) {
	// log out
	t.Get("/handlelogout")
	t.AssertOk()
	t.AssertContains("login-e5097dab")
	t.AssertContentType("text/html; charset=utf-8")
}

func deleteAccount(t *AccountManagement, email, password string) {
	// log in
	loginData := url.Values{}
	loginData.Set("user.Email", email)
	loginData.Set("user.Password", password)
	t.Get("/login")
	t.PostForm("/handlelogin", loginData)
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	if bytes.Contains(t.ResponseBody, []byte("dashboard-3e4472d5")) {
		// go to settings
		t.Get("/space/settings")
		t.AssertOk()
		t.AssertContains("settings-74ffdca7")
		// delete account
		t.PostForm("/handledeleteaccount", nil)
		t.AssertOk()
		t.AssertContains("success-2c8c13e1")
		t.AssertContentType("text/html; charset=utf-8")
	} else {
		t.AssertContains("login-e5097dab-error")
	}
}
