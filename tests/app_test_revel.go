package tests

/*
** TEST BASIC GET REQUETS
 */

// TestJobsPage vitrines page return 200
func (t *App) TestJobs1Page() {
	// App.Index
	t.Get("/")
	t.AssertOk()
	t.AssertContains("jobs-a40dc07f") // no index
	t.AssertContentType("text/html; charset=utf-8")
}

// TestJobJobsPages vitrines page return 200
func (t *App) TestJobs2Pages() {
	// App.Jobs
	t.Get("/job")
	t.AssertOk()
	t.AssertContains("jobs-a40dc07f")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestJobsPages vitrines page return 200
func (t *App) TestJobs3Pages() {
	// App.Jobs
	t.Get("/jobs")
	t.AssertOk()
	t.AssertContains("jobs-a40dc07f")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestAboutPage vitrines page return 200
func (t *App) TestAboutPage() {
	// App.About
	t.Get("/about")
	t.AssertOk()
	t.AssertContains("about-3f6537ca")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestCompaniesPage vitrines page return 200
func (t *App) TestCompaniesPage() {
	// App.Companies
	t.Get("/company")
	t.AssertOk()
	t.AssertContains("companies-61c26c62")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestCompanyJobsPages vitrines page return 200
func (t *App) TestCompanyJobsPage() {
	// App.CompanyJobs
	t.Get("/company/:id/jobs")
	t.AssertOk()
	t.AssertContains("companyjobs-3975db6a")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestCompanyPages vitrines page return 200
func (t *App) TestCompanyPage() {
	// App.Company
	t.Get("/company/uplever")
	t.AssertOk()
	t.AssertContains("company-8d82df06")
	t.AssertContentType("text/html; charset=utf-8")
}

func (t *App) TestLogoutPage() {
	// App.PostLogout
	t.Get("/handlelogout")
	t.AssertOk()
	t.AssertContains("login-e5097dab")
	t.AssertContains("Vous avez été déconnecté")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestJobPage vitrines page return 200
func (t *App) TestJobPage() {
	// App.Job
	t.Get("/job/business-development-manager-dae36d87-194c-4867-9e56-a786b3aa599e")
	t.AssertOk()
	t.AssertContains("job-a42dd19g")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestLoginPage vitrines page return 200
func (t *App) TestLoginPage() {
	// App.Login
	t.Get("/login")
	t.AssertOk()
	t.AssertContains("login-e5097dab")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestOopsPages vitrines page return 200
func (t *App) TestOopsPages() {
	// App.Oops
	t.Get("/oops")
	t.AssertOk()
	t.AssertContains("jobs-a40dc07f") // no erreur, get redirected to /
	t.AssertContentType("text/html; charset=utf-8")
}

// TestJobsPages vitrines page return 200
func (t *App) TestsubscribePages() {
	// App.Subscribe
	t.Get("/subscribe")
	t.AssertOk()
	t.AssertContains("subscribe-fce9eacd")
	t.AssertContentType("text/html; charset=utf-8")
}

// TestJobsPages vitrines page return 200
func (t *App) TestJobsPages() {
	// App.Success
	t.Get("/success")
	t.AssertOk()
	t.AssertContains("jobs-a40dc07f") // no success to show, get redirected to /
	t.AssertContentType("text/html; charset=utf-8")
}

// TestSimple404Pages stub
func (t *App) TestSimple404Pages() {
	// old id
	t.Get("/company/da8ab055-1309-4385-a15f-da1ecbd49362")
	t.AssertContains("error-8d82df06")
	t.AssertContentType("text/html; charset=utf-8")

	// 1 unexistant company
	t.Get("/company/meh123")
	t.AssertContains("error-8d82df06")
	t.AssertContentType("text/html; charset=utf-8")

	// revel block favicon get requests
	t.Get("/favicon.ico")
	t.AssertNotFound()
	t.AssertContentType("text/html; charset=utf-8")
}

// TestSimpleFeedPages stub
func (t *App) TestSimpleFeedPages() {
	// App.JobsFeed
	t.Get("/jobs/feed")
	t.AssertOk()
	t.AssertContains("RSS")
	t.AssertContentType("text/plain; charset=utf-8")
}
