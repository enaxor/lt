package tests

import (
	"lt/app/db"
	"net/url"
)

// App.NewsletterConfirm
//	t.Get("/newsletter/confirm/:id") // todo
// 	t.Get("/oops") 	t.Get("/success")

// TestNewsletterSubManagement stub
// Email           string
// Periodicity     int
// Params          string
// Terms           bool
// ActivationGUUID *guuid.UUID
// QueryFilter JobsInputSearchFilters
func (t *NewsletterManagement) TestNewsletterSubManagement() {
	// var subcription uimodels.NewsletterSubscription
	// subcription.Email = "rsp+lt_test@rxsoft.eu"
	// subcription.Periodicity = 1
	// subcription.Params = "remote=1"
	// subcription.Terms = true
	// insert newsletter
	saveData := url.Values{}
	saveData.Set("mail", "rsp+lt_test@rxsoft.eu")
	saveData.Set("params", "remote=1")
	saveData.Set("periodicity", "1")
	saveData.Set("terms", "true")
	// test w/o session
	t.PostForm("/handlecreatenewlettersub", saveData)
	t.AssertStatus(403)
	// visit index page
	t.Get("/")
	t.PostForm("/handlecreatenewlettersub", saveData)
	t.AssertContains("true")
	t.AssertOk()
	// method for test purpose only: get confirmation ID from a mail sub
	activationCode, err := db.GetSubActivationCode("rsp+lt_test@rxsoft.eu")
	t.AssertEqual(0, err)
	t.AssertNotEqual(nil, activationCode)
	// confirmation
	t.Get("/newsletter/confirm/" + activationCode.String())
	t.AssertContains("Merci pour votre inscription.")
	t.AssertOk()
	// deletion
	t.Get("/newsletter/delete/" + activationCode.String())
	t.AssertContains("Vous avez bien été désincrit")
	t.AssertOk()
	// todo test filter
	// todo test mock mails
}

func (t *NewsletterManagement) TestConfirmPage() {
	t.Get("/newsletter/confirm/:id")
	t.AssertOk()
	t.AssertContains("success-2c8c13e1")
	t.AssertContentType("text/html; charset=utf-8")
}
