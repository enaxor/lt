package tests

// Before tests
func (t *App) Before() {
	println("Set up")
}

// After tests
func (t *App) After() {
	println("Tear down")
}

// deprecated tests

// deleteAccount(t, emailApplicant, passwordApplicant)
// deleteAccount(t, "temporary_lt_email@rxsoft.eu", passwordApplicant)

// TestCreateDeleteAccount test company account creation/deletion

// TestLoginLogout stub
// func (t *AccountManagement) TestLoginLogout() {
// 	// correct login
// 	loginData := url.Values{}
// 	loginData.Set("user.Email", emailApplicant)
// 	loginData.Set("user.Password", passwordApplicant)
// 	// visit login page
// 	t.Get("/login")
// 	t.AssertContains("login-e5097dab")
// 	t.AssertContentType("text/html; charset=utf-8")
// 	// log in
// 	t.PostForm("/handlelogin", loginData)
// 	t.AssertOk()
// 	t.AssertContains("dashboard-3e4472d5")
// 	t.AssertContentType("text/html; charset=utf-8")
// 	// log out
// 	t.PostForm("/handlelogin", loginData)
// 	t.AssertOk()
// 	t.Get("/handlelogout")
// 	t.AssertContains("login-e5097dab")
// 	// incorrect login 1
// 	loginData.Set("user.Email", "notanemail")
// 	loginData.Set("user.Password", passwordApplicant)
// 	t.PostForm("/handlelogin", loginData)
// 	t.AssertOk()
// 	t.AssertContains("error-e5097dab")
// 	//	t.AssertContains("Identifiants incorrects") // todo code d'erreur
// 	// incorrect login 2
// 	loginData.Set("user.Email", emailApplicant)
// 	loginData.Set("user.Password", "badpassword")
// 	t.PostForm("/handlelogin", loginData)
// 	t.AssertOk()
// 	t.AssertContains("error-e5097dab")
// 	//t.AssertContains("Identifiants incorrects") // todo code d'erreur
// 	// // log in again
// 	// loginData.Set("user.Email", emailApplicant)
// 	// loginData.Set("user.Password", passwordApplicant)
// 	// t.PostForm("/handlelogin", loginData)
// 	// t.AssertOk()
// 	// t.AssertContains("dashboard-3e4472d5")
// 	// t.AssertContentType("text/html; charset=utf-8")
// }

// TestLoginPage just login
// func (t *AccountManagement) TestLoginPage() {
// 	println("TestLoginPage start")
// 	// log in
// 	loginData := url.Values{}
// 	loginData.Set("user.Email", emailApplicant)
// 	loginData.Set("user.Password", passwordApplicant)
// 	t.PostForm("/handlelogin", loginData)
// 	t.AssertOk()
// 	t.AssertContains("dashboard-3e4472d5")
// 	t.AssertContentType("text/html; charset=utf-8")
// 	println("TestLoginPage stop")
// }

// ***************************************

// TestIndexPage check 200 for this page
// func (t *App) TestIndexPage() {
// 	t.Get("/")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestDebugPage check 200 for this page
// func (t *App) TestDebugPage() {
// 	t.Get("/debug")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestAboutPage check 200 for this page
// func (t *App) TestAboutPage() {
// 	t.Get("/about")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestRecruitPage check 200 for this page
// func (t *App) TestRecruitPage() {
// 	t.Get("/recruit")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestJobsPage check 200 for this page
// func (t *App) TestJobsPage() {
// 	t.Get("/job")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestJobPage check 200 for this page
// func (t *App) TestJobPage() {
// 	t.Get("/job/AZERTY123")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestLoginPage check 200 for this page
// func (t *App) TestLoginPage() {
// 	t.Get("/login")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestSubscribePage check 200 for this page
// func (t *App) TestSubscribePage() {
// 	t.Get("/subscribe")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestCompaniesPage check 200 for this page
// func (t *App) TestCompaniesPage() {
// 	t.Get("/company")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestCompanyPage check 200 for this page
// func (t *App) Test2CompanyPage() {
// 	t.Get("/company/8b816229-2ff3-44e1-b8bd-5d4b6cb00f1d")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestUsersPage check 200 for this page
// func (t *App) TestUsersPage() {
// 	t.Get("/user")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestUserPage check 200 for this page
// func (t *App) TestUserPage() {
// 	t.Get("/user/da8ab055-1309-4385-a15f-da1ecbd49368")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")
// }

// // TestRobotsPage check 200 for this robot page
// func (t *App) TestRobotsPage() {
// 	t.Get("/public/robots.txt")
// 	t.AssertOk()
// 	t.AssertContentType("text/plain; charset=utf-8")
// }

// // TestFavIcon check 200 for this public icon
// func (t *App) TestFavIcon() {
// 	t.Get("/public/img/favicon.ico")
// 	t.AssertOk()
// 	t.AssertContentType("image/x-icon")
// }
