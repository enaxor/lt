package tests

import (
	"lt/app/db"
	"testing"
)

func TestAddNewJob(t *testing.T) {
	type args struct {
		publicKey        string
		employerID       string
		contrat          string
		experience       string
		remoteRegularity string
		worktime         string
		position         string
		description      string
		location         string
		salaryRange      string
		expireDate       string
		featured         string
		skills           []string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// got, got1 := db.AddJob(tt.args.publicKey, tt.args.employerID, tt.args.contrat, tt.args.experience, tt.args.remoteRegularity, tt.args.worktime, tt.args.position, tt.args.description, tt.args.location, tt.args.salaryRange, tt.args.expireDate, tt.args.featured, tt.args.skills)
			// if got != tt.want {
			// 	t.Errorf("AddNewJob() got = %v, want %v", got, tt.want)
			// }
			// if got1 != tt.want1 {
			// 	t.Errorf("AddNewJob() got1 = %v, want %v", got1, tt.want1)
			// }
		})
	}
}

func TestDeleteJob(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := db.DeleteJob(tt.args.id); got != tt.want {
				t.Errorf("DeleteJob() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetCompanyDataFromWebuser(t *testing.T) {
	type args struct {
		userID string
	}
	tests := []struct {
		name   string
		args   args 
		want   string
		// want1  []db.Industry
		want2  map[int]bool
		// want3  db.CompanySize
		// want4  db.Location
		want5  string
		want6  string
		want7  string
		want8  string
		want9  string
		want10 string
		want11 string
		want12 string
		want13 int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// got, got1, got2, got3, got4, got5, got6, got7, got8, got9, got10, got11, got12, got13 := db.GetCompany(tt.args.userID)
			// if got != tt.want {
			// 	t.Errorf("GetCompanyDataFromWebuser() got = %v, want %v", got, tt.want)
			// }
			// if !reflect.DeepEqual(got1, tt.want1) {
			// 	t.Errorf("GetCompanyDataFromWebuser() got1 = %v, want %v", got1, tt.want1)
			// }
			// if !reflect.DeepEqual(got2, tt.want2) {
			// 	t.Errorf("GetCompanyDataFromWebuser() got2 = %v, want %v", got2, tt.want2)
			// }
			// if !reflect.DeepEqual(got3, tt.want3) {
			// 	t.Errorf("GetCompanyDataFromWebuser() got3 = %v, want %v", got3, tt.want3)
			// }
			// if !reflect.DeepEqual(got4, tt.want4) {
			// 	t.Errorf("GetCompanyDataFromWebuser() got4 = %v, want %v", got4, tt.want4)
			// }
			// if got5 != tt.want5 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got5 = %v, want %v", got5, tt.want5)
			// }
			// if got6 != tt.want6 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got6 = %v, want %v", got6, tt.want6)
			// }
			// if got7 != tt.want7 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got7 = %v, want %v", got7, tt.want7)
			// }
			// if got8 != tt.want8 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got8 = %v, want %v", got8, tt.want8)
			// }
			// if got9 != tt.want9 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got9 = %v, want %v", got9, tt.want9)
			// }
			// if got10 != tt.want10 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got10 = %v, want %v", got10, tt.want10)
			// }
			// if got11 != tt.want11 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got11 = %v, want %v", got11, tt.want11)
			// }
			// if got12 != tt.want12 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got12 = %v, want %v", got12, tt.want12)
			// }
			// if got13 != tt.want13 {
			// 	t.Errorf("GetCompanyDataFromWebuser() got13 = %v, want %v", got13, tt.want13)
			// }
		})
	}
}

func TestPersonalTest(t *testing.T) {
	// var locationTown int64
	// var arr []int64
	// //b := db.IToiSlice(&locationTown)
	// if locationTown != 0 {
	// 	arr = append(arr, locationTown)
	// }
	// t.Errorf("not inited int = %v", arr)
}
