package tests

// TestCandidatePage Test Users Pages
// todo: candidate are available only for logged company
// otherwise redirect to login
func (t *CandidateMatching) TestCandidatePage() {
	// list
	t.Get("/user")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	t.Get("/users")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	// an user
	t.Get("/user/da8ab055-1309-4385-a15f-da1ecbd49361")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
	t.Get("/user/meh123")
	t.AssertNotFound()
	t.AssertContentType("text/html; charset=utf-8")
}
