package tests

import (
	"lt/app/utils"
	"testing"
	"time"
)

func TestCreatePublicKey(t *testing.T) {
	type args struct {
		jobPosition string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	// for _, tt := range tests {
	// 	t.Run(tt.name, func(t *testing.T) {
	// 		if got := utils.CreatePublicKey(tt.args.jobPosition); got != tt.want {
	// 			t.Errorf("CreatePublicKey() = %v, want %v", got, tt.want)
	// 		}
	// 	})
	// }
}

func TestJoin(t *testing.T) {
	type args struct {
		strs []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := utils.Join(tt.args.strs...); got != tt.want {
				t.Errorf("Join() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetSimpleSinceHumanReadableDate(t *testing.T) {
	type args struct {
		input time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := utils.GetSimpleSinceHumanReadableDate(tt.args.input); got != tt.want {
				t.Errorf("GetSimpleSinceHumanReadableDate() = %v, want %v", got, tt.want)
			}
		})
	}
}
