package tests

import "net/url"

// create account + login
// create new job
// admin: valider
// archiver
// editer todo v2
// suppression

// postulation / ajob redirection

// newjob-8aa1a140
//	println(string(t.ResponseBody))

// TestJobCreation stub
func (t *JobManagement) TestJobCreation() {
	loginData := url.Values{}
	loginData.Set("user.Email", "rsp+lt_e0@rxsoft.eu")
	loginData.Set("user.Password", "azeqsdwxc")
	// visit login
	t.Get("/login")
	t.AssertContains("login-e5097dab")
	t.AssertOk()
	// connect user
	t.PostForm("/handlelogin", loginData)
	t.AssertContains("dashboard-3e4472d5")
	t.AssertOk()
	// visit new job page
	t.Get("/board/job/new")
	t.AssertContains("newjob-8aa1a140")
	t.AssertOk()
	// create new job
	newjobData := url.Values{}
	//	newjobData.Set("NewJob.UserID", "test")
	newjobData.Set("newJob.Position", "UX/UI Designer/Lead")
	newjobData.Set("newJob.Description", "Ready and able to lead our UX/UI activities, and act as the senior stakeholder for this area;")
	newjobData.Set("newJob.URLMailApply", "https://example.com")
	newjobData.Set("newJob.Tagstr", "[{\"id\":16,\"label\":\"Design\"}]")
	newjobData.Set("newJob.Contrat", "2")
	newjobData.Set("newJob.Experience", "2")
	newjobData.Set("newJob.Worktime", "2")
	newjobData.Set("newJob.Salary", "1000")
	newjobData.Set("newJob.Regionals", "0")
	newjobData.Set("newJob.FullRemote", "false")
	newjobData.Set("newJob.Step", "2")
	t.PostForm("/handlenewjob", newjobData)
	t.AssertContains("dashboard-3e4472d5") // or preview, or payement, or success
	// archiving
	archivingData := url.Values{}
	archivingData.Set("jobPublicID", "todo")
	t.PostForm("/postarchivejob", archivingData)
	t.AssertNotContains("error-3e4472d5")
	t.AssertOk()
	// unarchive
	t.PostForm("/postunarchivejob", archivingData)
	t.AssertNotContains("error-3e4472d5")
	t.AssertOk()
	// suppression
	t.PostForm("/postarchivejob", archivingData) // archive first
	t.PostForm("/postdeletejob", archivingData)
	t.AssertNotContains("error-3e4472d5")
	t.AssertOk()
}

// TestCandidateSaveJob test save/unsave job functionality
// type: "POST",
// url: "/handleajaxsaveunsavejob",
// data: 'jobid=' + jobID + '&toggle=' + !saved,
// func (t *JobManagement) TestCandidateSaveJob() {
// 	// test unconnected saving (should not work and redirect to login page)
// 	saveData := url.Values{}
// 	saveData.Set("jobid", "java-application-development-manager-at-equian-1dda8204-709f-4661-8d0d-e9a4104d358c") // save with public id
// 	saveData.Set("toggle", "true")
// 	t.PostForm("/handleajaxsaveunsavejob", saveData)
// 	t.AssertOk()
// 	t.AssertContains("login-e5097dab")

// 	// saveData.Set("jobid", "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12") // old id - should not work

// 	// try to log user without session
// 	loginData := url.Values{}
// 	loginData.Set("user.Email", "rsp+lt_c0@rxsoft.eu")
// 	loginData.Set("user.Password", "azeqsdwxc")

// 	// visit login
// 	t.Get("/login")
// 	t.AssertContains("Se connecter")
// 	t.AssertOk()

// 	// connect user
// 	t.PostForm("/handlelogin", loginData)
// 	t.AssertContains("Bonjour")
// 	t.AssertOk()

// 	// go to job page
// 	t.Get("/job/python-software-engineer-at-smiledirectclub-b6c2dd14-4c04-44ea-9298-d335503b5f45")
// 	t.AssertOk()
// 	t.AssertContentType("text/html; charset=utf-8")

// 	// save job
// 	saveData.Set("jobid", "python-software-engineer-at-smiledirectclub-b6c2dd14-4c04-44ea-9298-d335503b5f45") // save with public id
// 	saveData.Set("toggle", "true")
// 	t.PostForm("/handleajaxsaveunsavejob", saveData)
// 	t.AssertOk()

// 	// list saved jobs with 1
// 	t.Get("/jobs?sonly=1")
// 	t.AssertContains("1 job found") // this test doees not pass

// 	// unsave job
// 	saveData.Set("jobid", "python-software-engineer-at-smiledirectclub-b6c2dd14-4c04-44ea-9298-d335503b5f45") // save with public id
// 	saveData.Set("toggle", "false")
// 	t.PostForm("/handleajaxsaveunsavejob", saveData)
// 	t.AssertOk()

// 	// list saved jobs with 0
// 	t.Get("/jobs?sonly=1")
// 	t.AssertContains("0 jobs found")

// 	// log out
// 	t.Get("/handlelogout")
// 	t.AssertContains("Vous avez été déconnecté.")
// }
