package tests

import (
	"lt/app/db"
	"net/url"
)

// profile-9b67cd20
// settings-74ffdca7
// dashboard-3e4472d5

// Before Run this before a request
func (t *ProfileManagement) Before() {
}

// After Run this after request
func (t *ProfileManagement) After() {
}

// TestProfileManagement tests
func (t *ProfileManagement) TestProfileManagement() {
	//println(string(t.ResponseBody))
	println("========================================== TestProfileManagement.createAccount")
	//testing.NewTestSuite().Client.Get("")
	subData := url.Values{}
	subData.Set("subscription.Email", emailApplicant)
	subData.Set("subscription.Password", passwordApplicant)
	subData.Set("subscription.PasswordConfirm", passwordApplicant)
	subData.Set("subscription.Terms", "true")
	// visit sub page
	t.Get("/subscribe")
	t.AssertOk()
	t.AssertContains("subscribe-fce9eacd")
	t.AssertContentType("text/html; charset=utf-8")
	// create account
	t.PostForm("/handlecreateaccount", subData)
	t.AssertOk()
	t.AssertContains("dashboard-3e4472d5")
	t.AssertContentType("text/html; charset=utf-8")

	webuser := db.GetWebUserEmail(emailApplicant)
	t.AssertNotEqual(webuser, nil)
	// update profile
	loginData := url.Values{}
	loginData.Set("user.UserID", webuser.ID.String())
	loginData.Set("user.Name", "emailApplicant")
	loginData.Set("user.Logo", "emailApplicant")
	loginData.Set("user.Location", "emailApplicant")
	loginData.Set("user.Industry", "emailApplicant")
	loginData.Set("user.CompanySize", "emailApplicant")
	loginData.Set("user.Founded", "emailApplicant")
	loginData.Set("user.Description", "emailApplicant")
	loginData.Set("user.Website", "emailApplicant")
	loginData.Set("user.Linkedin", "emailApplicant")
	loginData.Set("user.Facebook", "emailApplicant")
	loginData.Set("user.Twitter", "emailApplicant")
	loginData.Set("user.Instagram", "emailApplicant")
	loginData.Set("user.Apps", "emailApplicant")
	loginData.Set("user.Avatar", "emailApplicant")
	t.PostForm("/postcompanyprofile", nil)
	// delete account
	println("========================================== TestProfileManagement.deleteAccount")
	t.Get("/space/settings")
	t.AssertOk()
	t.AssertContains("settings-74ffdca7")
	t.PostForm("/handledeleteaccount", nil)
	t.AssertOk()
	t.AssertContains("success-2c8c13e1")
	t.AssertContentType("text/html; charset=utf-8")

}
