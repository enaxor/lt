#!/bin/sh

# psql lt < gen.sql 2>&1 | grep -v CREATE | grep -v DROP | grep -v NOTICE | grep -v DETAIL;
# psql lt < genDataTown.sql 2>&1 | grep -v INSERT ;
# psql lt < genData.sql 2>&1 | grep -v DELETE | grep -A2 ERROR ;

psql lt < gen.sql 2>&1 ;
psql lt < genDataTown.sql 2>&1 | grep -v INSERT ;
psql lt < genData.sql 2>&1  ;

# homemade data for next tests
psql lt < genTestData.sql 2>&1  ;

exit 0
