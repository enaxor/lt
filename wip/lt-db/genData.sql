-- create extension "uuid-ossp";


-- RESET
DELETE FROM langage CASCADE;
DELETE FROM user_type CASCADE;
DELETE FROM job_regional;
DELETE FROM company_size CASCADE;
DELETE FROM job_contrat CASCADE;
DELETE FROM job_experience CASCADE;
DELETE FROM job_time CASCADE;
DELETE FROM social_app CASCADE;
DELETE FROM location_town_available_radius CASCADE;
DELETE FROM tag CASCADE;
DELETE FROM newsletter_regularity CASCADE;
DELETE FROM industry CASCADE;

-- BASE
INSERT INTO
langage(label)
VALUES  ('English'),
('Mandarin'),
('Spanish'),
('German'),
('Portuguese'),
('Arabic'),
('Japanese'),
('Russian'),
('Hindi');

INSERT INTO
user_type (id, label)
VALUES  (0, 'special'),
(2, 'employer');

INSERT INTO
job_regional (id, label)
VALUES  (0, 'Autres (non spécifié)'),
(1, 'Partout dans le monde'),
(2, 'France uniquement'),
(3, 'Europe uniquement');

INSERT INTO
company_size (id, label)
VALUES  (0, ''),
(1, 'TPE (< 10 salariés)'),
(2, 'PME (10 à 249)'),
(3, 'ETI (250 à 4 999)'),
(4, 'GE (> 5 000)');

INSERT INTO
job_contrat (id, label)
VALUES (0, 'Autre / Non spéficié'),
(1, 'CDI'),
(2, 'CDD'),
(3, 'Freelance'),
(4, 'Stage'),
(5, 'Interim'),
(6, 'Contrat Pro'),
(7, 'Apprentissage');

INSERT INTO
job_experience (id, label)
VALUES (0, ''),
(1, 'Junior'),
(2, 'Senior')
;

INSERT INTO
job_time (id, label)
VALUES (0, ''),
(1, 'Temps Plein'),
(2, 'Temps Partiel');

INSERT INTO
social_app (id, label)
VALUES  (1, 'Mail'),
(2, 'Téléphone'),
(3, 'Drive'),
(4, 'Slack'),
(5, 'Skype'),
(6, 'Whatsapp'),
(7, 'Gitlab'),
(8, 'Github'),
(9, 'Discord'),
(10,'Dropbox'),
(11,'Cloud');

INSERT INTO
location_town_available_radius(id, km)
VALUES (0, 0);

INSERT INTO
location_town_available_radius(km)
VALUES (5),
(10),
(20),
(40)
;
-- TODO 100km

INSERT INTO
tag (label)
VALUES  ('animation 3d'),
('animation flash'),
('comptabilité'),
('administration'),
('pigiste'),
('facturation'),
('programmation'),
('coaching'),
('rédacteur'),
('service à la clientèle'),
('saisie de données'),
('big data'),
('recrutement'),
('base de données'),
('e-book'),
('design'),
('développeur'), -- todo sql like % that ignore accent
('web'),
('cryptomonaies'),
('jeux vidéos'),
('graphiste'),
('marketing '),
('formation'),
('secrétariat'),
('ecrivain'),
('presse'),
('chef de projet'),
('chercheur'),
('analyste'),
('langues étrangères'),
('support technique'),
('télémarketing'),
('transcription'),
('voyage'),
('dactylo'),
('edition'),
('vidéos'),
('informatique'),
('support'),
('technicien'),
('internet'),
('content manager'),
('traduction'),
('photo'),
('art'),
('immobilier'),
('business consultant'),
('conseiller juridique'),
('documentation');

INSERT INTO
job_status(id, label)
VALUES (0, 'created'), -- or cancelled
(1, 'pending'), -- need your validation
(2, 'online'),  -- an online job can go back to validation
(3, 'archived'),
(4, 'deleted'),
(5, 'unvalidated') -- failed to be validated by me, user need to reedit job
;

INSERT INTO
newsletter_regularity (label)
VALUES ('daily'),
('weekly'),
('monthly')
;

INSERT INTO
industry (id, label)
VALUES  (0, '');

INSERT INTO
industry (label)
VALUES  ('Agroalimentaire'),
('Assurance'),
('Automobile'),
('BTP'),
('Internet'),
('Banque'),
('Chaussure'),
('Chimie'),
('Commerce'),
('Communication'),
('Distribution'),
('Habillement'),
('Imprimerie'),
('Industrie pharmaceutique'),
('Informatique / Télécoms'),
('Multimédia'),
('Métallurgie'),
('Plastique'),
('Recrutement'),
('Services aux entreprises'),
('Textile'),
('eCommerce'),
('Édition'),
('Électricité'),
('Études / conseils'),
('Jeux vidéos'),
('Web'),
('Design'),
('Graphisme'),
('Audio'),
('Marketing'),
('Support'),
('Business')
;


-- DEBUG
-- INSERT INTO
-- webuser (id, type, avatar, email, password, subscription_date, email_validated, disabled_account, last_connection_date) VALUES
-- 				-- Placeholder for an unknown company (aggregation or deleted account) or an unknown candidate (deleted account)
-- 				('d7427962-ba6d-4127-9264-beae1d9b0a5e', 2, 'default.png', 'rsp+lt_e0@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now());

-- -- COMPANY
-- INSERT INTO
-- company  (user_id, company_name, industry, website, linkedin, facebook, twitter, instagram, founded, location, company_size, description) VALUES
-- 					('d7427962-ba6d-4127-9264-beae1d9b0a5e', 'uplever', 7, 'https://example.com/company', 'kinkedin.com/uplevel', 'effebé.com/uplevel', '@uplevel', 'instagram/@uplever', '2014', 953, 2, 'UpLever is a non stop company from the upper space.')
--           ;
