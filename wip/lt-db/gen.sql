-- CREATE EXTENSION pgcrypto;
-- CREATE EXTENSION hstore;
-- cat ~/devel/go/src/lt/lt-db/gen.sql | grep CREATE | tac | sed -e 's/CREATE TABLE/DROP TABLE IF EXISTS/g' -e 's/(/;/g' -e 's/"//g' 

DROP TABLE IF EXISTS newsletter_queue ;
DROP TABLE IF EXISTS newsletter_subscription ;
DROP TABLE IF EXISTS job_keywords ;
DROP TABLE IF EXISTS keyword ;
DROP TABLE IF EXISTS job_langage ;
DROP TABLE IF EXISTS langage ;
DROP TABLE IF EXISTS job_tag ;
DROP TABLE IF EXISTS job ;
DROP TABLE IF EXISTS aggregate_job_tag ;
DROP TABLE IF EXISTS aggregate_job ;
DROP TABLE IF EXISTS company_social_app ;
DROP TABLE IF EXISTS social_app ;
DROP TABLE IF EXISTS company ;
DROP TABLE IF EXISTS industry ;
DROP TABLE IF EXISTS webuser ;
DROP TABLE IF EXISTS tag ;
DROP TABLE IF EXISTS location_town_available_radius ;
DROP TABLE IF EXISTS location_town ;
DROP TABLE IF EXISTS location_agglomeration ;
DROP TABLE IF EXISTS location_department ;
DROP TABLE IF EXISTS newsletter_regularity ;
DROP TABLE IF EXISTS job_regional ;
DROP TABLE IF EXISTS user_type ;
DROP TABLE IF EXISTS job_time ;
DROP TABLE IF EXISTS job_contrat ;
DROP TABLE IF EXISTS job_status;
DROP TABLE IF EXISTS job_experience ;
DROP TABLE IF EXISTS company_size ;


CREATE TABLE "company_size" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE "job_experience" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE "job_status" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE "job_contrat" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE "job_time" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE "user_type" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" TEXT NOT NULL UNIQUE
);

CREATE TABLE "job_regional" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL
);

CREATE TABLE "newsletter_regularity" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL
);

CREATE TABLE "location_department" (
	"id" serial NOT NULL PRIMARY KEY, -- neccessary iso location_town
	"code_postal" VARCHAR(5) NOT NULL UNIQUE,
	"label" VARCHAR(255) NOT NULL,
	"pretty_label" VARCHAR(255) NOT NULL
);
-- 105 items /!\ if count change, modify sequence below (and golang sequence) to keep a concistent && unique location ID

CREATE TABLE "location_agglomeration" (
	"id" serial NOT NULL PRIMARY KEY,
	"codes_postaux" VARCHAR(5) ARRAY NOT NULL UNIQUE, -- ex: 75001-75002-75003-75004-75005..
	"pretty_label" VARCHAR(255) NOT NULL -- ex: toute la ville de Paris
);
ALTER SEQUENCE location_agglomeration_id_seq RESTART WITH 105;
-- 53 items /!\ if count change, modify sequence below (and golang sequence) to keep a concistent && unique location ID

CREATE TABLE "location_town" (
	"id" serial NOT NULL PRIMARY KEY, -- neccessary because code_postal is not an unique value
	"code_dept" VARCHAR(3) NOT NULL references location_department(code_postal),
	"code_postal" VARCHAR(5) NOT NULL,
	"label" VARCHAR(255) NOT NULL,
	"pretty_label" VARCHAR(255) NOT NULL,
	"lat" numeric,
	"lon" numeric
);
-- 36832 items
ALTER SEQUENCE location_town_id_seq RESTART WITH 158;
-- last location town id == 36988

CREATE TABLE "location_town_available_radius" (
	"id" serial NOT NULL PRIMARY KEY,
	"km" integer NOT NULL
);

CREATE TABLE "tag" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL UNIQUE,
	"external" BOOLEAN DEFAULT false
);

CREATE TABLE "webuser" (
	"id" uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
	"type" integer NOT NULL references user_type(id),
	"email" VARCHAR(255) NOT NULL UNIQUE,
	"password" text NOT NULL,
	"password_request_id" uuid DEFAULT NULL,
	"password_request_id_date" TIMESTAMPTZ DEFAULT NULL,	
	"avatar" VARCHAR(255) NOT NULL DEFAULT 'default.png',
	"email_validated" BOOLEAN NOT NULL DEFAULT 'false',
	"disabled_account" BOOLEAN NOT NULL DEFAULT 'false',
	"subscription_date" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"last_connection_date" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "industry" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL
);

CREATE TABLE "company" (
	"user_id" uuid NOT NULL references webuser(id) ON DELETE CASCADE PRIMARY KEY,
	"id" serial NOT NULL,
	"public_id" VARCHAR(255) DEFAULT NULL UNIQUE, -- company name + eventually concat with id
	"company_name" VARCHAR(255) NOT NULL DEFAULT '',
	"company_size" integer NOT NULL DEFAULT 0 references company_size(id),
	"description" TEXT NOT NULL DEFAULT '',
	"industry" integer NOT NULL DEFAULT 0 references industry(id),
    "phone" VARCHAR(255) NOT NULL DEFAULT '',
	"website" VARCHAR(255) NOT NULL DEFAULT '',
	"linkedin" VARCHAR(255) NOT NULL DEFAULT '',
	"twitter" VARCHAR(255) NOT NULL DEFAULT '',
	"facebook" VARCHAR(255) NOT NULL DEFAULT '',
	"instagram" VARCHAR(255) NOT NULL DEFAULT '',
	"founded" VARCHAR(4) NOT NULL DEFAULT '',
	"location" integer NOT NULL DEFAULT 0 CHECK ("location" <= 36988),
	"viewed" integer NOT NULL DEFAULT 0
);
ALTER SEQUENCE company_id_seq RESTART WITH 70; -- hide real db serials from users 

CREATE TABLE "social_app" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL
);

CREATE TABLE "company_social_app" (
	"social_app_id" integer NOT NULL references social_app(id),
	"user_id" uuid NOT NULL references company(user_id) ON DELETE CASCADE ,
	CONSTRAINT company_social_app_pk PRIMARY KEY ("social_app_id","user_id")
);

CREATE TABLE "aggregate_job" (
	"id" uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
	"public_id" VARCHAR(255) NOT NULL UNIQUE,
	"source" integer NOT NULL,
	"company_name" VARCHAR(255) NOT NULL,
	"contrat" integer NOT NULL references job_contrat(id),
	"experience" integer NOT NULL references job_experience(id),
	"worktime" integer NOT NULL references job_time(id),
	"full_remote" BOOLEAN NOT NULL,
	"location" integer NOT NULL DEFAULT 0 references location_town(id),
	"position" VARCHAR(255) NOT NULL,
	"description" text NOT NULL,
	"url" text NOT NULL,
	"posted_date" timestamptz NOT NULL ,
	"expire_date" timestamptz NOT NULL
);

CREATE TABLE "aggregate_job_tag" (
	"aggregate_job_id" uuid NOT NULL references aggregate_job(id),
	"tag_id" integer NOT NULL references tag(id),
	CONSTRAINT aggregate_job_tag_pk PRIMARY KEY ("aggregate_job_id","tag_id")
);

CREATE TABLE "job" (
	"id" uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
	"public_id" VARCHAR(255) NOT NULL UNIQUE,
	"company_id" uuid NOT NULL references company(user_id) ON DELETE CASCADE ,
	-- job data
	"contrat" integer NOT NULL references job_contrat(id),
	"experience" integer NOT NULL references job_experience(id),
	"full_remote" BOOLEAN NOT NULL,
	"worktime" integer NOT NULL references job_time(id),
	"regional" integer NOT NULL references job_regional(id),
	"position" VARCHAR(255) NOT NULL,
	"description" text NOT NULL DEFAULT '', -- can be empty if url exist (external job)
	"url_or_mail_apply" text NOT NULL DEFAULT '',
	"salary" integer NOT NULL DEFAULT 0,
	"featured" BOOLEAN NOT NULL DEFAULT 'false',
	-- current job stats 0:?, 1:NeedValidation, 2:Online, 3:Archived, 4:Deleted, 5: unvalided/need edition
	"status" integer NOT NULL DEFAULT 0 references job_status(id),
	-- statistics
	"viewed" bigint NOT NULL DEFAULT 0,
	"redirection" bigint NOT NULL DEFAULT 0,
	-- dates
	"post_date" timestamptz NOT NULL DEFAULT now(), -- maybe update this when validated ?
	"expire_date" timestamptz NOT NULL
);

CREATE TABLE "job_tag" (
	"job_id" uuid NOT NULL references job(id) ON DELETE CASCADE ,
	"tag_id" integer NOT NULL references tag(id) ON DELETE CASCADE,
	CONSTRAINT job_tag_pk PRIMARY KEY ("job_id","tag_id")
);

CREATE TABLE "langage" (
	"id" serial NOT NULL PRIMARY KEY,
	"label" VARCHAR(255) NOT NULL
);

CREATE TABLE "job_langage" (
	"job_id" uuid NOT NULL references job(id) ON DELETE CASCADE,
	"langage_id" integer NOT NULL references langage(id),
	CONSTRAINT job_langage_pk PRIMARY KEY ("job_id","langage_id")
);

CREATE TABLE "keyword" (
	"id" serial NOT NULL PRIMARY KEY,
	"keyword" VARCHAR(255) NOT NULL,
	"public" BOOLEAN NOT NULL DEFAULT 'false'
);

CREATE TABLE "job_keywords" (
	"job_id" uuid NOT NULL references job(id) ON DELETE CASCADE ,
	"keyword_id" integer NOT NULL references keyword(id) ON DELETE CASCADE ,
	"count" bigint DEFAULT 0 NOT NULL,
	CONSTRAINT job_keywords_pk PRIMARY KEY ("job_id","keyword_id")
);

CREATE TABLE "newsletter_subscription" (
	"id" serial NOT NULL UNIQUE,
	"email" VARCHAR(255) NOT NULL,
	"params" text NOT NULL DEFAULT '',
	"regularity" integer NOT NULL references newsletter_regularity(id),
	"active" BOOLEAN NOT NULL DEFAULT 'false',
	"activation_code" uuid NOT NULL,
    "subscription_date" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT newsletter_subscription_pk PRIMARY KEY ("email","params")
);

CREATE TABLE "newsletter_queue" (
	"id" serial NOT NULL PRIMARY KEY,
	"email" VARCHAR(255) NOT NULL,
	"title" VARCHAR(255) NOT NULL,
	"content_html" TEXT NOT NULL,
	"content_text" TEXT NOT NULL,
	"created_at" timestamptz NOT NULL default now(),
	"sent" BOOLEAN NOT NULL DEFAULT false,
	"sent_time" timestamptz DEFAULT NULL
);

-- PRIVATE

DROP TABLE IF EXISTS private.logs ;

CREATE TABLE private."logs" (
	"id" serial NOT NULL PRIMARY KEY,
	"date" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP, -- T
	"module" TEXT DEFAULT NULL, -- Module
	"lvl" TEXT DEFAULT NULL, -- Lvl
	"caller" TEXT DEFAULT NULL, -- Caller
	"section" TEXT DEFAULT NULL, -- Section
	"controller" TEXT DEFAULT NULL, -- Controller
	"method" TEXT DEFAULT NULL, -- Method
	"action_path" TEXT DEFAULT NULL, -- ActionPath
	"name" TEXT DEFAULT NULL, -- Name
	"error" TEXT DEFAULT NULL, -- Error
	"msg" TEXT DEFAULT NULL, -- Msg
	"controller_type" TEXT DEFAULT NULL, -- ControllerType
	"elapsed" bigint DEFAULT 0,
	"json" json NOT NULL -- full log
);
