

initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'

systemctl start postgresql.service

sudo su postgres

createuser --interactive

createdb lt -O shgz

psql

\c lt




 CREATE SCHEMA IF NOT EXISTS private AUTHORIZATION shgz;
 CREATE TABLE private."logs" (
	"id" serial NOT NULL PRIMARY KEY,
	"message" json,
	"date" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);