-- create extension "uuid-ossp";


-- RESET
DELETE FROM webuser CASCADE;
DELETE FROM company CASCADE;
DELETE FROM job CASCADE;
DELETE FROM job_tag CASCADE;
DELETE FROM company_social_app CASCADE;
DELETE FROM keyword CASCADE;
DELETE FROM job_keywords CASCADE;
DELETE FROM job_langage CASCADE;
DELETE FROM newsletter_subscription CASCADE;

-- important
INSERT INTO
webuser (id, type, avatar, email, password, subscription_date, email_validated, disabled_account, last_connection_date) VALUES
				-- Placeholder for an unknown company (aggregation or deleted account) or an unknown candidate (deleted account)
				('77777777-0000-7777-0000-777777777777', 0, 'default.png', '', crypt('COpNL25~!(5:q7d,k=', gen_salt('bf', 8)), now(), true, true, now());
INSERT INTO
company  (user_id, company_name, industry, website, linkedin, facebook, twitter, instagram, founded, location, company_size, description) VALUES
					-- company for aggregation
				  ('77777777-0000-7777-0000-777777777777', '', 0, '', '', '', '', '', '', 0, 0, '');

-- TEST DATA
-- BASE USER
INSERT INTO
webuser (id, type, avatar, email, password, subscription_date, email_validated, disabled_account, last_connection_date) VALUES
				-- employer users
        ('da8ab055-1309-4385-a15f-da1ecbd49362', 2, 'test1.png', 'rsp+lt_e0@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('0549d8fa-3c89-4cfd-82ef-25802040b2da', 2, 'test2.png', 'rsp+lt_e1@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('73acf37a-5cee-4e28-ace9-88dc0b0a2ad3', 2, 'test3.png', 'rsp+lt_e2@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('8b816229-2ff3-44e1-b8bd-5d4b6cb00f1d', 2, 'test4.png', 'rsp+lt_e3@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('ac443ebc-8b3a-475f-b9b7-88fb9efbdc28', 2, 'test5.png', 'rsp+lt_e4@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('f7fd4e80-4af1-4afc-8a1e-87e92e639130', 2, 'test6.png', 'rsp+lt_e5@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('1e88c5ef-b97c-4c1a-95e4-2054593692bd', 2, 'test7.png', 'rsp+lt_e6@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now()),
				('9858c1d5-b2ee-455d-9ca1-c2999887e2e3', 2, 'test8.png', 'rsp+lt_e7@rxsoft.eu', crypt('azeqsdwxc', gen_salt('bf', 8)), now(), true, false, now())
				 ;

-- COMPANY
INSERT INTO
company  (user_id, company_name, public_id, industry, phone, website, linkedin, facebook, twitter, instagram, founded, location, company_size, description) VALUES
					('da8ab055-1309-4385-a15f-da1ecbd49362', 'Uplever','uplever', 7, '0205012115', 'https://uplevel.com', 'kinkedin.com/uplevel', 'effebé.com/uplevel', '@uplevel', 'instagram/@uplever', '2014', 953, 2, 'UpLever is a non stop company from the upper space.'),
					('0549d8fa-3c89-4cfd-82ef-25802040b2da', 'Github','github', 1, '','https://github.com', '', '', '', '', '2007', 190, 4, 'GitHub is how people build software. With a community of more than 14 million people, developers can discover, use and contribute to over 25 million projects using a powerful, collaborative workflow. Whether using GitHub.com or your own instance of GitHub Enterprise, you can integrate GitHub with third party tools, from project management to continuous deployment, to build software in the way that works best for you.'),
					('73acf37a-5cee-4e28-ace9-88dc0b0a2ad3', 'Gitlab','gitlab', 2, '','https://about.gitlab.com', '', '', '', '', '2008', 1140, 4, 'GitLab is an open source tool used by developers to create and manage code bases collaboratively. Built on Git, which is a very popular and efficient distributed version control system, GitLab gives you all the tools needed for Git repository management from code reviews to issue tracking and more.'),
					('8b816229-2ff3-44e1-b8bd-5d4b6cb00f1d', 'Invision','invision', 3, '','https://www.invisionapp.com', '', '', '', '', '2010', 1133, 2, 'We help companies of all sizes unlock the power of design-driven product development. That''s why teams at Evernote, Adobe, Airbnb, Salesforce, and many more fire up InVision every day. InVision gives teams the freedom to design, review, and user test products—all without a single line of code. With intuitive tools for prototyping, task management, and version control, it''s your entire design process, all in one place.'),
					('ac443ebc-8b3a-475f-b9b7-88fb9efbdc28', 'Netlify','netlify', 4,'', 'https://www.netlify.com', '', '', '', '', '2014', 180, 1, 'Netlify is a Platform as a Service that integrates and automates all services that go in to making modern web projects. From free personal blogs to huge enterprise solutions, Netlify provides instant global performance. Netlify is based in Dogpatch, San Francisco, CA, USA.'),
					('f7fd4e80-4af1-4afc-8a1e-87e92e639130', 'Ergeon inc.', 'ergeon inc.', 1,'', 'https://www.ergeon.com','', '', '', '', '2007', 1130, 4, 'Ergeon aims to become the #1 trusted provider of tech-enabled Fence Installation Service. We provide an amazingly better experience using software and data that enable our service personnel to be more effective and efficient. We are pioneering the tech-enabled home services space with a full-stack approach.'),
					('1e88c5ef-b97c-4c1a-95e4-2054593692bd', 'Qualtrics','qualtrics', 0, '','https://www.qualtrics.com', '', '', '', '', '2001', 1137, 3, 'How do you turn a customer into a fanatic? An employee into an ambassador? A product into an obsession? A brand into a religion? With experiences. The write-home, tell-everyone-you-know kind of experiences. Qualtrics Experience Management (XM) is the only software platform that helps brands continually assess the quality of their four core experiences—customers, employees, products, and brands. With Qualtrics XM, organizations can be at every meaningful touchpoint, for every experience, and predict which changes will resonate most with stakeholders. At Qualtrics, our mission is to close the experience gap.'),
					('9858c1d5-b2ee-455d-9ca1-c2999887e2e3', 'Sonicwall','sonicwall', 10, '','https://sonicwall.com', '', '', '', '', '1990', 2121, 1, 'SonicWall has been fighting the cyber-criminal industry for over 25 years defending small, medium-size businesses and enterprises worldwide. Backed by research from the Global Response Intelligent Defense (GRID) Threat Network, our award-winning real-time breach detection and prevention solutions, coupled with the formidable resources of over 10,000 loyal channel partners around the globe, are the backbone securing more than a million business and mobile networks and their emails, applications and data. This combination of products and partners has enabled a real-time cyber defense solution tuned to the specific needs of the more than 500,000 global businesses in more than 215 countries and territories.')
					;

-- -- JOBS
-- 	"id" uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
-- 	"public_id" VARCHAR(255) NOT NULL UNIQUE,
-- 	"company_id" uuid NOT NULL references company(user_id),
-- 	"contrat" integer NOT NULL references job_contrat(id),
-- 	"experience" integer NOT NULL references job_experience(id),
-- 	"full_remote" BOOLEAN NOT NULL,
-- 	"worktime" integer NOT NULL references job_time(id),
-- 	"regional" integer NOT NULL references job_regional(id),
-- 	"position" VARCHAR(255) NOT NULL,
-- 	"description" text NOT NULL DEFAULT '', -- can be empty if url exist (external job)
-- 	"url_or_mail_apply" text NOT NULL DEFAULT '',
-- 	"salary" integer NOT NULL DEFAULT 0,
-- 	"featured" BOOLEAN NOT NULL DEFAULT 'false',
-- 	"paid" BOOLEAN NOT NULL DEFAULT 'false', -- maybe only featured job are to buy ?
-- 	"validated" BOOLEAN NOT NULL DEFAULT 'false',
INSERT INTO
job (public_id, company_id, contrat, experience, full_remote, worktime, regional, position, salary, status, post_date, expire_date, featured, description, url_or_mail_apply) VALUES
  ('business-development-manager-dae36d87-194c-4867-9e56-a786b3aa599e','9858c1d5-b2ee-455d-9ca1-c2999887e2e3',1,2,true,1,1,'Business Development Manager',300,2,now(), now() + '1 month'::interval, false, 'SonicWall has been fighting the cybercriminal industry for over 26 years, defending small- and medium-sized businesses and enterprises worldwide. Backed by research from SonicWall Capture Labs and the formidable resources of over 26,000 loyal channel partners around the globe, our award-winning, real-time breach detection and prevention solutions secure more than a million business and mobile networks and their emails, applications and data. ##Responsibilities - Directly responsible for managing new channel partner recruitment lifecycle; including prospecting, qualifying, onboarding and operationalizing - Executive-level prospecting into partners not currently doing business with SonicWall Establish technical alignment, financial models and cross-functional buy-in with prospective new partners ## Qualifications - 5+ years of channel business development experience - Proven track record of meeting and exceeding regional quota and/or management by objective (MBO) expectations Experience working closely with dispersed Territory Account Manager (TAM), Channel Account Manager (CAM), and Sales Engineer (SE) regional teams. SonicWall provides equal employment opportunities (EEO) to all employees and applicants for employment without regard to race, color, religion, sex, national origin, age, disability or genetics.', 'https://example.com/job/447'),
	('software-engineer-devops-33fa3bc6-4ea9-4a2c-beaf-8fd3bf9aac61','da8ab055-1309-4385-a15f-da1ecbd49362',1,2,false,1,2,'Software Engineer, DevOps',30,2,now() - '2 month'::interval, now() - '1 month'::interval, false, 'Udacity’s mission to democratize education online requires uptime, stability, and fast reaction to trouble. The Platform Engineering group develops and supports the shared platforms and tools that keep Udacity online, working closely with application and frontend development teams. As a member of the DevOps team you will be responsible for the foundations of our microservice architecture', 'jobs@rebelmouse.com'),
	('senior-solutions-consultant-brand-experience-8d9e2c96-7de3-4d2b-9e72-98bfccf4a439','0549d8fa-3c89-4cfd-82ef-25802040b2da',1,2,true,1,0,'Senior Solutions Consultant, Brand Experience',300,2,now(), now() + '1 month'::interval,false, '## The Company ## At Qualtrics, our mission is to close experience gaps—the costly differences between what customers, employees and the market are expecting, and what they’re receiving. 9,000+ organizations worldwide and more than 3/4 of the Fortune 100 rely on the Qualtrics Experience Management Platform™ to collect, analyze, and act on feedback—more feedback than they ever thought possible. With Qualtrics XM, organizations can manage the four core experiences of business—customer, employee, product, and brand experience. Organizations can be at every meaningful touchpoint, for every experience, and predict what will resonate most with customers and employees', 'jobs@rebelmouse.com'),
	('senior-sales-expert-remote--af9628e3-2861-496e-af27-a3b8e7877a5c','73acf37a-5cee-4e28-ace9-88dc0b0a2ad3',1,2,true,1,1,'Senior Sales Expert (Remote)',500,1,now(), now() + '1 month'::interval, false, 'We are looking for an experienced Senior Sales Expert to join our fast-growing startup in Silicon Valley. Ergeon aims to become the #1 trusted provider of tech-enabled Home Services. We provide an amazingly better customer experience using software and data. We are looking for smart and independent people who are organized and self starters, who love impressing customers with strong communication skills & deep product knowledge, and are willing to do whatever it takes to create happy customers. For those seeking longer careers, this is a great opportunity to join a great team with amazing benefits.', 'jobs@rebelmouse.com'),
	('senior-backend-engineer-c51a26e6-283a-495f-afab-c6528391ddbf','73acf37a-5cee-4e28-ace9-88dc0b0a2ad3',1,2,false,1,3,'Senior Backend Engineer',10000,3,now(), now() + '1 month'::interval, false, 'At Netlify, we''re building a platform to empower digital designers and developers to build better, more elaborate web projects than ever before. We''re aiming to change the landscape of modern web development.\nAs a Backend Engineer you’ll coordinate closely with the product team to create the backend services that support the user’s experience. The services range from our internal CI/CD system, to the core ruby API, to the variety of Go micro services we run. Our services span Go, Ruby, and Javascript and often you’ll have to work across all three languages to implement new features.', 'jobs@rebelmouse.com'),
	('engineering-manager-17483b39-d3ae-4090-990a-d3992f0a47c3','8b816229-2ff3-44e1-b8bd-5d4b6cb00f1d',1,2,true,1,3,'Engineering Manager',3899,4,now(), now() + '1 month'::interval, false, 'InVision is the digital product design platform used to make the world’s best customer experiences. We provide design tools and educational resources for teams to navigate every stage of the product design process, from ideation to development. Today, more than 5 million people use InVision to create a repeatable and streamlined design workflow; rapidly design and prototype products before writing code, and collaborate across their entire organization. That includes 100% of the Fortune 100, and organizations like Airbnb, Amazon, HBO, Netflix, Slack, Starbucks and Uber, who are now able to design better products, faster.', 'jobs@rebelmouse.com'),
	('drupal-developer-at-promet-source-4cfc1b69-7717-4da9-9c67-dd4e310eeb91','8b816229-2ff3-44e1-b8bd-5d4b6cb00f1d',1,2,true,1,2,'Drupal Developer',310,2,now(), now() + '1 month'::interval, false, 'Promet Source, a Chicago based web and mobile application development company, is looking to contract a Drupal Developer.\nWe are a technology firm that delivers web and mobile development solutions and provides ongoing support. Our clients include many Higher Education institutions, high volume eCommerce applications as well as startups. As a leading technology provider of web services, we focus on complex web development, Open Source CMS support and iOS mobile applications. We are dedicated to open source software solutions by providing managed services for Drupal-based websites, products, and applications.\nWe are looking for an experienced full-time Drupal Web Developer with great organizational and interpersonal skills who strives for code excellence.', 'https://example.com/job/447'),
	('editor-engineer-at-slite-d3609625-b6ab-47ed-8bf7-0f39e4db7490','ac443ebc-8b3a-475f-b9b7-88fb9efbdc28',1,2,true,1,0,'Editor Engineer',37888,2,now(), now() + '1 month'::interval, false, 'The editor is at the core of Slite. Our vision is to get teams of hundreds of people documenting, writing and sharing all of their knowledge in a simple, fast, yet powerful way. And to collaborate on this knowledge together in real-time. We have already managed to build a unique, market-leading editor on our own and there’s so much more potential.', 'https://example.com/job/447'),
	('java-application-development-manager-at-equian-1dda8204-709f-4661-8d0d-e9a4104d358c','f7fd4e80-4af1-4afc-8a1e-87e92e639130',1,2,true,1,2,'Expiré: Java Application Development Manager',4530,1, now() + '1 month'::interval, now(), false, 'At Equian, we are developing the next generation of integrated technology solutions that brings data mining, data analytics, workflow, document management and process management into a seamless platform to meet the challenges of a rapidly changing business environment. We offer a competitive salary, bonus and benefit package, which includes Medical, Dental, Vision, Life, Disability, a flexible Paid Time Off program, scheduled and floating holidays, educational assistance, wellness program and a 401(k) Plan with Company Match.',  'https://example.com/job/447'),
	('full-stack-engineer-at-canny-6a09b027-d188-48d5-84c4-537c9b05d9ef','f7fd4e80-4af1-4afc-8a1e-87e92e639130',1,2,true,1,3,'Full-Stack Engineer',3954,2,now(), now() + '1 month'::interval, false, '**Company** Canny helps SaaS companies build better products by being better at listening to and acting on user feedback.The benefit of working with SaaS companies is that our work impacts technology in virtually every sector (education, medicine, tech, and so much more). We’re using what we’re great at, building great products, to have the impact we want to have on the world. - Early-stage startup, 5 person team, launched about two years ago - Over 350 customers, including industry-leading companies like Flexport, Bench, and Compass - 100% remote, founders are digital nomads', 'https://example.com/job/447'),
	('senior-developer-at-gravity-forms-14734d73-5499-490b-8654-bc2673f42b44','1e88c5ef-b97c-4c1a-95e4-2054593692bd',1,2,true,1,3,'Senior Developer',31000,2,now(), now() + '1 month'::interval, false, 'Rocketgenius, Inc. is looking for full-time Senior Developers to help us take Gravity Forms to the next level.Relocation is not necessary, as we consider your geographic location a non-issue. Hours are flexible based on your time zone requirements.', 'https://example.com/job/447'),
	('full-stack-developer-at-planning-center-online-ab4bd7a8-931a-4705-91e1-84af6fae6c67','1e88c5ef-b97c-4c1a-95e4-2054593692bd',1,2,false,1,3,'Full Stack Developer',312220,3,now(), now() + '1 month'::interval, false, 'At Planning Center, we build powerful, beautiful applications specifically for churches. Each of our apps has a dedicated team of designers and developers who are committed to refining and expanding their specific app in response to real customer needs. Our customers include over 90 of the 100 largest churches in the country, but also tens of thousands of small community and neighborhood churches, and we strive to make day-to-day operations easier for all of them.', 'https://example.com/job/447'),
	('python-software-engineer-at-smiledirectclub-b6c2dd14-4c04-44ea-9298-d335503b5f45','9858c1d5-b2ee-455d-9ca1-c2999887e2e3',1,2,false,1,1,'Python Software Engineer',34000,2,now(), now() + '1 month'::interval, false, 'We’re SmileDirectClub, and we believe everyone deserves a smile they’ll love. We also believe that you deserve a job you’ll love. Good thing you found us, and we found you. At SmileDirectClub, we’re all about empowering transformation. We want people to become more confident in how they look, how they feel, and how they think. So, we’re spreading smiles and positivity all over the country.', 'https://example.com/job/447'),
	('tech-lead-new-york-city-ny-onsite-partial-remote-visa-3d92ebde-7754-4dc3-a285-74434208a49d','9858c1d5-b2ee-455d-9ca1-c2999887e2e3',1,2,false,1,2,'Tech Lead',90000,1,now(), now() + '1 month'::interval, false, 'Engineers at Kalepa will be solving interesting and challenging problems at the intersection of big data pipelines, cutting-edge machine learning models, intuitive frontend apps, and robust infrastructure. You will be working in a small team building technology from the ground up with the latest stack.',  'https://example.com/job/447'),
	('android-engineer-all-levels-san-francisco-remote-us-only--1d366dc4-305c-4124-bae8-e3a225316d4a','9858c1d5-b2ee-455d-9ca1-c2999887e2e3',1,2,false,1,3,'Android Engineer (all levels)',34422,2,now(), now() + '1 month'::interval, false, 'At Stride we''re working on a benefits suite for what we believe is an underserved community – freelance and gig economy workers. We want to create economic security for anyone who works for themselves. We offer an easy way for 1099 workers to apply for health/dental/vision coverage. We''re also expanding to other spaces like tax, so as to make it financially accessible for anybody who juggles multiple gigs, or is just pursuing a dream on their own.', 'jobs@rebelmouse.com')
	;


INSERT INTO
job (id, public_id, company_id, contrat, experience, full_remote, worktime, regional, position, salary, status, post_date, expire_date, featured, url_or_mail_apply, description) VALUES
    ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 'AZERTY123', 'da8ab055-1309-4385-a15f-da1ecbd49362', 1, 2, false, 1, 2, 'Virtual Reality Front End UI Engineer', 100000, 2, now(), now() + '1 month'::interval, false,'test@example.com', 'We need design-minded front end developers with deep knowledge of modern web development to create next generation user interfaces. We are leveraging a Chromium integration paired with powerful VR APIs to present web apps in holographic 3D to devices like the Oculus Rift and HTC Vive. Forget supporting IE. With us you''d be programming against HUDs, non-rectangular displays, and holographic projectors.<br><br>See more about AltspaceVR<br><br><br>We are:<br><br>A virtual reality software company https://twitter.com/AltspaceVR<br>Backed by some of the best investors on the planet http://altvr.com/about/<br>		A team that is passionate about the future of VR http://altvr.com/team/<br>Apply!<br><br>');

INSERT INTO
job (id, public_id, company_id, contrat, experience, full_remote, worktime, regional, position, description, salary, status, post_date, expire_date, url_or_mail_apply, featured) VALUES
    ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12', 'AZERTY456', 'da8ab055-1309-4385-a15f-da1ecbd49362', 3, 1, false, 2, 2, 'SDF de luxe, Coder du Web dans un carton',  'Description ici', 60000, 2, now(), now() + '2 month'::interval, 'http:example.com', false);

INSERT INTO
job (id, public_id, company_id, contrat, experience, full_remote, worktime, regional, position, url_or_mail_apply, salary, status, post_date, expire_date) VALUES
    ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13','AZERTY999', 'da8ab055-1309-4385-a15f-da1ecbd49362', 2, 2, true, 2, 1, 'Design Mentor',  'https://www.example.com', 25000, 2, now(), now() + '1 month'::interval );

-- INSERT INTO
-- job (public_id, company_id, contrat, experience, remote_regularity, worktime, position, external_job, url, salary_range, validated, post_date, expire_date, validated_date) VALUES
--     ('ligrsystems-96201', 'da8ab055-1309-4385-a15f-da1ecbd49363', 2, 2, 2, 1, 'Creative Developer', true, 'https://www.example.com/content/creative-developer', 4, true, now(), now(), now());


-- INSERT INTO
-- job (public_id, company_id, contrat, experience, remote_regularity, worktime, position, external_job, url, salary_range, validated, post_date, expire_date, validated_date, description) VALUES
--     ('ligrsystems-96202', 'da8ab055-1309-4385-a15f-da1ecbd49363', 3, 3, 3, 2, 'Senior Full Stack Engineer', false, '', 4, true, now(), now(), now(), '<p><strong>Who are we?</strong></p><p>Vestwell provides every employee in America with access to a retirement account by removing the friction points financial advisors and companies have in establishing and maintaining defined contribution &amp; benefit programs. We look out for everyone&rsquo;s best interest, including financial advisors, companies and employees.</p><p><strong>Why Vestwell?</strong></p><p>Vestwell is at the forefront of a wave of change in a $6 trillion 401(k) market; creating a new platform that lowers fees and ensures that individuals retire wealthier. We&rsquo;ve raised some money from great investors in the Fintech and Finserv world to achieve this goal and are already getting a great response from customers.</p><p><strong>Who are we looking for?</strong></p><p>The Engineering team is seeking an experienced Full Stack Engineer with 3-5 years of experience, who can be effective on all levels of our application stack, and is willing to play all necessary roles such as architect, Tech Lead, code reviewer, Agile coach, and mentor.</p><p>You''re a great fit for our team if you can balance detail-oriented tasks with long term strategy and growth initiatives. Most of all, you must be passionate about what you create and about the impact it can have. That matters to us, a lot. We offer relocation if needed.</p><h4>Requirements</h4><p><strong>Necessary Experience:</strong></p><ul><li>Extending core business logic within a shared backend codebase</li><li>Architecting, modifying, and working with SQL-based databases</li><li>Developing, maintaining, and supporting automation test suites</li><li>Experience building, specifying, and consuming RESTful API''s</li><li>Working closely with Product, QA, and other Engineers, in a cross functional vertical team, and a willingness to play any role necessary within that team</li></ul><p><strong>Day-to-day you may additionally: </strong></p><ul><li>Collaborate closely with other Senior Engineers and the rest of the Vestwell team to determine architecture for our core services</li><li>Work with both engineering and product teams to drive solution design</li><li>Create complex logical frameworks, workflows, RESTful API endpoints, and JSON formats to exchange key data with the rest of the platform</li><li>Write new services, including financial API integrations, maintain existing services, write tests, and yes, fix bugs</li><li>Help plan, automate and scale our infrastructure</li><li>Provide production support and train the junior engineers</li></ul><p><strong>The Extras: </strong></p><ul><li>Excellence in Scala, or a strong history with other functional languages</li><li>Node.js, React, or other frontend technologies</li><li>Microservice technologies</li><li>Devops or Sysops background, especially with Docker, and AWS services</li><li>An understanding of SRE best practices, including Post Mortems, Logging, Alerting, and Toil Reduction</li><li>CS degree</li></ul><h4>Benefits</h4><p>We&rsquo;re an early stage startup with lots of exciting milestones ahead. We value health and wellness at Vestwell and, as such, offer competitive health coverage, an open vacation policy, coffee, cappuccino &amp; healthy snacks, and a bright, comfortable office with lots of workspace options so you can be comfortable and as productive as possible, close to all transportation between Bryant Park and Herald Square in NYC. And naturally we have a great 401(k) plan!</p><p>&nbsp;</p><p><strong>No third parties please!</strong></p>');

-- JOB TAGS
INSERT INTO job_tag (job_id, tag_id) VALUES
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 1),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 2),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 3),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 4),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 5),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12', 6),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12', 7),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12', 8),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13', 8),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13', 10),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13', 11),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13', 12),
											('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13', 13);

-- SOCIAL APPS
INSERT INTO company_social_app (user_id, social_app_id) VALUES
                               ('da8ab055-1309-4385-a15f-da1ecbd49362', 1),
                               ('da8ab055-1309-4385-a15f-da1ecbd49362', 2),
                               ('da8ab055-1309-4385-a15f-da1ecbd49362', 3);


-- JOBs KEYWORD (à remplir par l'algo)
INSERT INTO keyword(keyword,public) VALUES
                    ('Developer', true),
                    ('SecretKeyword', false),
                    ('Web', true);
INSERT INTO job_keywords(keyword_id,job_id,count) VALUES
                            (1, 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',  10),
                            (3, 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',  1);

-- LANGAGE
INSERT INTO
job_langage (job_id, langage_id) VALUES
                                 ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 1),
																 ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 2),
																 ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 3),
																 ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 4),
																 ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 5)
                                 ;   


-- newsletter

INSERT INTO newsletter_subscription(email,regularity,activation_code) VALUES
                            ('rsp+lt_s0@rxsoft.eu',  2, 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'),
                            ('rsp+lt_s1@rxsoft.eu',  1, 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11');






















