var DEBUG = true;

// init dynamic vars
// var inputIndustry = $("#companyProfile_Industry_Label");
// var inputIndustryHidden = $("#companyProfile_Industry_ID");
// var inputIndustrySuggestion = $("#industry-peek-box");
// var inputIndustrySuggestionLoading = $("#industry-peek-box-loading");
// var processingIndustries = false

// // dynamic funcs

// function createIndustryItem(data) {
//     var sb = "<ul class='menu'>"
//     for (var i = 0; i < data.length; i++) {
//         var id = data[i].ID;
//         var label = data[i].Label;
//         // start string builder
//         sb += "<li class='menu-item'>"
//         // a
//         sb += "<a href='#' onclick='selectIndustry(" + id + ",\"" + label + "\");return false;' title='" + label + "'>"
//         // div complete
//         sb += "<div class='tile tile-centered'><div class='tile-content'>" + label + "</div></div>"
//         // end string builder
//         sb += "</a></li>"
//     }
//     sb += "</ul>"
//     return sb;
// }

// function selectIndustry(id, name) {
//     // fill inputs
//     inputIndustryHidden.val(id);
//     inputIndustry.val(name);
//     // hide suggestion
//     inputIndustrySuggestion.hide();
//     inputIndustrySuggestion.empty();
// }

// $(document).ready(function () {
//     inputIndustry.keyup(function () {
//         if ($(this).val().length >= 1 && processingIndustries != true) {
//             $.ajax({
//                 type: "POST",
//                 url: "/handleajaxgetindustrylist",
//                 data: 'keyword=' + $(this).val(),
//                 beforeSend: function () {
//                     processingIndustries = true;
//                     inputIndustrySuggestionLoading.show();
//                 },
//                 complete: function () {
//                     inputIndustrySuggestionLoading.hide();
//                     processingIndustries = false;
//                 },
//                 success: function (data) {
//                     if (data != null) {
//                         inputIndustrySuggestion.html(createIndustryItem(data));
//                         inputIndustrySuggestion.show();
//                     }
//                 }
//             });
//         }
//     });
// });