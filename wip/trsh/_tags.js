// var maxTag = 20;
// var containerTagSearch;
// $(document).ready(function () {
// 	// $("#candidateProfileTags_Add").click(function () {
// 	// 	if ($("#CandidateTags_table > tbody").children().length < maxTag) {
// 	// 		$('#CandidateTags_table > tbody:last-child')
// 	// 			.append('<tr class="candidateProfile_Tag" data-tagid="0"><td><input type="text" value="" class="candidateProfile_TagSearch"></td>' +
// 	// 				'<td><input class="candidateProfile_TagLevel" type="number" value="1" min="1" max="5"></td>' +
// 	// 				'<td><a class="deleteTagButton pure-button">Delete</a></td></tr>');
// 	// 	}
// 	// });

// 	$('#CandidateTags_table').on('keyup', '.candidateProfile_TagSearch', function (e) {
// 		var processing = false;
// 		if ($(this).val().length >= 1 && processing != true) {
// 			containerTagSearch = this;
// 			$.ajax({
// 				type: "POST",
// 				url: "/handleajaxgettaglist",
// 				data: 'keyword=' + $(this).val(),
// 				beforeSend: function () {
// 					processing = true
// 					$(this).css("background", "#FFF url('/public/img/loading.gif') right 0px no-repeat");
// 					$("#tag-peek-box").show();
// 				},
// 				success: function (data) {
// 					if (data != null) {
// 						const tul = $('<ul>', { class: "mylist" }).append(
// 							data.map(tag =>
// 								$("<li>")
// 									.attr('onClick', 'selectTag(' + parseInt(tag.ID) + ',"' + tag.Label + '")')
// 									.append($("<a>").text(tag.Label))
// 							)
// 						);
// 						$("#candidateProfile_TagSearch").css("background", "#FFF");
// 						$("#tag-peek-box").html(tul);
// 						$("#tag-peek-box").show();
// 					} else {
// 						$("#tag-peek-box").hide();
// 						$("#tag-peek-box").empty();
// 						$("#candidateProfile_TagSearch").css("background", "#FFF");
// 					}
// 					processing = false;
// 				}
// 			});
// 		}
// 	});
// });

// // function InitTag() {
// // 	if (searchTagHandler === null) searchTagHandler = []
// // 	for (var i = 0; i < searchTagHandler.length; i += 1) {
// // 		$('#CandidateTags_table > tbody:last-child')
// // 			.append('<tr class="candidateProfile_Tag" data-tagid=' + searchTagHandler[i].ID + '><td><input type="text" value="' + searchTagHandler[i].Label + '" class="candidateProfile_TagSearch"></td>' +
// // 				'<td><input class="candidateProfile_TagLevel" type="number" value=' + searchTagHandler[i].Level + ' min="1" max="5"></td>' +
// // 				'<td><a class="deleteTagButton pure-button">Delete</a></td></tr>');
// // 	}
// // }

// function selectTag(id, label) {
// 	var tr = $(containerTagSearch).closest('tr');
// 	var tagTmp = new Object();
// 	tagTmp.ID = id;
// 	tagTmp.Label = label;
// 	tagTmp.Level = parseInt(tr.find('.candidateProfile_TagLevel').val());
// 	$(containerTagSearch).val(label);

// 	// set data tagid to the parent tr
// 	$(tr).data('tagid', id);

// 	if (searchTagHandler.length < maxTag) {
// 		// check if not exist yet
// 		if (!tagExist(id)) {
// 			searchTagHandler.push(tagTmp);
// 			refreshTagSelection();
// 		}
// 	}
// 	$("#tag-peek-box").hide();
// 	$("#tag-peek-box").empty();
// }

// function tagExist(idint) {
// 	for (var i = 0; i < searchTagHandler.length; i += 1) {
// 		if (searchTagHandler[i].ID == idint) {
// 			return true;
// 		}
// 	}
// 	return false;
// }

// function refreshTagSelection() {
// 	$("#candidateProfile_HiddenJsonTags").val(JSON.stringify(searchTagHandler));
// }

// // update tag level
// $('#CandidateTags_table').on('change', '.candidateProfile_TagLevel', function (e) {
// 	var tr = $(this).closest('tr');
// 	var id = parseInt(tr.data('tagid'));
// 	var lvl = parseInt($(this).val());
// 	// update hidden input
// 	for (var i = 0; i < searchTagHandler.length; i += 1) {
// 		if (searchTagHandler[i].ID == id) {
// 			searchTagHandler[i].Level = lvl;
// 			break;
// 		}
// 	}
// 	refreshTagSelection();
// });

// // remove tag ligne
// $('#CandidateTags_table').on('click', '.deleteTagButton', function (e) {
// 	var tr = $(this).closest('tr');
// 	var idint = parseInt(tr.data('tagid'));
// 	if (idint != 0) {
// 		// remove from tag handler;
// 		for (var i = 0; i < searchTagHandler.length; i += 1) {
// 			if (searchTagHandler[i].ID == idint) {
// 				searchTagHandler.splice(i, 1);
// 			}
// 		}
// 		refreshTagSelection();
// 	}
// 	tr.remove();
// });


