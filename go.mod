module lt

go 1.15

require (
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b // indirect
	github.com/darkoatanasovski/htmltags v1.0.0
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/go-playground/form v3.1.4+incompatible
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/uuid v1.2.0
	github.com/gorilla/feeds v1.1.1
	github.com/lib/pq v1.9.0
	github.com/microcosm-cc/bluemonday v1.0.4
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/pkg/errors v0.9.1
	github.com/revel/modules v1.0.0
	github.com/revel/revel v1.0.0
	github.com/russross/blackfriday v1.6.0
	github.com/ventu-io/go-shortid v0.0.0-20201117134242-e59966efd125
	github.com/xeonx/timeago v1.0.0-rc4
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
